({
    openTab : function(component, event, helper) {

        var workspaceAPI = component.find("workspace");
        var recId = component.get("v.recordId");
        console.log('openTab in CaseSubTab Cmp: ' + recId + ' | ' + workspaceAPI);

        workspaceAPI.isConsoleNavigation().then(function(response) {
            console.log('Console Navigation Resp: ' + response);
            if(response) {
                console.log('Console view is supported');
                workspaceAPI.openTab({
                    url: "/lightning/r/SalesOrder__x/"+recId+"/view",
                    focus: true
                }).then(function(response) {
                    console.log('response--'+response);
                    workspaceAPI.openSubtab({
                        parentTabId: response,
                        pageReference: {
                            "type": "standard__component",
                            "attributes": {
                                "componentName": "c__CreateCase",
                                "recordId":"recId"
                            }
                        },
                    });
                    focus: true
                }).catch(function(error) {
                    console.log('Error in opening tab: ' + error);
                });
            } else {
                console.log('Console view is not supported');
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:CreateCase",
                    componentAttributes: {
                        recordId : recId
                    }
                });
                evt.fire();
            }
        })
        .catch(function(error) {
            console.log('Console Navigation Err: ' + error);
        });
    }
})