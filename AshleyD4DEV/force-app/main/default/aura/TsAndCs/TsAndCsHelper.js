({
   createSignatureObj : function(cmp, helper) {
       // Load all contact data
       var action = cmp.get("c.createSignatureObj");
       var recId = cmp.get("v.guestId");
       var thdresponse = cmp.get("v.tresholdenabledresponse");
       alert('recId and thdresponse--'+recId +'---'+thdresponse);
       var toastErrorHandler = cmp.find('toastErrorHandler');
       if ($A.util.isEmpty(recId) || $A.util.isUndefined(recId) )
           return;
       //alert('recId-----'+recId);
       //alert('thdresponse------'+thdresponse);
                               
       action.setParams({ "personAccId"  : recId,
                         "thdresponse" : thdresponse
                         });

       action.setCallback(this, function(response) {
           toastErrorHandler.handleResponse(
               response, // handle failure
               function(response){ // report success and navigate to contact record
                    alert('response.getReturnValue().Id------------------'+response.getReturnValue().Id)
                   cmp.set("v.sigId", response.getReturnValue().Id);
                   cmp.set("v.signObj", response.getReturnValue());
               },
               function(response, message){ // report failure
                   helper.showToast("error", 'Cart Load Error', cmp, message);
               }
           )
       });
       $A.enqueueAction(action);
    }, 
    goForward : function(cmp, helper) {
       // Load all contact data
       var action = cmp.get("c.checkForSignature");

       var recId = cmp.get("v.sigId");
        alert('recId---'+recId);
       var toastErrorHandler = cmp.find('toastErrorHandler');
       if ($A.util.isEmpty(recId) || $A.util.isUndefined(recId) )
           return;
       
       action.setParams({ "sigId"  : recId });

       action.setCallback(this, function(response) {
           toastErrorHandler.handleResponse(
               response, // handle failure
               function(response){ // report success and navigate to contact record
                   var attachmentFound = response.getReturnValue();
                   alert('attachmentFound---'+attachmentFound);
                   if (attachmentFound == true) {
                       alert('--Ts-');
                       cmp.getEvent("NotifyParentFinishedSign").fire();
                      
                   }
              //  $A.get('e.force:refreshView').fire(); 
               },
               function(response, message){ // report failure
                   helper.showToast("error", 'Cart Load Error', cmp, message);
               }
           )
       });
       $A.enqueueAction(action);
    }, 
    showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
        });
        toastEvent.fire();
    }
})