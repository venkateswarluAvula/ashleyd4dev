({
	doInit : function(component, event, helper) {
        
        
        var action = component.get("c.getcaserecords");
		action.setParams({ RecId : "5000n000005PH1CAAW" });
		//alert('action--'+action);
        action.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                
                var Responsedata = response.getReturnValue(); 
                //alert('Responsedata--'+JSON.stringify(Responsedata));
                component.set("v.recval", response.getReturnValue());
                
            }
            else if (response.getState() === "ERROR") {
                alert("error");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMsg", errors[0].message );
                    }
                }else {
                    component.set("v.errorMsg", 'Request Failed!' );
                }
            }   
            
        });
        $A.enqueueAction(action);
        
        var pliaction = component.get("c.getplirecords");
		pliaction.setParams({ pliRecId : "5000n000005PH1CAAW" });
		//alert('action--'+action);
        pliaction.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                
                var Responsedata = response.getReturnValue(); 
                //alert('Responsedata--'+JSON.stringify(Responsedata));
                component.set("v.plirecval", response.getReturnValue());
               
            }
            else if (response.getState() === "ERROR") {
                alert("error");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMsg", errors[0].message );
                    }
                }else {
                    component.set("v.errorMsg", 'Request Failed!' );
                }
            }   
            
        });
        $A.enqueueAction(pliaction); 
        
        var legacyaction = component.get("c.getlegacyrecords");
		legacyaction.setParams({ legacyRecId : "5000n000005PH1CAAW" });
		//alert('action--'+action);
        legacyaction.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                
                var Responsedata = response.getReturnValue(); 
                //alert('Responsedata--'+JSON.stringify(Responsedata));
                component.set("v.legacyval", response.getReturnValue());
               
            }
            else if (response.getState() === "ERROR") {
                alert("error");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMsg", errors[0].message );
                    }
                }else {
                    component.set("v.errorMsg", 'Request Failed!' );
                }
            }   
            
        });
        $A.enqueueAction(legacyaction); 
        
        	var pdf = new jsPDF();
        var canvas = pdf.canvas;
        canvas.height = 72 * 11;
        canvas.width= 72 * 8.5;;

        // can also be document.body
        var text = document.getElementById('prinpdf');
		pdf.text(20, 20, 'prinpdf');
        pdf.save('Test.pdf');
        
	},
 
toPDFFormat : function(component, event, helper) {
               
        var doc = new jsPDF();
        var specialElementHandlers = {
            '#editor': function (element, renderer) {
                return true;
            }
        };
        
        $('#cmd').click(function () {   
            doc.fromHTML($('#content').html(), 15, 15, {
                'width': 170,
                'elementHandlers': specialElementHandlers
            });
            doc.save('Case-Report.pdf');
        });
        
    }
    
})