({
    
    doInit : function(component, event, helper) {
        var action = component.get('c.getcaseHistoryrecords');
        var currentRecId = component.get("v.recordId");
        action.setParams({
            parentRecordId : currentRecId
        });
        action.setCallback(this, function(actionResult) {
            var succesData = actionResult.getReturnValue();
            //alert('succesData--'+JSON.stringify(succesData));
            component.set('v.CaseHis', succesData);
            if(succesData != null){
                component.set("v.CaseCount",succesData.length);
            }
            
        });
        $A.enqueueAction(action);
    },
    
    openRelatedList: function(component, _event){
        var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        //var recid = "5000n00000637qbAAA";
        var recid = component.get("v.recordId");
        //alert('recid2--'+component.get("v.recordId"));
        relatedListEvent.setParams({
            "relatedListId": "Histories",
            //"parentRecordId": component.get("v.recordId")
            "parentRecordId": recid
        });
        relatedListEvent.fire();
    }
})