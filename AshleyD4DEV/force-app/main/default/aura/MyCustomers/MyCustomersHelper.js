({
    getRSAAccountInfo: function(component, success, failure) {
        var self = this;
        var conciergeService = component.find("conciergeService");
        if (conciergeService){
            conciergeService.getRSAAccountInfo( 
                function(rsaAccountInfo){
                    component.set("v.rsaAccountInfo", rsaAccountInfo);
                    if (success) {
                        success.call(this);
                    }
                }
                , function(response, message){
                    if (failure) {
                        failure.call(this, message);
                    }                        
                }
            );
        }
    },    
    addToCart: function (component, recordId, subComponentName) {
        
        if(component.get("v.selectedCheckBoxes").length <= 0){
            if(component.get('v.productSKU') == null || component.get('v.productSKU') == '')
            {
                var action = component.get("c.addToCart");
                console.log('onproduct' + recordId);
                action.setParams({
                    "accountId": recordId,
                    "prod": JSON.stringify(component.get('v.productDetail')),
                    "prodPrice": JSON.stringify(component.get('v.productPrice')),
                    "qty": component.get('v.quantity')
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var val = response.getReturnValue();
                        console.log('val' + val);    
                    }else{
                        this.showToast("error", 'Error Product', component,
                                       'This Product is not found in Tax API, could not be added. Please contact administrator.');
                    }
                    var evt = component.getEvent("FinishedAddToCart");
                    evt.setParams({ "notifyParam1": subComponentName });
                    evt.fire();
                    //Close AddCustomer Modal
                    component.set("v.showModal", false);
                    var overlay = component.find('overlay');
                    $A.util.removeClass(overlay, 'slds-backdrop--open');
                    //Fix CR Work Needed Updates Item 28, remain within the context of what the RSA was searching for.
                    //The handler FinishedAddToCart will fire NotifyHeaderComponentEvent to update concierge_header active cart number.
                    // $A.get('e.force:refreshView').fire();
                    
                });
                action.setBackground();
                $A.enqueueAction(action);
            }
            else if(component.get('v.productSKU') != null)
            {
                alert('productSKU----'+component.get('v.productSKU'));
                var action = component.get("c.updateLineItemWithSKU");
                console.log('onproduct' + recordId);
                action.setParams({
                    "recordId": recordId,
                    "productSKU": component.get('v.productSKU'),
                    "Quantity": component.get('v.QuantityNumber')
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    alert('state---' + state);
                    if (state === "SUCCESS") {
                        var val = response.getReturnValue();
                        alert('val---' + val);    
                    }else{
                        this.showToast("error", 'Error Product', component,
                                       'This Product is not found in Tax API, could not be added. Please contact administrator.');
                    }
                    var evt = component.getEvent("NotifyParentCloseManuallyModal");
                    evt.fire();
                    //Close AddCustomer Modal
                    alert('test---'+component.get("v.showModal"));
                    component.set("v.showModal", false);
                    var overlay = component.find('overlay');
                    $A.util.removeClass(overlay, 'slds-backdrop--open');
                    
                    
                });
                action.setBackground();
                $A.enqueueAction(action);
            }
        }else{
            var action = component.get("c.multiaddToCart");
            //alert('selectedCheckBoxes------'+JSON.stringify(component.get('v.selectedCheckBoxes')));
            action.setParams({
                "accountId": recordId,
                "prod": JSON.stringify(component.get('v.selectedCheckBoxes')), 
                "qty": component.get('v.quantity')
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var val = response.getReturnValue();
                    var ExpectionItems = val.ExceptionItems;
                    console.log('ExpectionItems---'+ExpectionItems);
                    if(val.ExceptionItems.length >= 1 && val.AddedItems.length >= 1){
                        this.showToast("warning",'Warning', component,
                                       ExpectionItems+'items are not available/discontinued by manufacturer, remaining items are added to cart .');    
                    }
                    if(val.ExceptionItems.length >= 1 && val.AddedItems.length == 0){
                     this.showToast("warning",'Warning', component,
                                       ExpectionItems+'items are not available/discontinued by manufacturer. No items are Added to Cart.');   
                    }
                }else{
                    this.showToast("error", 'Error Adding To Cart', component,
                                   'This Products are failed to add to cart. Please contact administrator.');
                }
                var evt = component.getEvent("FinishedAddToCart");
                evt.setParams({ "notifyParam1": subComponentName });
                evt.fire();
                //Close AddCustomer Modal
                component.set("v.showModal", false);
                var overlay = component.find('overlay');
                $A.util.removeClass(overlay, 'slds-backdrop--open');
                //Fix CR Work Needed Updates Item 28, remain within the context of what the RSA was searching for.
                //The handler FinishedAddToCart will fire NotifyHeaderComponentEvent to update concierge_header active cart number.
                // $A.get('e.force:refreshView').fire();
                
            });
            action.setBackground();
            $A.enqueueAction(action);
            
            
        }      
    }, 
    navigateToPayment : function(component, recordId){
        var appEvent = $A.get("e.c:NavigateToWrapperComponentAppEvent");
        appEvent.setParams({
            "targetCmpName" : 'CheckoutWrapperCmp',
            "targetCmpParameters" : {"recordId": recordId}
        });
        appEvent.fire();
    },   
    showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
            duration:'5000',
            
        });
        toastEvent.fire();
    },
    createFindCustomerCmp :function(component, event, helper) {
        $A.createComponent("c:FindCustomer",  
                           {"showView":component.get('v.showView'),
                            "showSelectButton":  component.get('v.showSelectButton')}, 
                           function(newCmp) {
                               var body = component.get("v.body");
                               body = [];
                               // newCmp is a reference to another component
                               body.push(newCmp);
                               component.set("v.body", body);
                           }); 
    },
    isCaliforniaStore : function(component, event, helper, RecordId) {
        var action = component.get("c.isCaliforniaStore");
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var showSignatureCapture = response.getReturnValue();
                console.log('showSignatureCapture-->'+showSignatureCapture);
                
                var hasSigedInLastDay = false;
                if(showSignatureCapture == true && !hasSigedInLastDay){
                    helper.showSignature(component, event, helper, RecordId);
                }else{
                    /*var evt  = $A.get("e.force:navigateToURL");
                    evt.setParams({
                        "url": '/one/one.app#/n/CustomerDetailView?recordId=' + RecordId
                    })
                    evt.fire();*/
                    var appEvent = $A.get("e.c:NavigateToWrapperComponentAppEvent");
                    appEvent.setParams({
                        "targetCmpName" : 'CustomerDetailViewWrapperCmp',
                        "targetCmpParameters" : {"recordId": RecordId}
                    });
                    appEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    showSignature : function(component, event, helper, RecordId){      
        component.set("v.signatureForCustomer", RecordId);
        //open signature list modal
        component.set("v.showSignatureListModal", true);
        
        var overlay = component.find('overlay');
        $A.util.addClass(overlay, 'slds-backdrop--open');
    }
})