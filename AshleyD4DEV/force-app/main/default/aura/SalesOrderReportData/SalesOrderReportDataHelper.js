({
    /*// object and set the corresponding aura attribute.
   	renderPage: function(component, event, helper) {
    var records = component.get("v.customerwithActiveCartLst"),
    //var records = component.get("v.getCustomerwithActiveCarts),
    pageNumber = component.get("v.pageNumber"),
    pageRecords = records.slice((pageNumber-1)*100, pageNumber*100);
    //component.set("v.currentList", pageRecords);
    },*/
    
    onPageLoad : function(component, event, helper){
        
        var StartDate = new Date();
        var dd = StartDate.getDate();
        var mm = StartDate.getMonth() + 1; //January is 0!
        var yyyy = StartDate.getFullYear();
        
        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        var EndDate = new Date();
        var dd = EndDate.getDate();
        var mm = EndDate.getMonth() + 1; //January is 0!
        var yyyy = EndDate.getFullYear();
        /// var EndDate = new Date("2049-01-01");
        component.set('v.StartDate', yyyy + "-" + mm + "-" + dd);
        component.set('v.EndDate', yyyy + "-" + mm + "-" + dd);
        //  component.set('v.EndDate', EndDate.getFullYear() + "-" + (EndDate.getMonth() + 1) + "-" + EndDate.getDate());
        
    },
    
    onDeliveryConfirmation : function(component, event, helper){
        var orType = component.get("v.typeOfOrder");
        if(orType == 'Delivery Confirmation'){
            
            var StartDate = new Date();
            var dd = StartDate.getDate()+1;
            var mm = StartDate.getMonth() + 1; //January is 0!
            var yyyy = StartDate.getFullYear();
            
            // if date is less then 10, then append 0 before date   
            if(dd < 10){
                dd = '0' + dd;
            } 
            // if month is less then 10, then append 0 before date    
            if(mm < 10){
                mm = '0' + mm;
            }
            var EndDate = new Date();
            var dd = EndDate.getDate()+1;
            var mm = EndDate.getMonth() + 1; //January is 0!
            var yyyy = EndDate.getFullYear();
            /// var EndDate = new Date("2049-01-01");
            component.set('v.StartDate', yyyy + "-" + mm + "-" + dd);
            component.set('v.EndDate', yyyy + "-" + mm + "-" + dd);
            //  component.set('v.EndDate', EndDate.getFullYear() + "-" + (EndDate.getMonth() + 1) + "-" + EndDate.getDate());
        }
        
    },
    
    // VIP 
    getVIPValesfromUI: function(component,event,helper) {
        var type = component.find("SaleType").get("v.value");
        var rows = [];
        var eachRow ;
        
        var startDateFieldValue = component.find("StartDate").get("v.value");
        var sdateSplit = startDateFieldValue.split('-');
        var strtdat = new Date(sdateSplit[0], sdateSplit[1]-1, sdateSplit[2]);
        var strt = strtdat.toISOString().slice(0,10);
        var today = new Date(Date.now()).toISOString().slice(0,10);
        
        if (!isNaN(strtdat.getTime())) {
            // Months use 0 index.
            var startdatevalupd;
            startdatevalupd = strtdat.getFullYear() + '-' + (strtdat.getMonth() + 1) + '-' + strtdat.getDate();
            component.set('v.updatedstartdate', startdatevalupd);
        }
        
        var endDateFieldValue = component.find("EndDate").get("v.value");
        var edateSplit = endDateFieldValue.split('-');
        var enddate = new Date(edateSplit[0], edateSplit[1]-1, edateSplit[2]);
        var endd = enddate.toISOString().slice(0,10);
        if(startDateFieldValue && endDateFieldValue )
        {
            if (strt>endd){
                document.getElementById('error1').innerHTML="Start date should be less than End date";
                return;
            }
            
            else{
                document.getElementById('error1').innerHTML="";
            }
        }
        if (!isNaN(enddate.getTime())) {
            // Months use 0 index.
            var enddatevalupd;
            enddatevalupd = enddate.getFullYear() + '-' + (enddate.getMonth() + 1) + '-' + enddate.getDate();
            component.set('v.updatedenddate', enddatevalupd);
            
        }
        var marketField = component.find("MarktAccount");
        var marketFieldValue = marketField.get("v.value");
        var action = component.get("c.SalesOrderVIPStatus");
        action.setParams({
            'startdate':component.get('v.updatedstartdate'),
            'enddate':component.get('v.updatedenddate'),
            'market':marketFieldValue,
            'type' :type
        });
        action.setCallback(this, function(response) {
            var json = response.getReturnValue();
            for(var i=0;i<json.length;i++){
                var vipvalue = false;
                if(json[i].VIP == 'True'){
                    vipvalue = true;    
                }
                var libvalue = false;
                if(json[i].LIB == 'True'){
                    libvalue = true;    
                }
                var disableCaseBtn = false;
                if(json[i].CaseNumber == undefined){
                    disableCaseBtn = true;    
                }
                
                eachRow = {
                    SFContactID: json[i].SFContactID,
                    OrderNumber: json[i].OrderNumber,
                    ViewCase: json[i].ViewCase,
                    CaseId: json[i].CaseId,
                    Case: json[i].CaseNumber,
                    Sku: json[i].SKU,
                    Market: json[i].Market,
                    DeliveryDate: json[i].DeliveryDate,
                    Type: json[i].Type,
                    VIP: vipvalue,
                    LIB: libvalue,
                    CaseBtn: disableCaseBtn
                }
                rows.push(eachRow);
            }
           
            component.set('v.myData',rows);
            if(json.length > 0){
                component.set('v.pageNumber',1);
                component.set("v.maxPage", Math.floor((json.length+99)/100));
                helper.renderPage(component, event, helper);
                var totalRecordsList = json;
                var totalLength = totalRecordsList.length;
                component.set("v.isExport",false);
                component.set("v.totalRecordsCount", totalLength);
                component.set("v.bNoRecordsFound" , false);
                component.set("v.showTable", true);
            }else{
                // if there is no records then display message
                component.set("v.bNoRecordsFound" , true);
                component.set("v.showTable", false);
                component.set('v.pageNumber',0);
            }
        });
        $A.enqueueAction(action);
    },
    
    //ASAP 
    
    getASAPValesfromUI : function(component,event,helper) {
        
        var type = component.find("OrderType").get("v.value");
        
        var rows = [];
        var eachRow ;
        
        var marketFieldV = component.find("MarktAccount").get("v.value");
        //var marketFieldValue = marketField.get("v.value");
        /*if (marketFieldV == null || marketFieldV == ''){
                document.getElementById('error1').innerHTML="Please select the Market to Narrow the results.";
                return;
            }
        else{
                document.getElementById('error1').innerHTML="";
            }*/
        var action = component.get("c.SalesOrderASAPStatus");
        action.setParams({
            'market':marketFieldV,
            'type' :type
        });
        action.setCallback(this, function(response) { 
            var state = response.getState();
            
            if (state === "SUCCESS") {    
                var jsonval = response.getReturnValue();             
                var json = JSON.parse(jsonval);
                for(var i=0; i<json.length; i++){
                    var market = json[i].Market;
                    if (market.includes('#')){
                        market = market.toString().replace('#','');
                    }      
                    eachRow = {
                        SFContactID: json[i].SFContactID,
                        OrderNumber: json[i].OrderNumber,
                        Phone1: json[i].Phone1,
                        Phone2: json[i].Phone2,
                        Phone3: json[i].Phone3,
                        Market: market
                    }
                    rows.push(eachRow);
                }
                component.set('v.myData',rows);
                var clist =  component.set('v.myData',rows);
                component.set('v.currentList',clist);
                                 
                if(json.length > 0){
                    
                    component.set('v.pageNumber',1);
                    component.set("v.maxPage", Math.floor((json.length+99)/100));
                    helper.renderPage(component, event, helper);
                    var totalRecordsList = json;
                    var totalLength = totalRecordsList.length;
                    component.set("v.isExport",false);
                    component.set("v.totalRecordsCount", totalLength);
                    component.set("v.bNoRecordsFound" , false);
                    component.set('v.showTable',true);
                    
                }else{
                    // if there is no records then display message
                    component.set("v.bNoRecordsFound" , true);
                    component.set('v.pageNumber',0);
                }
                component.set("v.customerwithActiveCartLst", json);
            }  
        });
        $A.enqueueAction(action);
    },
    
    // object and set the corresponding aura attribute.
    // Hot 
    getHotValesfromUI : function(component,event,helper) {
        var action = component.get("c.SalesOrderHotStatus");
        var startDateField = component.find("StartDate");
        var startDateFieldValue = startDateField.get("v.value");
        var strtdat = new Date(startDateFieldValue);
        var strt = strtdat.toISOString().slice(0,10);
        
        if (!isNaN(strtdat.getTime())) {
            // Months use 0 index.
            var startdatevalupd =  strtdat.getMonth() + 1 + '/' + strtdat.getDate() + '/' + strtdat.getFullYear();
            component.set('v.updatedstartdate', startdatevalupd);
        }
        
        var endDateField = component.find("EndDate");
        var endDateFieldValue = endDateField.get("v.value");
        var enddate = new Date(endDateFieldValue);
        var endd = enddate.toISOString().slice(0,10);
        
        if(startDateFieldValue && endDateFieldValue )
        {
            if (strt>endd){
                document.getElementById('error1').innerHTML="Start date should be less than End date";
                return;
            }
            
            else{
                document.getElementById('error1').innerHTML="";
            }
        }
        
        if (!isNaN(enddate.getTime())) {
            // Months use 0 index.
            var enddatevalupd =  enddate.getMonth() + 1 + '/' + enddate.getDate() + '/' + enddate.getFullYear();
            component.set('v.updatedenddate', enddatevalupd);
        }
        var marketField = component.find("MarktAccount");
        var marketFieldValue = marketField.get("v.value");
        action.setParams({
            'startdate':component.get('v.updatedstartdate'),
            'enddate':component.get('v.updatedenddate'),
            'market':marketFieldValue
        });
        action.setCallback(this, function(response) {
            response = response.getReturnValue();
            var json = JSON.parse(response);
            component.set('v.myData',json);
            if(json.length > 0){
                component.set('v.pageNumber',1);
                component.set("v.maxPage", Math.floor((json.length+99)/100));
                helper.renderPage(component, event, helper);
                var totalRecordsList = json;
                var totalLength = totalRecordsList.length ;
                component.set("v.isExport",false);
                component.set("v.totalRecordsCount", totalLength);
                component.set("v.bNoRecordsFound" , false);
                component.set("v.showTable", true);
                
            }else{
                // if there is no records then display message
                component.set("v.bNoRecordsFound" , true);
                component.set("v.showTable", false);
                component.set('v.pageNumber',0);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Hot    
    convertHotArrayOfObjectsToCSV : function(component,objectRecords){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['OrderNumber','Market','AccountShipTo','DeliveryDate','WindowBegin','WindowEnd','Type','DateMarkedHot'];
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }  
                csvStringResult += '"'+ objectRecords[i][skey]+'"';
                counter++;
                
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        
        // return the CSV formate String 
        return csvStringResult;  
        
    },
    //VIP
    convertVIPArrayOfObjectsToCSV : function(component, objectRecords){
        
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['OrderNumber','Case','Sku','Market','DeliveryDate','VIP','LIB'];
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                if (objectRecords[i][skey] != undefined) {
                    csvStringResult += '"'+ objectRecords[i][skey] +'"'; 
                } else {
                    csvStringResult += '""'; 
                }
                counter++;
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close
        // return the CSV formate String 
        return csvStringResult;        
    },
    
    //ASAP
    convertASAPArrayOfObjectsToCSV : function(component,objectRecords){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['OrderNumber','Market','Phone1','Phone2','Phone3'];
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                
                csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
                
                counter++;
                
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        
        // return the CSV formate String 
        return csvStringResult;        
    },
	
    //DeliveryConfirmation  
    
    convertDeliveryConfirmationArrayOfObjectsToCSV : function(component,objectRecords){
        // declare variables
       
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        
        keys = ['OrderNumber','Type', 'SaleDate', 'DelPieces', 'DeliveryDate', 'ShipVia', 'ShipToName', 'ShipToAddr1', 'ShipToAddr2', 'ShipToCity', 'ShipToState', 'ShipToZip', 'Contact', 'AdjustedTime', 'RoutingPass', 'Confirm'];
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }  
                csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
                counter++;
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        
        // return the CSV formate String 
        return csvStringResult; 
    },
    
   	renderPage: function(component, event, helper) {
       var orType = component.get("v.typeOfOrder");
       if(orType == 'Delivery Confirmation'){
	//	var records = component.get("v.allrows"),
        var records = component.get("v.myData"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*100, pageNumber*100);
            //   component.set("v.allrowsData", pageRecords);
             component.set("v.allrows", pageRecords);
       }else{
           var records = component.get("v.myData"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*100, pageNumber*100);
            component.set("v.currentList", pageRecords);
            // component.set("v.myData", pageRecords);
       }
	}
})