({
    doInit : function(component, event, helper) {
        
        var val= component.get("v.customerwithActiveCartLst");
        component.set("v.maxPage", Math.floor((val.length+9)/10));
        //helper.renderPage(component, event, helper);
        helper.onPageLoad(component, event, helper);        
        var markets=component.get("c.marketvalues");
        var self = this;
        markets.setCallback(this, function(actionResult) {
        component.set("v.marketoptions", actionResult.getReturnValue());
        });
        $A.enqueueAction(markets);
    },
    
    onClickToDial: function (cmp, event, helper) {
        var clickToDialService = cmp.find("clickToDialService");
        clickToDialService.addDialListener(function(payload){
        console.log("This alert simulates the onClickToDial method for Open CTI in Lightning Experience. The phone number is dialed sending the following payload: " + JSON.stringify(payload));
        });
    },
    
   renderPage: function(component, event, helper) {
        helper.renderPage(component);
    },

    orderSelection : function(component, event, helper) {
        var orType = component.get("v.typeOfOrder");
        if(orType == 'Hot Orders')
        {
            component.set("v.isHot",true);
            component.set("v.isVIP",false);
            component.set("v.isASAP",false);
            component.set("v.isDeliveryConfirmation",false);
            component.set("v.isMarket",true);
            component.set('v.myColumns','');
            component.set('v.myData','');
            component.set('v.isExport',true);
            component.set('v.totalRecordsCount',0);
            component.set('v.showTable',false);
            component.set("v.bNoRecordsFound" , false);
            
            component.set('v.showpaginationVIP',false);
            component.set('v.showpaginationASAP',false);
            component.set('v.showpaginationDelivery',false);
            helper.onPageLoad(component, event, helper);
        }
        else if(orType == 'VIP/LIB Orders')
        {
            component.set("v.isVIP",true);
            component.set("v.isHot",false);
            component.set("v.isASAP",false);
            component.set("v.isDeliveryConfirmation",false);
            component.set("v.isMarket",true);
            component.set('v.myColumns','');
            component.set('v.myData','');
            component.set('v.isExport',true);
            component.set('v.totalRecordsCount',0);
            component.set('v.showTable',false);
            component.set("v.bNoRecordsFound" , false);
            component.set('v.showpaginationhot',false);
            component.set('v.showpaginationASAP',false);
            component.set('v.showpaginationDelivery',false);
            helper.onPageLoad(component, event, helper);
        }
        else if(orType == 'ASAP')
        {
            component.set("v.isASAP",true);
            component.set("v.isVIP",false);
            component.set("v.isHot",false);
            component.set("v.isDeliveryConfirmation",false);
            component.set("v.isMarket",true);
            component.set('v.myColumns','');
            component.set('v.myData','');
            component.set('v.isExport',true);
            component.set('v.totalRecordsCount',0);
            component.set('v.showTable',false);
            component.set("v.bNoRecordsFound" , false);
            component.set('v.showpaginationhot',false);
            component.set('v.showpaginationVIP',false);
            component.set('v.showpaginationDelivery',false);
         }
          else if(orType == 'Delivery Confirmation')
             {
                 component.set("v.isDeliveryConfirmation",true);
                 component.set("v.isASAP",false);
                 component.set("v.isVIP",false);
                 component.set("v.isHot",false);
                 component.set("v.isMarket",true);
                 component.set('v.myColumns','');
                 component.set('v.myData','');
                 component.set('v.isExport',true);
                 component.set('v.totalRecordsCount',0);
                 component.set('v.showTable',false);
                 component.set("v.bNoRecordsFound" , false);
                 //component.set("v.delCnfDate", true);
                component.set('v.showpaginationhot',false);
            	component.set('v.showpaginationVIP',false);
            	component.set('v.showpaginationASAP',false);
                 helper.onDeliveryConfirmation(component, event, helper);
             }
    },
    
    dateUpdate : function(component, event, helper) {
        var startDateField = component.find("StartDate");
        var startDateFieldValue = startDateField.get("v.value");
        var endDateField = component.find("EndDate");
        var endDateFieldValue = endDateField.get("v.value");
        
            if(startDateFieldValue && endDateFieldValue )
        {
            if(startDateFieldValue>endDateFieldValue)
            {
                component.set("v.dateValidationError" , true);
            }else{
                component.set("v.dateValidationError" , false);
            }
        }
    },
    
    marketUpdate : function(component, event, helper) {
        var marketField = component.find("MarktAccount");
        var marketFieldValue = marketField.get("v.value");
        if (marketFieldValue != null && marketFieldValue != ''){
                document.getElementById('error1').innerHTML="";
            }
    },
    
    //Hot
    
    hotClick : function(component, event, helper){
        
        component.set('v.myColumns', [
            {label:'Action',type:  'button',typeAttributes:
             {iconName: 'utility:view',label: 'View Record',name: 'viewRecord', disabled: false,value: 'viewBtn'}
            },
            //  {label: 'AccountId', fieldName: 'SFContactID', type: 'text'},
            {label: 'Sales Order', fieldName: 'OrderNumber', type: 'text', iconName: 'standard:opportunity'},
            {label: 'Market', fieldName: 'Market', type: 'text'},
            {label: 'AccountShipTo', fieldName: 'AccountShipTo', type: 'Date'},
            {label: 'DeliveryDate', fieldName: 'DeliveryDate', type: 'Date', cellAttributes: { iconName: 'utility:date_time', iconAlternativeText: 'Close Date'  }},
            {label: 'WindowBegin', fieldName: 'WindowBegin', type: 'Date'},
            {label: 'WindowEnd', fieldName: 'WindowEnd', type: 'text'},
            {label: 'Type', fieldName: 'Type', type: 'text'},
            {label: 'DateMarkedHot', fieldName: 'DateMarkedHot', type: 'Date', cellAttributes: { iconName: 'utility:date_time', iconAlternativeText: 'Close Date'  }}
        ]);
        helper.getHotValesfromUI(component,event,helper);
       		component.set('v.showpaginationhot',true);
            component.set('v.showpaginationVIP',false);
            component.set('v.showpaginationASAP',false);
            component.set('v.showpaginationDelivery',false);
    },
    
    //---VIP--- 
    
    vipLibClick: function(component, event, helper){
        
        component.set('v.myColumns', [
            {type:'button', initialWidth: 200,
             typeAttributes: {iconName:'utility:view', label:'View Sales Order', name:'viewSalesOrder', disabled:false, value:'viewBtn'}
            },
            {label: 'Sales Order', fieldName: 'OrderNumber', type: 'text', iconName: 'standard:opportunity'},
            {type:'button', 
             typeAttributes: {iconName:'utility:view', label: 'View Case', name: 'viewCase', title: 'Click to view Case', disabled: {fieldName: 'CaseBtn'}, value:'viewBtn'}
            },
            {label: 'Case', fieldName: 'Case', type: 'URL'},
            {label: 'SKU', fieldName: 'Sku', type: 'text'},
            {label: 'Market', fieldName: 'Market', type: 'text'},
            {label: 'DeliveryDate', fieldName: 'DeliveryDate', type: 'Date', cellAttributes: { iconName: 'utility:date_time', iconAlternativeText: 'Close Date'  }},
            {label: 'VIP', fieldName: 'VIP', type: 'boolean', initialWidth: 100},
            {label: 'LIB', fieldName: 'LIB', type: 'boolean', initialWidth: 100},
        ]);
            helper.getVIPValesfromUI(component,event,helper); 
            component.set('v.showpaginationhot',false);
            component.set('v.showpaginationVIP',true);
            component.set('v.showpaginationASAP',false);
            component.set('v.showpaginationDelivery',false);
      },
            
     /* //----ASAP-----
      ASAPClick : function(component,event,helper) {
		var type = component.find("OrderType").get("v.value");
        
        var rows = [];
        var eachRow ;
        
        var marketFieldV = component.find("MarktAccount").get("v.value");
        //var marketFieldValue = marketField.get("v.value");
        
        var action = component.get("c.SalesOrderASAPStatus");
        action.setParams({
            'market':marketFieldV,
            'type' :type
        });
        action.setCallback(this, function(response) { 
            var state = response.getState();
            
            if (state === "SUCCESS") {    
                var jsonval = response.getReturnValue();             
                var json = JSON.parse(jsonval);
                for(var i=0; i<json.length; i++){
                    var market = json[i].Market;
                    if (market.includes('#')){
                        market = market.toString().replace('#','');
                    }      
                    eachRow = {
                        SFContactID: json[i].SFContactID,
                        OrderNumber: json[i].OrderNumber,
                        Phone1: json[i].Phone1,
                        Phone2: json[i].Phone2,
                        Phone3: json[i].Phone3,
                        Market: market
                    }
                    rows.push(eachRow);
                }
                component.set('v.myData',rows);
                var clist =  component.set('v.myData',rows);
                component.set('v.currentList',clist);
                                 
                if(json.length > 0){
                    component.set('v.pageNumber',1);
                    component.set("v.maxPage", Math.floor((json.length+99)/100));
                    helper.renderPage(component, event, helper);
                    
                    component.set('v.showpaginationhot',false);
                    component.set('v.showpaginationVIP',false);
                    component.set('v.showpaginationASAP',true);
                    component.set('v.showpaginationDelivery',false);
                    
                    var totalRecordsList = json;
                    var totalLength = totalRecordsList.length;
                    component.set("v.isExport",false);
                    component.set("v.totalRecordsCount", totalLength);
                    component.set("v.bNoRecordsFound" , false);
                    component.set('v.showTable',false);
                    
                }else{
                    // if there is no records then display message
                    component.set("v.bNoRecordsFound" , true);
                    component.set('v.pageNumber',0);
                }
                component.set("v.customerwithActiveCartLst", json);
            }  
        });
        $A.enqueueAction(action);
    },*/
       ASAPClick: function(component, event, helper){
            
            component.set('v.myColumns', [
            {label:'Action',type:'button',
            typeAttributes:{iconName: 'utility:view',label: 'View Record',name: 'viewRecord', disabled: false,value: 'viewBtn'}
            },
           // {label: 'Sales Order', fieldName: 'OrderNumber', type: 'text', iconName: 'standard:opportunity', sortable: true},
           // {label: 'Market', fieldName: 'Market',  type: 'text', sortable: true},
            {label: 'Sales Order', fieldName: 'OrderNumber', type: 'text', iconName: 'standard:opportunity'},
            {label: 'Market', fieldName: 'Market',  type: 'text'},
            {label: 'Phone 1', fieldName: 'Phone1', type: 'phone'},
            {label: 'Phone 2', fieldName: 'Phone2', type: 'phone'}, 
            {label: 'Phone 3', fieldName: 'Phone3', type: 'phone'},
        ]);
        //  {label: 'AccountId', fieldName: 'SFContactID', type: 'text'},
            helper.getASAPValesfromUI(component,event,helper);
            window.setTimeout(
            $A.getCallback(function() {
            helper.renderPage(component, event, helper); 
            component.set('v.showpaginationhot',false);
            component.set('v.showpaginationVIP',false);
            component.set('v.showpaginationASAP',true);
            component.set('v.showpaginationDelivery',false);
            }), 2000
        );
    }, 
	
       //Added for Delivery confirmation       
       getAccountOrders : function(component, event, helper) {
            var startDateField = component.find("StartDate");
            var startDateFieldValue = startDateField.get("v.value");
           	var splitvalue = startDateFieldValue.split("-");
			var strtdatevalue = splitvalue[1]+'/'+splitvalue[2]+'/'+splitvalue[0];
            var strtdat = new Date(startDateFieldValue);
            var strt = strtdat.toISOString().slice(0,10);
         
            /*if (!isNaN(strtdat.getTime())) {
            // Months use 0 index.
            
            var startdatevalupd =  strtdat.getMonth() + 1 + '/' + strtdat.getDate() + '/' + strtdat.getFullYear();
            component.set('v.updatedstartdate', startdatevalupd);
            }*/
            component.set('v.updatedstartdate', strtdatevalue);
            var endDateField = component.find("EndDate");
            var endDateFieldValue = endDateField.get("v.value");
            var endsplitvalue = endDateFieldValue.split("-");
			var enddatevalue = endsplitvalue[1]+'/'+endsplitvalue[2]+'/'+endsplitvalue[0];
            var enddate = new Date(endDateFieldValue);
            var endd = enddate.toISOString().slice(0,10);
          
            if(startDateFieldValue && endDateFieldValue )
            {
            if (strt>endd){
            document.getElementById('error1').innerHTML="Start date should be less than End date";
            return;
            }
            else{
            document.getElementById('error1').innerHTML="";
                //return;
            }
            }
           
            /*if (!isNaN(enddate.getTime())) {
            // Months use 0 index.
            var enddatevalupd =  enddate.getMonth() + 1 + '/' + enddate.getDate() + '/' + enddate.getFullYear();
            component.set('v.updatedenddate', enddatevalupd);
            }*/
            component.set('v.updatedenddate', enddatevalue);
            var marketField = component.find("MarktAccount");
            var marketFieldValue = marketField.get("v.value");
            var action = component.get("c.SalesOrderDeliveryConfirmationStatus");
           
            action.setParams({
            'startdate':component.get('v.updatedstartdate'),
            'enddate':component.get('v.updatedenddate'),
            'market':marketFieldValue
            });
            action.setCallback(this, function(response){
           
            var state = response.getState();
            var rows = [];
            var eachRow ;
            var rowjson;
                              
        if (state === "SUCCESS") { 
            	var callResponse = response.getReturnValue();
          
            if(JSON.stringify(callResponse).includes('error'))		
			{
                component.set("v.isError",true);
                component.set("v.errorMsg", 'There is no information Available');
            }
            else
            {
                var parsedjson = JSON.parse(callResponse);
                component.set('v.totalRecordsCount',parsedjson.length);
                if(parsedjson.length != '0'){
                component.set("v.bNoRecordsFound" , false);
                component.set('v.isExport',false);
                    
				for(var i=0;i<parsedjson.length;i++){
                var mkt = parsedjson[i].Market;
               	var orN = parsedjson[i].OrderNumber;
                var ty  =parsedjson[i].Type;
                var saldat = parsedjson[i].SaleDate;
                var delpie= parsedjson[i].DelPieces;
                var deldat = parsedjson[i].DeliveryDate;
                var shipv = parsedjson[i].ShipVia;
                var stn =parsedjson[i].ShipToName ;
                var sta1 = parsedjson[i].ShipToAddr1;
                var sta2 = parsedjson[i].ShipToAddr2;
                var stc = parsedjson[i].ShipToCity;
                var sts =parsedjson[i].ShipToState ;
                var stz = parsedjson[i].ShipToZip;
                var cont = parsedjson[i].Contact;
                var adjt =parsedjson[i].AdjustedTime;
                var routp = parsedjson[i].RoutingPass;
                var con = parsedjson[i].Confirm;
                var accId = parsedjson[i].SFPersonAccountID;
                var accShipTo = parsedjson[i].AccountShipTo;
                var pctr = parsedjson[i].ProfitCenter;
                var trkId = parsedjson[i].TruckID;
                var cstOpen = parsedjson[i].CustomerWindowOpen;
                var cstCls = parsedjson[i].CustomerWindowClose;
                var exid = orN+':'+accId;
                var isConfirmed;
                var iscnf;
                //var isConfirmed1;
                if (con == 'True'){
                isConfirmed = 'Confirmed';
                iscnf = true;
                //isConfirmed1 = 'Confirmed';
                }
                else{
                isConfirmed = 'Confirm';
                iscnf = false;
                //isConfirmed1 = 'Confirm';
                }
                eachRow = {
                Market:mkt,
                OrderNumber:orN,
                Type:ty,
                //inCfrmd1:isConfirmed1,
                SaleDate : saldat,
                DelPieces : delpie,
                DeliveryDate :deldat,
                ShipVia:shipv,
                ShipToName:stn,
                ShipToAddr1:sta1,
                ShipToAddr2:sta2,
                ShipToCity:stc,
                ShipToState:sts,
                ShipToZip:stz,
                Contact:cont,
                AdjustedTime:adjt,
                RoutingPass:routp,
                Confirm:isConfirmed,
                iscnf:iscnf,
                accountId:accId,
                accountShipto:accShipTo,
                profitCtr:pctr,
                truckId:trkId,
                ExternalId:exid,
                windowOpn:cstOpen,
                windowCls:cstCls
                }
                rowjson = JSON.stringify(eachRow);
                rows.push(eachRow);
                }
                //var cbn = rowjson.inCfrmd;
                component.set('v.allrows',rows);
                component.set('v.myData',rows);
     
				component.set('v.showpaginationhot',false);
                component.set('v.showpaginationVIP',false);
                component.set('v.showpaginationASAP',false);
                component.set('v.showpaginationDelivery',true);
                            
                component.set('v.pageNumber',1);
                component.set("v.maxPage", Math.floor((parsedjson.length+99)/100));
                helper.renderPage(component, event, helper);
                component.set("v.isError",false);
                }else{
                component.set('v.isExport',true);  
                component.set("v.bNoRecordsFound" , true);
                }
            }
        } /*else if(response.getState() === "ERROR") {
            var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    component.set("v.errorMsg", 'There is no information Available');
                }
            }else {
                component.set("v.errorMsg", 'Request Failed!' );
            }
        }*/   
    });
        
    $A.enqueueAction(action);                  
},
   
 updateSalesOrder2: function(component, event, helper) {
        var soRoute = event.currentTarget.dataset.orderlineid;
     	var target = event.target;
		var i = target.getAttribute("data-row-index");
        var soData =component.get("v.allrows");
    
    var so;
        var ast;
        var pc;
        var tID;
        var dd;
        var rp;
        var cstop;
        var cstcls;
        
        //for(var i=0;i<soData.length;i++){
            if(soRoute == soData[i].RoutingPass){
                ast =  soData[i].accountShipto;
                pc =  soData[i].profitCtr;   
                tID =  soData[i].truckId;   
                dd =  soData[i].DeliveryDate;
                so =  soData[i].OrderNumber;
                rp =  soData[i].RoutingPass;   
                cstop =  soData[i].windowOpn;
                cstcls =  soData[i].windowCls;
            }   
//        }
	
    var strttim;
    var endtim;
    if(cstop.includes("PM") || cstcls.includes("PM")){
         strttim = cstop.replace("PM","");
         endtim = cstcls.replace("PM","");
    }
    else if(cstop.includes("AM")|| cstcls.includes("AM")){
         strttim = cstop.replace("AM","");
         endtim = cstcls.replace("AM","");
            }
        var timestampBegin = cstop.split('.')[0];
        var tsb0 = timestampBegin.slice(0,10);
        var tsb1 = timestampBegin.slice(11,19);
        var tsb3 = ' ';
        var tsb4 = tsb0.concat(tsb3, tsb1);
        var timestampend = cstcls.split('.')[0];
        var tse0 = timestampend.slice(0,10);
        var tse1 = timestampend.slice(11,19);
        var tse3 = ' ';
        var tse4 = tse0.concat(tse3, tse1);
        component.set('v.PopupsoId',so);
        var actionConfirm = component.get("c.soConfirmData");
        actionConfirm.setParams({"accShipto" : ast,
                                 "soNumber" : so,
                                 "profitCenter" : pc,
                                 "trkID" : tID,
                                 "delDate" : dd,
                                 "rPass" : rp,
                                 "custOpen" : cstop,
                                 "custClose" : cstcls
                                });
        
        actionConfirm.setCallback(this, function(response) {  
            var state = response.getState();
            
            if (state === "SUCCESS") {	 
                var callResponse = response.getReturnValue();
               // for(var i=0;i<soData.length;i++){
                //    if(so == soData[i].salesorder_number){
               	 	if(soRoute == soData[i].RoutingPass)
					{
					if(callResponse.includes('success')){
                        soData[i].isCnfs = true;
                        soData[i].inCfrmd = 'Confirmed';
                    }   
              	 }
                component.set("v.allrows",soData);
            }
            else if (response.getState() === "ERROR") {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                errorToast.fire();                
            }
        });
        $A.enqueueAction(actionConfirm); 
    },
 
 navigateToRecord : function (component, event, helper) {
        var externalId = event.currentTarget.dataset.orderlineid;
        var action = component.get("c.searchAccountsSOQL");
    action.setParams({
        'searchString' : externalId
    });
    
    action.setCallback(this, function(response) {           
        var state = response.getState();
        
        if (state === "SUCCESS") {
            var response = response.getReturnValue();
            component.set("v.responseId",response);
            var navEvent = $A.get("e.force:navigateToURL");
            navEvent.setParams({
                "url": '/lightning/r/SalesOrder__x/' + response + '/view'
            });
            navEvent.fire();
        }
    });
    $A.enqueueAction(action);
},
   
 //Ended           
 // Row Action-- Added by praneeth
 handleRowAction :function(component,event,helper){
    var action = event.getParam('action');
    var row = event.getParam('row');
    var salesorder = row.OrderNumber;
    var accountId = row.SFContactID;
    var extid = salesorder + ':' + accountId ;
    
    var action = component.get("c.searchAccountsSOQL");
    action.setParams({
        'searchString' : extid
    });
    
    action.setCallback(this, function(response) {           
        var state = response.getState();
        
        if (state === "SUCCESS") {
            var response = response.getReturnValue();
            component.set("v.responseId",response);
            var navEvent = $A.get("e.force:navigateToURL");
            
            navEvent.setParams({
                "url": '/lightning/r/SalesOrder__x/' + response + '/view'
            });
            
            navEvent.fire();
        }
    });
    $A.enqueueAction(action);
},
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
        // this function automatic call by aura:doneWaiting event 
        hideSpinner : function(component,event,helper){
            // make Spinner attribute to false for hide loading spinner    
            component.set("v.Spinner", false);
        },
            //## function call on click on the "Download As CSV" Button.
            exportExcel : function(component,event,helper){
                //get the Records [contact] list from "mydata" attribute
                  var stockData = component.get('v.myData');
                
                //call the helper function which "return" the CSV data as string
                
                //Hot
                var csvHot;
                if(component.get("v.isHot") == true)
                {
                    csvHot = helper.convertHotArrayOfObjectsToCSV(component,stockData);
                    if (csvHot == null){return;}
                    //###--code for create a temp. <a> html tag  [link tag] for download the CSV file --###
                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvHot);
                    hiddenElement.target = '_self';//
                    hiddenElement.download = 'Hot Orders.csv';  // CSV file Name* you can change it.[only name not .csv] 		
                    document.body.appendChild(hiddenElement); // Required for FireFox browser		
                    hiddenElement.click(); // using click() js function to download csv file
                }
                else 
                    //VIP         
                    var todoVIP = component.get("v.isVIP");
                
                if(component.get("v.isVIP") == true)
                {
                   
                    var csvVIP = helper.convertVIPArrayOfObjectsToCSV(component, stockData); 
                    if (csvVIP == null){return;}
                    // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
                    var hiddenElement = document.createElement('a');
                    
                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvVIP);
                    hiddenElement.target = '_self'; // 
                    hiddenElement.download = 'VIP_LIB Orders.csv';  // CSV file Name* you can change it.[only name not .csv] 
                    document.body.appendChild(hiddenElement); // Required for FireFox browser
                    hiddenElement.click(); // using click() js function to download csv file
                }
                else 
                    //ASAP            
                    if(component.get("v.isASAP") == true)
                    {
                        var csvASAP = helper.convertASAPArrayOfObjectsToCSV(component,stockData);
                       
                        if (csvASAP == null){return;}
                        //###--code for create a temp. <a> html tag  [link tag] for download the CSV file --###
                        var hiddenElement = document.createElement('a');
                        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvASAP);
                        hiddenElement.target = '_self';//
                        hiddenElement.download = 'ASAP Orders.csv';  // CSV file Name* you can change it.[only name not .csv] 		
                        document.body.appendChild(hiddenElement); // Required for FireFox browser		
                        hiddenElement.click(); // using click() js function to download csv file
                    }
                    else
                        //DeliveryConfirmation
                        
                        if(component.get("v.isDeliveryConfirmation") == true)
                        {
                            var csvDeliveryConfirmation = helper.convertDeliveryConfirmationArrayOfObjectsToCSV(component,stockData);
                            
                            if (csvDeliveryConfirmation == null){return;}
                            //###--code for create a temp. <a> html tag  [link tag] for download the CSV file --###
                            var hiddenElement = document.createElement('a');
                            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvDeliveryConfirmation);
                            hiddenElement.target = '_self';//
                            hiddenElement.download = 'Delivery Confirmation.csv';  // CSV file Name* you can change it.[only name not .csv] 		
                            document.body.appendChild(hiddenElement); // Required for FireFox browser		
                            hiddenElement.click(); // using click() js function to download csv file
                        }
            },
		
	calculateWidth : function(component, event, helper) {
            var childObj = event.target
            var parObj = childObj.parentNode;
            var count = 1;
            while(parObj.tagName != 'TH') {
                parObj = parObj.parentNode;
                count++;
            }
            var mouseStart=event.clientX;
            component.set("v.mouseStart",mouseStart);
            component.set("v.oldWidth",parObj.offsetWidth);
    },
    setNewWidth : function(component, event, helper) {
        
            var childObj = event.target
            var parObj = childObj.parentNode;
            var count = 1;
            while(parObj.tagName != 'TH') {
                parObj = parObj.parentNode;
                count++;
            }
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = component.get("v.oldWidth");
            var newWidth = event.clientX- parseFloat(mouseStart)+parseFloat(oldWidth);
            parObj.style.width = newWidth+'px';
    }
})