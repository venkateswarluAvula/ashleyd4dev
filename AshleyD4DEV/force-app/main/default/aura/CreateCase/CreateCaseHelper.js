({
    helperFun : function(component, event, secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');
            $A.util.toggleClass(acc[cmp], 'slds-hide');
        }
    },
    
    closingtab : function(component) {
        var closeparenttab = component.find("workspace");
        if(closeparenttab != undefined){
            closeparenttab.isConsoleNavigation().then(function(consoleResponse) {
                console.log('Console Navigation Resp: ' + consoleResponse);
                if(consoleResponse) {
                    console.log('Console view is supported');
                    closeparenttab.getFocusedTabInfo().then(function(response) {
                        var focusedTabId = response.tabId;
                        if(response.isSubtab == true){
                            closeparenttab.refreshTab({
                                tabId: response.parentTabId,
                                includeAllSubtabs: true
                            });
                        }
                        closeparenttab.closeTab({tabId: focusedTabId});
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
                } else {
                    console.log('Console view is not supported');
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get("v.recordId"),
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }
            })
            .catch(function(error) {
                console.log('Console Navigation Err: ' + error);
            });
        }
    }
})