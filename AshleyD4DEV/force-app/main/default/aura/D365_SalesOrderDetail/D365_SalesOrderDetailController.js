({
	doInit : function(component, event, helper) {
		var action = component.get("c.getOrders");
        
        /*var testid = component.get("v.recordId");
        action.setParams({
	        "salesforceOrderId" : testid
	    });
        */
        //alert('action is '+JSON.stringify(action));
        action.setCallback(this,function(response) {
          	
           var state = response.getState();
            //alert('state is'+state);
            if (state === "SUCCESS") {
                //alert('succes');
            	var succesData = response.getReturnValue();
                //alert('succesData is '+JSON.stringify(succesData));
                //console.log('test'+JSON.stringify(succesData));
                component.set("v.data", succesData);
                
            }
        });
        $A.enqueueAction(action);
	}
})