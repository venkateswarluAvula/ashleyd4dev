({
    myaction : function(component, event, helper) {
        var actiondata = component.get("c.rsalesorder");	
        
        actiondata.setParams({ AId : component.get("v.recordId")});  
        actiondata.setCallback(this, function(response){
            // alert("setcallBack");    
            var state = response.getState();
            //alert("state "+state);
            if (state === "SUCCESS") {
                 alert("success");
                // alert("From server: " + JSON.stringify(response.getReturnValue())); 
                var Responsedata = response.getReturnValue(); 	
                console.log("SalesOrder" ,response.getReturnValue());
                component.set("v.SalesOrder", Responsedata); 	 				  
                
            }
            else if (response.getState() === "ERROR") {
                alert("error");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMsg", errors[0].message );
                    }
                }else {
                    component.set("v.errorMsg", 'Request Failed!' );
                }
            }   
            
        });
        $A.enqueueAction(actiondata);        
        
    }
})