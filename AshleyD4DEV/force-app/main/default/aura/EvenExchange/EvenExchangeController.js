({
	myAction : function(component, event, helper) {
		
	},    
    openModal : function(component, event, helper) {
        //component.set("v.evenexchangeavilable", true);
        component.set("v.openModal", true);        
        var curDate = new Date().toISOString().slice(0, 10);
        component.set("v.currentDate", curDate);
        var defv = [];
        component.set("v.selectedPR", defv);
        component.set("v.selectedPRv", defv);
        component.set("v.selectedLineItem", defv);
        component.set("v.productlineItemsinvoice", defv);
        component.set("v.selproductlineItems", defv);
        console.log('Modal Window Called ' + component.get("v.recordId"));        
        var action = component.get("c.getProductLineItem");
        console.log('my EXrecord id', action);
        //	component.find('v.Sales_Order_id').set('v.value', action);
        action.setParams({
            "caseId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() == "SUCCESS") {
                //component.set("v.productlineItems" , response.getReturnValue().productlineItems);
                component.set("v.caseAddress", response.getReturnValue().caseAddress);
                component.set("v.orderAddress", response.getReturnValue().salesvaleItem);
                component.set("v.salesOrder", response.getReturnValue().salesvale);
                component.set("v.AddressValue", "Order Address");
                component.set("v.evenexchangeavilable", false);
        		component.set("v.openModal", true);
                var invoiced = component.get("v.productlineItemsinvoice");
				var scom = '';  
                var litemverify = '';
                var invoicediff = '';
                var firstval = '';
                var obj1 = response.getReturnValue().productlineItems;
                var obj2 = response.getReturnValue().salesvaleItem;
                for (var z = 0; z< obj1.length; z++){
                    if(obj1[z].Part_Order_Status__c == 'Invoiced'){
                        litemverify = 'Yes';
                        invoiced.push(obj1[z]);
                    }
                    if(z==0)
                    	firstval = obj1[z].Invoice_Number__c;                    
                    if(obj1[z].Invoice_Number__c != firstval){
                        invoicediff = 'Yes';                        
                    }
                }
               /* for (var y = 0; y< obj2.length; y++){
                    if(y==0)
                    	firstval = obj2[y].phdInvoiceNo__c;
                    if(obj2[y].phdSaleType__c == 'Invoiced'){
                        if(obj2[y].phdInvoiceNo__c != firstval){
                            invoicediff = 'Yes';                        
                        }
                    }                    
                }   */             
                
                if(litemverify == 'Yes'){
                    component.set("v.productlineItems" , invoiced);
                }else{
                    component.set("v.productlineItems" , response.getReturnValue().productlineItems);
                }
                /*if(response.getReturnValue().productlineItems.length > 0){
                    var obj = response.getReturnValue().productlineItems;
                    for (var j = 0; j < obj.length; j++) {
                        var str = obj[j].Item_SKU__c;
                        if(!str.includes("*")){
                            scom += str + ',';
                        }
                    }
                }
                if(scom.length > 2){
                    scom = scom.slice(0,-1);
                } */
                if(response.getReturnValue().salesvale.fulfillerID__c != null){
                    var fulvalue = response.getReturnValue().salesvale.fulfillerID__c;
                    var fullist = fulvalue.split("-");
                    if(fullist.length > 0 ){
                        component.set("v.accountNumber", fullist[0]);
                        component.set("v.rdcId", fullist[1]);
                    }
                }
                if (invoicediff == 'Yes'){
                    component.set("v.errorMessage" , "Different Invoice number Items selected for EE:");
                    component.set("v.evenexchangeavilable", true);
        			component.set("v.openModal", false);
                    component.set("{!v.showSpinner}", false);
                }else if(response.getReturnValue().salesvale.phhSaleType__c == 'Invoiced' || litemverify == 'Yes'){
                    component.set("v.evenexchangeavilable", false);
        			component.set("v.openModal", true);
                }else{
                    component.set("v.errorMessage" , "Even Exchange is unavailable because the order is still open.");
                    component.set("v.evenexchangeavilable", true);
        			component.set("v.openModal", false);
                    component.set("{!v.showSpinner}", false);
                }
                if(response.getReturnValue().productlineItems.length == 0){
                    component.set("v.errorMessage" , "Even Exchange is unavailable because No Product Line Items.");
                    component.set("v.evenexchangeavilable", true);
        			component.set("v.openModal", false);
                    component.set("{!v.showSpinner}", false);
                }
                //Item_SKU__c
                component.set("v.calenderShow", true);
                var OrderexternalId = response.getReturnValue().salesvale.ExternalId;
                //var salesordrcoment = "EE Done on SO # " + response.getReturnValue().salesvale.phhSalesOrder__c +" | "+ scom +" | Case ID " + response.getReturnValue().caseAddress[0].CaseNumber;
                //component.set("v.salesOrdercomment", salesordrcoment);
                console.log('Product Line Items' + JSON.stringify(response.getReturnValue()));
                if(component.get("v.openModal")){
                	var action1 = component.get("c.getStarFlags");
                    console.log('my EXrecord id', action1);
                    //	component.find('v.Sales_Order_id').set('v.value', action);
                    action1.setParams({
                        "externalId": OrderexternalId
                    });
                    action1.setCallback(this, function(response) {
                        if (component.isValid() && response.getState() == "SUCCESS") { 
                            if( response.getReturnValue() == 'No records founds--'){
                                component.set("{!v.showSpinner}", false);
                            }else{
                                var obj = JSON.parse(response.getReturnValue());                        
                                var items = [];
                                /*if(obj.lenght > 0 ){
                                    component.set("v.starstaddvalue", true);
                                }*/
                                for (var i = 0; i < obj.length; i++) {
                                    if(obj[i].ItemID !== '*VIP' && obj[i].ItemID !== '*HCC') {
                                        items.push(obj[i]);  
                                    }else{
                                        if(obj[i].ItemID == '*VIP'){
                                            component.set("v.vipDescription", obj[i].ItemDescription);
                                        }else{
                                            component.set("v.hccDescription", obj[i].ItemDescription);
                                        }
                                    }
                                }
                                component.set("v.starFlags", items);
                                if(items.length > 0){
                                    component.set("v.starstaddvalue", true);
                                }
                                component.set("{!v.showSpinner}", false);
                            }                        
                            console.log('EE Status ' + JSON.stringify(response.getReturnValue()));
                        } else {
                            console.log("Failed with state: " + JSON.stringify(response.getError()));
                            component.set("{!v.showSpinner}", false);
                        }
                    });
                    $A.enqueueAction(action1);  
                }
            } else {
                component.set("{!v.showSpinner}", false);
                component.set("v.openModal", false);                
                console.log("Failed with state: " + JSON.stringify(response.getError()));                
            }
        });
        $A.enqueueAction(action);        
    },    
    updateDeliveryDate : function(component, event, helper) {
        var selectedDeliveryDateFromEvent = event.getParam("deliveryDate");
        component.set("v.selectedDateToDelivery", selectedDeliveryDateFromEvent);
        component.set("v.calenderValueShow", true);
        component.set("v.calenderShow", false);
    },
    checkDeliveryDate : function(component, event, helper) {
        var selectedDeliveryDateFromEvent = event.getParam("deliveryDate");
        component.set("v.selectedDateToDelivery", selectedDeliveryDateFromEvent);
        component.set("v.calenderValueShow", false);
        component.set("v.calenderShow", true);
    },
    closeCreate : function(component, event, helper) {
        component.set("v.evenexchangeavilable", false);
        component.set("v.openModal", false);
        component.set("v.shippingsection", false);
        component.set("v.schedulesection", false);
        component.set("v.finalSection", false);
        component.set("{!v.showSpinner}", true);
    },

    closeshipping : function(component, event, helper) {        
        component.set("v.shippingsection", false);
        component.set("v.openModal", true); 
    },
    shippingAddress : function(component, event, helper) {
        var selectedPRv = component.get("v.selectedPR");
        var checkvalue = component.find("checkstarPR"); 
        var checkvaluel = component.find("checkPR");
        var selectedlitemv = component.get("v.selectedLineItem");
        var scom = '';
        if(!$A.util.isEmpty(checkvaluel)){
            for (var i = 0; i < checkvaluel.length; i++) {
                if (checkvaluel[i].get("v.value") == true) {
                    //if(selectedPR.indexOf(checkvalue[i].get("v.text")) < 0){
                        selectedlitemv.push(checkvaluel[i].get("v.text"));
                    //}                                   
                }
            }
        }
        
        //if (selectedPR.length == 0){
        if(!$A.util.isEmpty(checkvalue)){
            for (var i = 0; i < checkvalue.length; i++) {
                if (checkvalue[i].get("v.value") == true) {
                    //if(selectedPR.indexOf(checkvalue[i].get("v.text")) < 0){
                        selectedPRv.push(checkvalue[i].get("v.text"));
                    //}                                   
                }
            }
        }        
        if(checkvaluel.length == undefined){
            component.set("v.selproductlineItems", component.get("v.productlineItems"));
        }
        component.set("v.selectedLineItem", selectedlitemv);
        component.set("v.selectedPR", selectedPRv);
        
        var selectedlitem = component.get("v.selectedLineItem");
        var selpro = component.get("v.productlineItems");
        var finalpli = component.get("v.selproductlineItems");
        
        for (var j =0; j < selectedlitem.length;j++){
            for(var z = 0; z < selpro.length; z++){
                if(selpro[z].Id == selectedlitem[j]){
                    finalpli.push(selpro[z]);
                }
            }
        }        
        
        if(finalpli.length > 0){
            //var obj = response.getReturnValue().productlineItems;
            for (var j = 0; j < finalpli.length; j++) {
                var str = finalpli[j].Item_SKU__c;
                if(!str.includes("*")){
                    scom += str + ',';
                }
            }
        }
        if(scom.length > 2){
            scom = scom.slice(0,-1);
        }
        var salesordrcoment = "EE Done on SO # " + component.get("v.salesOrder").phhSalesOrder__c +" | "+ scom +" | Case ID " + component.get("v.caseAddress")[0].CaseNumber;
        component.set("v.salesOrdercomment", salesordrcoment);        
        //}
        component.set("{!v.showSpinner}", true);
        var OrderexternalId = component.get("v.salesOrder").ExternalId;
        var action1 = component.get("c.getDiscontinuedItem");
        console.log('my EXrecord id', action1);
        //  component.find('v.Sales_Order_id').set('v.value', action);
        action1.setParams({
            "Items" : scom,
            "externalId": OrderexternalId
        });
        action1.setCallback(this, function(response) {
            if (component.isValid() && response.getState() == "SUCCESS") { 
                if( response.getReturnValue() == 'No records founds--'){
                    component.set("{!v.showSpinner}", false);
                    component.set("v.openModal", false);        
        			component.set("v.shippingsection", true);
                }else{
                    var obj = JSON.parse(response.getReturnValue()); 
                    
                    if(obj.length > 0 ){
                        //component.set("v.evenexchangedisconnect", true);
                        var megv = '';
                        var itv = '';
                        if (obj.length == 1){
                            megv = "Product " + obj[0] + " is discontinued and may not eligible for Event Exchange if we don't have standing inventory please follow the SOP for when this scenario happens or ask your supervisor. Click Ok to continue with the Even Exchange or Cancel to change your selection.";
                        }else{
                            for (var j = 0; j < obj.length; j++) {
                                var str = obj[j];                                
                                itv += str + ',';
                            }
                            if(itv.length > 2){
                                itv = itv.slice(0, -1);
                            }
                            megv = "Product(s) " + itv +" are discontinued and may not eligible for Event Exchange if we don't have standing inventory please follow the SOP for when this scenario happens or ask your supervisor. Click Ok to continue with the Even Exchange or Cancel to change your selection.";
                        }
                        
                        $A.createComponent("c:OverlayLibraryModal", {errorMessage : megv},
                                function(content, status) {
                                   if (status === "SUCCESS") {
                                       var modalBody = content;     
                                       //modalBody = "Product(s) <SKU(s)> is(are) discontinued and may not eligible for Event Exchange if we don't have standing inventory please follow the SOP for when this scenario happens or ask your supervisor. Click Ok to continue with the Even Exchange or Cancel to change your selection.";
                                       component.find('overlayLib').showCustomModal({
                                           header: "Even Exchnage",
                                           body: modalBody, 
                                           showCloseButton: false,
                                           closeCallback: function(ovl) {
                                               console.log('Overlay is closing');
                                           }
                                       }).then(function(overlay){
                                           console.log("Overlay is made");
                                       });
                                   }
                                });
                    }                    
                    component.set("{!v.showSpinner}", false);
                    component.set("v.openModal", false);
        			component.set("v.shippingsection", true);
                }                        
                console.log('EE Status ' + JSON.stringify(response.getReturnValue()));
            } else {
                console.log("Failed with state: " + JSON.stringify(response.getError()));
                component.set("{!v.showSpinner}", false);
                component.set("v.openModal", false);        
        		component.set("v.shippingsection", true);
            }
        });
        $A.enqueueAction(action1); 
    },
    scheduleEx : function(component, event, helper) {
        component.set("v.openModal", false);        
        component.set("v.shippingsection", false);
        component.set("v.schedulesection", true);
    },
    closeschedule :function(component, event, helper) {
        component.set("v.openModal", false);        
        component.set("v.shippingsection", true);
        component.set("v.schedulesection", false);
    },
    complete :function(component, event, helper) {
        component.set("v.openModal", false);        
        component.set("v.shippingsection", false);
        component.set("v.schedulesection", false);
        component.set("v.finalSection", true);
        component.set("{!v.showSpinner}", true);
        helper.sendValues(component, event, helper);
    },
    
    backtoSchedule : function(component, event, helper) {
        component.set("v.openModal", false);        
        component.set("v.shippingsection", false);
        component.set("v.schedulesection", true);
        component.set("v.finalSection", false);
    },
    handleApplicationEvent : function(cmp, event) {
        var message = event.getParam("message");
        //alert('@@@ ==> ' + message);
        if(message == 'Ok')
        {
        // if the user clicked the OK button do your further Action here
        }
       else if(message == 'Cancel')
      {
          var defv = [];
          cmp.set("v.selproductlineItems", defv);
          cmp.set("v.shippingsection", false);
          cmp.set("v.openModal", true);
        // if the user clicked the Cancel button do your further Action here for canceling
      }
    }
})