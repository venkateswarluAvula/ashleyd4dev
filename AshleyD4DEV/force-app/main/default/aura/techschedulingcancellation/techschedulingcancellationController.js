({
	doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var actionCaseObj = component.get("c.getRelatedServiceRequestsforcancelling");
        actionCaseObj.setParams({
            "recordId" : recordId
        });
    	actionCaseObj.setCallback(this, function(response) {
    		component.set("v.isUnSchTech", false);
        	if (component.isValid() && response.getState() === "SUCCESS") {
        		console.log('response: ' + JSON.stringify(response.getReturnValue()));
				component.set('v.SiblingsExist',true);
				var res= response.getReturnValue();
				if(res != null) {
	                var reqNoArray = res.split(':');
	                var reqNo = reqNoArray[1];
					var unSch;

					if(res.includes("Closed")) {
						component.set("v.apiCallout", false);
						unSch = 'Unable to unscheduled a Technician for Service Request: ' + reqNo + ' because the Service Request is Closed';
					} else if(res.includes("UnScheduled")) {
						component.set("v.apiCallout", false);
						unSch = 'There is no scheduled technician so you cannot unschedule Service Request: ' + reqNo;
					} else if(res.includes("Allow")) {
						component.set("v.apiCallout", true);
						component.set("v.isUnSchTech", true);
						unSch = 'Are you sure you want to unschedule the technician for Service Request: ' + reqNo + '?';
					} else {
						// confirm
						component.set("v.apiCallout", false);
						component.set("v.isUnSchTech", true);
						unSch = 'Because this technician is scheduled in the past we are going to unschedule this Technician for reporting purposes in Salesforce but HOMES will remain unchanged. Please note this means if anything is updated in HOMES all tech schedule details will be updated to Salesforce again from Service Request: ' + reqNo;
					}

					component.set("v.casesAre", unSch);
				} else {
					component.set("v.casesAre",'There is no scheduled technician so you cannot unschedule this case.');
				}
        	}
        	else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                errorToast.fire();
        	}
    	});
        $A.enqueueAction(actionCaseObj);
    },

	GobackToRecord : function (component, event, helper) {
		component.set('v.SiblingsExist',false);
        $A.get('e.force:refreshView').fire();
    },

    unschedule: function (component, event, helper){
		component.set("v.isUnSchTech", false);
		var recordId = component.get("v.recordId");
		var callout = component.get("v.apiCallout");
		console.log('recordId: ' + recordId + ' :callout: ' + callout);
        var actionTechSch = component.get("c.getApiResponse");
		actionTechSch.setParams({
			"recordId" : recordId,
			"apiCall" : callout,
			"adrId": null
		});
		actionTechSch.setCallback(this, function(response) {
			console.log('here1');  
            if (component.isValid() && response.getState() === "SUCCESS") {
            	console.log('response: ' + JSON.stringify(response.getReturnValue()));
				var res= response.getReturnValue();
            
				var toastEvent = $A.get("e.force:showToast");
				if(res.includes("Successfully")) {
					toastEvent.setParams({"title": "Success!", "message": res, "type":"success"});
				} else {
					toastEvent.setParams({"title": "Error!", "message": res, "mode": "sticky", "type":"error"});
				}
				toastEvent.fire();

				component.set('v.SiblingsExist',false);
				$A.get('e.force:refreshView').fire();
			}
        });
        $A.enqueueAction(actionTechSch);
    }
})