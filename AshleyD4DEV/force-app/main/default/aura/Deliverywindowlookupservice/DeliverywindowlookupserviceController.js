({
    init : function(component, event, helper) {
       
        var deliveryMode = component.get('v.deliveryMode');
        var today = new Date();
        var dayDigit = today.getDate();
        console.log('dayDigit: ' + dayDigit);
        var dateToday = today.toISOString().slice(0, 10);
        console.log('dateToday: ' + dateToday);
        component.set("v.todayDate",dateToday);
        console.log('deliveryMode: ' + deliveryMode);
        console.log('l timezone: ' + $A.get("$Locale.timezone"));
        console.log('l timezone1: ' + moment().tz($A.get("$Locale.timezone")));
        console.log('l timezone2: ' + moment().tz($A.get("$Locale.timezone")).startOf('day'));
        var dateminusone = dayDigit - 1;
        var dwProcessingStartDate = ((['DS','TW'].indexOf(deliveryMode) > -1) ? moment().tz($A.get("$Locale.timezone")).startOf('day') : moment().tz($A.get("$Locale.timezone")).startOf('day').clone().add(-dateminusone, 'days'));
        var ProcStart = dwProcessingStartDate.toISOString().slice(0, 10);
        var lineItemDeliveryDate = !$A.util.isEmpty(component.get("v.lineItemDeliveryDate")) ? moment(component.get("v.lineItemDeliveryDate")).startOf('day') : null;
        var lineItem = (lineItemDeliveryDate).toISOString().slice(0, 10);
        component.set("v.dwProcessingStartDate", dwProcessingStartDate);
        component.set("v.isLoading", true);
        var isValidLineItemDeliveryDate = false;  
        if (!$A.util.isEmpty(lineItemDeliveryDate) && (lineItemDeliveryDate.month() != dwProcessingStartDate.month())){
            isValidLineItemDeliveryDate = true;
           
            helper.getDeliveryWindow(component, lineItemDeliveryDate.clone().startOf('month').format('YYYY-MM-DD'), function(calendar){
                var mostDistantDateAvailForDelivery = helper.getMostDistantDateAvailForDelivery(calendar.months[0]);
                var mostD = (mostDistantDateAvailForDelivery).toISOString().slice(0, 10);
                             
                component.set("v.dwCurrentMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
                component.set("v.selectedMonth", calendar.months[0]);  
                component.set("v.deliveryCalendar", calendar);	    		
                component.set("v.isLoading", false);	    			
            },
                                     function(message){
                                         component.set("v.isLoading", false);
                                     });	    		
        }
         if (!$A.util.isEmpty(lineItemDeliveryDate) && (lineItemDeliveryDate.month() == dwProcessingStartDate.month())){
            isValidLineItemDeliveryDate = true;
           
            helper.getDeliveryWindow(component, lineItemDeliveryDate.clone().startOf('month').format('YYYY-MM-DD'), function(calendar){
                var mostDistantDateAvailForDelivery = helper.getMostDistantDateAvailForDelivery(calendar.months[0]);
                var mostD = (mostDistantDateAvailForDelivery).toISOString().slice(0, 10);
                          
                component.set("v.dwCurrentMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
                component.set("v.selectedMonth", calendar.months[0]);  
                component.set("v.deliveryCalendar", calendar);	    		
                component.set("v.isLoading", false);	    			
            },
                                     function(message){
                                         component.set("v.isLoading", false);
                                     });	    		
        }
        helper.getDeliveryWindow(component, dwProcessingStartDate.format('YYYY-MM-DD'), function(calendar){
            var mostDistantDateAvailForDelivery = helper.getMostDistantDateAvailForDelivery(calendar.months[0]);
            var mostD = (mostDistantDateAvailForDelivery).toISOString().slice(0, 10);            
           
            component.set("v.dwEarliestMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
            if (!isValidLineItemDeliveryDate){
                
                component.set("v.dwCurrentMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
                component.set("v.selectedMonth", calendar.months[0]);  
                component.set("v.deliveryCalendar", calendar);
                component.set("v.isLoading", false);
            }	    		
        }, function(message){
            component.set("v.isLoading", false);
        });
    },
    
    nextMonth : function(component, event, helper) {
        helper.changeSelectedMonth(component, 1);	    
    },
    
    
      getThresholdParam : function(component, event, helper) {
        var thresholdVal = event.getParam("notifyThreshold") ;
          var raj ='0';
          component.set("v.Dummy",raj);
          //alert('thresholdVal--->'+thresholdVal);
    },
    currentMonth : function(component, event, helper) {
        
        var deliveryMode = component.get('v.deliveryMode');
        var today = new Date();
        //alert('today'+today);
        var dayDigit = today.getDate();
        var dwProcessingStartDate = ((['DS','TW'].indexOf(deliveryMode) > -1) ? moment().tz($A.get("$Locale.timezone")).startOf('day') : moment().tz($A.get("$Locale.timezone")).startOf('day').clone().add(-dayDigit+1, 'days'));
        //alert('dwProcessingStartDate-->'+dwProcessingStartDate);
        var lineItemDeliveryDate = !$A.util.isEmpty(component.get("v.lineItemDeliveryDate")) ? moment(component.get("v.lineItemDeliveryDate")).startOf('day') : null;
        //alert('lineItemDeliveryDate-->'+lineItemDeliveryDate.month());
        component.set("v.dwProcessingStartDate", dwProcessingStartDate);
        //alert('dwProcessingStartDate1-->'+dwProcessingStartDate.month());
        component.set("v.isLoading", true);
        var isValidLineItemDeliveryDate = false;  
        
        helper.getDeliveryWindow(component, dwProcessingStartDate.format('YYYY-MM-DD'), function(calendar){
            var mostDistantDateAvailForDelivery = helper.getMostDistantDateAvailForDelivery(calendar.months[0]);
            component.set("v.dwEarliestMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
            if (!isValidLineItemDeliveryDate){
                component.set("v.dwCurrentMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
                component.set("v.selectedMonth", calendar.months[0]);  
                component.set("v.deliveryCalendar", calendar);
                component.set("v.isLoading", false);
            }	    		
        }, function(message){
            component.set("v.isLoading", false);
        });
        
        
    },
    
    futureMonth : function(component, event, helper) {
        // alert('enterd future');
        var deliveryMode = component.get('v.deliveryMode');
        var dwProcessingStartDate = ((['DS','TW'].indexOf(deliveryMode) > -1) ? moment().tz($A.get("$Locale.timezone")).startOf('day') : moment().tz($A.get("$Locale.timezone")).startOf('day').clone().add(1, 'days'));
        //alert('dwProcessingStartDate-->'+dwProcessingStartDate);
        var lineItemDeliveryDate = !$A.util.isEmpty(component.get("v.lineItemDeliveryDate")) ? moment(component.get("v.lineItemDeliveryDate")).startOf('day') : null;
        //alert('lineItemDeliveryDate-->'+lineItemDeliveryDate);
        component.set("v.dwProcessingStartDate", dwProcessingStartDate);
        //alert('enterd future1');
        component.set("v.isLoading", true);
        var isValidLineItemDeliveryDate = false;  
        
        if (!$A.util.isEmpty(lineItemDeliveryDate) && (lineItemDeliveryDate.month() != dwProcessingStartDate.month())){
            isValidLineItemDeliveryDate = true;
            
            helper.changeFutureMonth(component, lineItemDeliveryDate.clone().startOf('month').format('YYYY-MM-DD'), function(calendar){
                
                var mostDistantDateAvailForDelivery = helper.getMostDistantDateAvailForDelivery(calendar.months[0]);
                component.set("v.dwCurrentMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
                component.set("v.selectedMonth", calendar.months[0]);  
                component.set("v.deliveryCalendar", calendar);	    		
                component.set("v.isLoading", false);	    			
            },
                                     function(message){
                                         component.set("v.isLoading", false);
                                     });	    		
        }
        
        helper.changeFutureMonth(component, dwProcessingStartDate.format('YYYY-MM-DD'), function(calendar){
            
            var mostDistantDateAvailForDelivery = helper.getMostDistantDateAvailForDelivery(calendar.months[0]);
            component.set("v.dwEarliestMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
            if (!isValidLineItemDeliveryDate){
                
                component.set("v.dwCurrentMonthStartDate", mostDistantDateAvailForDelivery.clone().startOf('month').format('YYYY-MM-DD'));
                component.set("v.selectedMonth", calendar.months[0]);  
                component.set("v.deliveryCalendar", calendar);
                component.set("v.isLoading", false);
            }	    		
        }, 
                                 function(message){
                                     component.set("v.isLoading", false);
                                 });
    },
    
    previousMonth : function(component, event, helper) {
        helper.changeSelectedMonth(component, -1);	    
    },
    
    defaultCloseAction : function(component, event, helper) {  
        component.destroy();
    },
    
    displaySection1 : function(component, event, helper) {
        var tommDate = new Date(Date.now() + 24*60*60*1000).toISOString().slice(0, 10);
        //alert('tommDate'+tommDate);
        var sLevel = component.get("v.serLevel");
        var dSLevel;
        var selectedDateString= event.currentTarget.dataset.date;
        component.set("v.displayedSe",selectedDateString);
        var capOverride = component.get("v.calendarData");
        var nddOverride = component.get("v.calendarData2");
        var tempVal = component.get("v.selectedMonth.weeks");
        var IsNddSetupEnabled;
        
        for(var i=0; i<tempVal.length; i++) {
            for(var j=0;j<tempVal[i].length;j++){
                var test = ((tempVal[i])[j].d);               
                if(test==selectedDateString){
                    IsNddSetupEnabled = JSON.stringify((tempVal[i])[j].IsNddSetupEnabled);
                    dSLevel = JSON.stringify((tempVal[i])[j].ServiceLevel);
                    //alert(IsNddSetupEnabled);
                }
            }
        }
        //alert('capOverride'+capOverride);
        
        if ((sLevel == null) || (sLevel == undefined) || (sLevel === 'PDI') || ((sLevel === 'THD') && (dSLevel === '"Threshold Delivery"'))) { 
        //alert('1');
            if (selectedDateString==tommDate) {
            if (IsNddSetupEnabled==='true'){
                if ((capOverride==='true') && (nddOverride==='true')){
                    component.set("v.displayedSection1","true");
                }
                else{
                    component.set("v.displayedSection1","false");
                    document.getElementById('error').innerHTML="Only Managers with override authority can schedule this date";
                    return;
                }
            }
            else{
                if ((capOverride==='true')){
                    component.set("v.displayedSection1","true");
                }
                else{
                    component.set("v.displayedSection1","false");
                    document.getElementById('error').innerHTML="Only Managers with override authority can schedule this date";
                    return;
                }
                
            }
        }
        else{
            if ((capOverride==='true')){
                component.set("v.displayedSection1","true");
            }
            else{
                component.set("v.displayedSection1","false");
                document.getElementById('error').innerHTML="Only Managers with override authority can schedule this date";
                return;
            }
        }
        }
        
        else {
            if ((sLevel==='THD') && (dSLevel==='"Premium Delivery"')){
            document.getElementById('error').innerHTML="Please select a Threshold delivery date or change the service level to Premium delivery";
            return;
            }
          /*  if ((sLevel==='PDI') && (dSLevel==='"Threshold Delivery"')){
            document.getElementById('error').innerHTML="Please select a Premium delivery date or change the service level to Threshold delivery";
            return;
            }*/
        }
    },
    
    confirmUserProfile : function(component, event, helper) {        
        var selectedDateString=component.get("v.displayedSe");
        var currentOrderId = component.get("v.orderId");
        //alert('currentOrderId'+currentOrderId);
        var currentOrderLineId = component.get("v.orderLineId");
        var deliveryDateSelectedEvent = $A.get("e.c:DeliveryDateSelected");   
        deliveryDateSelectedEvent.setParams({
            "deliveryDate": selectedDateString, 
            "orderID": currentOrderId,
            "orderLineId": currentOrderLineId 
        }).fire();
        component.destroy();
        component.set("v.displayedSection1","false");
    },
    
    returnDeliveryDate : function(component, event, helper) {
        var sLevel = component.get("v.serLevel");
        //alert('sLevel returnDeliveryDate'+sLevel);
        
		var tommDate = new Date(Date.now() + 24*60*60*1000).toISOString().slice(0, 10);
        var tempVal = component.get("v.selectedMonth.weeks");
        var selectedDateString= event.currentTarget.dataset.date;
        //alert('selectedDateString'+selectedDateString);
        var dSLevel;
        var capOverride = component.get("v.calendarData");
        console.log('tempVal----'+tempVal);
        var IsNddSetupEnabled;
        
        for(var i=0; i<tempVal.length; i++) {
            for(var j=0;j<tempVal[i].length;j++){
                var test = ((tempVal[i])[j].d); 
                //alert('test'+test);
                
                if(test==selectedDateString){
                    IsNddSetupEnabled = JSON.stringify((tempVal[i])[j].IsNddSetupEnabled);
                    dSLevel = JSON.stringify((tempVal[i])[j].ServiceLevel);
                	//alert('in for loop dsLevel '+ dSLevel);
                    //alert(IsNddSetupEnabled);
                }
            }
        }
        //alert('out  loop dsLevel :'+ dSLevel);
        //alert(('123')+IsNddSetupEnabled);
        
        if ((sLevel == null) || (sLevel == undefined) || (sLevel === 'PDI') || ((sLevel === 'THD') && (dSLevel === '"Threshold Delivery"'))) { 
        	//alert('2');
            if (IsNddSetupEnabled==='true'){
            //component.get("v.nddSetup", response.getReturnValue().IsNddSetupEnabled);
            	if (selectedDateString==tommDate){
                component.set("v.displayedSe2",selectedDateString);
                var nddOverride = component.get("v.calendarData2");
                //alert('nddOverride'+nddOverride);
                	if(nddOverride==='true'){
                    component.set("v.displayedSection2","true");
                }
                else{
                    component.set("v.displayedSection2","false");
                    document.getElementById('error').innerHTML="Only users with override authority can schedule next day delivery";
                    return;
                }
            }
            else {
                component.set("v.displayedSe2",selectedDateString);
                component.set("v.selectedDateToDelivery", selectedDateString);
                document.getElementById('error').innerHTML="Delivery Date " + selectedDateString;
                var currentOrderId = component.get("v.orderId");
                var currentOrderLineId = component.get("v.orderLineId");
                var deliveryDateSelectedEvent = $A.get("e.c:DeliveryDateSelected");
                deliveryDateSelectedEvent.setParams({
                    "deliveryDate": selectedDateString, 
                    "orderID": currentOrderId,
                    "orderLineId": currentOrderLineId 
                }).fire();
                component.destroy();
            }
        }
        
        else {
            var currentOrderId = component.get("v.orderId");
            var currentOrderLineId = component.get("v.orderLineId");
            var deliveryDateSelectedEvent = $A.get("e.c:DeliveryDateSelected");
            deliveryDateSelectedEvent.setParams({
                "deliveryDate": selectedDateString, 
                "orderID": currentOrderId,
                "orderLineId": currentOrderLineId 
            }).fire();
            component.destroy();
        }
        }
        
        
        else {
            if ((sLevel==='THD') && (dSLevel==='"Premium Delivery"')){
            document.getElementById('error').innerHTML="Please select a Threshold delivery date or change the service level to Premium delivery";
            return;
            }
          /*  if ((sLevel==='PDI') && (dSLevel==='"Threshold Delivery"')){
            document.getElementById('error').innerHTML="Please select a Premium delivery date or change the service level to Threshold delivery";
            return;
            }*/
        }
    },
    
    confirmUserProfile2 : function(component, event, helper) {        
        var selectedDateString=component.get("v.displayedSe2");
        var currentOrderId = component.get("v.orderId");
        var currentOrderLineId = component.get("v.orderLineId");
        var deliveryDateSelectedEvent = $A.get("e.c:DeliveryDateSelected");   
        deliveryDateSelectedEvent.setParams({
            "deliveryDate": selectedDateString, 
            "orderID": currentOrderId,
            "orderLineId": currentOrderLineId 
        }).fire();
        component.destroy();
        component.set("v.displayedSection2","false");
    },
    
    blackColor : function(component, event, helper){
        //alert('entered black');
        var today = new Date().toISOString().slice(0, 10);
        var selectedDateString= event.currentTarget.dataset.date;
        var tommDate = new Date(Date.now() + 24*60*60*1000).toISOString().slice(0, 10);
        //alert('tommDate'+tommDate);
        var sLevel = component.get("v.serLevel");
        var dSLevel;
        component.set("v.displayedSe3",selectedDateString);
        var closedPerm = component.get("v.calendarData3");
        //alert('closedPerm'+closedPerm);
        var nddOverride = component.get("v.calendarData2");
        var tempVal = component.get("v.selectedMonth.weeks");
        var IsNddSetupEnabled;
        
        for(var i=0; i<tempVal.length; i++) {
            for(var j=0;j<tempVal[i].length;j++){
                var test = ((tempVal[i])[j].d);               
                if(test==selectedDateString){
                    IsNddSetupEnabled = JSON.stringify((tempVal[i])[j].IsNddSetupEnabled);
                    dSLevel = JSON.stringify((tempVal[i])[j].ServiceLevel);
                    //alert(IsNddSetupEnabled);
                }
            }
        }
         
        if ((sLevel == null) || (sLevel == undefined) || (sLevel === 'PDI') || ((sLevel === 'THD') && (dSLevel === '"Threshold Delivery"'))) { 
        //alert('1');
            if (selectedDateString==tommDate) {
            if (IsNddSetupEnabled==='true'){
                if ((closedPerm==='true') && (nddOverride==='true')){
                    component.set("v.displayedSection3","true");
                }
                else{
                    component.set("v.displayedSection3","false");
                    document.getElementById('error').innerHTML="Only users with override authority can schedule this date.";
                    return;
                }
            }
            else{
                if ((closedPerm==='true')){
                    component.set("v.displayedSection3","true");
                }
                else{
                    component.set("v.displayedSection3","false");
                    document.getElementById('error').innerHTML="Only users with override authority can schedule this date";
                    return;
                }
                
            }
        }
        else{
            if (selectedDateString >= today){
            if ((closedPerm==='true')){
                component.set("v.displayedSection3","true");
            }
            else{
                component.set("v.displayedSection3","false");
                document.getElementById('error').innerHTML="Only users with override authority can schedule this date";
                return;
            }
            }
            else{
                    document.getElementById('error').innerHTML="Please select a future date";
                    return;
                }
        }
        }
        
        else {
            if ((sLevel==='THD') && (dSLevel==='"Premium Delivery"')){
            document.getElementById('error').innerHTML="Please select a Threshold delivery date or change the service level to Premium delivery";
            return;
            }
          /*  if ((sLevel==='PDI') && (dSLevel==='"Threshold Delivery"')){
            document.getElementById('error').innerHTML="Please select a Premium delivery date or change the service level to Threshold delivery";
            return;
            }*/
        }
        },
    
    grayColor : function(component, event, helper){
        //alert('entered gray');
        var today = new Date().toISOString().slice(0, 10);
        var selectedDateString= event.currentTarget.dataset.date;
        var tommDate = new Date(Date.now() + 24*60*60*1000).toISOString().slice(0, 10);
        //alert('today'+today);
        var sLevel = component.get("v.serLevel");
        var dSLevel;
        component.set("v.displayedSe4",selectedDateString);
        var grayPerm = component.get("v.calendarData4");
        //alert('closedPerm'+closedPerm);
        var nddOverride = component.get("v.calendarData2");
        var tempVal = component.get("v.selectedMonth.weeks");
        var IsNddSetupEnabled;
        
        for(var i=0; i<tempVal.length; i++) {
            for(var j=0;j<tempVal[i].length;j++){
                var test = ((tempVal[i])[j].d);               
                if(test==selectedDateString){
                    IsNddSetupEnabled = JSON.stringify((tempVal[i])[j].IsNddSetupEnabled);
                    dSLevel = JSON.stringify((tempVal[i])[j].ServiceLevel);
                    //alert(IsNddSetupEnabled);
                }
            }
        }
         
        if ((sLevel == null) || (sLevel == undefined) || (sLevel === 'PDI') || ((sLevel === 'THD') && (dSLevel === '"Threshold Delivery"'))) { 
        //alert('1');
            if (selectedDateString==tommDate) {
            if (IsNddSetupEnabled==='true'){
                if ((grayPerm==='true') && (nddOverride==='true')){
                    component.set("v.displayedSection4","true");
                }
                else{
                    component.set("v.displayedSection4","false");
                    document.getElementById('error').innerHTML="Only users with override authority can schedule this date.";
                    return;
                }
            }
            else{
                if ((grayPerm==='true')){
                    component.set("v.displayedSection4","true");
                }
                else{
                    component.set("v.displayedSection4","false");
                    document.getElementById('error').innerHTML="Only users with override authority can schedule this date";
                    return;
                }
                
            }
        }
        else{
            if (selectedDateString >= today){
            if ((grayPerm==='true')){
                component.set("v.displayedSection4","true");
            }
            else{
                component.set("v.displayedSection4","false");
                document.getElementById('error').innerHTML="Only users with override authority can schedule this date";
                return;
            }
            }
            else{
                    document.getElementById('error').innerHTML="Please select a future date";
                    return;
                }
        }
        }
        
        else {
            if ((sLevel==='THD') && (dSLevel==='"Premium Delivery"')){
            document.getElementById('error').innerHTML="Please select a Threshold delivery date or change the service level to Premium delivery";
            return;
            }
          /*  if ((sLevel==='PDI') && (dSLevel==='"Threshold Delivery"')){
            document.getElementById('error').innerHTML="Please select a Premium delivery date or change the service level to Threshold delivery";
            return;
            }*/
        }
        },
    
    confirmUserProfile3 : function(component, event, helper) {        
        var selectedDateString=component.get("v.displayedSe3");
        var currentOrderId = component.get("v.orderId");
        var currentOrderLineId = component.get("v.orderLineId");
        var deliveryDateSelectedEvent = $A.get("e.c:DeliveryDateSelected");   
        deliveryDateSelectedEvent.setParams({
            "deliveryDate": selectedDateString, 
            "orderID": currentOrderId,
            "orderLineId": currentOrderLineId 
        }).fire();
        component.destroy();
        component.set("v.displayedSection3","false");
    },
    
    confirmUserProfile4 : function(component, event, helper) {        
        var selectedDateString=component.get("v.displayedSe4");
        var currentOrderId = component.get("v.orderId");
        var currentOrderLineId = component.get("v.orderLineId");
        var deliveryDateSelectedEvent = $A.get("e.c:DeliveryDateSelected");   
        deliveryDateSelectedEvent.setParams({
            "deliveryDate": selectedDateString, 
            "orderID": currentOrderId,
            "orderLineId": currentOrderLineId 
        }).fire();
        component.destroy();
        component.set("v.displayedSection4","false");
    },
    
    openPop : function(component, event, helper) {
        var selectedDateStrings= event.currentTarget.dataset.date;
        //alert('selectedDateStrings--'+selectedDateStrings);
        component.set("v.popupDate",selectedDateStrings);
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        // var selectedItem = event.currentTarget;
        // var Id = selectedItem.dataset.record;
    },
    
    pastDate : function(component, event, helper) {
        //alert('entered past');
        document.getElementById('error').innerHTML="Please select a future date";
        return;
    },
    
    closedDate : function(component, event, helper) {
        document.getElementById('error').innerHTML="The selected date is unavailable for delivery. Please select a date which is available for delivery";
        return;
    },
    
    closeSec : function(component, event, helper) {
        component.set("v.displayedSection1",false);
    },
    
    closeSec1 : function(component, event, helper) {
        component.set("v.displayedSection2",false);
    },  
    
    closeSec2 : function(component, event, helper) {
        component.set("v.displayedSection3",false);
    },
    
    closeSec3 : function(component, event, helper) {
        component.set("v.displayedSection4",false);
    }
})