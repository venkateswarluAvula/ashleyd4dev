({

    openProducts:function(component, event, helper)  
    {
        var appEvent = $A.get("e.c:NavigateToWrapperComponentAppEvent");
        //appEvent.setStorable();
        appEvent.setParams({
            "targetCmpName" : 'ProductCategoryWrapperCmp',
            "targetCmpParameters" : {}
        });
		appEvent.fire();
        window.location.reload(true);
       
  },
    addCustomer:function(component, event, helper) 
    {
        
        component.set("v.showGuestModal", true);
        var overlay = component.find('overlay');
        $A.util.addClass(overlay, 'slds-backdrop--open');
    },
    closeGuestModal: function(component, event, helper) {
        
        component.set("v.showGuestModal", false);
        var overlay = component.find('overlay');
        $A.util.removeClass(overlay, 'slds-backdrop--open');
    },
    
    openMyCustomers:function(component, event, helper) 
    {				alert('alert1');
        
                    var appEvent = $A.get("e.c:NavigateToWrapperComponentAppEvent");
                    appEvent.setParams({
                        "targetCmpName" : 'MyCustomersWrapperCmp',
                        "targetCmpParameters" : {}
                    });
                    appEvent.fire();
                   
       
        /* SIGNATURE MODAL OUT FOR NOW 1/11/2018 */
        /*var evt  = $A.get("e.force:navigateToURL");
                                    evt.setParams({
                                        "url": '/one/one.app#/n/My_Customers1' 
                                    })
                                    evt.fire();*/
    
    },
    closeSignatureModal: function(component, event, helper){
        var eType = event.getParam("eventType");
        var rtnId = event.getParam("recordId");
        
        // hide signature
		helper.hideSigModal(component);
        
        if(eType != 'CLOSE'){
            var appEvent = $A.get("e.c:NavigateToWrapperComponentAppEvent");
            appEvent.setParams({
                "targetCmpName" : 'MyCustomersWrapperCmp',
                "targetCmpParameters" : {}
            });
            appEvent.fire();
            $A.get('e.force:refreshView').fire()
        }
        
        
    },
    closeModal:function(component,event,helper){    
		helper.hideSigModal(component);
       /*  REQ-299 Enhancement, For Find A Guest, RSA must collect a signature.
        var evt  = $A.get("e.force:navigateToURL");
        evt.setParams({
            "url": '/one/one.app#/n/My_Customers1' 
        })
        evt.fire();
        */
    },
    
    //Manually add Product Start (US-291480)
    showManualModal : function(component, event, helper) {
       // alert('after button click');
        component.set("v.isManualPricing", true);
        
        var overlay = component.find('overlay');
        $A.util.addClass(overlay, 'slds-backdrop--open');
       
    }, 
    
    hideManualModal : function(component, event, helper) {
        component.set("v.isManualPricing", false);
        //alert('va is'+component.get('c.MultiLineType'));
        var overlay = component.find('overlay');
        $A.util.removeClass(overlay, 'slds-backdrop--open');
        component.set("v.manualShoppingCartLineItem.Product_SKU__c","");
        component.set("v.manualShoppingCartLineItem.List_Price__c","");
        component.set("v.manualShoppingCartLineItem.Quantity__c","");
        
     component.set("v.productSKU", "");
     component.set("v.Quantity", "");
    }, 
    
	  
    addProductToGuest : function(component, event, helper) {
        helper.checkDisplayError(component, helper);
        
    },
    
    closeManuallyAddModal :function(component, event, helper) {
        
        component.set("v.isShowCustmerModal", false);
        var overlay = component.find('overlay');
        $A.util.removeClass(overlay, 'slds-backdrop--open');
    },
    //Manually add Product End ( US-291480)
})