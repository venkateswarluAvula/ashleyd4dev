({
        hideSigModal : function(component) {
            var cmpBack = component.find('Modalbackdrop');
            $A.util.removeClass(cmpBack,'slds-backdrop--open');
            component.set("v.showSignature", false);		
        },
        
        getProductDetails : function(component,helper) {
            // to avoid multiple spinners issue
            //component.set("v.spinner", true);
            var c = component.get("v.manualShoppingCartLineItem"); 
        
            //alert('manualShoppingCartLineItem-->'+JSON.stringify(c));
            var action = component.get("c.getProductDetailWithSKU");
            action.setParams({"lineItem" : component.get('v.manualShoppingCartLineItem') });
            
            var toastErrorHandler = component.find('toastErrorHandler');
            //alert('test');
           action.setCallback(this, function(response){ 
               //alert('test 1');
            //toastErrorHandler.handleResponse(
//response,
                //function(response){
                    //alert('test 2');
                    //alert('return value is -->'+JSON.stringify(response.getReturnValue()));
                    //console.log('return value is -->'+JSON.stringify(response.getReturnValue));
                    var rtnValue = response.getReturnValue();
                    if (rtnValue.status == 'Success') { 
                        //alert('test 3');
                        component.set("v.isShowCustmerModal", true);
                        component.set("v.isManualPricing", false);
                    }
                    else if(rtnValue.status == 'Error' && rtnValue.msg == 'ItemDetailsAPIError'){
                        //alert('test 4');
                        helper.showToast("error", 'Error while creating/updating LineItem', component,
                                         'This Product could not be added as it is not found in Source System. Please contact administrator.');
                    }
                    else {
                         helper.showToast("error", 'Error while creating/updating LineItem', component,
                                          'Error while creating/updating LineItem.');                        
                    }
                //})
             });
            $A.enqueueAction(action);
        },
        
       checkDisplayError : function(component, helper){ 
            //alert('checkDisplayError-->'+component.get("v.manualShoppingCartLineItem.Quantity__c"));
            //alert('Quantity----'+component.get("v.Quantity"));
            //alert('ProductSKU----'+component.get("v.manualShoppingCartLineItem.Product_SKU__c"));
            component.set("v.isGetProduct",false);
            var Quantity__c=component.get("v.manualShoppingCartLineItem.Quantity__c");
            if($A.util.isEmpty(component.get("v.manualShoppingCartLineItem.Product_SKU__c")) ||
               $A.util.isEmpty(component.get("v.manualShoppingCartLineItem.Quantity__c"))|| 
               isNaN(Quantity__c)){ 
                	component.set("v.isGetProduct",true);
                    helper.showToast("error", 'Please enter valid values for all available fields', component,
                                             'Invalid data'); 
            }
                
           		var prdutSK = component.get("v.manualShoppingCartLineItem.Product_SKU__c");
                var prdtSKU = prdutSK.replace(/[^a-zA-Z0-9-* ]/g, "");
                
                var qunty = component.get("v.manualShoppingCartLineItem.Quantity__c");
                var quntyPrd = qunty.replace(/[^a-zA-Z0-9]/g, "");
                
                
                if(prdutSK !==null){ 
                    if(prdtSKU.length == prdutSK.length){ 
                    //component.set("v.isShowCustmerModal", true);
                    //component.set("v.isManualPricing", false); 
                    }else{
                    	component.set("v.isGetProduct",true); 
                        helper.showToast("error", 'You cannot enter any special Characters in Product please check.',
                        component,'Only Allowed characters are - * and Space'); 
                    }
                }
                if(qunty !==null){ 
                
                    if(quntyPrd.length == qunty.length){ 
                    //component.set("v.isShowCustmerModal", true);
                    //component.set("v.isManualPricing", false);  
                    }else{
                    	component.set("v.isGetProduct",true);
                        helper.showToast("error", 'You cannot enter any special Characters in Product please check.',
                        component,'Only Allowed characters are - * and Space'); 
                    }
                }
           
                if(component.get("v.isGetProduct") == false)
                {
                    //alert('test');
                	helper.getProductDetails(component,helper); 
                }
        },
        
      showToast : function(type, title, component, message) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                type: type,
                title: title,
                message: message,
            });
            toastEvent.fire();
        }
    })