({
	doInit : function(component, event, helper) {
		//helper.getProductDetail(component, helper);
        helper.getProductPrice(component, helper);
	},
    operateCheckbox : function(component, event, helper) {
       
        
        var ProductTitle;
        ProductTitle = component.get('v.productDetail').productTitle;
        if(ProductTitle === undefined){
        ProductTitle = component.get('v.productDetail').itemName;
        }
        var skuid=  component.find("setCurrentItemChkd").get("v.value"); 
        var str2 ='*';
        
        if(skuid){
            var prodskutitle = ProductTitle.concat(str2,skuid); 
            var multiItemEvent = component.getEvent('MultiItemAddEvent');
            multiItemEvent.setParams({ "notifyParam":prodskutitle  });
            multiItemEvent.fire(); 
        }else{
            var prodskutitle = ProductTitle.concat(str2,null); 
            var multiItemEvent = component.getEvent('MultiItemAddEvent');
            multiItemEvent.setParams({ "notifyParam":prodskutitle  });
            multiItemEvent.fire(); 
        }
       
       
    },
    goToProductDetail : function(component, event, helper) {
        var selectedItem = event.currentTarget; // Get the target object
        var clickedProductInfo = selectedItem.dataset.record; // Get its binding value
        var parentCategoryId = component.get('v.parentCategoryId');

        var productEvent = component.getEvent('productEvent');
        productEvent.setParams({
            "parentCategoryId" : parentCategoryId, "productDetailId": clickedProductInfo
        });
        productEvent.fire(); 
    },
    addToCart : function(component,event,helper){ 
        //helper.helperMethod(component, event, helper);
        var eventComponent = component.getEvent("NotifyParentOpenCustomerModal"); 
        var chkProd =  component.get('v.ChkboxList');   
       
       
         eventComponent.setParams({ "notifyParam":  component.get('v.productDetail'),
          
                                  "notifyParam1": component.get('v.productPrice') });
        eventComponent.fire();   
          
     },
    isRefreshed: function(component, event, helper) {
        location.reload();
    }
})