({
    openTabWithSubtab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            url: '/lightning/r/Account/001q000000ucD7CAAU/view',
            focus: true
        }).then(function(response) {
            workspaceAPI.openSubtab({
                parentTabId: response,
                url: '/lightning/r/SalesOrder__x/x010n0000000JHgAAM/view',
                focus: true
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})
/*
({
    openTab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        var testid = component.get("v.recordId");
        alert('openTab');
        workspaceAPI.openTab({
            
            url: '/lightning/r/Case/'+testid+'/view',
            focus: true
        }).then(function(response) {
            workspaceAPI.getTabInfo({
                tabId: response
            }).then(function(tabInfo) {
            console.log("The recordId for this tab is: " + tabInfo.recordId);
            });
        }).catch(function(error) {
                console.log(error);
        });
    }
})
*/