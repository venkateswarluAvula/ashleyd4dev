({
    doInit : function(component, event, helper) {
        var fulfill; 
        var cstatus;
        
        //show loading spinner
        component.set("v.isLoading", true);
        var currentOrderId = component.get("v.recordId");
        //var currentOrderId = "x010n0000000JHZAA2";
        var action = component.get("c.getOrderData");
        action.setParams({
            salesforceOrderId : currentOrderId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var callResponse = response.getReturnValue();	
                //alert('call response test'+JSON.stringify(callResponse));
                console.log('call response test'+JSON.stringify(callResponse));
                var vip = callResponse.vip;
                var lib = callResponse.lib;
                var sLevel = callResponse.servicelevel;
                
                fulfill = callResponse.fulfillerId;
                component.set("v.soContactStatus", callResponse.ContactStatus);
                //alert('callResponse.ContactStatus---'+callResponse.ContactStatus);
                component.set("v.soShipVia", callResponse.ShipVia);
                //alert('callResponse.ShipVia---'+callResponse.ShipVia);
                
                
                //alert('sLevel---'+sLevel);
                var isEdit = callResponse.isEditable;
                //alert('isEdit--'+isEdit);
                //var WindowEndPM
                var comment1 = callResponse.deliveryComments;
                if(comment1 != null){
                    var count = comment1.length;
                    component.set("v.charsRemaining",count);
                }
                
                var comment2 = callResponse.orderDeliveryComments;
                if(comment2 != null){
                    var count2 = comment2.length;
                    component.set("v.charsRemaining2",count2);
                }
                
                component.set("v.salesOrder", callResponse);
                component.set("v.fulfillerId", response.getReturnValue().fulfillerId);
                
                component.set("v.shipToState", response.getReturnValue().shipToState);
                component.set("v.shipToState11", response.getReturnValue().rescheduleReasonOptions);
                
                //Added by Vamsi for contact status       
                
                var actionscall =component.get("c.getContactStatus");
                actionscall.setParams({
                    "fulfillerId" : fulfill
                });
                actionscall.setCallback(this, function(responsescall){
                    var State = responsescall.getState();
                    if (component.isValid() && responsescall.getState() === "SUCCESS") { 
                        var resultvalue = responsescall.getReturnValue();
                        var responsevalue;
                        var arrayMapKeysmapconStatus = [];
                        var arrayMapKeysmapshipVia = [];
                        for(var key in resultvalue){
                            if(key == 'contactstatus'){
                                responsevalue = resultvalue[key];
                                if (callResponse.ContactStatus == null){
                                    arrayMapKeysmapconStatus.push({key: "None", value: "None"});
                                }
                                for(var int in responsevalue){
                                    //alert('int value--'+int);
                                    arrayMapKeysmapconStatus.push({key: int, value: responsevalue[int]});
                                }
                            }
                            if(key == 'shipvia'){
                                responsevalue = resultvalue[key];
                                if (callResponse.ShipVia == null){
                                    arrayMapKeysmapshipVia.push({key: "None", value: "None"});
                                }
                                for(var int in responsevalue){
                                    //alert('int value--'+int);
                                    arrayMapKeysmapshipVia.push({key: int, value: responsevalue[int]});
                                }
                            }
                            
                        }
                        
                        console.log('arrayMapKeysmapconStatus--'+arrayMapKeysmapconStatus);
                        console.log('arrayMapKeysmapshipVia--'+arrayMapKeysmapshipVia);
                        
                        component.set("v.ContactStatus",arrayMapKeysmapconStatus);
                        component.set("v.shipViaMap",arrayMapKeysmapshipVia);
                        
                    } else if (State === "ERROR") {
                        //Error message display logic.
                        var errors = responsescall.getError();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "ERROR!",
                            "message": errors[0].message
                        });
                        toastEvent.fire();
                    } 
                }); 
                $A.enqueueAction(actionscall);
                
                //Ended for contact status 
                
                var actionStatePl = component.get("c.getStatePl");
                if(isEdit == true){
                    actionStatePl.setCallback(this, function(response6) {
                        if (component.isValid() && response6.getState() === "SUCCESS") {
                            console.log('Picklist: ' + JSON.stringify(response6.getReturnValue()));
                            var opts = [];
                            opts.push({ class: "optionClass", label: "State", value: "State", disabled: "true" });
                            response6.getReturnValue().forEach(function(key){
                                var pl = component.get("v.shipToState");
                                if((pl != null) && (pl == key))
                                {
                                    opts.push({ class: "optionClass", label: key, value: key, selected: "true" });
                                } else {
                                    opts.push({ class: "optionClass", label: key, value: key });
                                }
                            });
                            console.log('opts: ' + JSON.stringify(opts));
                            component.find("state").set("v.options", opts);
                        }
                        else {
                            var errorToast = $A.get("e.force:showToast");
                            errorToast.setParams({"message": response6.getError()[0].message, "type":"error"});
                            errorToast.fire();
                        }
                    });
                    
                    $A.enqueueAction(actionStatePl);
                }
                component.set("v.vip", vip);
                component.set("v.lib", lib);
                //console.log("deliveryDate11-->", component.get("v.deliveryDate"));
                //alert(callResponse.WindowBegin);
                
                if(callResponse.WindowBegin != null && callResponse.WindowBegin.includes("AM") ){
                    var StartTime= callResponse.WindowBegin.split(' ');
                    var StartTimeinx= StartTime[0];
                    var StartTimeSplit= StartTimeinx.split(':'); 
                    var StartTimeSplitInxOne= StartTimeSplit[1];
                    var StartTimeSplitInx = StartTimeSplit[0];   
                    var StartTimeAM = (StartTimeSplitInx ) + ':' + (StartTimeSplitInxOne) ;
                    component.set("v.salesOrder.WindowBegin", StartTimeAM);
                }
                if(callResponse.WindowEnd != null && callResponse.WindowEnd.includes("AM") ){
                    var EndTime= callResponse.WindowEnd.split(' ');
                    var EndTimeinx=EndTime[0];
                    var EndTimeSplit= EndTimeinx.split(':'); 
                    var EndTimeSplitInxOne= EndTimeSplit[1];
                    var EndTimeSplitInx = EndTimeSplit[0];   
                    var EndTimeAM = (EndTimeSplitInx) + ':' + (EndTimeSplitInxOne) ;
                    component.set("v.salesOrder.WindowEnd", EndTimeAM);
                }
                if(callResponse.WindowBegin != null && callResponse.WindowBegin.includes("PM"))
                {
                    var StartTime= callResponse.WindowBegin.split(' ');
                    var StartTimeinx=StartTime[0];
                    var StartTimeSplit= StartTimeinx.split(':'); 
                    var StartTimeSplitInxOne= StartTimeSplit[1];
                    var StartTimeSplitInx = StartTimeSplit[0];   
                    
                    var StartTimePM = (+StartTimeSplitInx + 12 ) + ':' + (StartTimeSplitInxOne) ;
                    component.set("v.salesOrder.WindowBegin", StartTimePM);
                }
                if(callResponse.WindowEnd != null && callResponse.WindowEnd.includes("PM") ){
                    
                    
                    var EndTime= callResponse.WindowEnd.split(' ');
                    
                    var EndTimeinx=EndTime[0];
                    var EndTimeSplit= EndTimeinx.split(':'); 
                    var EndTimeSplitInxOne= EndTimeSplit[1];
                    var EndTimeSplitInx = EndTimeSplit[0];   
                    
                    var EndTimePM = (+EndTimeSplitInx + 12 ) + ':' + (EndTimeSplitInxOne) ;
                    //alert('EndTimePM is --'+EndTimePM);
                    component.set("v.salesOrder.WindowEnd", EndTimePM);
                }
                
                // code aadded by Sudeshna
                
                /*    var evt = $A.get("e.c:NotifyParentCalendar1");
   			 evt.setParams({
       			 "notifyThreshold":component.get("v.salesOrder.servicelevel")
   				 });
   			 evt.fire();
        
    			alert("event 2init");
        		eventComponent.fire();*/
                
                if(sLevel == "THD"){
                    component.set("v.LevelTHD",true);
                    component.set("v.serLevel",false);
                    console.log("sLevelThreshold-->", sLevel);
                    console.log("serLevel-->", component.get('v.serLevel'));
                    component.set("v.servicelevelTHD",true);
                }
                else if(sLevel == "PDI"){
                    component.set("v.LevelPDI",true);
                    component.set("v.serLevel",false);
                    console.log("sLevelPDI-->", sLevel);
                    console.log("serLevel--->", component.get('v.serLevel'));
                    component.set("v.servicelevelPDI",true);
                }
                    else {
                        component.set("v.isGetDates",false);
                        component.set("v.serLevel",true);
                        console.log("serLevel-->", component.get('v.serLevel'));
                    }
                if(component.get("v.salesOrder.orderSource") == "Profit")
                {
                    console.log("-->Order Source", component.get("v.salesOrder.orderSource"));
                    component.set("v.ISProfit",true);
                }              
                //component.set("v.salesOrder", callResponse); 
                //component.set("v.vip", vip);
                //component.set("v.lib", lib);
                // code ends
                
                if(component.get("v.salesOrder.currentDeliverydate") == "1990-01-01")
                {
                    console.log("deliveryDate-->", component.get("v.deliveryDate"));
                    component.set("v.salesOrder.asap",true);
                }
                if(component.get("v.salesOrder.asap") == true)
                {
                    component.set("v.isGetDates", false);
                    component.set("v.isTime", true);
                    component.set("v.isDelDate", true);
                }
                if(component.get("v.salesOrder.asap") == false)
                {
                    component.set("v.isGetDates", true);
                    //alert('start');
                    component.set("v.isTime", true);
                    //component.set("v.isDelDate", true);
                }	
                if(component.get("v.salesOrder.hot") == true)
                {
                    component.set("v.isTime", false);
                    component.set("v.isDelDate", false);
                    //alert('0.1');
                    component.set("v.isGetDates", true);
                    //component.set("v.isResch", true);
                }
                else if(component.get("v.salesOrder.hot") == false)
                {
                    component.set("v.isTime", true);
                    //component.set("v.salesOrder.WindowBegin", "07:00");
                    //alert('0.2');
                    //component.set("v.salesOrder.WindowEnd", "19:00");
                    //component.set("v.isResch", true);
                }
                console.log("salesorder" , JSON.stringify(response.getReturnValue()));
            }
            else{
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error",  "mode":"dismissible", "duration":10000});
                errorToast.fire();
            }
            //hide loading spinner
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
        
        //Added by Sudeshna
        var actionGD = component.get("c.GetDeliveryTypeOLI");
        actionGD.setParams({
            salesforceOrderId : currentOrderId
        });
        actionGD.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var callResponse = response.getReturnValue();	
                //alert('call response test'+JSON.stringify(callResponse));
                console.log('call GD-->'+JSON.stringify(callResponse));
                if(JSON.stringify(callResponse) == "true"){
                    console.log('call GD2-->'+JSON.stringify(callResponse));
                    component.set("v.isOLIDT", false);
                }
            } 
        });
        $A.enqueueAction(actionGD);
        //End By Sudeshna
        
        //Added by Vamsi for threshold ON/OFF
        
        var actionser =component.get("c.thresholdenabledcall");
        actionser.setParams({"b" : true,
                             "salesforceOrderId" : currentOrderId
                            });
        actionser.setCallback(this, function(responseser){
            var State = responseser.getState();
            //alert('State'+ State);
            if (component.isValid() && responseser.getState() === "SUCCESS") { 
                //alert('responsevalue2' + JSON.stringify(responseser.getReturnValue()));
                console.log('responsevalue1' + JSON.stringify(responseser.getReturnValue()));
                console.log('responsevalue2' + JSON.parse(responseser.getReturnValue()));
                var jsonData = JSON.parse(responseser.getReturnValue());
                
                if(jsonData == null){
                    //component.set("v.ISProfit",true);
                }
                else {
                    for(var i=0; i<jsonData.length; i++) {
                        console.log('here' + jsonData[i].Value);
                        if(jsonData[i].Value == 0)
                        {
                            component.set("v.thdMode",true);
                        }
                        
                    }	
                }
            }
            
            else if (State === "ERROR") {
                //Error message display logic.
                var errors = responseser.getError();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "ERROR!",
                    "message": errors[0].message
                });
                toastEvent.fire();
            } 
            
        }); 
        $A.enqueueAction(actionser);
        //End by Vamsi
        
        
    },
    
    countLength : function(component, event, helper) {
        var commentarea = component.get("v.salesOrder.deliveryComments");
        var count = commentarea.length;
        component.set("v.charsRemaining",count);
    },
    
    countLength1 : function(component, event, helper) {
        var commentarea2 = component.get("v.salesOrder.orderDeliveryComments");
        var count2 = commentarea2.length;
        component.set("v.charsRemaining2",count2);
    },
    
    checkASAP : function(component, event, helper) {
        var asp = component.get("v.salesOrder.asap");
        //alert('asp----'+component.get("v.salesOrder.asap"));
        if(asp==true)
        {
            component.set("v.salesOrder.hot", false);
            var tst= component.set("v.isGetDates", false);
            //alert('tst----'+tst);
            component.set("v.isTime", true);
            component.set("v.deliveryDate","1990-01-01");
            component.set("v.salesOrder.currentDeliverydate","1990-01-01");
            component.set("v.deliveryDateUpdated",true);
            component.set("v.isDelDate", true);
            component.set("v.isResch",false);
            component.set("v.salesOrder.WindowBegin", '');
            component.set("v.salesOrder.WindowEnd", '');
            //var changeElement = component.find("gadID");
            // by using $A.util.toggleClass add-remove slds-hide class
            //$A.util.toggleClass(changeElement, "slds-hide");
            console.log('deldate' + component.get("v.salesOrder.currentDeliverydate"));
        } 
        else if(asp==false)
        {
            // alert('asap false');
            //component.set("v.salesOrder.hot", false);
            component.set("v.isGetDates", true);
            component.set("v.isTime", true);
            component.set("v.salesOrder.WindowBegin", '');
            component.set("v.salesOrder.WindowEnd", '');
            component.set("v.isDelDate", false);
        }
    },
    //Code added by Sudeshna
    
    checkServiceLevel : function(component, event, helper){
        var libValue = component.get("v.salesOrder.lib");
        var oliDT = component.get("v.isOLIDT");
        console.log('oliDT--->' +oliDT);
        //alert('libValue-->' +libValue);
        var serviceLevel = component.get("v.salesOrder.servicelevel");
        //alert('serviceLevel-nowpart->' +serviceLevel);
        if(serviceLevel == 'THD'){
            component.set("v.salesOrder.servicelevel","PDI");
            component.set("v.LevelTHD",false);
            component.set("v.parentAttribute1","PDI");
            //alert('parentAttribute1 THD '+component.get("v.parentAttribute1"));
            if(oliDT == true){
                var a = component.get('c.launchDeliveryDateLookup');
                $A.enqueueAction(a); 
            }
            
        }
        else if(serviceLevel == 'PDI'){
            component.set("v.salesOrder.servicelevel","THD");
            component.set("v.LevelTHD",true);
            component.set("v.isThreshold",true);
            component.set("v.parentAttribute1","THD");
            //alert('parentAttribute1 PDI '+component.get("v.parentAttribute1"));
            if(oliDT == true){
                var a = component.get('c.launchDeliveryDateLookup');
                $A.enqueueAction(a); 
            }
            if(libValue){
                component.set("v.salesOrder.lib",false);
            }
        }
        //var evt = component.getEvent("NotifyParentCalendar");
        // evt.setParams({
        //	 "notifyThreshold": component.get("v.salesOrder.servicelevel")
        //	 });
        // evt.fire();
        
        // var attribute1 = component.get('v.serviceLevel');
        // var attribute2 = component.get('v.salesOrder.servicelevel');
        // var childComponent = component.find('child');
        // childComponent.myMethod(attribute2);
    },
    //Code ends
    
    checkHot : function(component, event, helper) {
        var soHot = component.get("v.salesOrder.hot");
        var salesOrder = component.get("v.salesOrder");
        if(soHot==true)
        {
            component.set("v.salesOrder.asap", false);
            component.set("v.isGetDates", true);
            component.set("v.isTime", false);
            component.set("v.isDelDate", false);
            component.set("v.salesOrder.WindowBegin", "07:00");
            component.set("v.salesOrder.WindowEnd", "19:00");
            
        } 
        else if(soHot==false)
        {
            //component.set("v.salesOrder.WindowBegin", "");
            //component.set("v.salesOrder.WindowEnd", "");
            component.set("v.isGetDates", true);
            component.set("v.isTime", true);
        }
    },
    checkDeliveryDate : function(component, event, helper) {
        console.log("isResch", component.get("v.isResch"));
        component.set("v.deliveryDateUpdated",true); 
        component.set("v.isResch",false);
        //alert('123'+component.get("v.isResch"));
        console.log("isResch", component.get("v.isResch"));
    },
    /*    validateStartTime : function(component, event, helper) {
        var sTime = component.get("v.salesOrder.WindowBegin");
        var eTime = component.get("v.salesOrder.WindowEnd");
        if((sTime != null || sTime != '') && (eTime != null || eTime != '') && sTime > eTime)
        {
            document.getElementById('error').innerHTML="Start Time Should be Lessthan End time.";
            return;
        }
    },
    validateEndTime : function(component, event, helper) {
        var sTime = component.get("v.salesOrder.WindowBegin");
        var eTime = component.get("v.salesOrder.WindowEnd");
        
        var StartTime;
        var StartTimeInxOne;
        var StartTimeInx;
        var EndTime;
        var EndTimeinxOne;
        var EndTimeinx;
        var StartTimeSplit;
        var EndTimeSplit;
        var StartTimeSplitInxOne;
        var StartTimeSplitInx;
        var EndTimeSplitInxOne;
        var EndTimeSplitInx;
        
        if(sTime != null)
        {
            
            StartTime= sTime.split(' ');
            StartTimeInxOne=StartTime[1];
            StartTimeInx=StartTime[0];
            StartTimeSplit= StartTimeInx.split(':');
            StartTimeSplitInxOne= StartTimeSplit[1]; 
            StartTimeSplitInx = StartTimeSplit[0];
        }
        if(eTime != null)
        {
            
            EndTime= eTime.split(' ');
            EndTimeinxOne=EndTime[1];
            EndTimeinx=EndTime[0];
            EndTimeSplit= EndTimeinx.split(':'); 
            EndTimeSplitInxOne= EndTimeSplit[1];
            EndTimeSplitInx = EndTimeSplit[0]; 
        }
        
        if(EndTimeSplitInx != null && EndTimeSplitInxOne != null && StartTimeSplitInx != null && StartTimeSplitInxOne != null)
        {
            result = (+(EndTimeSplitInx*60) + (+EndTimeSplitInxOne)) - (+(StartTimeSplitInx*60) + (+StartTimeSplitInxOne));
            //alert('result------  '+result);
            
            if(result < 0 ) {
                
                document.getElementById('error').innerHTML="End Time Should be Greaterthan Start time.";
                
                return;
            }
            else if(result < 30 && result > 0) {
                
                document.getElementById('error').innerHTML="Please save with a longer time window.";
                
                return;
            }
            /* else if((sTime != null || sTime != '') && (eTime != null || eTime != '') && sTime > eTime)
           //  else if( sTime > eTime && sTime == null && eTime == null)
            {
                document.getElementById('error').innerHTML="End Time Should be Greater than Start time.";
                return;
            } 
        }
    },*/
    updateDeliveryDate : function(component, event, helper) {
        var order = component.get("v.salesOrder"); 		
        var orderIdFromEvent = event.getParam("orderID");
        var selectedDeliveryDateFromEvent = event.getParam("deliveryDate");
        var orderLineIdFromEvent = event.getParam("orderLineId");
        if(orderIdFromEvent == order.externalId){
            if(orderLineIdFromEvent == null){
                order.currentDeliverydate = selectedDeliveryDateFromEvent;
                component.set("v.salesOrder", order); 
                component.set("v.deliveryDateUpdated", true);
                //alert('upd');
                component.set("v.isResch",false);
                //alert('ate');
                console.log("isResch", component.get("v.isResch"));
                //alert('dbvfkgf'+component.get("v.isResch"));
            }else if(order.lineItems != null){
                var arrayLength = order.lineItems.length;
                for (var i = 0; i < arrayLength; i++) {
                    if(order.lineItems[i].externalId == orderLineIdFromEvent){
                        order.lineItems[i].deliverydate = selectedDeliveryDateFromEvent;
                        component.set("v.salesOrder", order); 
                        break;
                    }
                }
            }			
        }
    },
    launchDeliveryDateLookup : function(component, event, helper) {
        //alert('launchDeliveryDateLookup1');
        
        var deliverydatefromorder = component.find("deliveryDate").get("v.value");
        //alert('deliverydatefromorder'+deliverydatefromorder);
        var isDate = component.get("v.isGetDates");
        var curDate = new Date().toISOString().slice(0, 10);
        //alert('curDate: ');
        console.log("curDate--->",curDate);
        //alert('isDate'+isDate);
        console.log("isDate--->",isDate);
        
        //	var desiredDeliveryDate = component.get("v.selectedDesiredDeliveryDate"); 
        if(deliverydatefromorder <= curDate){
            var order = component.get("v.salesOrder"); 
            var currentOrderId = order.externalId;
            var accountNumber = order.accountNumber;
            var rdcId = order.rdcId;
            var orderNumber = order.orderNumber;
            var profitCenter = order.profitCenter;
            var serLevel = component.get("v.parentAttribute1");
            var salesforceOrderId = component.get("v.parentAttribute3");
            if (salesforceOrderId == null){
                var salesforceOrderId = order.lineItems[0].salesforceId;
            }
            //alert('salesforceOrderId now'+salesforceOrderId);
            if (serLevel == null){
                var serLevel = order.servicelevel;
                // alert (' 460 serLevel'+serLevel);
            }
            //alert("serLevel-462->"+serLevel);
            //  alert('salesorder number-->'+orderNumber);
            $A.createComponent(
                "c:Deliverywindowlookupservice",
                {
                    "orderId": currentOrderId,
                    "selectedDeliveryDate": curDate,
                    "lineItemDeliveryDate": curDate,
                    "accountNumber": accountNumber,
                    "isdate": isDate,
                    "rdcId": rdcId,
                    "orderNumber":orderNumber,
                    "profitCenter":profitCenter,
                    "serLevel":serLevel,
                    "salesforceOrderId":salesforceOrderId
                },
                function(msgBox){                
                    if (component.isValid()) {
                        var popupPlaceholder = component.find('deliveryWindowLookupPlaceHolder');
                        popupPlaceholder.set("v.body", msgBox); 
                    }
                }
            );
        }
        
        if(deliverydatefromorder > curDate){
            //alert('deliverydatefromorder:- '+ deliverydatefromorder);
            var order = component.get("v.salesOrder"); 
            var currentOrderId = order.externalId;
            var accountNumber = order.accountNumber;
            var rdcId = order.rdcId;
            var orderNumber = order.orderNumber;
            var profitCenter = order.profitCenter;
            var serLevel = component.get("v.parentAttribute1");
            var salesforceOrderId = component.get("v.parentAttribute3");
            //alert('salesforceOrderId'+salesforceOrderId)
            if (serLevel == null){
                var serLevel = order.servicelevel;
                //alert (' 497 serLevel'+serLevel);
            }
            if (salesforceOrderId == null){
                var salesforceOrderId = order.lineItems[0].salesforceId;
            }
            //alert('salesforceOrderId now'+salesforceOrderId);
            //alert("serLevel-483->"+serLevel);
            //alert('salesorder number-->'+orderNumber);
            $A.createComponent(
                "c:Deliverywindowlookupservice",
                {
                    "orderId": currentOrderId,
                    "selectedDeliveryDate": deliverydatefromorder,
                    "lineItemDeliveryDate": deliverydatefromorder,
                    "accountNumber": accountNumber,
                    "isdate": isDate,
                    "rdcId": rdcId,
                    "orderNumber":orderNumber,
                    "profitCenter":profitCenter,
                    "serLevel":serLevel,
                    "salesforceOrderId":salesforceOrderId
                },
                function(msgBox){                
                    if (component.isValid()) {
                        var popupPlaceholder = component.find('deliveryWindowLookupPlaceHolder');
                        popupPlaceholder.set("v.body", msgBox); 
                    }
                }
            );
        }
        
        /* else{
            var errorToast = $A.get("e.force:showToast");
            errorToast.setParams({"message": $A.get("$Label.c.Update_Sales_Order_Missing_Desired_Delivery_Date"), "type":"error", "mode":"dismissible", "duration":10000});
            errorToast.fire();
        }*/
    },
    saveChanges : function(component, event, helper) {
        var salesOrder = component.get("v.salesOrder"); 
        var vip = component.get("v.vip");
        var lib = component.get("v.lib");
        var comment = component.get("v.salesOrder.deliveryComments");
        var SOComment = component.get("v.salesOrder.orderDeliveryComments");
        var sTime = component.get("v.salesOrder.WindowBegin");
        var eTime = component.get("v.salesOrder.WindowEnd");
                
        var constatus = component.find("ConStatus").get("v.value");
        var constatusvals = constatus.split("||");
        var constatusId = constatusvals[0];
        var constatusdesc = constatusvals[1];
        
        console.log('constatusId: '+ constatusId +' || constatusdesc: ' + constatusdesc);
        
        var newShipvia = component.find("shipVia").get("v.value");
        var newShipviavals = newShipvia.split("||");
        var newShipviaId = newShipviavals[0];
        var newShipviadesc = newShipviavals[1];
        
        console.log('newShipviaId: '+ newShipviaId +' || newShipviadesc: ' + newShipviadesc);
        
        var fulfill1 = component.get("v.fulfillerId");
        //alert('fulfill11234'+fulfill1);
        
        console.log("val"+ JSON.stringify(vip));
        console.log("value"+ JSON.stringify(lib));
        console.log("valu"+ JSON.stringify(salesOrder));
        var slvl = component.get('v.radio4');
        console.log("slvl",slvl);
        var deliveryDateChanged = component.get("v.deliveryDateUpdated"); 
        // var deliverydate = component.find("deliveryDate").set("v.value","1990-01-01");
        var deliverydate = component.get("v.salesOrder.currentDeliverydate");
        console.log('deliveryDateChanged-----' + deliveryDateChanged);
        var asp = component.get("v.salesOrder.asap");
        var hot = component.get("v.salesOrder.hot");
        var StartTime;
        var StartTimeInxOne;
        var StartTimeInx;
        var EndTime;
        var EndTimeinxOne;
        var EndTimeinx;
        var StartTimeSplit;
        var EndTimeSplit;
        var StartTimeSplitInxOne;
        var StartTimeSplitInx;
        var EndTimeSplitInxOne;
        var EndTimeSplitInx;
        var AfterMeridian ='AM';
        var PostMeridian='PM';
        var FixedTime = '420';
        var EndFixedTime = '1140';
        
        if(sTime != null)
        {
            StartTime= sTime.split(' ');
            StartTimeInxOne=StartTime[1];
            StartTimeInx=StartTime[0];
            StartTimeSplit= StartTimeInx.split(':');
            StartTimeSplitInxOne= StartTimeSplit[1]; 
            StartTimeSplitInx = StartTimeSplit[0];
        }
        if(eTime != null)
        {
            EndTime= eTime.split(' ');
            EndTimeinxOne=EndTime[1];
            EndTimeinx=EndTime[0];
            EndTimeSplit= EndTimeinx.split(':'); 
            EndTimeSplitInxOne= EndTimeSplit[1];
            EndTimeSplitInx = EndTimeSplit[0]; 
        }
        
        if(EndTimeSplitInx != null && EndTimeSplitInxOne != null && StartTimeSplitInx != null && StartTimeSplitInxOne != null)
        {
            result = (+(EndTimeSplitInx*60) + (+EndTimeSplitInxOne)) - (+(StartTimeSplitInx*60) + (+StartTimeSplitInxOne));
            console.log('result-----' + result);
            if(result <= 0 ) {
                
                document.getElementById('error').innerHTML="End Time Should be Greaterthan Start time.";
                
                return;
            }
            else if(result < 30 && result > 0) {
                
                document.getElementById('error').innerHTML="Please save with a longer time window.";
                
                return;
            }
            
            
        }
        //Start Time greater than 7 AM
        
        StartFixTime =  (+(StartTimeSplitInx*60) + (+StartTimeSplitInxOne));
        //if(component.set("v.isTime", false)){
        if( FixedTime > StartFixTime) {
            //alert("vdhfcadk");
            document.getElementById('error').innerHTML="Time Should Be Greater than 7AM"; 
            return;
        }
        
        //End Time Lesser than 7 PM
        
        EndFixTime = (+(EndTimeSplitInx*60) + (+EndTimeSplitInxOne));
        
        if( EndFixedTime < EndFixTime) {
            
            document.getElementById('error').innerHTML="Time Should Be Lesser than 7PM"; 
            return;
        }
        // }
        //validate reason code is provided when delivery date is changed
        if(deliveryDateChanged == true && (salesOrder.rescheduleReasonCodeId == '' || salesOrder.rescheduleReasonCodeId == null)){
            
            document.getElementById('error').innerHTML="Reschedule reason must be selected.";
            return;
        }
        else if(asp == true && deliveryDateChanged == true &&(salesOrder.rescheduleReasonCodeId == '' || salesOrder.rescheduleReasonCodeId == null))
        {
            document.getElementById('error').innerHTML="Reschedule reason must be selected.";
            return;
        }
            else if(asp == false && deliverydate == "1990-01-01")
            {
                document.getElementById('error').innerHTML="Delivery Date must be selected or ASAP must be select.";
                return;
            }
                else if(hot == true && (deliverydate == "1990-01-01" || deliverydate == null))
                {
                    document.getElementById('error').innerHTML="Delivery Date must be selected.";
                    return;
                }
                    else if(hot == true && (sTime == null || sTime == ''))
                    {
                        document.getElementById('error').innerHTML="Window begin Time must be selected.";
                        return;
                    }
                        else if(hot == true && sTime != null && (eTime == null || eTime == ''))
                        {
                            document.getElementById('error').innerHTML="Window end Time must be selected.";
                            return;
                        }
                            else if((sTime != null || sTime != '') && (eTime != null || eTime != '') && sTime > eTime)
                            {
                                document.getElementById('error').innerHTML="Start Time Should be Lessthan End time.";
                                return;
                            }
        //show loading spinner
        component.set("v.isLoading", true);
        var deserializedSalesOrder = JSON.stringify(salesOrder);
        console.log('json' +deserializedSalesOrder);
        var action = component.get("c.updateSalesOrder");
        action.setParams({
            salesOrderInfo : deserializedSalesOrder,
            contStatus : constatusId,
            contStatusdesc : constatusdesc,
            shipViaId : newShipviaId,
            shipViaDesc : newShipviadesc
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var callResponse = response.getReturnValue();
                if(callResponse.errorMessages != null){
                    component.set("v.errbool",true);
                    component.set("v.errmsg",callResponse.errorMessages);
                    	
                }	
                if(callResponse.successMessages != null){
                    if(asp == true)
                    {
                        component.set("v.salesOrder.asap",true);
                    }
                    var successToast = $A.get("e.force:showToast");
                    successToast.setParams({"message": callResponse.successMessages, "type":"success", "mode":"dismissible", "duration":5000});
                    successToast.fire();
                }	
                if(callResponse.hasErrors == false){
                    $A.get("e.force:closeQuickAction").fire();																	
                }
            }
            else{
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error",  "mode":"dismissible", "duration":10000});
                errorToast.fire();
                //$A.get("e.force:closeQuickAction").fire();	
            }
            //hide loading spinner
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);		 
    },
    // EDQ added Validation logic
    handleSearchChange : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleSearchChange(component, event, settings);
        helper.handleValidationStatus(component, settings);
    },
    // EDQ added Validation logic
    handleSuggestionNavigation :  function(component, event, helper) {
        var settings = helper.getSettings();
        var keyCode = event.which;
        if(keyCode == 13) { // Enter
            helper.acceptFirstSuggestion(component, settings);
        } else if(keyCode == 40) { //Arrow down
            helper.selectNextSuggestion(component, null, settings);
        } else if(keyCode == 38) { //Arrow up
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // EDQ event listener
    onSuggestionKeyUp : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.onSuggestionKeyUp(component, event, settings);
    },
    // EDQ event listener
    handleResultSelect : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleResultSelect(component, event, settings);
    },
    // EDQ event listener
    onAddressChanged : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleValidationStatus(component, settings);
    },
    // EDQ event listener
    onElementFocusedOut : function (component, event, helper) {
        var settings = helper.getSettings();
        var useHasNotSelectedASuggestion = helper.isNull(event.relatedTarget) || event.relatedTarget.id.indexOf(settings.suggestionIndexClassPrefix) === -1;
        if (useHasNotSelectedASuggestion) {
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    
    doCancel : function(component, event, helper) {  
        $A.get("e.force:closeQuickAction").fire();
    }
})