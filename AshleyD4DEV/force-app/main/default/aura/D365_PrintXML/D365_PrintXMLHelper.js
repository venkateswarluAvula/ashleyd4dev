({
	prepareXML: function(component,event) {
		
        var strRecId = event.getParam("strRecordId");
        
        var action = component.get('c.SOrecordXML');
        action.setParams({
            "recId":strRecId,
        });
        var self = this;
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            console.log('state===='+state);
            console.log('actionResult.getReturnValue()===='+actionResult.getReturnValue());
            component.set('v.strXML', actionResult.getReturnValue());
            
        });
        $A.enqueueAction(action);
        
        
	}
})