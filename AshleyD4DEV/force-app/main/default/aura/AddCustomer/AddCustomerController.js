({
    doInit: function(component, event, helper) {
        console.log('test---'+component.get("v.recordId"));  
        helper.initialUserZone(component,helper);
        var recId = component.get("v.recordId");
        component.set("v.guestMessage",'Temporary guest profile are only active for 24 hours unless all mandatory fields are captured.');
        component.set("v.newCustomer.LastName",'');
        if (recId) {
            component.set("v.modalContext", "Edit");
	        component.find('forceEditRecordCustomer').reloadRecord(true);
        }
    }, 
    saveRecords : function(component, event, helper) {
		//alert('show--1---'+component.get("v.isSignatureHideForCA"));
		helper.clearMsg(component);

        var newCustomer = component.get("v.newCustomer");
        var newShipping = component.get("v.newShipping");
        var newBilling = component.get("v.newBilling");
        var title = component.get("v.title");
        var body = component.get("v.body");
        var useShipping = component.get("v.useShipping");
       
        if (useShipping) {
        	newBilling = null;
        } else {
            // if address has no fields, make object null
            newBilling = helper.makeAddrNull(newBilling);
        }
        // if address has no fields, make object null
        newShipping = helper.makeAddrNull(newShipping);
		//alert('test G');
        var isValid = helper.customerIsValid(newCustomer, newShipping, newBilling, title, body, component, helper);
        //alert('test G1');        
        if (isValid || component.get("v.newCustomer.Temporary_Guest_Text__c") != null) { 
            //alert('test G2');
            component.set("v.isExecuting", true);
            var noteString = null; // default null 
            if (((title != null)&&(title != '')) && 
                ((body != null)&&(body != ''))) 
            {
                var noteObj = {
                    id: null,
                    title: title, 
                    body: body
                };    
    	
                var noteString = JSON.stringify(noteObj); 
            } 
            
            var mode = component.get("v.modalContext");
            if (mode == 'New') {
                 	component.set("v.isExecuting", false);
                	helper.createCustomer(component, newCustomer, newShipping, newBilling, noteString, function(response){
                    //alert('craeted customer?????');
					var str = component.get("v.isSignatureHideForCA");   
                         //alert('creatchkzonetrue=>'+component.get("v.isSignatureHideForCA"));
                        if(str == true){
                            
                            helper.createSignObj( component, response.getReturnValue());
                        }else{
                        var recId= component.get("v.showSignVal");
                        helper.navigateTo(component, recId,helper); 
                    }
                },
                function(response, message){
                    component.set("v.isExecuting", false);
                    helper.showToast("error", 'Customer Creation Error', component, message);
                });   
            } else {
                //alert('update G');
                helper.updateCustomer(component, newCustomer, noteString, function(response){
                    var inConsole = component.get("v.inSvcConsole");
                    //alert('inConsole-----'+inConsole);
                    if (!inConsole) {
                        component.getEvent("NotifyParentCloseModal").fire();
                    } else {
                        helper.navigateTo( component, response.getReturnValue());
                    }
                },
                function(response, message){
                    component.set("v.isExecuting", false);
                    helper.showToast("error", 'Customer Update Error', component, message);
                });   
            }
        }

	}, 
	saveRecordsSign:function(component, event, helper) {
       if(component.get("v.isSignatureHideForCA") ){
        component.set("v.showSignature",true);
        component.set("v.showSign",false);
        component.set("v.isOldAccount",false);
       }
       helper.navigateTo(component, recId,helper);
    },
     cancelDialogSign : function(component, event, helper) {
        helper.cancelDilgSign(component, event, helper);
       
    },
     saveRecordSign : function(component, event,helper) {
        //alert('sigId---'+component.get("v.sigId"));
       		 //var recId= component.get("v.showSignVal"); 
             //alert('recId----'+recId);
            // helper.navigateTo(component, recId,helper);
            helper.validateRecordSign(component, event,helper);
         
      },
    cancelDialog : function(component, event, helper) {
        component.set("v.isExecuting", true);
        var inConsole = component.get("v.inSvcConsole");
        if (!inConsole) {
            component.getEvent("NotifyParentCloseModal").fire();
        } else {
            window.history.go(-1);
        }
    },
    
       
    promotionalEmails : function(component, event, helper) {
        
        //messages.push('I have confirmed that customer agrees to recieve promotional emails');
        //alert('messagebox--------'+component.get("v.newCustomer.Receive_Promotional_Emails__c"));
        if(component.get("v.newCustomer.Email_Opt_In__pc") == true)
        {
        	alert(' I have confirmed that customer agrees to recieve promotional emails');
        }	
        
    },
    
    messageClose: function(component, event, helper) {
      // Display alert message on the click on the "Like and Close" button from Model Footer 
      // and set set the "isOpen" attribute to "False for close the model Box.
      //alert('thanks for like Us :)');
      component.set("v.isMessageBox", false);
   },
    
    //Added by Ramanand US - 319909
    guestAction: function(component, event, helper) {
        helper.clearMsg(component);
        if(component.get("v.newCustomer.Temporary_Guest_Text__c") != null)
        {
            component.set("v.guestMessage",'Temporary guest profile are only active for 24 hours unless all mandatory fields are captured.');
        }
        if(component.get("v.newCustomer.Temporary_Guest_Text__c") == null || component.get("v.newCustomer.Temporary_Guest_Text__c") == '')
        {
            component.set("v.guestMessage",'');
        }
       
    },
    
    clearGuest: function(component, event, helper) {
        helper.clearMsg(component);
        var newCustomer = component.get("v.newCustomer");
        if(!$A.util.isEmpty(newCustomer.FirstName))
        {
            var guest= newCustomer.Temporary_Guest_Text__c;
        	component.set("v.tempGuest",guest);
            component.set("v.newCustomer.Temporary_Guest_Text__c",'');
            component.set("v.newCustomer.LastName",'');
            component.set("v.guestMessage",'');
        }
    },
    
    makeMyGuest: function(cmp, event, helper) {
      //var selectedItem = event.currentTarget; 
        var record = cmp.get("v.contactRecord");
        var recId = record.Id;
        var action = cmp.get("c.updateOwner");
        action.setParams({"Id":recId});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //helper.navigateTo(component, recId,helper);
                cmp.getEvent("NotifyParentCloseModal").fire();
                
                var appEvent = $A.get("e.c:NavigateToWrapperComponentAppEvent");
                appEvent.setParams({
                    "targetCmpName" : 'CustomerDetailViewWrapperCmp',
                    "targetCmpParameters" : { "recordId": recId }
                });
                appEvent.fire();
                
            }
        });
        $A.enqueueAction(action);
    },
    
    cancelGuest: function(component, event, helper) {
        component.set("v.isSignatureHideForCA",false)  
        component.set("v.showSignature",false);
        component.set("v.showSign",true);
        component.set("v.isOldAccount",false);
        component.set("v.isExecuting",false);
    },
    //ended by Ramanand US - 319909
    
    // EDQ added Validation logic
    handleSearchChange : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleSearchChange(component, event, settings);
        helper.handleValidationStatus(component, settings);
    },
    // EDQ added Validation logic
    handleSuggestionNavigation :  function(component, event, helper) {
        var settings = helper.getSettings();
        var keyCode = event.which;
        if(keyCode == 13) { // Enter
            helper.acceptFirstSuggestion(component, settings);
        } else if(keyCode == 40) { //Arrow down
            helper.selectNextSuggestion(component, null, settings);
        } else if(keyCode == 38) { //Arrow up
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // EDQ event listener
    onSuggestionKeyUp : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.onSuggestionKeyUp(component, event, settings);
    },
    // EDQ event listener
    handleResultSelect : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleResultSelect(component, event, settings);
    },
    // EDQ event listener
    onAddressChanged : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleValidationStatus(component, settings);
    },
    // EDQ event listener
    onElementFocusedOut : function (component, event, helper) {
        var settings = helper.getSettings();

        var useHasNotSelectedASuggestion = helper.isNull(event.relatedTarget) || event.relatedTarget.id.indexOf(settings.suggestionIndexClassPrefix) === -1;
        if (useHasNotSelectedASuggestion) {
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // EDQ added ------------- Handler for second address --------------
    handleOtherSearchChange : function(component, event, helper) {
        var settings = helper.getOtherSettings();
        helper.handleSearchChange(component, event, settings);
        helper.handleValidationStatus(component, settings);
    },
    // EDQ event listener
    handleOtherSuggestionNavigation :  function(component, event, helper) {
        var settings = helper.getOtherSettings();
        var keyCode = event.which;
        if(keyCode == 13) { // Enter
            helper.acceptFirstSuggestion(component, settings);
        } else if(keyCode == 40) { //Arrow down
            helper.selectNextSuggestion(component, null, settings);
        } else if(keyCode == 38) { //Arrow up
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // EDQ event listener
    onOtherSuggestionKeyUp : function(component, event, helper) {
        var settings = helper.getOtherSettings();
        helper.onSuggestionKeyUp(component, event, settings);
    },
    // EDQ event listener
    handleOtherResultSelect: function(component, event, helper) {
        var settings = helper.getOtherSettings()
        helper.handleResultSelect(component, event, settings);
    },
    // EDQ event listener 
    onOtherAddressChanged : function(component, event, helper) {
        var settings = helper.getOtherSettings();
        helper.handleValidationStatus(component, settings);
    },
    // EDQ event listener
    onOtherElementFocusedOut : function (component, event, helper) {
        var settings = helper.getOtherSettings();

        var useHasNotSelectedASuggestion = helper.isNull(event.relatedTarget) || event.relatedTarget.id.indexOf(settings.suggestionIndexClassPrefix) === -1;
        if (useHasNotSelectedASuggestion) {
            helper.hideAndRemoveSuggestions(component, settings);
        }
    }
})