({
    myaction : function(component, event, helper) {
        var actiondata = component.get("c.rsalesorder");	
        
        actiondata.setParams({ AId : component.get("v.recordId")}); 
        
        component.set("v.recordvalue" , component.get("v.recordId"));
        //alert('recordvalue1--'+component.get("v.recordvalue"));
        
        actiondata.setCallback(this, function(response){
            // alert("setcallBack");    
            var state = response.getState();
            //alert("state "+state);
            if (state === "SUCCESS") {
                // alert("success");
                //alert("related SO: " + JSON.stringify(response.getReturnValue())); 
                var Responsedata = response.getReturnValue(); 	
                console.log("SalesOrder" ,response.getReturnValue());
                component.set("v.SalesOrder", Responsedata); 	 				  
                
                component.set("v.isSoitem", 'True' );
        		//alert('isSoitem--'+component.get("v.isSoitem"));
            }
            else if (response.getState() === "ERROR") {
                alert("error");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMsg", errors[0].message );
                    }
                }else {
                    component.set("v.errorMsg", 'Request Failed!' );
                }
            }   
            
        });
        
        $A.enqueueAction(actiondata);        
        
    },
    
   Addressone : function(component, event, helper) {
       helper.finalone(component,event,'address');
    }, 
    Contactone : function(component, event, helper) {
       helper.finalone(component,event,'contact');
    }, 
    mischargeone : function(component, event, helper) {
       helper.finalone(component,event,'mischarge');
    }, 
  
    handleClick : function(component, event, helper) {
     /*   var navEvent = $A.get("e.force:navigateToSObject");
        alert("enter");
        
           
            navEvent.setParams({
                  recordId:component.get("v.SalesOrder").Id,


                  slideDevName: "detail"
            });
            navEvent.fire(); */
     window.location='/'+component.get("v.SalesOrder").Ids+'';
    }


    
})