({
	createSignatureObj : function(cmp, helper) {
       // Load all contact data
       var action = cmp.get("c.createSignatureObj");
       var recId = cmp.get("v.guestId");
       var thdresponse = cmp.get("v.tresholdenabledresponse");
       var toastErrorHandler = cmp.find('toastErrorHandler');
       if ($A.util.isEmpty(recId) || $A.util.isUndefined(recId) ) 
           return;
                       
       action.setParams({ "personAccId"  : recId});
		
       action.setCallback(this, function(response) {
           
           toastErrorHandler.handleResponse(
           		response,
                function(response){
                    
                    cmp.set("v.tcSigId", response.getReturnValue().Id);
                    cmp.set("v.signObj", response.getReturnValue());
                },
               function(response, message){ // report failure
                   helper.showToast("error", 'Cart Load Error', cmp, message);
               }
           )
       });
       $A.enqueueAction(action);
    }, 
    goForward : function(cmp, helper) {
       // Load all contact data
       var action = cmp.get("c.checkForSignature");

       // Srinivas var recId = cmp.get("v.sigId");
       var recId = cmp.get("v.tcSigId");
       var toastErrorHandler = cmp.find('toastErrorHandler');
       if ($A.util.isEmpty(recId) || $A.util.isUndefined(recId) )
           return;
       
       //Srinivas action.setParams({ "sigId"  : recId });
       action.setParams({ "tcSigId"  : recId });     
      
       action.setCallback(this, function(response) {
           
           var state = response.getState();
           toastErrorHandler.handleResponse(
               response, // handle failure
               function(response){
                    //if (state === "SUCCESS") {
                       var attachmentFound = response.getReturnValue();
                       
                       if (attachmentFound == true) {
                           //cmp.getEvent("NotifyParentFinishedSign").fire();
                           cmp.set("v.showTsAndCs", false);
                           cmp.set("v.showTextOptInModal", true);
                           cmp.set("v.showCustomerOrderAcceptance", false);
                           helper.TextOptInInit(cmp, helper);
                       }
                    },
                   function(response, message){ // report failure
                       helper.showToast("error", 'Cart Load Error', cmp, message);
                   }
            )
       });
       $A.enqueueAction(action);
    },
    
    TextOptInInit : function(cmp, helper)
    {
        var action = cmp.get("c.getOpty");
        action.setParams({
            "personAccountId" : cmp.get("v.guestId"),
            "opportunityId" : cmp.get("v.oppId")
        });
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var obj = response.getReturnValue();
                cmp.set('v.guestObj',obj.guestRecord);
                cmp.set("v.oppty", obj.optyRecord);
                cmp.set('v.SigObjId',obj.signId);
                cmp.set("v.showSignature", obj.optyRecord.Text_Message_Opt_In__c);
                //REQ-455 - Text Opt In
                cmp.set("v.entityFullName", obj.entityFullName);
            }
            else if (state === "INCOMPLETE") {
     			helper.handleError(null, true);
            }
            else if (state === "ERROR") {
                helper.handleError(response.getError(), false);
            }
        });
        $A.enqueueAction(action);
    },
    
    acceptTextOptIn : function (cmp, helper)
    {
        var legalContent = $(".legalVerbiage" ).html();
        var action = cmp.get("c.acceptOptions");
        action.setParams({ 
            "personAccountId"  : cmp.get("v.guestId"),
            "opportunityId" : cmp.get("v.oppId"),
            "eSignId" : cmp.get("v.SigObjId"),
            "promotionalEmails" : cmp.get("v.oppty.Survey_Opt_In__c"),
            "textOptIn" : cmp.get("v.oppty.Text_Message_Opt_In__c"),
            "timeWhenCheckedTextOptIn" : cmp.get("v.checkedTime"),
            //"legalContent":component.find("legalContent").getElement().innerHTML
            "legalContent": legalContent
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() == 'Success') {
                    //component.getEvent("NotifyParentTextOptInComplete").fire();
                    cmp.set("v.showTsAndCs", false);
                    cmp.set("v.showTextOptInModal", false);
                    cmp.set("v.showCustomerOrderAcceptance", true);
                    helper.coaSignatureObj(cmp,helper);
                }
            }
            else if (state === "INCOMPLETE") {
                helper.handleError(null, true);
            }
            else if (state === "ERROR") {
                helper.handleError(response.getError(), false);
            }
        });
        $A.enqueueAction(action);
    },
   
    coaSignatureObj : function(cmp, helper) {
       // Load all contact data
       var action = cmp.get("c.coaSignatureObj");

       var recId = cmp.get("v.guestId");
       var toastErrorHandler = cmp.find('toastErrorHandler');
       if ($A.util.isEmpty(recId) || $A.util.isUndefined(recId) )
           return;
       
       action.setParams({ "personAccId"  : recId });

       action.setCallback(this, function(response) {
           toastErrorHandler.handleResponse(
               response, // handle failure
               function(response){ // report success and navigate to contact record
                   cmp.set("v.coaSigId", response.getReturnValue());
               },
               function(response, message){ // report failure
                   helper.showToast("error", 'Error in loading signature', cmp, 'Error in loading Customer Order Acceptance signature. Please contact a system administrator with the following error message'+ message);
               }
           )
       });
       $A.enqueueAction(action);
    }, 
    
    coaGoForward : function(cmp, helper) {
       // Load all contact data
       var action = cmp.get("c.coaCheckForSignature");

       var recId = cmp.get("v.coaSigId");
       var toastErrorHandler = cmp.find('toastErrorHandler');
       if ($A.util.isEmpty(recId) || $A.util.isUndefined(recId) )
           return;
       
       action.setParams({ "coaSigId"  : recId });

       action.setCallback(this, function(response) {
           toastErrorHandler.handleResponse(
               response, // handle failure
               function(response){ // report success and navigate to contact record
                   var attachmentFound = response.getReturnValue();
                   if (attachmentFound == true) {
                       cmp.getEvent("NotifyParentFinishedSign").fire();
                   }
               },
               function(response, message){ // report failure
                   helper.showToast("error", 'Error in saving signature', cmp, 'Error in saving Customer Order Acceptance signature. Please contact a system administrator with the following error message'+ message);
               }
           )
       });
       $A.enqueueAction(action);
    },
    
    handleError : function(errors, isIncomplete) {
        var error = "Unknown error";
        if (errors) {
            if (errors[0] && errors[0].message) {
                error =  errors[0].message;
            }
        }
        if(isIncomplete) {
            error = 'could not complete request, please check back later';
        }
        this.showToast('Error', '', error);
    },
    showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
        });
        toastEvent.fire();
    } 
})