({
	doInit : function(component, event, helper) {
        
        alert('Final TS ANDCS----');
        component.set("v.showTsAndCs", true);
        component.set("v.showTextOptInModal", false);
        component.set("v.showCustomerOrderAcceptance", false);
        helper.createSignatureObj(component, helper); 
    }, 
    cancelDialog : function(component, helper) {
        component.getEvent("NotifyParentCloseTsAndCs").fire();
    }, 
    goToNext : function(component, event, helper) {
        helper.goForward(component, helper);
       //window.location.reload(true);
    },
    
    //Text Opt in Start
    /*textOptIn : function(component, event, helper) { 
        
        helper.TextOptInInit(component, helper);
	},*/

	onCheckTextOptIn: function(component, event, helper) {
		var promotionalEmailsCmp = component.get("v.oppty.Survey_Opt_In__c");
        var textMessagesCmp = component.get("v.oppty.Text_Message_Opt_In__c");
        if(textMessagesCmp == true) {
            var action = component.get("c.getOrCreateESignature");
            	action.setParams({
                    "personAccountId" : component.get("v.guestId"),
                    "opportunityId" : component.get("v.oppId"),
                    "eSignId" : component.get("v.SigObjId")
                });
            	action.setCallback(this, function(response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    var obj = response.getReturnValue();     
                    component.set('v.SigObjId',obj.signId);
                    component.set("v.checkedTime", obj.checkedTime);
                    component.set('v.showSignature',true);
                }
                else if (state === "INCOMPLETE") {
                    helper.handleError(null, true);
                }
                else if (state === "ERROR") {
                    helper.handleError(response.getError(), false);
                }
            });
            $A.enqueueAction(action);
        }
        else {
            component.set('v.showSignature',false);
        }
		
	},
	accept : function(component, event, helper) {   
        helper.acceptTextOptIn(component, helper);
        
	},
    
    decline : function(component, event, helper) {
        var action = component.get("c.declineOptions");
        action.setParams({
            "personAccountId" : component.get("v.guestId"),
            "opportunityId" : component.get("v.oppId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('state--in--'+state);
                //component.getEvent("NotifyParentCloseAcceptance").fire();
                
                component.set("v.showTsAndCs", false);
                component.set("v.showTextOptInModal", false);
                component.set("v.showCustomerOrderAcceptance", true);
                helper.coaSignatureObj(component,helper);
                alert('state--out--'+state);
                
            }
            else if (state === "INCOMPLETE") {
                helper.handleError(null, true);
            }
            else if (state === "ERROR") {
                helper.handleError(response.getError(), false);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Text Optin End
    
    //Customer Order Accpetance Start
    
    // Method to create signature object
   /* coaStart : function(component, event, helper) {
        helper.coaSignatureObj(component, helper);
    }, */
    
    
    // Method to close Customer Order Acceptance overlay 
    cancelDialog : function(component, helper) {
        component.getEvent("NotifyParentCloseAcceptance").fire();
    }, 
    
    // Method to proceed with placing the order once the customer has signd and accepted
    goToCoaNext : function(component, event, helper) {
        helper.coaGoForward(component, helper);
    },
    
    handleScroll : function(component, event, helper) {
        var scrollCompleted = false;
        $(".coaSignatureWrapper").hide();
        $(".coaTcsContent").scroll(function(){
            var a=$(".coaTcsContent").scrollTop();
            var b=$(".coaTcsContent").innerHeight();
            var c=$(".coaTcsContent")[0].scrollHeight;
            if(!scrollCompleted){
                if(a + b >= c){
                   $(".coaSignatureWrapper").show();
                   scrollCompleted = true;
                }else{
                   $(".coaSignatureWrapper").hide();
                }
            }
        });	
	}
    
    //Customer Order Accpetance End
   
})