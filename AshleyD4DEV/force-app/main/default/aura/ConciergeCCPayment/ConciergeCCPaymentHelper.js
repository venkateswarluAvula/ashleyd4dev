({
    toastMessage : function(type,title,message){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title, 
            message: message,
        });
        toastEvent.fire();
    },
    printCcReceipts:function(component){
        alert('testing......');
        
        //alert('guestIds'+component.get("v.guestId"));
        var paymentInfo = component.get("v.pm");
        //alert('paymentInfo'+JSON.stringify(paymentInfo));
        var action = component.get("c.printCcRece");
         var spinner = component.find("largeSpinner");
        $A.util.toggleClass(spinner, "slds-hide");  
        action.setParams({
            "personAccId"   : component.get("v.guestId"),
            "pmtInfoString" : JSON.stringify(paymentInfo)
        });
        
        action.setCallback(this, function(response){
            
            var state = response.getState();
           // alert('state is nw2 :'+state);
            if (state === "SUCCESS") {
                 var spinner = component.find("largeSpinner");
       			 $A.util.toggleClass(spinner, "slds-hide");  
                var result = response.getReturnValue();
                console.log('result-->'+JSON.stringify(result));
                component.set("v.rrr",result);
                var mess = component.get("v.rrr.ccPrintResponse");
                //alert('mess-->'+mess);
                console.log('return status'+component.get("v.rrr.ccPrintResponse"));
                this.toastMessage('info', '',mess);
               // component.getEvent("NotifyParentPmtAdded").fire();
            }
            else{
               var spinner = component.find("largeSpinner");
       			 $A.util.toggleClass(spinner, "slds-hide");   
                 helper.toastMessage("error","ERROR",state);
            }
            
        });
        $A.enqueueAction(action);
        
    },
    processPayment : function(component,paymentInfo,helper){
        //alert('test12');
        var toastErrorHandler = component.find('toastErrorHandler');
        var action = component.get("c.addPayment");
        action.setParams({
            "personAccId"   : component.get("v.guestId"),
            "pmtInfoString" : JSON.stringify(paymentInfo)
        });
         var spinner = component.find("largeSpinner");
        $A.util.toggleClass(spinner, "slds-hide");    
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var paymentInfo = response.getReturnValue();
                if(paymentInfo.paymentAPIStatus == 'success'){
                    var spinner = component.find("largeSpinner");
                    $A.util.toggleClass(spinner, "slds-hide");    
                    helper.toastMessage("success","","Payment has been applied. Thank you.");
                    var updateAmount = component.getEvent("updateAmount");
                    updateAmount.fire();
                    component.getEvent("NotifyParentPmtAdded").fire();
                }else{
                    helper.toastMessage("error","ERROR",paymentInfo.paymentAPIStatus);
                    var spinner = component.find("largeSpinner");
                    $A.util.toggleClass(spinner, "slds-hide");
                }
            }    
        });
        $A.enqueueAction(action);                       
    },
    removePayment : function(component){
        var paymentInfo = component.get("v.pm");
        var action = component.get("c.removePayment");
        action.setParams({
            "personAccId"   : component.get("v.guestId"),
            "pmtInfoString" : JSON.stringify(paymentInfo)
        });
        var spinner = component.find("largeSpinner");
        $A.util.toggleClass(spinner, "slds-hide");    
        action.setCallback(this, function(response){
            var spinner = component.find("largeSpinner");
            $A.util.toggleClass(spinner, "slds-hide");    
            component.set("v.showVoidConfirmation", false);
            component.getEvent("updateAmount").fire();
            component.getEvent("NotifyParentPmtAdded").fire();
        });
        $A.enqueueAction(action);
    },
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    }
})