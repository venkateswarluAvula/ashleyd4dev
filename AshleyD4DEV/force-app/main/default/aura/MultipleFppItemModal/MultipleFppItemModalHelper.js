({
        showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
        });
        toastEvent.fire();
    },
	fppUpdateItems : function(component,helper){
        
        var fppsku = component.find("fppTypes").get("v.value");
        var seletedItem=new Array();
        var hasError = false;
        var currentItem = component.find("setCurrentItemFpp");
        var currentItemId=  currentItem.get("v.value");
        if(currentItem.get("v.checked")){
            seletedItem.push(currentItemId);
        }
        var allItems =  component.get("v.shoppingCartLineItems");
        var itemTotal=[];
        var totalSize;
        allItems.forEach(function(item){
            totalSize=itemTotal.push(item["item"]);
        });
        if(totalSize>1){
            component.find("setFpp").forEach(function(element){
                var itemId= element.get("v.value");
                if(itemId!=currentItemId ){
                if(element.get("v.checked")){
                    seletedItem.push(itemId);
                }
                }
            });
        }else{
            var itemId= component.find("setFpp").get("v.value");
            if(component.find("setFpp").get("v.checked")){
                seletedItem.push(itemId);
            }  
        }
        if(seletedItem.length==0){
            helper.showToast("error", 'Please Select at least one item.', component,
                             'Please  Select at least one item.');   
            hasError = true;
        }
         if(fppsku=='null'){
            helper.showToast("error", 'Please Select a FPP.', component,
                             'Please Select a FPP.');   
            hasError = true;
        }
      
        if(hasError){
            return;
        }
        var action = component.get("c.multipleFppUpdateCart");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setParams({"fppsku" : fppsku,
                          "lineItemIdList" : seletedItem
                         });
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                response, // handle failure
                function(response){ 
                    var rtnValue = response.getReturnValue();
                    if (rtnValue !== null && rtnValue=='Success') {
                        component.getEvent("NotifyParentCloseFppModal").fire();  
                        var event = component.getEvent("shoppingCartLineItemEvent");
                        event.setParams({"action" : component.get("v.CART_ACTION_UPDATE_FPP_SKU"), "lineItemIds" :seletedItem});
                        event.fire();
                    } else {
                        helper.showToast("error", 'Failed to Remove FPP Items', component,
                                         'Failed to Delete Items.');                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Failed to Remove FPP Items', component,
                                     message);
                }
            )            
        });        
        action.setBackground();
        $A.enqueueAction(action); 
    
        
	},
    removeFppItems : function(component,helper){
        
        var fppsku = '';
	    var seletedItem=new Array();
        var hasError = false;
        var currentItem = component.find("setCurrentItemFpp");
        var currentItemId=  currentItem.get("v.value");
        if(currentItem.get("v.checked")){
            seletedItem.push(currentItemId);
        }
        var allItems =  component.get("v.shoppingCartLineItems");
        var itemTotal=[];
        var totalSize;
        allItems.forEach(function(item){
            totalSize=itemTotal.push(item["item"]);
        });
        if(totalSize>1){
            component.find("setFpp").forEach(function(element){
                var itemId= element.get("v.value");
                if(itemId!=currentItemId ){
                if(element.get("v.checked")){
                    seletedItem.push(itemId);
                }
                }
            });
        }else{
            var itemId= component.find("setFpp").get("v.value");
            if(component.find("setFpp").get("v.checked")){
                seletedItem.push(itemId);
            }  
        }
        if(seletedItem.length==0){
            helper.showToast("error", 'Please Select at least one item.', component,
                             'Please  Select at least one item.');   
            hasError = true;
        }
       
        if(hasError){
            return;
        }
        //alert('seletedItem'+seletedItem);
        console.log('seletedItem'+seletedItem);
        var action = component.get("c.multipleFppUpdateCart");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setParams({"fppsku" : fppsku,
                          "lineItemIdList" : seletedItem
                         });
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                response, // handle failure
                function(response){ 
                    var rtnValue = response.getReturnValue();
                    if (rtnValue !== null && rtnValue=='Success') {
                        component.getEvent("NotifyParentCloseFppModal").fire();  
                        var event = component.getEvent("shoppingCartLineItemEvent");
                        event.setParams({"action" : component.get("v.CART_ACTION_UPDATE_FPP_SKU"), "lineItemIds" :seletedItem});
                        event.fire();
                    } else {
                        helper.showToast("error", 'Failed to remove FPP Items', component,
                                         'Failed to Remove Fpp Items.');                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Failed to Remove FPP Items', component,
                                     message);
                }
            )            
        });        
        action.setBackground();
        $A.enqueueAction(action); 
    
        
	}
		
	
})