public class SalesOrderReportDataController { 
   
    @AuraEnabled
    Public static String SalesOrderHotStatus(String startdate,String enddate,String market){
        SORoutingAuthorization CARAPart= new SORoutingAuthorization();
        String accessTkn = CARAPart.accessTokenData();
        String endpoint = System.label.HotOrdersAPI +'startDate='+startdate+'&endDate='+enddate+'&marketID='+market;
        System.debug('endpoint----' + endpoint);
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        System.debug('apiresponse...' + res.getBody());
        return res.getBody();//resultsmap;
    }
    
    @AuraEnabled
    Public static List<SalesOrderVIPControlWrapper> SalesOrderVIPStatus(String startdate,String enddate,String market,String type){
        SalesOrderVIPControlWrapper ctrl = new SalesOrderVIPControlWrapper();
        SORoutingAuthorization CARAPart= new SORoutingAuthorization();
        String accessTkn = CARAPart.accessTokenData();
        system.debug('accessTkn--'+accessTkn);
        String endpoint =System.label.VIPOrdersAPI + 'marketID='+market+'&startDate='+startdate+'&endDate='+enddate+'&type='+type;
        
        //System.label.VIPOrdersAPI + 'marketID=&startDate=1900-01-01&endDate=1900-01-01&type=0';
        System.debug('endpoint---' + endpoint);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(5000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        System.debug('apiresponse...' + res.getBody());
        
        string s =  res.getBody();//resultsmap;

        List<SalesOrderVIPControlWrapper> vipOrdersList = new List<SalesOrderVIPControlWrapper>();
        list<SalesOrderVIPControlWrapper> responseList = (List<SalesOrderVIPControlWrapper>) JSON.deserialize(s, List<SalesOrderVIPControlWrapper>.class);

        if (responseList.size() > 0) {
            List<string> salesOrderIds = new list<string>();
            for (SalesOrderVIPControlWrapper so:responseList) {
                salesOrderIds.add(so.OrderNumber + ':' + so.SFContactID);
            }
            map<string, Case> openCaseMap = new map<string, Case>();
            map<string, Case> closedCaseMap = new map<string, Case>();
            List<Case> caselist = [Select Id, CaseNumber, Sales_Order__c, IsClosed From Case where Sales_Order__c IN :salesOrderIds Order by Createddate Desc];
            if (caselist.size() > 0) {
                for (Case cs:caselist) {
                    if (cs.IsClosed) {
                        if (closedCaseMap.get(cs.Sales_Order__c) == null) {
                            closedCaseMap.put(cs.Sales_Order__c, cs);
                        }
                    } else {
                        if (openCaseMap.get(cs.Sales_Order__c) == null) {
                            openCaseMap.put(cs.Sales_Order__c, cs);
                        }
                    }
                }
            }

            for (SalesOrderVIPControlWrapper so:responseList) {
                string salesOrderId = so.OrderNumber + ':' + so.SFContactID;
                if (so.Market != null) {
                    so.Market = so.Market.replace('#', '');
                }
                if (openCaseMap.get(salesOrderId) != null) {
                    so.CaseId = openCaseMap.get(salesOrderId).Id;
                    so.CaseNumber = openCaseMap.get(salesOrderId).CaseNumber;
                } else if (closedCaseMap.get(salesOrderId) != null) {
                    so.CaseId = closedCaseMap.get(salesOrderId).Id;
                    so.CaseNumber = closedCaseMap.get(salesOrderId).CaseNumber;
                }
                vipOrdersList.add(so);
            }
        }
        return vipOrdersList;
    }
    
    //ASAP
    @AuraEnabled
    Public static String SalesOrderASAPStatus(String market, String type){
      
        SORoutingAuthorization CARAPart= new SORoutingAuthorization();
        String accessTkn = CARAPart.accessTokenData();
        String endpoint = System.label.ASAPOrderAPI +'&marketID='+market+'&type='+type;
        System.debug('endpoint----' + endpoint);
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        System.debug('apiresponse...' + res.getBody());
        
       //return res.getBody();//resultsmap;
       string s =  res.getBody();
       System.debug('s----------------'+s);
       return s;
        
      //  System.debug('@@@@@s----------------'+s);
      //string sortJsonData = JSONStringParser.parserJsonString(s);
      //system.debug('@@@@sortJsonData ---->'+sortJsonData );
      // return sortJsonData;
       
   }  

//DeliveryConfirmation
    @AuraEnabled
        Public static String SalesOrderDeliveryConfirmationStatus(String startdate,String enddate,String market){
        SORoutingAuthorization CARAPart= new SORoutingAuthorization();
        String accessTkn = CARAPart.accessTokenData();
        String endpoint = System.label.DeliveryConfirmationAPI +'startDate='+startdate+'&endDate='+enddate+'&marketID='+market;
        System.debug('endpoint----' + endpoint);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        System.debug('apiresponse...' + res.getBody());
        
        return res.getBody();//resultsmap;
    }

    @AuraEnabled
    public Static string soConfirmData(String accShipto, string soNumber, integer profitCenter, string trkID, string delDate, Integer rPass, string custOpen, string custClose)
    { 
        string responsebody;
        system.debug('here');
        system.debug('herevals' + accShipto);
        system.debug('herevals' + soNumber );
        system.debug('herevals' + profitCenter ); 
        system.debug('herevals' +  trkID );
        system.debug('herevals' + delDate );
        system.debug('herevals' +  rPass );
        system.debug('custOpen--'+custOpen);
        system.debug('custClose--'+custClose);
        //string custOpen1=custOpen.replace('/','-');
        //system.debug('custOpen1--'+custOpen1);
                
        delDate = delDate.replace('/','-');
        //custOpen = custOpen.replace('/','-');
        //custClose = custClose.replace('/','-');
        //if(accShipto != null && soNumber !=null && profitCenter !=null && trkID !=null && delDate !=null && rPass !=null){
       
            User objUsr = [Select Id, Name, Alias,CommunityNickname from User where Id=:UserInfo.getUserId()];
            String currentUser = objUsr.CommunityNickname;//UserInfo.getLastName()+','+UserInfo.getFirstName();
            String apikeys = System.label.AshleyApigeeApiKey;
            System.Debug('currentUser------' + currentUser);
           
             //SalesOrder__x so = [Select Id, phhSalesOrder__c,phhStoreID__c, ExternalId,phhProfitcenter__c,phhDatePromised__c from SalesOrder__x where Id=:soID];
             String EPoint = System.Label.AshleyApigeeEndpoint+accShipto+'/salesorders/'+soNumber+'/SalesOrderConfirmDelivery?profitcenter='+profitCenter+'&ConfirmedFlag='+true+'&TruckId='+trkID+'&DeliverDate='+delDate+' 00:00:00.000'+'&RoutingPass='+rPass+'&CustomerETAOpen='+custOpen+'.000&CustomerETAClose='+custClose+'.000&profitusername='+currentUser+'&apikey='+apikeys;
             //String EPoint = 'https://stageapigw.ashleyfurniture.com/homestores/8888300-164/salesorders/200467740/SalesOrderConfirmDelivery?profitcenter=23&ConfirmedFlag=true&TruckId=G03&DeliverDate=2019-05-15 00:00:00.000&RoutingPass=1&CustomerETAOpen=2019-05-14 00:00:00.000&CustomerETAClose=2019-05-14 12:45:00.000&profitusername=Ashley&apikey=bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
           
        EPoint = EPoint.replace(' ','%20'); 
            //EPoint = EPoint.replace('/','-');200462350 
            system.debug('EPoint'+EPoint);
            String response;
            
            Http http = new Http();
            HttpRequest httpReq = new HttpRequest();
            HttpResponse httpResp = new HttpResponse();
            httpReq.setHeader('apikey',System.label.AshleyApigeeApiKey);
            //httpReq.setHeader('Authorization', 'Bearer '+accessTkn);
            httpReq.setHeader('Content-Type', 'application/json');
            httpReq.setHeader('Accept' ,'application/json');
            httpReq.setMethod('PUT'); 
            httpReq.setTimeout(120000);
            httpReq.setBody('{}');
            httpReq.setEndpoint(EPoint);
            
            httpResp = http.send(httpReq); 
             System.debug('httpResp-------Hstry--------'+httpResp);
            if (httpResp.getStatusCode() == 200) {
                System.debug('httpResp-------Hstryenter--------'+httpResp.getBody());
                response = httpResp.getBody();
                System.debug('response--------'+response);
                if(response.contains('successfully'))
                {
                 responsebody = response;   
                } 
            }
        else if (httpResp.getStatusCode() == 404) {
            response = httpResp.getBody();
            if (response.contains('TransportationOrderId')){
            throw new AuraHandledException('TransportationOrderId not found for SalesOrderNumber:'+soNumber+' and ProfitCenter: '+profitCenter);
        }
            else{
                throw new AuraHandledException('The request could not be processed');
            }
        }
        else{
            
            system.debug('httpResponse--'+httpResp.getBody());
        }
        return responsebody;
    }
    
    @AuraEnabled
    public static String searchAccountsSOQL(String searchString) {
        Salesorder__x Order = new Salesorder__x();
        Order  = [SELECT Id,ExternalId FROM SalesOrder__x WHERE ExternalId =:searchString];
        return Order.Id;
    }
   
    @AuraEnabled
    public static List<HotOrders__c> marketvalues(){
        List<HotOrders__c> options = new List<HotOrders__c>();
        options = HotOrders__c.getAll().values();
        System.debug('values----' +options);
        return options;
    }
}