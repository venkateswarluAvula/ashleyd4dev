global class SalesOrderVIPControl {
    @AuraEnabled
    Public static List<SalesOrderVIPControlWrapper> SalesOrderVIPStatus(String startdate,String enddate,String market,String type){
        SalesOrderVIPControlWrapper ctrl = new SalesOrderVIPControlWrapper();
        SORoutingAuthorization CARAPart= new SORoutingAuthorization();
        String accessTkn = CARAPart.accessTokenData();
        system.debug('accessTkn--'+accessTkn);
        String endpoint =System.label.VIPOrdersAPI + 'marketID='+market+'&startDate='+startdate+'&endDate='+enddate+'&type='+type;
        
        //System.label.VIPOrdersAPI + 'marketID=&startDate=1900-01-01&endDate=1900-01-01&type=0';
        System.debug('endpoint---' + endpoint);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(5000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        System.debug('apiresponse...' + res.getBody());
        
        string s =  res.getBody();//resultsmap;

        List<SalesOrderVIPControlWrapper> vipOrdersList = new List<SalesOrderVIPControlWrapper>();
        list<SalesOrderVIPControlWrapper> responseList = (List<SalesOrderVIPControlWrapper>) JSON.deserialize(s, List<SalesOrderVIPControlWrapper>.class);

        if (responseList.size() > 0) {
            List<string> salesOrderIds = new list<string>();
            for (SalesOrderVIPControlWrapper so:responseList) {
                salesOrderIds.add(so.OrderNumber + ':' + so.SFContactID);
            }
            map<string, Case> openCaseMap = new map<string, Case>();
            map<string, Case> closedCaseMap = new map<string, Case>();
            List<Case> caselist = [Select Id, CaseNumber, Sales_Order__c, IsClosed From Case where Sales_Order__c IN :salesOrderIds Order by Createddate Desc];
            if (caselist.size() > 0) {
                for (Case cs:caselist) {
                    if (cs.IsClosed) {
                        if (closedCaseMap.get(cs.Sales_Order__c) == null) {
                            closedCaseMap.put(cs.Sales_Order__c, cs);
                        }
                    } else {
                        if (openCaseMap.get(cs.Sales_Order__c) == null) {
                            openCaseMap.put(cs.Sales_Order__c, cs);
                        }
                    }
                }
            }

            for (SalesOrderVIPControlWrapper so:responseList) {
                string salesOrderId = so.OrderNumber + ':' + so.SFContactID;
                if (so.Market != null) {
                    so.Market = so.Market.replace('#', '');
                }
                if (openCaseMap.get(salesOrderId) != null) {
                    so.CaseId = openCaseMap.get(salesOrderId).Id;
                    so.CaseNumber = openCaseMap.get(salesOrderId).CaseNumber;
                } else if (closedCaseMap.get(salesOrderId) != null) {
                    so.CaseId = closedCaseMap.get(salesOrderId).Id;
                    so.CaseNumber = closedCaseMap.get(salesOrderId).CaseNumber;
                }
                vipOrdersList.add(so);
            }
        }
        return vipOrdersList;
    }
    
    @AuraEnabled
    public static String searchAccountsSOQL(String searchString) {
        Salesorder__x Order = new Salesorder__x();
        Order = CaseHelper.getSalesOrderInfo(searchString);
        //Order = [SELECT Id,ExternalId FROM SalesOrder__x WHERE ExternalId =:searchString];
        return Order.Id;
    }
    @AuraEnabled
    public static List<HotOrders__c> marketvalues(){
        List<HotOrders__c> options = new List<HotOrders__c>();
        options = HotOrders__c.getAll().values();
        System.debug('values' +options);
        return options;
    }
}