public class SignatureController {
    public static final String CALIFORNIA_LOOKUP_SIGNATURE_TYPE ='California Lookup';
    /****** REQ-299| 2/21/2017 | JoJo Zhao */
    @AuraEnabled
    public static Id saveCapturedSignature(){
        //Create Electronic_Signature__c  record
         Electronic_Signature__c escObj = new Electronic_Signature__c();
           
        try{
            escObj.Logged_In_User__c=UserInfo.getUserId();
            escObj.Signature_Date_Time__c=system.now();
            escObj.Signature_Type__c=CALIFORNIA_LOOKUP_SIGNATURE_TYPE;
            //esObj.ObjectID__c = ;//no opp now.
            //esObj.MAC_ID__c='';//dono how to get it
            insert escObj;
        } catch (Exception e) {  new ErrorLogController().createLog(new ErrorLogController.Log('SignatureController', 'saveCapturedSignature','Save Electronic Signature Exception: ' + e.getMessage() +' Stack Trace: ' + e.getStackTraceString())
               
           // system.debug('Fail to save Electronic Signature, exception ' + e.getMessage());
            //new ErrorLogController().createLog(new ErrorLogController.Log('SignatureController', 'saveCapturedSignature','Save Electronic Signature Exception: ' + e.getMessage() +' Stack Trace: ' + e.getStackTraceString()
               
            );
            throw new AuraHandledException('Get Error when Save Electronic Signature, Exception:'+e.getMessage());
            
        }
        if(escObj!=null && escObj.id!=null){
            //Attach signature pic to Electronic_Signature__c  record
            saveSignatureasAttachment(escObj.Id);
            return escObj.id;
        }
        return null;
    }
     @AuraEnabled
    public static List<Id> getCaliforniaSignatureList() {
        List<Electronic_Signature__c> escList = [ Select Id from Electronic_Signature__c where AccountId__c=null
                                                 and Logged_In_User__c = :UserInfo.getUserId() 
                                                 and Signature_Type__c=:CALIFORNIA_LOOKUP_SIGNATURE_TYPE 
                                                 and Signature_Date_Time__c=LAST_N_DAYS:1 ];
        Set<Id> escIdSet = new Set<Id>();
        for(Electronic_Signature__c esc : escList){
            escIdSet.add(esc.Id);
        }
        List<Attachment> attachmentList = [select Id, Body, ParentId from  Attachment where ParentId in :escIdSet];
         List<Id> attachmentIdSet = new List<Id>();
       for(Attachment attach : attachmentList){
            attachmentIdSet.add(attach.Id);
        }
        return attachmentIdSet;
    }
    @AuraEnabled
    public static Boolean checkSignatureSaved(){
        
        List<ContentVersion> SignatureVersion = [SELECT Id,ContentDocumentId,FileExtension,Title,VersionData FROM ContentVersion where OwnerId=:UserInfo.getUserId() and ContentDocument.Title =: 'Signature.png'];
        if(SignatureVersion!=null && SignatureVersion.size()>0){
            return true;
        }else{
            return false;
        }
    }
     @AuraEnabled
     public static Id saveSignatureToCustomer(String picId, String customerId){
         
         Attachment picAttach = [select Id, Body, ParentId from  Attachment where Id = :picId];
        
         Electronic_Signature__c escObj = [Select Id, AccountId__c from Electronic_Signature__c 
                                           where Id = :picAttach.ParentId
                                          and Signature_Type__c=:CALIFORNIA_LOOKUP_SIGNATURE_TYPE];
           
        try{
            escObj.AccountId__c=customerId;
            update escObj;
        } catch (Exception e) {new ErrorLogController().createLog( new ErrorLogController.Log('SignatureController', 'saveSignatureToCustomer','Save Electronic Signature to Customer Exception: ' + e.getMessage() +' Stack Trace: ' + e.getStackTraceString()
                )
            //system.debug('Fail to save Electronic Signature to Customer, exception ' + e.getMessage());
            
            );
            throw new AuraHandledException('Get Error when Save Electronic Signature to Customer, Exception:'+e.getMessage());
            
        }
         return customerId;
    }
    
    private static void saveSignatureasAttachment(Id recId){
        List<ContentVersion> SignatureVersion = [SELECT Id,ContentDocumentId,FileExtension,Title,VersionData FROM ContentVersion where OwnerId=:UserInfo.getUserId() and ContentDocument.Title =: 'Signature.png' ORDER BY CreatedDate DESC];
        Attachment att = new Attachment();
        att.ParentId = recId;
        att.Body = SignatureVersion[0].VersionData;
        att.Name = 'SignatureConfirmation-'+ System.now().format('yyyy_MM_dd_hh_mm_ss') + '..png';
        insert att;
        
        //now delete the ContentDocument
        
        Delete [Select Id  from ContentDocument where Id=:SignatureVersion[0].ContentDocumentId];
    }
    
    //used in QuickAction component to navigate based on market code(California Check)
    @AuraEnabled
    static public Boolean isCaliforniaStore(){  
        StoreInfoWrapper si = StoreInfo.getStoreInfo();
        
        Boolean showSignatureCapture = false;
        system.debug('si-->'+si);
        if(si.marketCode=='SLF'){
            showSignatureCapture=true;
        }
        return showSignatureCapture;
    }
        //Using For Manually Add Product  
    @AuraEnabled
    public static Map<String, Object> getProductDetailWithSKU(Shopping_cart_line_item__c lineItem,string MultiLineType){
        List<Shopping_cart_line_item__c> lstLineItems = new  List<Shopping_cart_line_item__c>();
        //system.debug('MultiLineType'+MultiLineType);
        //REQ-438,  add Product_Title__c for Discount Modal product name column show for items list
        for (Opportunity master : [Select (Select Id, WarrantySku__c,  Product_Title__c, Product_SKU__c, Discount__c,Discount_Price__c,DiscountType__c,
                                           Flat_Discounted_Amount__c,Flat_Discounted_Amount_On_Total__c,Last_Price__c,List_Price__c, Average_Cost__c, Quantity__c,Extended_Price__c,ItemType__c,
                                           Opportunity__c,Delivery_Mode__c,Discount_Reason_Code__c,Discount_Status__c, Estimated_Tax__c
                                           from  Shopping_cart_line_items__r WHERE Product_SKU__c =:lineItem.Product_SKU__c) 
                                   From Opportunity where 
                                   StageName != 'Closed Won' 
                                   and StageName != 'Closed Lost'
                                   and StageName != 'Sale Suspended'
                                   and createdDate = LAST_N_DAYS:30]) {
                                       
                                       // for (Shopping_cart_line_item__c detail : master.Shopping_cart_line_items__r) {
                                       //     system.debug('detailProduct value is'+detail.Product_SKU__c);
                                       //    system.debug('lineItemProduct value is'+lineItem.Product_SKU__c);
                                       //   if(detail.Product_SKU__c.equals(lineItem.Product_SKU__c)){
                                       //       lstLineItems.add(detail);
                                       //   }
                                       //}
                                   }
        if(lstLineItems !=null && lstLineItems.size() >0){  
        
            Shopping_cart_line_item__c sLineItem = lstLineItems.get(0);
            sLineItem.Quantity__c = sLineItem.Quantity__c + lineItem.Quantity__c;
            system.debug('slineitemquantity'+sLineItem.Quantity__c);
            update lstLineItems;
            
            return populateResult('Success', 'Updated', lstLineItems);
        }else{
            
            //Use central addToCart function in MyCustomerController -JoJoToTheyagu
            Map<String, String> objMap = new Map<String, String> ();
            objMap.put('sku', lineItem.Product_SKU__c.toUpperCase());
            objMap.put('productTitle', lineItem.Product_Title__c);
            
            //DEF-0648 getting Price from Api    
            try{
                ProductPriceWrapper prodPrice= new ProductPriceWrapper();
                //exclude *SKU when call Price API
                if(!lineItem.Product_SKU__c.startsWith('*')){                    
                    prodPrice=ProductPriceCmpCtrl.getProductPrice(lineItem.Product_SKU__c.toUpperCase());   
                    system.debug('prodPrice is -->'+prodPrice);
                }
                //Use central addToCart function in MyCustomerController
                //lstLineItems = MyCustomerController.addToCart(recordId,JSON.serialize(objMap),JSON.serialize(prodPrice),Integer.valueOf(lineItem.Quantity__c),MultiLineType);
                //system.debug('lstLineItems is -->'+lstLineItems);
            }catch(AuraHandledException auex){ return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
                //ProductPriceCmpCtrl.getProductPrice throw exception, product is not found in Price API.
                //Because product list will exclude this kind of prod, so exclude this kind of prod from mannual add product process also.
                //return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
            }catch(MyCustomerController.ItemDetailsAPIException ex){ return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
                //MyCustomerController.addToCart throw exception, product is not found in item-details API
                //return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
            } return populateResult('Success', 'Inserted', lstLineItems);
        }
        return null;
    }
    
    @AuraEnabled
    public static Map<String, Object> updateLineItemWithSKU(Id recordId, Shopping_cart_line_item__c lineItem,string MultiLineType){
        List<Shopping_cart_line_item__c> lstLineItems = new  List<Shopping_cart_line_item__c>();
        //system.debug('MultiLineType'+MultiLineType);
        //REQ-438,  add Product_Title__c for Discount Modal product name column show for items list
        for (Opportunity master : [Select (Select Id, WarrantySku__c,  Product_Title__c, Product_SKU__c, Discount__c,Discount_Price__c,DiscountType__c,
                                           Flat_Discounted_Amount__c,Flat_Discounted_Amount_On_Total__c,Last_Price__c,List_Price__c, Average_Cost__c, Quantity__c,Extended_Price__c,ItemType__c,
                                           Opportunity__c,Delivery_Mode__c,Discount_Reason_Code__c,Discount_Status__c, Estimated_Tax__c
                                           from  Shopping_cart_line_items__r ) 
                                   From Opportunity where 
                                   AccountId=:recordId 
                                   and StageName != 'Closed Won' 
                                   and StageName != 'Closed Lost'
                                   and StageName != 'Sale Suspended'
                                   and createdDate = LAST_N_DAYS:30]) {
                                       
                                       // for (Shopping_cart_line_item__c detail : master.Shopping_cart_line_items__r) {
                                       //     system.debug('detailProduct value is'+detail.Product_SKU__c);
                                       //    system.debug('lineItemProduct value is'+lineItem.Product_SKU__c);
                                       //   if(detail.Product_SKU__c.equals(lineItem.Product_SKU__c)){
                                       //       lstLineItems.add(detail);
                                       //   }
                                       //}
                                   }
        if(lstLineItems !=null && lstLineItems.size() >0){  
        
            Shopping_cart_line_item__c sLineItem = lstLineItems.get(0);
            sLineItem.Quantity__c = sLineItem.Quantity__c + lineItem.Quantity__c;
            system.debug('slineitemquantity'+sLineItem.Quantity__c);
            update lstLineItems;
            
            return populateResult('Success', 'Updated', lstLineItems);
        }else{
            
            //Use central addToCart function in MyCustomerController -JoJoToTheyagu
            Map<String, String> objMap = new Map<String, String> ();
            objMap.put('sku', lineItem.Product_SKU__c.toUpperCase());
            objMap.put('productTitle', lineItem.Product_Title__c);
            
            //DEF-0648 getting Price from Api  
            try{
                ProductPriceWrapper prodPrice= new ProductPriceWrapper();
                //exclude *SKU when call Price API
                if(!lineItem.Product_SKU__c.startsWith('*')){                    
                    prodPrice=ProductPriceCmpCtrl.getProductPrice(lineItem.Product_SKU__c.toUpperCase());   
                    system.debug('prodPrice is -->'+prodPrice);
                }
                //Use central addToCart function in MyCustomerController
                lstLineItems = MyCustomerController.addToCart(recordId,JSON.serialize(objMap),JSON.serialize(prodPrice),Integer.valueOf(lineItem.Quantity__c),MultiLineType);
                system.debug('lstLineItems is -->'+lstLineItems);
            }catch(AuraHandledException auex){ return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
                //ProductPriceCmpCtrl.getProductPrice throw exception, product is not found in Price API.
                //Because product list will exclude this kind of prod, so exclude this kind of prod from mannual add product process also.
                //return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
            }catch(MyCustomerController.ItemDetailsAPIException ex){ return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
                //MyCustomerController.addToCart throw exception, product is not found in item-details API
                //return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
            } return populateResult('Success', 'Inserted', lstLineItems);
        }
        return null;
    }
    private static Map<String, Object> populateResult(String status, String action, List<Shopping_cart_line_item__c> lineItems) {
        Set<Id> lineItemIds = new Set<Id>();
        for(Shopping_cart_line_item__c lineItem : lineItems) {  lineItemIds.add(lineItem.Id);
            
        }
       return new Map<String, Object> {'status' => status, 'msg' => action, 'lineItemIds' => lineItemIds};
  }
}