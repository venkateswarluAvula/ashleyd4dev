public without sharing class CaseTriggerHandler {
    
    public static final string CASE_SUB_TYPE_DAMAGE = 'Damage';
    public static final string CASE_ORIGIN_EMAIL = 'Email';
    public static final string CASE_STATUS_CLOSED = 'Closed in Salesforce';
    
    public static final string CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME = 'Service_Request';
    public static final string ASHCOMM_ORDER_MARKET_NAME = 'ASHCOMM';
    public static final Set<String> CASE_SUB_TYPES_FOR_STRIKE_COUNT_CHANGE;
    
    static{
        //initialize case sub types
        CASE_SUB_TYPES_FOR_STRIKE_COUNT_CHANGE = new Set<String>();
        for(Case_Sub_Types_for_Strike_Counter__mdt cst: [Select Case_Sub_Type__c from Case_Sub_Types_for_Strike_Counter__mdt]){
            CASE_SUB_TYPES_FOR_STRIKE_COUNT_CHANGE.add(cst.Case_Sub_Type__c.toUppercase());
        }
    }
    
    public static void handleBeforeInsert(List<Case> newCases){
        closeConsumerAffairsCases(newCases);
        updateCaseStrikeCounter(newCases, null);
        copyTempSalesOrderToSalesOrder(newCases);
        newCaseNPSInsert(newCases);
        newCaseSubjectInsertUpdate(newCases, null);
        newCaseTypeInsertUpdate(newCases,null);
        updateCaseStatusbeforeInsert(newCases);
    }
    
    public static void handleAfterInsert(Map<Id,Case> newCaseMap){
        //updateAutoPopulationFields(newCaseMap.values());
        //mergeDuplicateAddresses(newCaseMap.values());
        updateCaseAddress(newCaseMap.values());
        updatePersonAccountStrikerCounter(newCaseMap.values(), null);
        //updateSalesOrderLookup(newCaseMap.values(), null);
    }
    
    public static void handleBeforeUpdate(Map<Id,Case> newCaseMap, Map<Id,Case> oldCaseMap){
        system.debug('Before Update: ' + newCaseMap);
        updateCaseStatus(newCaseMap.values(), oldCaseMap);
        newCaseTypeInsertUpdate(newCaseMap.values(),oldCaseMap);
        newCaseSubjectInsertUpdate(newCaseMap.values(), oldCaseMap);
        updateCaseStrikeCounter(newCaseMap.values(), oldCaseMap);
        updateSuppliedEmailAndCaseStatus(newCaseMap.values(), oldCaseMap);
    }
    
    public static void handleAfterUpdate(Map<Id,Case> newCaseMap, Map<Id,Case> oldCaseMap){
        system.debug('After Update: ' + newCaseMap);
        // updateAutoPopulationFields(newCasemap.values());
        updatePersonAccountStrikerCounter(newCaseMap.values(), oldCaseMap);
        if (CaseHelper.recursiveCount == 0) {
            ++CaseHelper.recursiveCount;
            updateCaseAddress(newCaseMap.values());
            NPSUpdate(newCaseMap, oldCaseMap);
        }
        //if(!System.isFuture() && !System.isBatch()){
        //updateSalesOrderLookup(newCaseMap.values(), oldCaseMap);
        //}
    }
    
    //Added to auto-populate "Subject" based on the Type/Sub-Type selection
    private static void newCaseSubjectInsertUpdate(List<Case> cases, Map<Id,Case> oldCaseMap){
        for(Case c: cases){
            if(c.Sub_Type__c != null && c.Type!=null){
                //new cases
                if(oldCaseMap == null){
                    c.Subject = c.Type + ' : ' + c.Sub_Type__c;
                }
                //updated cases
                else{
                    Case oldCase = oldCaseMap.get(c.Id);
                    if((c.Sub_Type__c != oldCase.Sub_Type__c) || (c.Type != oldCase.Type)){
                        c.Subject = c.Type + ' : ' + c.Sub_Type__c;
                    }
                }
            }
        }
    }
    
    //Ended for auto-populate of Subject
    
    //Added to update Type/Sub-Type based on the selection
    
    private static void newCaseTypeInsertUpdate(List<Case> cases,Map<Id,Case> oldCaseMap){
        //266261
        // Get the Queue Names from the MetaData "Special_Processing_Map__mdt"
        list<Special_Processing_Map__mdt> queueNameFromMdt = [SELECT Special_Processing__c,Market__c,Queue_Name__c FROM Special_Processing_Map__mdt];
        
        map<String, String> splAccMap = new map<String, String>();
        Map<String,String> quIdNameMap = new Map<String,String>();
        for (Special_Processing_Map__mdt typeConfig:queueNameFromMdt) {
            
            splAccMap.put(typeConfig.Special_Processing__c+'%%'+typeConfig.Market__c,typeConfig.Queue_Name__c);
        }
        
        if (splAccMap.size() > 0) {
            list<QueueSObject> queueList = [select queue.id, queue.name from queueSObject where queue.name IN:splAccMap.values()];
            if (queueList.size() > 0) {
                for (QueueSObject que:queueList) {
                    quIdNameMap.put(que.queue.name, que.queue.id);
                }
            }
        }
        //get the type, sub-type details to set the status and queue fields
        
        //Added to get the queues for respective type and sub types selection
        
        list<Type_SubType_Map__mdt> typeSubtypelist = CaseHelper.getTypeSubType();
        map<string, string> queueMaptype = new map<string, string>();
        list<string> queuetypelist = new list<string>();
        for (Type_SubType_Map__mdt typeConfig:typeSubtypelist) {
            queuetypelist.add(typeConfig.Queue_Name__c);
        }
        
        if (queuetypelist.size() > 0) {
            list<QueueSObject> queueList = [select queue.id, queue.name from queueSObject where queue.name IN:queuetypelist];
            if (queueList.size() > 0) {
                for (QueueSObject que:queueList) {
                    queueMaptype.put(que.queue.name, que.queue.id);
                }
            }
        }
        
        system.debug('queueMaptype---'+queueMaptype);
        
        //Ended for type and sub type queues
        
        //get all the market configuration details based on market value and routing queue name
        list<Market_Configuration__mdt> marketConfigList = CaseHelper.getMarketConfig();
        map<string, string> marketMap = new map<string, string>();
        list<string> queueNameList = new list<string>(); 
        for (Market_Configuration__mdt markConfig:marketConfigList) {
            marketMap.put(markConfig.Fulfiller_Id__c, markConfig.Routing_Queue_Name__c);
            queueNameList.add(markConfig.Routing_Queue_Name__c);
        }
        system.debug('queueNameList--'+queueNameList);
        
        map<string, string> queueMap = new map<string, string>();
        //if the case owner name is set then get the queue name id and set the case owner
        if (queueNameList.size() > 0) {
            list<Group> groupList = [SELECT Id, DeveloperName FROM Group WHERE Type = 'Queue' AND DeveloperName IN :queueNameList];
            if (groupList.size() > 0) {
                for (Group grp:groupList) {
                    queueMap.put(grp.DeveloperName, grp.Id);
                }
            }
        }
        
        for(Case c: cases){
            //if a technician is scheduled in future date for this case then update the owner to a queue based on the market
            date techSchDate;
            if (c.Technician_Schedule_Date__c != null) {
                techSchDate = c.Technician_Schedule_Date__c;
            } else if (c.Tech_Scheduled_Date__c != null) {
                techSchDate = c.Tech_Scheduled_Date__c;
            }
            
            string queueName;
            
            if(c.IsEscalated == true){
                c.OwnerId = System.Label.Escalations_Queue;       // Escalations queue
            }
            
            else if(c.Gift_Card__c == true){
                if (c.IsEscalated == true) {
                    c.OwnerId = System.Label.Escalations_Queue;        // Escalations queue
                }
                else{
                    c.OwnerId = System.Label.Gift_Card_Queue;    // Gift Card queue
                }
            }
            
            else if ((techSchDate != null) && (techSchDate >= date.today())){
                if(string.isNotBlank(c.Legacy_Account_Ship_To__c)) {
                    if (marketMap.get(c.Legacy_Account_Ship_To__c) != null) {
                        queueName = marketMap.get(c.Legacy_Account_Ship_To__c);
                    }
                } else {
                    if (marketMap.get('Blank') != null) {
                        queueName = marketMap.get('Blank');
                    }
                }
                if (string.isNotBlank(queueName) && (queueMap.get(queueName) != null)) {
                                             
                        for(case oldcase :oldCaseMap.values())
                         {
                         if(oldcase.OwnerId == c.OwnerId )
                         {                                                        
                            c.OwnerId = queueMap.get(queueName);
                            c.Status = 'Open';
                         }
                         
                     }
                         
                     
                         
                }
            }
            
            else if (string.isBlank(queueName)) {
                //if there is no technician scheduled for this case then enter here
                
                // User Story 322389
                if((c.type == 'Post Delivery') && (c.Sub_Type__c == 'Changed Mind') 
                   && (c.Market__c == 'ASHCOMM')){
                       //new cases
                       if(oldCaseMap == null){
                           c.addError('Ashcomm orders set to Type: Post Delivery and Sub-Type: Changed Mind cannot be resolved immediately. "Resolved?" must be set to "No" ');
                       }
                       //updated cases
                       else{
                           c.Status = 'Closed in Salesforce';
                       }
                   }
                else {
                    //new cases
                    if(oldCaseMap == null){
                        for(Type_SubType_Map__mdt mtd:typeSubtypelist){
                            if(mtd.Type__c == c.Type && mtd.Sub_Type__c == c.Sub_Type__c){
                                if(mtd.Market__c != null){
                                    if(mtd.Market__c == c.Market__c){
                                        if(mtd.Queue_Name__c != null){
                                            c.OwnerId = queueMaptype.get(mtd.Queue_Name__c);
                                        }
                                    }
                                }
                                else{
                                    if(mtd.Queue_Name__c != null){
                                        c.OwnerId = queueMaptype.get(mtd.Queue_Name__c);
                                    }
                                }
                            }
                        }
                    }
                    //updated cases
                    else{
                        for(Type_SubType_Map__mdt mtd:typeSubtypelist){
                            if(mtd.Type__c == c.Type && mtd.Sub_Type__c == c.Sub_Type__c){
                                if(mtd.Market__c != null){
                                    if(mtd.Market__c == c.Market__c){
                                        if(mtd.Queue_Name__c != null){
                                            if(oldCaseMap.get(c.id).Type == c.Type && oldCaseMap.get(c.Id).Sub_Type__c == c.Sub_Type__c){
                                                if(c.OwnerId != queueMaptype.get(mtd.Queue_Name__c)){
                                                    
                                                }  
                                            }
                                            else{
                                                c.OwnerId = queueMaptype.get(mtd.Queue_Name__c);
                                            }                                        
                                        }
                                    }
                                }
                                else{
                                    system.debug('old sub type---'+oldCaseMap.get(c.Id).Sub_Type__c);
                                    if(mtd.Queue_Name__c != null){
                                        if(oldCaseMap.get(c.id).Type == c.Type && oldCaseMap.get(c.Id).Sub_Type__c == c.Sub_Type__c){
                                                if(c.OwnerId != queueMaptype.get(mtd.Queue_Name__c)){
                                                    
                                                }  
                                            }
                                        else{
                                            c.OwnerId = queueMaptype.get(mtd.Queue_Name__c);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //*266261
                for(Special_Processing_Map__mdt md:queueNameFromMdt){
                    if(md.Special_Processing__c == c.Special_Processing__c && md.Market__c == c.Market__c){
                        c.OwnerId = quIdNameMap.get(splAccMap.get(c.Special_Processing__c+'%%'+c.Market__c));  
                    }
                } 
            }
        }
    }
    
    //Ended for Type/Sub-Type
    
    //Added by Vamsi for NPS checkbox update
    private static void NPSUpdate(Map<Id, Case> newMap, Map<Id, Case> oldMap) {
        list<string> accIdList = new list<string>();
        Map<Id, Case> caseSurveyMap = new Map<Id, Case>();
        for(Id caseId : newMap.keySet()) {
            Case newCase =  (case) newMap.get(caseId);
            Case oldCase =  (case) oldMap.get(caseId);
            caseSurveyMap.put(newCase.AccountId, newCase);
            if (newCase.Survey_Opt_In__c != oldCase.Survey_Opt_In__c) {
                accIdList.add(newCase.AccountId);
            }
        }
        system.debug('accIdList: ' + accIdList);
        
        if (accIdList.size() > 0) {
            Map<Id, Account> accountMap = new Map<Id, Account>();
            // if the case survey opt in/out value is changed for a case then update the respective account
            List<Account> accList = [SELECT Survey_Opt_In__pc FROM Account WHERE Id IN :accIdList];
            for (Account acc : accList) {
                accountMap.put(acc.Id, acc);
            }
            
            list<account> updateAccList = new list<account>();
            for (string accId : accIdList) {
                //if the account survey opt in/out values are different from case then
                if(accountMap.get(accId).Survey_Opt_In__pc != caseSurveyMap.get(accId).Survey_Opt_In__c) {
                    Account accUpd = new Account();
                    accUpd.id = accId;
                    accUpd.Survey_Opt_In__pc = caseSurveyMap.get(accId).Survey_Opt_In__c;
                    updateAccList.add(accUpd);
                } 
            }
            
            if(updateAccList.size() > 0) {
                update updateAccList;
            }
        }
    }
    
    
    //Ended for Survey checkbox update
    
    //Added if a case is created from an account for NPS
    private static void newCaseNPSInsert(List<Case> cases){
        set<String> accids = new Set<String>();
        for (Case c: cases) {
            if (c.AccountId != null) {
                accids.add(c.AccountId);
            }
        }
        
        if (accids.size() > 0) {
            map<String, Account> myaccmap = new Map<String, Account>();
            List<Account> acclist = [Select Id,Survey_Opt_In__pc from Account where ID IN:accids];
            for (Account acc:acclist) {
                myaccmap.put(acc.id,acc);
            }
            for (Case c: cases) {
                if (c.Sales_Order__c == null) {
                    if (myaccmap.get(c.AccountId) != null) {
                        c.Survey_Opt_In__c = myaccmap.get(c.AccountId).Survey_Opt_In__pc;
                    }
                }
            }
        }
    }
    //Ended
    
    /**
* Method to check case Status and Update added by Sudeshna
*/
    private static void updateCaseStatus(List<Case> cases, Map<Id,Case> oldCaseMap){
        for(Case c: cases){
            
            //new cases
            if(oldCaseMap == null){
                c.Status = 'Ready for Review';
            }
            
            /*
Case oldCase = oldCaseMap.get(c.Id);
if(c.Type_of_Resolution__c != 'Stale - 30 Day Limit Reached'){
if (c.Status != oldCase.Status){
if(c.Status == 'Closed In Salesforce' || c.Status == 'Closed'){
c.Type_of_Resolution__c = 'Yes';
}
else{
c.Type_of_Resolution__c = 'No';
}
}
else if (c.Type_of_Resolution__c != oldCase.Type_of_Resolution__c){
if(c.Type_of_Resolution__c == 'Yes'){
if(c.Status != 'Closed'){
c.Status = 'Closed in Salesforce';
}
}
else if(c.Type_of_Resolution__c == 'No'){
if(c.Status != 'Closed' || c.Status != 'Closed in Salesforce'){
c.Status = 'Open';
}
}
}
} 
if(c.Type_of_Resolution__c == '' || c.Type_of_Resolution__c == NULL){
if(c.Status == 'Closed' || c.Status == 'Stale' || c.Status == 'Closed in Salesforce'){
c.Type_of_Resolution__c = 'Yes';
}
else{
c.Type_of_Resolution__c = 'No';
}
}
*/
        }
    }
    
    private static void updateCaseStatusbeforeInsert(List<Case> cases){
        for(Case c: cases){
            if(c.Type_of_Resolution__c == 'Exchange'){
                if(c.Resolution_Notes__c != Null && c.Resolution_Notes__c != ''){
                    List<ContentNote> nte = new List<ContentNote>();
                    List<ContentDocumentLink> lnk = new List<ContentDocumentLink>();
                    ContentNote cnt = new ContentNote();
                    cnt.Content = Blob.valueof(c.Resolution_Notes__c);
                    cnt.Title = 'EE Details';
                    nte.add(cnt);
                    if(nte.size()>0){
                        insert nte;
                    }
                    ContentDocumentLink clnk = new ContentDocumentLink();
                    clnk.LinkedEntityId = c.Id;
                    clnk.ContentDocumentId = nte[0].Id;
                    clnk.ShareType = 'I';
                    lnk.add(clnk);
                    if(nte.size()>0){
                        insert lnk;
                    }
                }
            }
        }
    }
    /* if(c.Type_of_Resolution__c == 'Yes'){
c.Status = 'Closed In Salesforce' ;
}
else if(c.Type_of_Resolution__c == 'No'){
c.Status = 'Ready for Review';
}
else {
c.Type_of_Resolution__c = 'No';
}
}
}
*/
    
    
    /**
* Method to close all consumer affairs cases
* If the Case Origin is Email and Owner is Consumer Affairs then auto close the case 
*/
    private static void closeConsumerAffairsCases(List<Case> cases) {
        for(Case c: cases) {
            if((c.Origin == system.label.Consumer_Affairs_Origin) && !CASE_STATUS_CLOSED.equalsIgnoreCase(c.status) && (c.OwnerId == system.label.Consumer_Affairs_Owner_Id)) {
                c.Type_of_Resolution__c = 'Yes';
                c.status = CASE_STATUS_CLOSED;
            }
        }
    }
    
    
    /**
* Increases the Strike count on a case when a case is created with one of the Sub Types/reschedule reasons that trigger a strike count update
* or when a case is updated and the sub type/reschedule reason is changed to one of the Sub Types/reschedule reasons that trigger a strike count update. 
* The Sub Types/Reschedule reasons that trigger a strike count update are stored in Custom Metadata Types.
*
* @param  cases  list of new/updated cases
* @param  oldCaseMap a map containing versions of updated cases before the latest update, this parameter can be null in case of an insert
*/
    private static void updateCaseStrikeCounter(List<Case> cases, Map<Id,Case> oldCaseMap){
        for(Case c: cases){
            if(c.Sub_Type__c != null && CASE_SUB_TYPES_FOR_STRIKE_COUNT_CHANGE.contains(c.Sub_Type__c.toUppercase())){
                //new cases
                if(oldCaseMap == null){
                    c.Strike__c = (c.Strike__c > 0 ? c.Strike__c : 0 ) + 1;
                }
                //updated cases
                else{
                    Case oldCase = oldCaseMap.get(c.Id);
                    if(c.Sub_Type__c != oldCase.Sub_Type__c){
                        c.Strike__c = (c.Strike__c > 0 ? c.Strike__c : 0 ) + 1;
                    }
                }
            }
        }
    }
    
    /**
* Increases the Strike count on Person Accounts associated to new/updated cases if the Cases have a strike counter which is greater than one.
* On Updated cases, strike counter on the Account is updated only if the strike count on the case was changed in the latest update.
*
* @param  cases  list of new/updated cases
* @param  oldCaseMap a map containing versions of updated cases before the latest update, this parameter can be null in case of an insert
*/
    private static void updatePersonAccountStrikerCounter(List<Case> cases, Map<Id,Case> oldCaseMap){
        Map<Id,Integer> accountStrikeCountMap = new Map<Id,Integer>();
        for(Case c: cases){
            if(c.AccountId != null && c.Strike__c > 0){
                boolean updateAccountStrikeCount = false;
                //new cases
                if(oldCaseMap == null){
                    updateAccountStrikeCount = true;
                }
                //updated cases
                else{
                    Case oldCase = oldCaseMap.get(c.Id);
                    if(c.Strike__c != oldCase.Strike__c){
                        updateAccountStrikeCount = true;
                    }
                }
                
                if(updateAccountStrikeCount){
                    if(accountStrikeCountMap.containsKey(c.AccountId)){
                        accountStrikeCountMap.put(c.AccountId, accountStrikeCountMap.get(c.AccountId)+1);
                    }else{
                        accountStrikeCountMap.put(c.AccountId, 1);
                    }
                }
            }
        }
        
        //update contact strike count
        if(!accountStrikeCountMap.isEmpty()){
            Map<Id,Account> accountsToUpdate = new Map<Id,Account> ([Select Id, Strike_Counter__pc from Account 
                                                                     where Id in:accountStrikeCountMap.keySet() 
                                                                     and IsPersonAccount = true]);
            if(!accountsToUpdate.isEmpty()){
                for(Account a: accountsToUpdate.values()){
                    a.Strike_Counter__pc = (a.Strike_Counter__pc > 0 ? a.Strike_Counter__pc : 0 ) + accountStrikeCountMap.get(a.Id);
                }
                
                update accountsToUpdate.values();
            }
        }
        
    }
    
    
    /*
Update Address
*/
    private static void updateCaseAddress(List<Case> cases){
        List<Id> caseIds = new List<Id>();
        for(Case c: cases){
            if(c.Sales_Order__c == null && c.Legacy_Service_Request_ID__c == null){
                caseIds.add(c.Id);
                System.debug('My case -->'+caseIds);
            }
        }
        try{
            Case casee = [SELECT AccountId,Address_Line_1__c,Address_Line_2__c,Address__c,City__c,Id,Sales_Order__c 
                          FROM Case WHERE Id =:caseIds[0]];
            System.debug('My case -->'+casee);
            if(!caseIds.isEmpty()){
                updateAddress(caseIds);
            }
        }
        catch(exception e){
            System.debug('Tech Scheduling case It is');
        }
    }
    
    /*
Update Address
*/
    private static void updateAddress(List<Id> caseIds){
        List<Case> cases = [SELECT Id, AccountId, Sales_Order__c, Address__c, Case_Email__c, Case_Phone_Number__c
                            FROM Case WHERE Id =:caseIds[0]];
        system.debug('cases-->'+cases[0].Address__c);
        //system.debug('cases-->'+cases[0].Address_Line_1__c);
        Boolean Addressflag =false;
        Address__c myAddress = new Address__c();
        List<Case> casesToUpdate = new List<Case>();
        boolean performUpdate = false;
        for(Case c: cases){
            if(c.Sales_Order__c == null){
                if(c.Address__c == '' || c.Address__c == null){
                    try{
                        system.debug('myAddress initial-->'+c.Address__c);
                        myAddress= [SELECT AccountId__c,Address_Line_1__c,Address_Line_2__c,Address_Type__c,City__c,Country__c,CreatedDate,Id,LastModifiedDate,Preferred__c 
                                    FROM Address__c WHERE AccountId__c =: c.AccountId AND Preferred__c = true 
                                    ORDER BY LastModifiedDate DESC NULLS FIRST LIMIT 1];
                        if (myAddress.Id != null) {
                            c.Address__c = myAddress.Id;
                            performUpdate = true;
                        }
                        Addressflag = true;
                        system.debug('myAddress final-->'+c.Address__c);
                    }
                    catch(Exception e){
                        try{
                            system.debug('myAddress initial without preferred address-->'+c.Address__c);
                            myAddress= [SELECT AccountId__c,Address_Line_1__c,Address_Line_2__c,Address_Type__c,City__c,Country__c,CreatedDate,Id,LastModifiedDate,Preferred__c 
                                        FROM Address__c WHERE AccountId__c =:c.AccountId 
                                        ORDER BY LastModifiedDate DESC NULLS FIRST LIMIT 1];
                            if (myAddress.Id != null) {
                                c.Address__c = myAddress.Id;
                                performUpdate = true;
                            }
                            Addressflag = true;
                            system.debug('myAddress final without preferred address-->'+c.Address__c);
                        }
                        catch(Exception ex){
                            system.debug('myAddress initial without Account address-->'+c.Address__c);
                            c.Address__c = NULL;
                            Addressflag = true;
                            system.debug('myAddress final-->'+c.Address__c);
                        }
                    }
                }
                Account myAccount = [SELECT AccountNumber,Id,PersonEmail,Phone FROM Account WHERE Id =:c.AccountId];
                system.debug('My Account-->'+ myAccount);
                if(String.isBlank(c.Case_Email__c)){
                    system.debug('Email-->'+ myAccount.PersonEmail);
                    if (myAccount.PersonEmail != null) {
                        c.Case_Email__c = myAccount.PersonEmail;
                        performUpdate = true;
                    }
                }
                if(String.isBlank(c.Case_Phone_Number__c) || c.Case_Phone_Number__c == 'Use main phone instead'){
                    system.debug('Phone-->'+ myAccount.Phone);
                    if (myAccount.Phone != null) {
                        c.Case_Phone_Number__c = myAccount.Phone;
                        performUpdate = true;
                    }
                }
            }
            if (performUpdate) {
                casesToUpdate.add(c);
            }
        }
        if(!casesToUpdate.isEmpty()){
            update casesToUpdate;
        }
    }
    
    
    /**
* Copy Contact Email to supplied email and update case status based on custom Close Status field.
*
* @param  cases  list of updated cases
*/
    private static void updateSuppliedEmailAndCaseStatus(List<Case> cases, Map<Id,Case> oldCaseMap){
        system.debug('oldCaseMap--->'+oldCaseMap);
        if (!CaseHelper.recursiveCall) {
            CaseHelper.recursiveCall = true;
            for(Case c: cases){
                if(CASE_ORIGIN_EMAIL.equalsIgnoreCase(c.Origin) && c.SuppliedEmail == null && c.ContactEmail != null && (CASE_STATUS_CLOSED.equalsIgnoreCase(c.status) || c.Close_Case_Quick_Action__c)){
                    c.SuppliedEmail = c.ContactEmail;
                }
                
                if(CASE_STATUS_CLOSED.equalsIgnoreCase(c.status) || c.Close_Case_Quick_Action__c){
                    //check if the type of resolution and resolution notes are not empty then do call out to HOMES to close service request
                    if (/*(c.Type_of_Resolution__c != null) && (c.Resolution_Notes__c != null) &&*/ ((c.Sales_Order__c != null) || (c.Legacy_Account_Ship_To__c != null)) && (c.Legacy_Service_Request_ID__c != null) && !System.isFuture() && !System.isBatch()) {
                        if (((oldCaseMap.get(c.Id) != null) && (oldCaseMap.get(c.Id).status != c.status)) || c.Close_Case_Quick_Action__c){
                            //fire this call out, only if the status changes
                            CaseHelper.closeCaseInHomes(c.Sales_Order__c, c.Legacy_Service_Request_ID__c, c.Legacy_Account_Ship_To__c);
                        }
                    }
                    c.Close_Case_Quick_Action__c = false;
                    system.debug('c.status --'+c.status+'c.Close_Status__c--'+c.Close_Status__c);
                    c.status = c.Close_Status__c;
                }
            }
        }
    }
    
    /**
* Copy The Temp Sales Order field to Sales Order field. This temp Sales Order field is populated as default value when a case
* is created from a sales order as the External Object Lookup can't be initialized.
*
* @param  cases  list of new cases
*/
    private static void copyTempSalesOrderToSalesOrder(List<Case> cases){
        for(Case c: cases){
            if(c.Temp_Sales_Order__c != NUll){
                c.Sales_Order__c = c.Temp_Sales_Order__c;
            }
        }
    }
    
    /**
@deleted Duplicate Address records 
*/
    public static void mergeDuplicateAddresses(List<Case> newCaseLst) {
        List<Account> acctList = new List<Account>();
        List<Address__c> addressAccntList = new List<Address__c>();
        List<Address__c> addressCaseList = new List<Address__c>();
        List<Address__c> addressMerged = new List<Address__c>();
        List<Address__c> lstAccAdrss = new List<Address__c>();
        List<Address__c> lstAdrss = new List<Address__c>();
        List<Case> caseObj = new List<Case>();
        Set<String> setCaseAddress = new Set<String>();
        Set<String> setAcct = new Set<String>();
        Map<String, Address__c> addressMap = new Map<String, Address__c>();
        Map<String, Address__c> addressListMap = new Map<String, Address__c>();
        String dupAddrValues;
        
        for(Case objCse :newCaseLst){
            
            setAcct.add(objCse.AccountId); 
            setCaseAddress.add(objCse.Address__c);
            
        }
        System.debug('setAcct-----'+setAcct);
        System.debug('setCaseAddress-----'+setCaseAddress);
        
        addressAccntList = [Select Id,Name,AccountId__c,AccountId__r.Name,Address_Line_1__c,Address_Line_2__c,Address_Type__c,
                            Address_Validation_Status__c,Address_Validation_Timestamp__c,City__c,Country__c,Geocode__c,
                            Preferred__c, StateList__c, Zip_Code__c 
                            From Address__c Where AccountId__c = '0010n00000vwGd2AAE'];
        //IN :setAcct];
        
        System.debug('addressAccntList-------'+addressAccntList );
        
        /* addressCaseList = [SELECT Id,Name,AccountId__c,AccountId__r.Name,Address_Line_1__c,Address_Line_2__c,Address_Type__c,
Address_Validation_Status__c,Address_Validation_Timestamp__c,City__c,Country__c,Geocode__c,Preferred__c,StateList__c,
StatePL__c,Zip_Code__c From Address__c Where Id IN :setCaseAddress AND AccountId__c IN :acctList ];

System.debug('addressCaseList-------'+addressCaseList);  
*/
        acctList = [Select Id, Name, (SELECT Id,Name,AccountId__c,Address_Line_1__c,Address_Line_2__c,Address_Type__c,
                                      Address_Validation_Status__c,Address_Validation_Timestamp__c,City__c,Country__c,
                                      Geocode__c, Preferred__c, StateList__c, Zip_Code__c From Addresses__r)
                    From Account WHERE Id = '0010n00000vwGd2AAE'];
        //From Account WHERE Id IN :setAcct
        System.debug('acctList-------'+acctList );
        
        for(Account objAcc :acctList){
            lstAccAdrss = objAcc.Addresses__r;
            System.debug('lstAccAdrss-------'+lstAccAdrss);
        }
        
        for(Address__c objAddress : lstAccAdrss){
            addressMap.put(objAddress.AccountId__c, objAddress);
            System.debug('addressMap-------'+addressMap);
        }
        
        Address__c objAddr = new Address__c();
        for(Address__c objAddress : addressAccntList){
            dupAddrValues = objAddress.Address_Line_1__c+','+objAddress.Address_Line_2__c+','+objAddress.City__c+','+objAddress.StateList__c+' '+objAddress.Zip_Code__c;
            if(objAddress.Preferred__c == false){
                if(addressMap.containsKey(dupAddrValues)){
                    objAddr.Id = objAddress.Id;
                    lstAdrss.add(objAddr);
                    System.debug('lstAdrss-------'+lstAdrss);
                }
            }
        }
        System.debug('lstAdrss-------'+lstAdrss.size());
        delete lstAdrss;
    }
}