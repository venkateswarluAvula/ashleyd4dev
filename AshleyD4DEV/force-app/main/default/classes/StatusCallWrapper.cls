public class StatusCallWrapper {
        @AuraEnabled
        public Decimal  StatusDescription {get;set;}

        @AuraEnabled
        public Decimal  StatusId {get;set;}

}