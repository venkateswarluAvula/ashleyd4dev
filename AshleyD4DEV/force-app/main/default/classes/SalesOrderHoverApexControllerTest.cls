@isTest
public class SalesOrderHoverApexControllerTest {
    public static testMethod void testsoRoutingData() {
        
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                     ExternalId = '17331400:001q000000raDkvAAE',  
                                                     phhProfitcenter__c = 1234567,
                                                     Phhcustomerid__c = '784584585',
                                                     phhSalesOrder__c = '88845758',
                                                     phhStoreID__c = '133',
                                                     phhGuestID__c = '001q000000raI3DAAU',
                                                     phhStoreLocation__c = 'Tampa',
                                                     phhCustomerType__c='Ashcomm',
                                                     phhErpNumber__c=''
                                                    );
        system.debug('order------' + salesOrder);
        SalesOrderDAO.mockedSalesOrders.add(salesOrder);
        system.debug('order--2----' + salesOrder.Id);
        SalesOrder__x salesOrderObj = SalesOrderDAO.getOrderById(salesOrder.Id);
        SalesOrderItem__x salesOrderItem = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE', 
                                                                 phdShipZip__c = '30548');
        SalesOrderDAO.mockedSalesOrderLineItems.add(salesOrderItem);
        SalesOrderItem__x salesOrderItemObj = SalesOrderDAO.getOrderLineItemByExternalId(salesOrderItem.ExternalId);
        
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        
        mock.setStatusCode(200);
        
        mock.setHeader('Content-Type', 'application/json');
        
        List<string> soid = new List<string>();
        string soName = '300522790';
        soid.add(soName);
        Id currentRecId = '001q000000raI3DAAU';
        string messagesub = 'test subject';
        string messagebody = 'test body';
        string toemailadd = 'test@mail.com';
        
        Account acc = new Account(Name='Abce');
        insert acc;
        
        
        Attachment Attachment =new Attachment();
        Attachment.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        Attachment.body=bodyBlob;
        //Attachment.setContentType('application/pdf');
        
         
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        SalesOrderHoverApexController.getAccounts(salesOrder.phhGuestID__c);
        SalesOrderHoverApexController.getSalesOrderLineItem(salesOrder.Id); 
        //SalesOrderHoverApexController.pdfdata(soid,currentRecId,messagesub,messagebody,toemailadd);
        
        //SalesOrderHoverApexController.AccVal = acc;
        //PageReference pageRef = Page.SOHistoryReportPDF;
        //Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('id',acc.id);
        //pageRef.getParameters().put('SO',soid);
        
        //SalesOrderHoverApexController.getrecords(currentRecId);
        //SalesOrderHoverApexController.sendEmail(acc.Id, null);
        
        Test.stopTest(); 
    }
    
    public static testmethod void  downloadpdf(){
        List<string> soid = new List<string>();
        string soName = '300522790';
        soid.add(soName);
        Id currentRecId = '001q000000raI3DAAU';
        SalesOrderHoverApexController.downloadpdf(soid,currentRecId);
    }
    
    public static testmethod void  pdfdata(){
        
        
        Account acc = new Account();
        acc.name = 'Acc Name';
        insert acc;
        
        List<string> soid = new List<string>();
        string soName = '300522790';
        soid.add(soName);
        Id currentRecId = '001q000000raI3DAAU';
        string messagesub = 'test subject';
        string messagebody = 'test body';
        string toemailadd = 'test@mail.com';
        
        SalesOrderHoverApexController.pdfdata(soid,acc.Id,messagesub,messagebody,toemailadd);
    }
    
}