public with sharing class CreateNoteRecord {
    
    @AuraEnabled
    public static void createRecord (ContentNote cn, Id ParentId){
        try{
            if(cn != null){
                Insert cn;
                ContentDocument cd=[SELECT Id FROM ContentDocument WHERE Id=:cn.Id];
                ContentDocumentLink cdl=new ContentDocumentLink();
                cdl.ContentDocumentId=cd.Id;
                cdl.LinkedEntityId=ParentId;
                cdl.ShareType='V';
                cdl.Visibility='AllUsers';
                Insert cdl;
                
            }
        } catch (Exception ex){
        }
    }
    
    @AuraEnabled
    public static void UpdateNotes(String title,String content,String Id,Id ParentId){
        ContentNote note = new ContentNote();
        note.Title = title;
        note.Content = Blob.valueOf(content.escapeHtml4());
        System.debug('id--->'+Id);
        if(Id != null && Id != ''){
            note.Id = Id;
            update note;
        }
    }
    
    @AuraEnabled
    public static ContentNote getNotes(String recordId){
        ContentNote note = new ContentNote();
        note = [SELECT Content,ContentSize,CreatedById,CreatedDate,Id,LastModifiedById,LastModifiedDate,
                     OwnerId,SharingPrivacy,TextPreview,Title 
                     FROM ContentNote 
                     WHERE Id =:recordId];
        System.debug('Notes-->'+note);
        return note;
    }
    
    @AuraEnabled
    public static List<ContentNote> searchNotes(String parentRecordId){
        List<NoteWrapper> noteWrpr = new List<NoteWrapper>();
        NoteWrapper rw = new NoteWrapper();
        List<ContentDocumentLink> cdln = new List<ContentDocumentLink>();
        List<ContentNote> Cnote = new List<ContentNote>();
        Set<Id> nId = new Set<Id>();
        try{
            cdln = [SELECT ContentDocumentId, ContentDocument.Title,ContentDocument.LastModifiedById,ContentDocument.FileType,
                    ContentDocument.OwnerId,ContentDocument.Owner.Name,Id, IsDeleted, LinkedEntityId,
                    ShareType, SystemModstamp, Visibility 
                    FROM ContentDocumentLink 
                    WHERE LinkedEntityId =:parentRecordId];
            System.debug('note--->'+cdln);
            for(ContentDocumentLink cl : cdln){
                nId.add(cl.ContentDocumentId);
            }
        }
        catch(exception ex){
        }
        try{
            Cnote = [SELECT Content,ContentSize,CreatedById,CreatedDate,Id,LastModifiedById,LastModifiedDate,
                     OwnerId,SharingPrivacy,TextPreview,Title 
                     FROM ContentNote 
                     WHERE Id IN :nId ORDER BY LastModifiedDate DESC];  
            rw.cn = Cnote;
            noteWrpr.add(rw);
        }catch(Exception ex){
            rw.cn = null;
        }
        System.debug('Notes-->'+Cnote);
        return Cnote;
    }
    public class NoteWrapper {
        public List<ContentNote> cn;
        public NoteWrapper(){
            cn = new List<ContentNote>();
        }
    }
}