@isTest
public class DeliveryDateUpdateTest {
@isTest
 static void DeliveryDate() {
        Account testCustomer = new Account(
            FirstName ='TestCoverage',
            LastName = 'CoveragePerson', 
            PersonEmail = 'tc@example.com', 
            Phone = '615-555-1212', 
            Primary_Language__pc = 'English'
        );
        insert testCustomer;
     DateTime dT = System.now();
     date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
      opportunity testOpp = new opportunity(
          Billing_Email__c = 'tc@example.com',
          Name= 'test new-6/27/2018',
          StageName='Saved Shopping Cart',
          CloseDate=myDate,
          AccountId = testCustomer.Id
        );
       insert testOpp;
       
        List<Shopping_cart_line_item__c> addlist = new List<Shopping_cart_line_item__c>();
      	
       // string sdate = String.valueOf(myDate);
        Shopping_cart_line_item__c testShipping = new Shopping_cart_line_item__c(
           	Opportunity__c = testOpp.id,
            Product_SKU__c = 'D596-60',
            DeliveryDate__c = myDate
          );
        addlist.add(testShipping);
        insert addlist;
        Test.startTest();
        Database.executeBatch(new DeliveryDateUpdate());
        Test.stopTest();
    }
}