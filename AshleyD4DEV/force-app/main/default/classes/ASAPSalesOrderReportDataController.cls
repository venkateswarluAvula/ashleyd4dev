public class ASAPSalesOrderReportDataController {    
    //ASAP
    @AuraEnabled
    Public static JSONStringParser.JSONParserCls SalesOrderASAPStatus(){
      String market = 'ASAP';
        String type = 'All Markets';
        SORoutingAuthorization CARAPart= new SORoutingAuthorization();
        String accessTkn = CARAPart.accessTokenData();
        String endpoint = System.label.ASAPOrderAPI +'&marketID='+market+'&type='+type;
        System.debug('endpoint----' + endpoint);
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        System.debug('apiresponse...' + res.getBody());
        
       //return res.getBody();//resultsmap;
       string s =  res.getBody();
       //System.debug('s----------------'+s);
       //return s;
        
    JSONStringParser.JSONParserCls  clsLsrWrpr = new JSONStringParser.JSONParserCls();
    clsLsrWrpr  = JSONStringParser.parserJsonString(s);
      system.debug('@@@@clsLsrWrpr ---->'+clsLsrWrpr);
       return clsLsrWrpr;
       
   }  // method
  }// class