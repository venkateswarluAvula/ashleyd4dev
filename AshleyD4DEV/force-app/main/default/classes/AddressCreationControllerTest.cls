/* This is a test class for address creation controller class.*/
@isTest(SeeAllData=true)
public class AddressCreationControllerTest {
    
    @isTest static void getAccountTest() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;
        Id accId = accList[0].Id;

        Test.startTest();
        try {
	        AddressCreationController.getAddress(null);
	        AddressCreationController.getAccount(null);
	        AddressCreationController.getAccount(accId);
	        Address__c adrObj = new Address__c(); 
	        AddressCreationController.saveAddress(adrObj, accId, 'update');
	        AddressCreationController.saveAddress(adrObj, accId, 'insert');
        }
        catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void getAddressTest() {
    	Id accId, contId, adrId;
    	List<Case> caseList = [SELECT Id, AccountId, ContactId, Address__c FROM Case WHERE Address__c != null AND AccountId != null AND (Technician_Schedule_Date__c > TODAY OR Tech_Scheduled_Date__c > TODAY) AND Legacy_Service_Request_ID__c != null LIMIT 1];
    	if (caseList.size() > 0) {
    		accId = caseList[0].AccountId;
    		contId = caseList[0].ContactId;
    		adrId = caseList[0].Address__c;
    	}

    	Address__c adrObj = new Address__c();
    	Address__c newAdrObj = new Address__c();
    	if (adrId != null) {
    		adrObj = [SELECT Id, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Address_Type__c FROM Address__c WHERE Id = :adrId LIMIT 1];
	    	newAdrObj.Address_Line_1__c = adrObj.Address_Line_1__c;
	    	newAdrObj.Address_Line_2__c = adrObj.Address_Line_2__c;
	    	newAdrObj.City__c = adrObj.City__c;
	    	newAdrObj.StateList__c = adrObj.StateList__c;
	    	newAdrObj.Zip_Code__c = adrObj.Zip_Code__c;
	    	newAdrObj.Address_Type__c = adrObj.Address_Type__c;
    	}

        Test.startTest();
        try {
	        AddressCreationController.getAddress(adrId);
	        AddressCreationController.validateAddress(newAdrObj, accId); //duplicate
	        adrObj.Preferred__c = true;
	        AddressCreationController.saveAddress(adrObj, accId, 'update'); //update
	        AddressCreationController.saveAddress(adrObj, accId, 'insert'); //insert
        }
        catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void validateAddressMoreTest() {
    	Id accId, contId;
        Id singleCaseAdrId, multiCaseAdrId;
        Map<Id, Integer> adrCntMap = new Map<Id, Integer>();
        Map<Id, case> caseMap = new Map<Id, case>();
    	List<Case> caseList = [SELECT Id, AccountId, ContactId, Address__c FROM Case WHERE Address__c != null AND AccountId != null AND (Technician_Schedule_Date__c > TODAY OR Tech_Scheduled_Date__c > TODAY) AND Legacy_Service_Request_ID__c != null LIMIT 10];
    	if (caseList.size() > 0) {
            for (Case cs:caseList) {
                Integer adrCnt = 0;
                if (adrCntMap.get(cs.Address__c) != null) {
                    adrCnt = adrCntMap.get(cs.Address__c);
                }
                ++adrCnt;
                adrCnt = adrCntMap.put(cs.Address__c, adrCnt);
                caseMap.put(cs.Address__c, cs);
            }
    	}

        if (adrCntMap.size() > 0) {
            for (Id adrId:adrCntMap.keySet()) {
                if (adrCntMap.get(adrId) == 1) {
                    singleCaseAdrId = adrId;
                } else {
                    multiCaseAdrId = adrId;
                }
            }
        }

    	Address__c adrObj = new Address__c();
    	Address__c newAdrObj = new Address__c();

    	if (multiCaseAdrId != null) {
    		adrObj = [SELECT Id, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Address_Type__c FROM Address__c WHERE Id = :multiCaseAdrId LIMIT 1];
	    	newAdrObj.Id = adrObj.Id;
            newAdrObj.Address_Line_1__c = adrObj.Address_Line_1__c;
	    	newAdrObj.Address_Line_2__c = adrObj.Address_Line_2__c;
	    	newAdrObj.City__c = 'City';
	    	newAdrObj.StateList__c = adrObj.StateList__c;
	    	newAdrObj.Zip_Code__c = adrObj.Zip_Code__c;
	    	newAdrObj.Address_Type__c = adrObj.Address_Type__c;

            accId = caseMap.get(multiCaseAdrId).AccountId;
    	}

        Test.startTest();
        try {
	        AddressCreationController.validateAddress(newAdrObj, accId);
        }
        catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void validateAddressSingleTest() {
    	Id accId, contId;
        Id singleCaseAdrId, multiCaseAdrId;
        Map<Id, Integer> adrCntMap = new Map<Id, Integer>();
        Map<Id, case> caseMap = new Map<Id, case>();
    	List<Case> caseList = [SELECT Id, AccountId, ContactId, Address__c FROM Case WHERE Address__c != null AND AccountId != null AND (Technician_Schedule_Date__c > TODAY OR Tech_Scheduled_Date__c > TODAY) AND Legacy_Service_Request_ID__c != null LIMIT 10];
    	if (caseList.size() > 0) {
            for (Case cs:caseList) {
                Integer adrCnt = 0;
                if (adrCntMap.get(cs.Address__c) != null) {
                    adrCnt = adrCntMap.get(cs.Address__c);
                }
                ++adrCnt;
                adrCnt = adrCntMap.put(cs.Address__c, adrCnt);
                caseMap.put(cs.Address__c, cs);
            }
    	}

        if (adrCntMap.size() > 0) {
            for (Id adrId:adrCntMap.keySet()) {
                if (adrCntMap.get(adrId) == 1) {
                    singleCaseAdrId = adrId;
                } else {
                    multiCaseAdrId = adrId;
                }
            }
        }

    	Address__c adrObj = new Address__c();
    	Address__c newAdrObj = new Address__c();

    	if (singleCaseAdrId != null) {
    		adrObj = [SELECT Id, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Address_Type__c FROM Address__c WHERE Id = :singleCaseAdrId LIMIT 1];
	    	newAdrObj.Id = adrObj.Id;
            newAdrObj.Address_Line_1__c = adrObj.Address_Line_1__c;
	    	newAdrObj.Address_Line_2__c = adrObj.Address_Line_2__c;
	    	newAdrObj.City__c = 'City';
	    	newAdrObj.StateList__c = adrObj.StateList__c;
	    	newAdrObj.Zip_Code__c = adrObj.Zip_Code__c;
	    	newAdrObj.Address_Type__c = adrObj.Address_Type__c;

            accId = caseMap.get(singleCaseAdrId).AccountId;
    	}

        Test.startTest();
        try {
	        AddressCreationController.validateAddress(newAdrObj, accId);
        }
        catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void validateAddressSingleZipTest() {
    	Id accId, contId;
        Id singleCaseAdrId, multiCaseAdrId;
        Map<Id, Integer> adrCntMap = new Map<Id, Integer>();
        Map<Id, case> caseMap = new Map<Id, case>();
    	List<Case> caseList = [SELECT Id, AccountId, ContactId, Address__c FROM Case WHERE Address__c != null AND AccountId != null AND (Technician_Schedule_Date__c > TODAY OR Tech_Scheduled_Date__c > TODAY) AND Legacy_Service_Request_ID__c != null LIMIT 10];
    	if (caseList.size() > 0) {
            for (Case cs:caseList) {
                Integer adrCnt = 0;
                if (adrCntMap.get(cs.Address__c) != null) {
                    adrCnt = adrCntMap.get(cs.Address__c);
                }
                ++adrCnt;
                adrCnt = adrCntMap.put(cs.Address__c, adrCnt);
                caseMap.put(cs.Address__c, cs);
            }
    	}

        if (adrCntMap.size() > 0) {
            for (Id adrId:adrCntMap.keySet()) {
                if (adrCntMap.get(adrId) == 1) {
                    singleCaseAdrId = adrId;
                } else {
                    multiCaseAdrId = adrId;
                }
            }
        }

    	Address__c adrObj = new Address__c();
    	Address__c newAdrObj = new Address__c();
    	if (singleCaseAdrId != null) {
    		adrObj = [SELECT Id, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Address_Type__c FROM Address__c WHERE Id = :singleCaseAdrId LIMIT 1];
	    	newAdrObj.Id = adrObj.Id;
            newAdrObj.Address_Line_1__c = adrObj.Address_Line_1__c;
	    	newAdrObj.Address_Line_2__c = adrObj.Address_Line_2__c;
	    	newAdrObj.City__c = adrObj.City__c;
	    	newAdrObj.StateList__c = adrObj.StateList__c;
	    	newAdrObj.Zip_Code__c = '32541';
	    	newAdrObj.Address_Type__c = adrObj.Address_Type__c;

            accId = caseMap.get(singleCaseAdrId).AccountId;
    	}

        Test.startTest();
        try {
	        AddressCreationController.validateAddress(newAdrObj, accId);
        }
        catch(exception e) {}
        Test.stopTest();
    }

    @isTest
    static void schTechAddressTest(){
        Test.startTest();
        List<Case> caseList = [SELECT Id, Address__c FROM Case WHERE Address__c != null LIMIT 1];
    	if (caseList.size() > 0) {
			AddressCreationController.schTechAddress(caseList[0].Address__c, caseList[0].Id, '17331400:001q000000raDkvAAE', 12345, '8888300-164');
    	}
		Test.stopTest();
    }

    @isTest
    static void unSchTechAddressTest(){
        Test.startTest();
    	List<Case> caseList = [SELECT Id, Address__c FROM Case WHERE Address__c != null LIMIT 1];
    	if (caseList.size() > 0) {
			AddressCreationController.unSchTechAddress(caseList[0].Address__c, caseList[0].Id);
    	}
		Test.stopTest();
    }

    @isTest
    static void testSearchAddress(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(200));
		String result = AddressCreationController.SearchAddress('Akron', 'US', 7);
		Test.stopTest();
    }
    
    @isTest
    static void testFormatAddress(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(200));
		String result = AddressCreationController.FormatAddress('https://api.edq.com/capture/address/v2/format?country=AUS&id=700AUS-NOAUSHAHgBwAAAAAIAwEAAAABV3R_gAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAASGFtaWx0b24A');
        Test.stopTest();
    }
}