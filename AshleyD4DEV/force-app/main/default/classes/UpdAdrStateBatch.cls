global class UpdAdrStateBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, State__c, StatePL__c, StateList__c FROM Address__c]); 
    }

    global void execute(Database.BatchableContext bc, List<Address__c> adrList) {
        List<string> statePlList = new List<string> ();
        Schema.DescribeFieldResult fieldResult = Address__c.StatePL__c.getDescribe();
        List<schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (schema.PicklistEntry f : ple) {
            if(f.isActive()) {
                statePlList.add(f.getValue());
            }
        }

        for(Address__c adr : adrList) {
            if(!string.isBlank(adr.StatePL__c)) {
            	adr.StateList__c = adr.StatePL__c;
            } else if(!string.isBlank(adr.State__c)) {
                if(statePlList.contains(adr.State__c)) {
                    adr.StateList__c = adr.State__c;
                } else if ((adr.State__c == 'ATLANTA') || (adr.State__c == 'G') || (adr.State__c == 'Georgia')){
                    adr.StateList__c = 'GA';
                } else if(adr.State__c == 'California') {
                    adr.StateList__c = 'CA';
                } else if(adr.State__c == 'Colorado') {
                    adr.StateList__c = 'CO';
                } else if((adr.State__c == 'FLORIDA') || (adr.State__c == 'Tampa, fl') || (adr.State__c == 'Brandon') || (adr.State__c == 'Fl.') || (adr.State__c == 'FL33701') || (adr.State__c == 'FLA') || (adr.State__c == 'Flh') || (adr.State__c == 'Fll') || (adr.State__c == 'Florda') || (adr.State__c == 'Florid a r')) {
                    adr.StateList__c = 'FL';
                } else if(adr.State__c == 'ILLINOIS') {
                    adr.StateList__c = 'IL';
                } else if(adr.State__c == 'Kentucky') {
                    adr.StateList__c = 'KY';
                } else if ((adr.State__c == 'Minnesota') || (adr.State__c == 'Mi.')) {
                    adr.StateList__c = 'MI';
                } else if ((adr.State__c == 'N') || (adr.State__c == 'N/')) {
                    adr.StateList__c = 'NE';
                } else if(adr.State__c == 'Ohio') {
                    adr.StateList__c = 'OH';
                } else if(adr.State__c == 'Pennsylvania') {
                    adr.StateList__c = 'PA';
                } else if(adr.State__c == 'WISCONSIN') {
                    adr.StateList__c = 'WI';
                } else {
                    adr.StateList__c = null;
                }
            } else {
                adr.StatePL__c = null;
            }
        }

        Database.SaveResult[] srList = Database.update(adrList, false);
        List<string> errMsgList = new List<string>();
        for(Database.SaveResult sr : srList) {
            if(!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    system.debug(err.getStatusCode() + ': ' + err.getMessage());
                    errMsgList.add(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }

        if(errMsgList.size() > 0) {
            //insert in ErrorLog__c object
            ErrorLog__c errLog = new ErrorLog__c(Name = 'State PL Update Batch Error', ApexClass__c = 'UpdAdrStateBatch', Method__c = 'execute', Message__c = string.join(errMsgList,' | '));
            insert errLog;
        }
    }

    global void finish(Database.BatchableContext bc) {}
}