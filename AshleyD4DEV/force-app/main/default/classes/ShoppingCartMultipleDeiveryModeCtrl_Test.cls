@isTest
public class ShoppingCartMultipleDeiveryModeCtrl_Test {
    
    @testSetup
    public static void initsetup(){
        TestDataFactory.prepareAPICustomSetting();
    }  
    public static testmethod void saveDeliveryModetest(){
        string selectedShippingWay = 'CPS';
        string deliveryType = 'PDI';
        
        List<string> selecteditems = new List<string>();
        Account acc =  TestDataFactory.prepareCustomerInfo();
        Opportunity opp =  TestDataFactory.prepareShoppingCart(acc);
        opp.Shipping_Discount_Status__c = 'Discount Pending Manager Approval';
        update opp;
        Shopping_cart_line_item__c line = new Shopping_cart_line_item__c();
        line.Product_SKU__c ='1200014';
        line.Quantity__c =1;
        line.Delivery_Mode__c='HD';
        line.List_Price__c = 288.99;
        line.Opportunity__c = opp.Id;
        line.Average_Cost__c = 40.99;
        line.DeliveryDate__c=system.today();
        insert line;
        
        selecteditems.add(line.Id);
        
            Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
            psr.setComments('Submitting for approval.');
            psr.setObjectId(opp.id);
            psr.setSubmitterId(UserInfo.getUserId() ); 
            psr.setProcessDefinitionNameOrId('Shipping_Discount_Approval');
            psr.setSkipEntryCriteria(true); 
            psr.setNextApproverIds(new List<Id>{UserInfo.getUserId()});
            Approval.ProcessResult aprresult = Approval.process(psr);
        Test.startTest();
        string result = ShoppingCartMultipleDeiveryModeCtrl.saveDeliveryMode(selecteditems,selectedShippingWay,acc.Id,deliveryType);
        system.assert(result == 'Success');  
        Test.stopTest();
    }
    
   public static testmethod void saveDelivermodedeliveryTest(){
        
        string selectedShippingWay = 'DS';
        string deliveryType = 'PDI';
        
        List<string> selecteditems = new List<string>();
        Account acc =  TestDataFactory.prepareCustomerInfo();
        Opportunity opp =  TestDataFactory.prepareShoppingCart(acc);
        List<Shopping_cart_line_item__c> lineItems =   TestDataFactory.prepareShoppingCartLineItems(opp);
        selecteditems.add(lineItems[0].id);
        selecteditems.add(lineItems[1].id);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock());
        ShoppingCartMultipleDeiveryModeCtrl.saveDeliveryMode(selecteditems,selectedShippingWay,acc.Id,deliveryType);
        Test.stopTest();
    } 
    public static testMethod void multipleFppUpdateCartexceptiontest(){
        try{
            string selectedShippingWay = 'HD';
            string deliveryType = 'PDI';
            Account acc =  TestDataFactory.prepareCustomerInfo();
            List<string> selecteditems = new List<string>();  
            string result = ShoppingCartMultipleDeiveryModeCtrl.saveDeliveryMode(selecteditems,selectedShippingWay,acc.Id,deliveryType);
        }catch(Exception ex){  
        }
    }
        
    public class Mock implements HttpCalloutMock {
        private string sampleResponse = 'Direct Shipment is  not available for item';   
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();
            res.setStatusCode(400); 
            res.setBody(sampleResponse); 
            return res;
        }
    }
    
}