public class D365_SalesOrderAuthorization
{
    public class oAuthJson{
        public String token_type;   //Bearer
        public String scope;    //user_impersonation
        public String expires_in;   //3599
        public String ext_expires_in;   //0
        public String expires_on;   //1537192739
        public String not_before;   //1537188839
        public String resource; // https://5a9d9cfd-c32e-4ac1-a9ed-fe83df4f9e4d/cara-api-dev
        public String access_token; 
        public String refresh_token;    
        public String id_token; 
    }

    public String accessTokenData()
    {
           String body = 'grant_type=password';
            
            body += '&client_secret=' + EncodingUtil.urlEncode('HsExOP91gUdOhpRP8g+nu18xiEYJTaapDyeotQFvCwc=', 'UTF-8');
            body += '&client_id=' + EncodingUtil.urlEncode('d3e15e4a-5584-4f1d-bdc8-0400f289fc2c', 'UTF-8');
            body += '&scope=' + EncodingUtil.urlEncode('openid', 'UTF-8');
            body += '&resource=' + EncodingUtil.urlEncode('https://5a9d9cfd-c32e-4ac1-a9ed-fe83df4f9e4d/cara-api-dev', 'UTF-8');
            body += '&username=' + EncodingUtil.urlEncode('s_CRMDevCara@ashleyfurniture.com', 'UTF-8');
            body += '&password=' + EncodingUtil.urlEncode('F84!cJ#pkaX84Rfv', 'UTF-8');
            
            String ePnt =  'https://login.microsoftonline.com/5a9d9cfd-c32e-4ac1-a9ed-fe83df4f9e4d/oauth2/token';
                    
            Http http = new Http();
            HttpRequest httpReq = new HttpRequest();
            HttpResponse httpResp = new HttpResponse();
            httpReq.setEndpoint(ePnt);
            httpReq.setBody(body);
            httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            httpReq.setMethod('POST');  
            httpResp = http.send(httpReq); 
            system.debug('resp-------'+httpResp.getBody());
            
           oAuthJson oaJson = (oAuthJson)JSON.deserialize(httpResp.getBody(), oAuthJson.class); 
            system.debug('resp_access_token-------'+oaJson.access_token); 
           return oaJson.access_token;
    }
}