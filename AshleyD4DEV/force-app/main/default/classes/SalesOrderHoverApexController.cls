public class SalesOrderHoverApexController {
    @AuraEnabled
    public static Account AccVal{get; set;}
    public static string id{get; set;}
    
    @AuraEnabled
    public static List<SalesOrder__x> getAccounts(Id salesforceOrderId){
        
       List < SalesOrder__x > lstOfAccount = [SELECT Id, ExternalId,phhOrderType__c,phhDatePromised__c,phhSalesOrder__c,phhSaleType__c,
                                              phhPurchaseValue__c, phhSOSource__c, phhCustomerName__c,phhSalesOrderDate__c,phhHot__c,
                                              phhWindowBegin__c,phhWindowEnd__c From SalesOrder__x where phhGuestID__r.id =: salesforceOrderId ];
        system.debug('lstOfAccount value is'+lstOfAccount);
        return lstOfAccount;
        
    }

    @AuraEnabled
    public static List<SalesOrderItem__x> getSalesOrderLineItem (id salesOrderId){
       
    List < SalesOrderItem__x > lstOfSalesOrdLineItem = [SELECT Id, phdItemSKU__c,phdRSA__c,phdRSAName__c,phdWholeSalePrice__c,phdItemDesc__c,
                                                        phdItemDesc2__c,phdQuantity__c,phdDeliveryType__c From SalesOrderItem__x 
                                                        where phdSalesOrder__r.id =: salesOrderId];
         system.debug('lstOfSalesOrdLineItem value is '+lstOfSalesOrdLineItem);
       
        return lstOfSalesOrdLineItem;
        
    }
    
    @AuraEnabled
    public static Account pdfdata(List<string> soid, Id currentRecId, string messagesub, string messagebody, string toemailadd){
        system.debug('soid----'+soid);
        system.debug('currentRecId----'+currentRecId);
        system.debug('toemailadd----'+toemailadd); 
        
        AccVal = [select id,Name,Type_of_Customer__pc,Phone_2__pc,PersonEmail from Account where Id=:currentRecId];
        system.debug('AccVal----'+AccVal);
        
        
        PageReference pdf=new PageReference ('/apex/SOHistoryReportPDF?id='+currentRecId+'&SO='+soid); 
        
        Blob body;
        system.debug('pdf----'+pdf);
        
        try {
            // returns the output of the page as a PDF
            
            if (!Test.isRunningTest()){
                body = pdf.getContentAsPDF(); 
            }
            else{
                body = Blob.valueof('Some random String');
            }
            
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
        
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        attach.setFileName('SalesOrderHistory.pdf');
        attach.setInline(false);
        attach.Body = body;
        
        
        List <String> addressList = toemailadd.split(';');
        system.debug('addressList--'+addressList.size()+'--'+addressList);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        OrgWideEmailAddress[] owea = [select Id,Address from OrgWideEmailAddress where Address = :system.label.Ashley_customer_care];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        mail.setUseSignature(false);
        String currentUserEmail = UserInfo.getUserEmail();
        //mail.setToAddresses(new String[] { currentUserEmail });
        if(toemailadd == '' || toemailadd == null){
            mail.setToAddresses(new String[] { AccVal.PersonEmail});
        }else{
            mail.setToAddresses(toemailadd.split(';'));
        }
        
        mail.setSubject(messagesub);
        mail.setHtmlBody(messagebody);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
        
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        system.debug('mail----'+mail);
        
        return AccVal;
    }
    
    @AuraEnabled
    public static string downloadpdf(List<string> soid, Id currentRecId){
        system.debug('soid----'+soid); system.debug('currentRecId----'+currentRecId);
        system.debug('soid size----'+soid.size());
        //AccVal = [select id,Name,Type_of_Customer__pc,Phone_2__pc,PersonEmail from Account where Id=:currentRecId];
        //system.debug('AccVal----'+AccVal);
        
        //PageReference pdf=new PageReference ('/apex/SOHistoryReportPDF?id='+currentRecId+'&SO='+soid+'&dwl=download'); 
        //system.debug('pdf----'+pdf);
        //pdf.setRedirect(true);
        
		string pdfdownloadURL = '/apex/SOHistoryReportPDF?id='+currentRecId+'&SO='+soid+'&dwl=download';
        system.debug('pdfdownloadURL----'+pdfdownloadURL);
        /*
        Blob body;
        
        try {
            // returns the output of the page as a PDF
            body = pdf.getContentAsPDF();
            //system.debug('body----'+body);
            // need to pass unit test -- current bug  
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }

        
        Attachment attach = new Attachment();
        attach.Body = body;
        string filename = 'PurchaseHistory.pdf';
        attach.Name = filename;
        attach.IsPrivate = false;
        // attach the pdf to the object
        attach.ParentId = currentRecId;
        //insert attach;
        system.debug('attach--'+attach);
        
        string attid = attach.id;
        string downloadURL = '/servlet/servlet.FileDownload?file='+attid;
        // -- /servlet/servlet.FileDownload?file=00PM0000001fH6h
        system.debug('downloadURL--'+downloadURL);
        */
        return pdfdownloadURL;
    }
    
}