public class CasePDFCompCont{
    
    @AuraEnabled
    public static List<case> getcaserecords(String RecId){
        system.debug('recordId----'+RecId);
        
        List<case> lstOfRec = [select Id,Status,Address__c,Address_Line_1__c,Address_Line_2__c,Technician_ServiceReqId__c,Technician_Company__c,Technician_Name__c,
                                Estimated_time_for_stop__c,Technician_Schedule_Date__c,CreatedDate,Follow_up_Date__c,
                                CreatedById,Owner.Name,Type_of_Resolution__c,CreatedBy.Name from case where id =: RecId];
        
        system.debug('lstOfRec ----'+lstOfRec );
        return lstOfRec;
    }
    
    @AuraEnabled
    public static List<ProductLineItem__c> getplirecords(String pliRecId){
        system.debug('pliRecId----'+pliRecId);
        
        List<ProductLineItem__c> pli = [ select id,Item_SKU__c,Part_Order_Tracking_Number__c,Delivery_Date__c,Item_Serial_Number__c,
                                        Legacy_Service_Request_Item_ID__c from ProductLineItem__c where Case__c =: pliRecId];
        
        system.debug('pli----'+pli);
        return pli;
    }
    
    @AuraEnabled
    public static List<Legacy_Comment__c> getlegacyrecords(String legacyRecId){
        system.debug('Legacycomments----'+legacyRecId);
        
        List<Legacy_Comment__c> Legacycomments = [ select id,Comment__c from Legacy_Comment__c where Case__c =: legacyRecId];
        
        system.debug('Legacycomments----'+Legacycomments);
        return Legacycomments;
    }
}