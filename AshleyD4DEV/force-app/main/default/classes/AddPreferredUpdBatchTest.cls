@isTest
public class AddPreferredUpdBatchTest {
    @isTest
    public static void shipToAdrTest() {
        Account testCustomer = new Account(
            FirstName ='TestCoverage',
            LastName = 'CoveragePerson', 
            PersonEmail = 'tc@example.com', 
            Phone = '615-555-1212', 
            Primary_Language__pc = 'English'
        );
        insert testCustomer;

        List<Address__c> addlist = new List<Address__c>();
        Address__c testShipping = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = false,
            Address_Type__c ='Ship To',
            Address_Line_2__c='123 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping);
        Address__c testShipping1 = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = false,
            Address_Type__c ='Ship To',
            Address_Line_2__c='1234 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping1);
        Address__c testShipping2 = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = false,
            Address_Type__c ='Bill To',
            Address_Line_2__c='1234 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping2);
        insert addlist;

        Test.startTest();
        Database.executeBatch(new AddPreferredUpdBatch());
        Test.stopTest();
    }
    @isTest
    public static void billToAdrTest() {
        Account testCustomer = new Account(
            FirstName ='TestCoverage',
            LastName = 'CoveragePerson', 
            PersonEmail = 'tc@example.com', 
            Phone = '615-555-1212', 
            Primary_Language__pc = 'English'
        );
        insert testCustomer;

        List<Address__c> addlist = new List<Address__c>();
        Address__c testShipping = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = false,
            Address_Type__c ='Bill To',
            Address_Line_2__c='123 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping);
        Address__c testShipping1 = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = false,
            Address_Type__c ='Bill To',
            Address_Line_2__c='1234 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping1);
        Address__c testShipping2 = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = false,
            Address_Type__c ='Bill To',
            Address_Line_2__c='1234 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping2);
        insert addlist;

        Test.startTest();
        Database.executeBatch(new AddPreferredUpdBatch());
        Test.stopTest();
    }
    @isTest
    public static void adrTest() {
        Account testCustomer = new Account(
            FirstName ='TestCoverage',
            LastName = 'CoveragePerson', 
            PersonEmail = 'tc@example.com', 
            Phone = '615-555-1212', 
            Primary_Language__pc = 'English'
        );
        insert testCustomer;

        List<Address__c> addlist = new List<Address__c>();
        Address__c testShipping = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = false,
            Address_Type__c ='',
            Address_Line_2__c='123 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping);
        insert addlist;

        Test.startTest();
        Database.executeBatch(new AddPreferredUpdBatch());
        Test.stopTest();
    }
    @isTest
    public static void preferredAdrTest() {
        Account testCustomer = new Account(
            FirstName ='TestCoverage',
            LastName = 'CoveragePerson', 
            PersonEmail = 'tc@example.com', 
            Phone = '615-555-1212', 
            Primary_Language__pc = 'English'
        );
        insert testCustomer;

        List<Address__c> addlist = new List<Address__c>();
        Address__c testShipping = new Address__c(
            Address_Line_1__c = '123 Other Test Coverage St', 
            City__c = 'Franklin', 
            State__c = 'TN',
            Preferred__c = true,
            Address_Type__c ='Ship To',
            Address_Line_2__c='123 test',
            Country__c='India',
            AccountId__c = testCustomer.Id
        );
        addlist.add(testShipping);
        insert addlist;

        Test.startTest();
        Database.executeBatch(new AddPreferredUpdBatch());
        Test.stopTest();
    }
}