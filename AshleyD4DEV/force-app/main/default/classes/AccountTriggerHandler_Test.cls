@isTest
public class AccountTriggerHandler_Test {

    @isTest static void NPSupdateTest() {
		SingleRequestMock fakeResponse = new SingleRequestMock(200, 'OK', '{"CustomerId":"007063406292","SONumber":"400349550","AccountShipTo":"8888300-164","CustomerType":"RET","OptType":"START","OldTextConsent":0,"TimeStamp":"6/19/2019 4:12:46 AM"}', null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
       
        List<Account> testAccs = TestDataFactory.initializePersonAccounts(1);
        insert testAccs;
        
        Test.startTest();
        for(Account acc: testAccs){
            acc.Survey_Opt_In__pc = false;
        }
        update testAccs;

        for(Account acc: testAccs){ 
            acc.Survey_Opt_In__pc = true;
        }
        update testAccs;
        Test.stopTest();
    }
    
}