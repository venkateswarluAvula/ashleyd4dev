/* This is a helper class which contains generic method that can be invoked where ever it is required.*/
public class CaseHelper {

    public static string endpointTech = system.label.TechSchedulingEndPoint;
    public static string apiKeyTech = system.label.TechSchedulingApiKey;
    public static boolean recursiveCall = false;
    public static integer recursiveCount = 0;

    public static list<Market_Configuration__mdt> getMarketConfig(){
		list<Market_Configuration__mdt> marketConfigList = [SELECT Id, MasterLabel, DeveloperName, Routing_Queue_Name__c, Fulfiller_Id__c FROM Market_Configuration__mdt];
		return marketConfigList;
    }
    
    public static list<Type_SubType_Map__mdt> getTypeSubType(){
		list<Type_SubType_Map__mdt> typeSubtypelist = [SELECT Id, Type__c, DeveloperName, Sub_Type__c, Market__c, Queue_Name__c FROM Type_SubType_Map__mdt];
		return typeSubtypelist;
    }
    
    public static SalesOrder__x getSalesOrderInfo(string soId){
        SalesOrder__x orderObj = new SalesOrder__x();
        if(Test.isRunningTest()){
            orderObj = new SalesOrder__x(fulfillerID__c = '8888300-164', ExternalId = '17331400:001q000000raDkvAAE');
        } else {
            orderObj = [SELECT ExternalId, fulfillerID__c, phhERPAccounShipTo__c, phhERPCustomerID__c, Id, phhProfitcenter__c, phhMarketAccount__c, phhStoreNameStoreNumberPC__c,
            				phhCustomerID__c, phhSalesOrder__c, phhSaleType__c, phhCustomerType__c, phhShipToName__c 
            				FROM SalesOrder__x WHERE ExternalId =: soId];
        }
        return orderObj;
    }

    public static SalesOrderItem__x getSalesOrderLineInfo(string soId){
        SalesOrderItem__x orderItemObj = new SalesOrderItem__x();
        if(Test.isRunningTest()){
            orderItemObj = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE', phdShipZip__c = '30548');
        } else {
            orderItemObj = [SELECT Id, ExternalId, phdSalesOrderDate__c, phdSalesOrder__c, Ship_Customer_Name__c, phdShipAddress1__c, phdShipAddress2__c, phdShipCity__c, 
            				phdShipState__c, phdShipZip__c FROM SalesOrderItem__x Where phdSalesOrder__r.ExternalId =: soId limit 1];
        }
        return orderItemObj;
    }

    public static List<SalesOrderItem__x> getSalesOrderLineList(string soId){
        List<SalesOrderItem__x> orderItemList = new List<SalesOrderItem__x>();
        if(Test.isRunningTest()){
            orderItemList.add(new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE', phdShipZip__c = '30548'));
        } else {
            orderItemList = [SELECT Id, ExternalId, phdSalesOrderDate__c, phdSalesOrder__c, Ship_Customer_Name__c, phdShipAddress1__c, phdShipAddress2__c, phdShipCity__c, 
            				phdShipState__c, phdShipZip__c FROM SalesOrderItem__x Where phdSalesOrder__r.ExternalId =: soId];
        }
        return orderItemList;
    }

    /*
     * Method to return case fulfiller id
     */
    public static string getCaseFulfillerId(string accShipTo, string soId) {
    	string marketFulfillerId;
    	if (string.isNotBlank(accShipTo)) {
    		marketFulfillerId = accShipTo;
    	} else if (string.isNotBlank(soId)) {
	    	SalesOrder__x salesOrderObj = new SalesOrder__x();
	    	salesOrderObj = CaseHelper.getSalesOrderInfo(soId);
	    	if (string.isNotBlank(salesOrderObj.fulfillerID__c)) {
	    		marketFulfillerId = salesOrderObj.fulfillerID__c;
	    	}
    	}
    	return marketFulfillerId;
    }

    /*
     * Method to get case status either closed as true or open as false
     */
    public static boolean caseIsClosed(string csStatus) {
    	boolean isCsClosed = false;
		if (string.isNotBlank(csStatus)) {
			CaseStatus[] csList = [SELECT ApiName, IsClosed FROM CaseStatus WHERE ApiName = :csStatus];
            if (csList.size() > 0) {
                if (csList[0].IsClosed) {
                	isCsClosed = true;
                }
            }
		}
		return isCsClosed;
	}

    /*
     * Method to get all close status values from case object
     */
    public static list<string> getCaseClosedValues() {
    	list<string> closeValList = new list<string>();
		List<CaseStatus> csList = [SELECT ApiName, IsClosed FROM CaseStatus WHERE IsClosed = true];
        if (csList.size() > 0) {
            for (CaseStatus cs:csList) {
            	closeValList.add(cs.ApiName);
            }
        }
        return closeValList;
	}

    /*
     * Method to update case address for the same technician
     */
    public static string updateCaseAdrSch(Id addressId, Id caseId, string fulfillerId, integer reqId) {
    	string rtnStr;
		if ((addressId != null) && (caseId != null)) {
	    	List<Address__c> addressList = [SELECT Id, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c FROM Address__c WHERE Id = :addressId];
	    	for(Address__c address : addressList) {
	    		string adrStr = AddressHelper.getCommaAddressStr(address.Address_Line_1__c, address.Address_Line_2__c, address.City__c, address.StateList__c, address.Zip_Code__c);
		    	string jsonData = '{';
	    		jsonData += '	"AddressLine1":"' + address.Address_Line_1__c + '",';
				if (address.Address_Line_2__c != null) {
					jsonData += '	"AddressLine2":"' + address.Address_Line_2__c + '",';
				} else {
					jsonData += '	"AddressLine2":"",';
				}
				jsonData += '	"City":"' + address.City__c + '",';
				jsonData += '	"State":"' + address.StateList__c + '",';
				jsonData += '	"ZipCode":"' + address.Zip_Code__c + '"';
		    	jsonData += '}';
				rtnStr = TechSchedulingController.updateTechnicianAddress(fulfillerId, reqId, jsonData, adrStr, caseId, address.Address_Line_1__c, address.Address_Line_2__c, address.City__c, address.StateList__c, address.Zip_Code__c);
				break;
	    	}
		}
		return rtnStr;
	}

    /*
     * Method to call external web service to close case in HOMES while closing a case in Salesforce
     */
    @future(callout=true)
    public static void closeCaseInHomes(string extId, string reqId, string fulfillerId) {
    	string marketFulfillerId;
    	if (string.isNotBlank(fulfillerId)) {
    		marketFulfillerId = fulfillerId;
    	} else {
	    	SalesOrder__x salesOrderObj = new SalesOrder__x();
	    	salesOrderObj = CaseHelper.getSalesOrderInfo(extId);
	    	marketFulfillerId = salesOrderObj.fulfillerID__c;
    	}
		//https://stageapigw.ashleyfurniture.com/homestores/8888300-164/customer-service/service-requests/90709/close?apikey=bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713
        string httpReqVar = endpointTech + marketFulfillerId + '/customer-service/service-requests/' + reqId + '/close';

        Httprequest req = new HttpRequest();
        req.setHeader('apikey', apiKeyTech);
        req.setHeader('Accept', 'application/json');
        req.setEndpoint(httpReqVar);
        req.setTimeOut(120000);
        req.setMethod('PUT');

        HttpResponse res = callApi(req, 'Close service request', 'CaseHelper', 'closeCaseInHomes', httpReqVar);
        integer resCode = res.getStatusCode();
        string resBody = res.getBody();

        system.debug('****** PUT Response Status: ' + res.getStatusCode());
        system.debug('****** PUT Response: ' + res.getBody());
	    if ((resCode == 200 || resCode == 201) && (resBody != null && resBody != '[]')) {
			// successfull callout
	    } else {
			// callout failed
			List<string> errMsgList = new List<string>();
	        errMsgList.add('Response Code: ' + resCode + ' : Response Body: ' + resBody);
            errMsgList.add(' : input data=' + httpReqVar);
	        insertError('Close service request', 'CaseHelper', 'closeCaseInHomes', errMsgList);
	    }
    }

    /*
     * Method to call external web service to create comments for the associated service request in HOMES
     */
    @future(callout=true)
    public static void servReqCommentInHomes(map<string, string> strMap, map<string, integer> intMap, map<string, id> idMap) {
    	//string section, Id caseId, integer RequestId, string fulfillerId, string scheduledDate, integer totLineItem, string oldAdr, string newAdr

		//	PREPARING DATA TO MAKE A CALLOUT COMMENT API STARTS
		Case caseObj = TechSchedulingController.getCaseObj(idMap.get('caseId'));

		//getting user time zone and community name to pass in comments
		User objUser = [Select Id, CommunityNickname, timezonesidkey from User where Id=:UserInfo.getUserId()];
		Datetime dtDateTime = system.now();
		string strDateTime = dtDateTime.format('MM/dd/yyyy HH:mm:ss', objUser.timezonesidkey );

		transient string CommentText = '';
		if (strMap.get('section') == 'schedule') {
			CommentText = 'Technician Scheduled from Salesforce Case: ' + caseObj.CaseNumber + ' by ' + objUser.CommunityNickname + ' for ' + strMap.get('scheduledDate') + ' on ' + strDateTime + ' (' + objUser.timezonesidkey + ') with ' + intMap.get('totLineItem') + ' total items - ' + caseObj.Subject + ' - ' + caseObj.Description;
		} else if (strMap.get('section') == 'unschedule') {
			CommentText = 'Technician Unscheduled from Salesforce Case: ' + caseObj.CaseNumber + ' by ' + objUser.CommunityNickname + ' on ' + strDateTime + ' (' + objUser.timezonesidkey + ')';
		} else if (strMap.get('section') == 'shipto') {
			CommentText = 'The Ship To Address was updated in Salesforce on this Service Request from ' + strMap.get('oldAdr') + ' to ' + strMap.get('newAdr') + ' on ' + strDateTime + ' (' + objUser.timezonesidkey + ') by ' + objUser.CommunityNickname + '. See Case ' + caseObj.CaseNumber + ' in Salesforce for more details.';
		}

		boolean split = false;
		if(CommentText.length() > 1024){
			split = true;
		}

		transient List<string> resultArray = new List<string>();
		if(split){
			transient List<string> commArray = CommentText.split(' ', 0);
			CommentText = '';
			resultArray = CaseHelper.splitComment(commArray, 0, new List<string>());
			//system.debug('Heap size used: ' + Limits.getHeapSize() + ' : ' + resultArray.size());
			//system.debug('commArray: ' + resultArray);
		}
		else{
			resultArray.add(CommentText);
		}

		JSONGenerator comGen = JSON.createGenerator(true);

		comGen.writeStartObject();
		comGen.writeNumberField('RequestId', intMap.get('RequestId'));
		comGen.writeFieldName('CommentText');
		comGen.writeStartArray();
		integer cnt = 0;
		for(string res : resultArray){
			if(split){
				++cnt;
				//have to add some text in the front (10 of 99 - 10/29/2018 17:28:28)
				comGen.writeString('(' + cnt + ' of ' + resultArray.size() + ' - ' + strDateTime + ') ' + res);
			}
			else{
				comGen.writeString(res);
			}
		}
		comGen.writeEndArray();
		comGen.writeNumberField('CreatedUserID', 0);
		comGen.writeEndObject();

		string jsoncomString = comGen.getAsString();
		system.debug('****** jsoncomString: ' + jsoncomString);

		//Comment callout starts
        HttpRequest comReq = new HttpRequest();
        comReq.setEndpoint(endpointTech + strMap.get('fulfillerId') +'/customer-service/service-requests/comments');
        comReq.setHeader('apikey', apiKeyTech);
        comReq.setMethod('POST');
        comReq.setBody(jsoncomString);
        comReq.setTimeOut(120000);
        comReq.setHeader('Content-Type' ,'application/json');

    	system.debug('****** COM POST End Point: ' + comReq.getEndpoint());
    	system.debug('****** COM POST body: ' + comReq.getBody());

		HttpResponse comRes = CaseHelper.callApi(comReq, 'Comments call ( '+strMap.get('section')+' )', 'CaseHelper', 'servReqCommentInHomes', jsoncomString);
		//Comment callout ends

    	system.debug('****** COM POST Response Status: ' + comRes.getStatusCode());
    	system.debug('****** COM POST Response: ' + comRes.getBody());
    	//	PREPARING DATA TO MAKE A CALLOUT COMMENT API ENDS

    }

    /*
     * Method to catch database update and insert error in error log object
     */
    public static void databaseErrorLog(Database.SaveResult sr, string eName, string aClass, string eMethod, string ipStr) {
	    if (!sr.isSuccess()) {
	        List<string> errMsgList = new List<string>();
	        for (Database.Error err : sr.getErrors()) {
	            errMsgList.add('Status Code=' + err.getStatusCode());
	            errMsgList.add('Message=' + err.getMessage()); 
	            errMsgList.add('Fields=' + err.getFields());
	        }
	        if(!string.isBlank(ipStr)){
	            errMsgList.add('input data=' + ipStr);
	        }
	        if(errMsgList.size() > 0) {
	            //insert in ErrorLog__c object
	            insertError(eName, aClass, eMethod, errMsgList);
	        }
	    }
    }

    /*
     * Method to catch database update and insert error in error log object
     */
    public static void databaseErrorLog(Database.SaveResult[] srList, string eName, string aClass, string eMethod, string ipStr) {
        List<string> errMsgList = new List<string>();
    	for (Database.SaveResult sr : srList) {
		    if (!sr.isSuccess()) {
		        for (Database.Error err : sr.getErrors()) {
		            errMsgList.add('Status Code=' + err.getStatusCode());
		            errMsgList.add('Message=' + err.getMessage()); 
		            errMsgList.add('Fields=' + err.getFields());
		        }
		    }
    	}
        if(errMsgList.size() > 0) {
	        if(!string.isBlank(ipStr)){
	            errMsgList.add('input data=' + ipStr);
	        }
            //insert in ErrorLog__c object
            insertError(eName, aClass, eMethod, errMsgList);
        }
    }

    /*
     * Method to make an api callout and insert in error log in case of error 
     */
    public static HttpResponse callApi(HttpRequest req, string eName, string aClass, string eMethod, string ipStr) {
        List<string> errMsgList = new List<string>();
        HttpResponse res = new HttpResponse();
		Integer resCode;
    	String resBody;
        Http h = new Http();
		// Attempt the callout - create return error on exception
        try {

            // Perform callout and set response
            res = h.send(req);
            resCode = res.getStatusCode();
            resBody = res.getBody();
			//system.debug('****** res: ' + res + ' : ' + resBody);
            // check response 
            if ((resCode == 200 || resCode == 201) && (resBody != null && resBody != '[]')) {
				// successfull callout
            } else {
				// callout failed
				/*
				in order to avoid runtime error 'You have uncommitted work pending. Please commit or rollback before calling out', i am commenting out this block for this time --- sekar on 12th July 2019
                errMsgList.add('Response Code: ' + resCode + ' : Response Body: ' + resBody);
                if(!string.isBlank(ipStr)){
                    errMsgList.add(' : input data=' + ipStr);
                }
                insertError(eName, aClass, eMethod, errMsgList);
                */
            }

        } catch (exception e) {
            // Unexpected exceptions will be caught here
            exceptionError(e, eName, aClass, eMethod, ipStr);
        }

        return res;
    }

    /*
     * Method to catch exception and insert error in error log object
     */
    public static void exceptionError(exception e, string eName, string aClass, string eMethod, string ipStr) {
        List<string> errMsgList = new List<string>();
    	errMsgList.add('Exception type caught: ' + e.getTypeName());    
    	errMsgList.add('Message: ' + e.getMessage());    
    	errMsgList.add('Cause: ' + e.getCause());    // returns null
    	errMsgList.add('Line number: ' + e.getLineNumber());    
    	errMsgList.add('Stack trace: ' + e.getStackTraceString());    
        if(!string.isBlank(ipStr)){
            errMsgList.add('input data=' + ipStr);
        }
        if(errMsgList.size() > 0) {
            //insert in ErrorLog__c object
            insertError(eName, aClass, eMethod, errMsgList);
        }
    }

    /*
     * Method to catch dml exception and insert error in error log object
     */
    public static void dmlExceptionError(DmlException de, string eName, string aClass, string eMethod, string ipStr) {
        List<string> errMsgList = new List<string>();
        Integer numErrors = de.getNumDml();
        if(!string.isBlank(ipStr)){
            errMsgList.add('input data=' + ipStr);
        }
        errMsgList.add('getNumDml=' + numErrors);
        for(Integer i=0; i<numErrors; i++) {
            errMsgList.add('getDmlFieldNames=' + de.getDmlFieldNames(i));
            errMsgList.add('getDmlMessage=' + de.getDmlMessage(i)); 
        }
        if(errMsgList.size() > 0) {
            //insert in ErrorLog__c object
            insertError(eName, aClass, eMethod, errMsgList);
        }
    }

    /*
     * Method to insert error in error log object
     */
    public static void insertError(string eName, string aClass, string eMethod, list<string> eMsg) {
    	//if this method is not called from future or batch class then do asynchronous error log insert
    	if(!System.isFuture() && !System.isBatch()){
    		//asynchronous error log insert
    		asynInsertError(eName, aClass, eMethod, eMsg);
    	} else {
    		//synchronous error log insert from EDAServiceRequestCase
	        ErrorLog__c errLog = new ErrorLog__c(Name = eName, ApexClass__c = aClass, Method__c = eMethod, Message__c = string.join(eMsg,' | '));
	        Database.insert(errLog, false);
	        system.debug('errLog synchronous: ' + errLog);
    	}
    }

    /*
     * Method to asynchronous insert error in error log object
     */
    @future
    public static void asynInsertError(string eName, string aClass, string eMethod, list<string> eMsg) {
        ErrorLog__c errLog = new ErrorLog__c(Name = eName, ApexClass__c = aClass, Method__c = eMethod, Message__c = string.join(eMsg,' | '));
        Database.insert(errLog, false);
        system.debug('errLog asynchronous: ' + errLog);
    }

    /*
     * Method to make an api callout and return response & error details in case of error
     */
    public static CalloutResponse returnCalloutResponse(HttpRequest req, string eName, string aClass, string eMethod, string ipStr) {
        List<ErrorLogWrap> elWrapList = new List<ErrorLogWrap>();
        HttpResponse res = new HttpResponse();

        List<string> errMsgList = new List<string>();
		Integer resCode;
    	String resBody;
        Http h = new Http();
		// Attempt the callout - create return error on exception
        try {

            // Perform callout and set response
            res = h.send(req);
            resCode = res.getStatusCode();
            resBody = res.getBody();
			//system.debug('****** res: ' + res + ' : ' + resBody);
            // check response 
            if ((resCode == 200 || resCode == 201) && (resBody != null && resBody != '[]')) {
				// successfull callout
            } else {
				// callout failed
                errMsgList.add('Response Code: ' + resCode + ' : Response Body: ' + resBody);
                if(!string.isBlank(ipStr)){
                    errMsgList.add(' : input data=' + ipStr);
                }

                ErrorLogWrap elwrap = new ErrorLogWrap();
	            elwrap.ErrLogName = eName;
	            elwrap.ErrLogApexClass = aClass;
				elwrap.ErrLogMethod = eMethod;
				elwrap.ErrLogMessage = string.join(errMsgList,' | ');
				elWrapList.add(elwrap);
            }

        } catch (exception e) {
            // Unexpected exceptions will be caught here
            elWrapList.add(returnExcError(e, eName, aClass, eMethod, ipStr));
        }

        CalloutResponse coRespObj = new CalloutResponse(res, elWrapList);

        return coRespObj;
    }

    /*
     * Method to catch exception and return error
     */
    public static ErrorLogWrap returnExcError(exception e, string eName, string aClass, string eMethod, string ipStr) {
    	ErrorLogWrap elwrap = new ErrorLogWrap();
        List<string> errMsgList = new List<string>();
    	errMsgList.add('Exception type caught: ' + e.getTypeName());    
    	errMsgList.add('Message: ' + e.getMessage());    
    	errMsgList.add('Cause: ' + e.getCause());    // returns null
    	errMsgList.add('Line number: ' + e.getLineNumber());    
    	errMsgList.add('Stack trace: ' + e.getStackTraceString());    
        if(!string.isBlank(ipStr)){
            errMsgList.add('input data=' + ipStr);
        }
        if(errMsgList.size() > 0) {
            elwrap.ErrLogName = eName;
            elwrap.ErrLogApexClass = aClass;
			elwrap.ErrLogMethod = eMethod;
			elwrap.ErrLogMessage = string.join(errMsgList,' | ');
        }
        return elwrap;
    }

    /*
     * Method to insert error in error log object
     */
    public static void insertError(List<ErrorLogWrap> insErrLogWrapList) {
		if (insErrLogWrapList.size() > 0) {
			List<ErrorLog__c> insErrLogList = new List<ErrorLog__c>();
			for (ErrorLogWrap elw : insErrLogWrapList) {
		        ErrorLog__c errLog = new ErrorLog__c(Name = elw.ErrLogName, ApexClass__c = elw.ErrLogApexClass, Method__c = elw.ErrLogMethod, Message__c = elw.ErrLogMessage);
				insErrLogList.add(errLog);
			}
			if (insErrLogList.size() > 0){
		        Database.insert(insErrLogList, false);
		        system.debug('ins error: ' + insErrLogList);
			}
		}
    }


    /*
     * Wrapper class to capture API response along with error
     */
	public class CalloutResponse {
		public HttpResponse HttpRespObj { get; set; }
		public List<ErrorLogWrap> InsErrorLogWrapList { get; set; }

		public CalloutResponse(HttpResponse httpRespObj, List<ErrorLogWrap> insErrLogList) {
			this.HttpRespObj = httpRespObj;
			this.InsErrorLogWrapList = insErrLogList;
		}
	}

    /*
     * Wrapper class to capture API response error
     */
	public class ErrorLogWrap {
		public string ErrLogName { get; set; }
		public string ErrLogApexClass { get; set; }
		public string ErrLogMethod { get; set; }
		public string ErrLogMessage { get; set; }
	}

	public static List<string> splitComment(List<string> commArray, integer len, List<string> rtnArray){
		transient string tmpStr = '', rtnStr = '';
		for(integer c=len; c<commArray.size(); c++){
			transient string comm = commArray[c];

			tmpStr = '';
			tmpStr = rtnStr;

			if(tmpStr != ''){
				tmpStr += ' ';
			}
			tmpStr += comm;

			if(tmpStr.length() > 950){
				rtnArray.add(rtnStr);
				//system.debug('Heap size used: ' + len + ':' + Limits.getHeapSize());
				return splitComment(commArray, c, rtnArray);
			}
			else{
				rtnStr = tmpStr;
			}
		}
		rtnArray.add(rtnStr);
		return rtnArray;
	}

}