global class OpportunityCloseBatch implements Database.Batchable<sObject>,Schedulable
{
    
    global void execute(SchedulableContext SC) 
    {
        OpportunityCloseBatch batch = new OpportunityCloseBatch ();
        ID batchprocessid = Database.executeBatch(batch,400); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
        String query='SELECT id,StageName FROM opportunity WHERE StageName != \'Closed Won\' AND StageName != \'Closed Lost\' AND StageName != \'Sale Suspended\' AND CreatedDate != LAST_N_DAYS:30 limit 5';  
        System.debug('@@@@@@@@@'+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> listOppRecords)
    {
        try{
            List<Opportunity> oppList = new List<Opportunity>();
            
            for(Opportunity opp : listOppRecords)
            {
                 opp.StageName='Closed Lost';
                 oppList.add(opp);
            }
                
                if(!oppList.isEmpty())
                {
                    Database.update(oppList,false);
                }
            }
            
            catch (Exception ex) {
                ErrorLog__c el = new ErrorLog__c(ApexClass__c = 'OpportunityCloseBatch',Method__c = 'OpportunityCloseBatch',Message__c = 'Error is this: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString());
            	insert el;
                
            }
        }
        
        
        
        global void finish(Database.BatchableContext BC){
        }
    }