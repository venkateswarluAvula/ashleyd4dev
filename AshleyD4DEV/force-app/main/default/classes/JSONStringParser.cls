//OrderNumber,Market,Phone1',Phone2', Phone3'
public class JSONStringParser{
 
 public JSONStringParser(){
 
 }
 
 public static JSONParserCls  parserJsonString(String jsonString){
 JSONParserCls jsnWrpr  = new JSONParserCls(); 
 List<ASAPWrapper> lstAsapwrpr = new List<ASAPWrapper>();
 JSONParser parser = JSON.createParser(jsonString );
 //check parser data
 while(parser.nextToken() != null){
   if(parser.getCurrentToken() == JSONToken.START_Array){
       while(parser.nextToken() != null){
            if(parser.getCurrentToken() == JSONToken.Start_Object){
       ASAPWrapper jpc = (ASAPWrapper)parser.readValueAs(ASAPWrapper.class);
         ASAPWrapper wrpr = new ASAPWrapper();
     wrpr.OrderNumber = jpc.OrderNumber;
     wrpr.Market = jpc.Market;
     wrpr.Phone1 = jpc.Phone1;
     wrpr.Phone2 = jpc.Phone2;
     wrpr.Phone3 = jpc.Phone3;
     lstAsapwrpr.add(wrpr);
            
            }//if
           }//while
       
       }//if
 
        }//while
	jsnWrpr.message = 'ASAP records are retrieved ';
	jsnWrpr.success = true;
	jsnWrpr.asapList = lstAsapwrpr;
 System.debug('@@@@@@ wrpr size------->'+jsnWrpr);
 return jsnWrpr;
     }//method
     
public class JSONParserCls {
  @AuraEnabled
  public List<ASAPWrapper> asapList ;
  @AuraEnabled
  public String message;
@AuraEnabled
  public Boolean success;

 }
Public class ASAPWrapper {
 @AuraEnabled
 public string OrderNumber;
  @AuraEnabled
 public string Market; 
  @AuraEnabled
 public string Phone1; 
  @AuraEnabled
 public string Phone2; 
  @AuraEnabled
 public string Phone3; 	
} 
     
     
  }//class