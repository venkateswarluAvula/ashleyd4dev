@isTest
public class ProductLineItemFromOrderHelper_Test {
//For direct delete
@isTest static void testGetMethod(){
Account acc = new Account(Name = 'TestAccountName');
ApexPages.StandardController sc = new ApexPages.standardController(acc);
ProductLineItemFromOrderHelper controller = new ProductLineItemFromOrderHelper(sc);
//Create Account
Account delacct = new Account();
// delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
delacct.LastName = 'last' + Math.random();
delacct.FirstName = 'first' + Math.random();
delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
delacct.Primary_Language__pc = 'English';
string phone = '888' + Integer.valueOf(Math.random() * 10000);
while (phone.length() < 10){
phone = phone + '0';
}
string phone2 = '888' + Integer.valueOf(Math.random() * 10000);
while (phone.length() < 10){
phone = phone + '0';
}
string phone3 = '889' + Integer.valueOf(Math.random() * 10000);
while (phone.length() < 10){
phone = phone + '0';
}
delacct.phone = phone;
delacct.Phone_2__pc = phone2;
delacct.Phone_3__pc = phone3;
delacct.PersonEmail = 'pqr@mm.com';
delacct.Email_2__pc ='pqr@mm.com';
delacct.Strike_Counter__pc = 10;
Insert delacct;
System.debug('delacct-->'+delacct.Id);
//Create Address
//Create Case
Case c = new Case();
c.Status = 'New';
c.Priority = 'Medium';
c.Origin = 'Web';
c.Subject = 'Subject ' +  Math.random();
c.Type = 'Delivery Order inquiry';
c.Sub_Type__c = 'Parts Issue';
c.AccountId = delacct.Id;
//  c.Address__c = testDelAd.Id;
c.Sales_Order__c = '200453150:001q000000raI3BAAU';
Insert c;
//Create PLI
ProductLineItem__c PLI = new ProductLineItem__c();
PLI.Case__c = c.Id;
PLI.Part_Order_Tracking_Number__c = '999999';
PLI.Defect_Location__c = 'BACK';
PLI.Fulfiller_ID__c = '8888300-164';
//  PLI.Ashley_Direct_Link_ID__c ='SF001321';
// PLI.Part_Order_Shipping_Date__c = '2018/01/17';
insert PLI;
POJSON data1 = new POJSON();
POJSON.ExtDetail data = new POJSON.ExtDetail();
data.Customer = '8888300';
data.ShipTo = '480';
data.PONumber = '37996C';
data.Address1 = '2204 DELIGHTFUL DR';
data.Address2 = '';
data.Address3 = 'RUSKIN';
data.Street = 'FL';
data.ZipCode = '33570';
data.ShipDate = '20180117';
data.UPSTracking = '1Z3545130317318146';
data.DefectCode = 'PF';
data.Defect = 'PEELING FINISH';
data.DefectLocationCode = 'BK';
data.DefectLocation = 'BACK';
data.Model = '9880035';
data1.ExtDetail = data;
ProductLineItem__c record = [SELECT Id,Part_Order_Number__c,Fulfiller_ID__c
FROM ProductLineItem__c Where ID =:PLI.Id];
Test.startTest();
try{
ProductLineItem__c plirec = ProductLineItemFromOrderHelper.getRecord(PLI.Id);
Case resCase = ProductLineItemFromOrderHelper.getCaseDetail(PLI.Id);
// System.debug(resCase);
}
catch(exception ex){}
Test.stopTest();
}
private class RestMock implements HttpCalloutMock {
public HTTPResponse respond(HTTPRequest req) {
String fullJson = '';
HTTPResponse res = new HTTPResponse();
res.setHeader('Content-Type', 'text/json');
res.setBody(fullJson);
res.setStatusCode(200);
return res;
}
}
@isTest
public static void Usecase(){
StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
//Create PLI
//Create Account
Account delacct = new Account();
// delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
delacct.LastName = 'last' + Math.random();
delacct.FirstName = 'first' + Math.random();
delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
delacct.Primary_Language__pc = 'English';
string phone = '888' + Integer.valueOf(Math.random() * 10000);
while (phone.length() < 10){
phone = phone + '0';
}
string phone2 = '888' + Integer.valueOf(Math.random() * 10000);
while (phone.length() < 10){
phone = phone + '0';
}
string phone3 = '889' + Integer.valueOf(Math.random() * 10000);
while (phone.length() < 10){
phone = phone + '0';
}
delacct.phone = phone;
delacct.Phone_2__pc = phone2;
delacct.Phone_3__pc = phone3;
delacct.PersonEmail = 'pqr@mm.com';
delacct.Email_2__pc ='pqr@mm.com';
delacct.Strike_Counter__pc = 10;
Insert delacct;
System.debug('delacct-->'+delacct.Id);
//Create Case
Case c = new Case();
c.Status = 'New';
c.Priority = 'Medium';
c.Origin = 'Web';
c.Subject = 'Subject ' +  Math.random();
c.Type = 'Delivery Order inquiry';
c.Sub_Type__c = 'Parts Issue';
c.AccountId = delacct.Id;
//    c.Address__c = testDelAd.Id;
c.Sales_Order__c = '200453150:001q000000raI3BAAU';
Insert c;
//Create PLI
ProductLineItem__c PLI = new ProductLineItem__c();
PLI.Case__c = c.Id;
PLI.Part_Order_Tracking_Number__c = '999999';
PLI.Defect_Location__c = 'BACK';
PLI.Fulfiller_ID__c = '8888300-164';
PLI.Part_Order_Number__c = 'SF001321';
PLI.Item_SKU__c = '*DELIV-TAX';
// PLI.Part_Order_Shipping_Date__c = '2018/01/17';
insert PLI;
POJSON data1 = new POJSON();
POJSON.ExtDetail data = new POJSON.ExtDetail();
data.Customer = '8888300';
data.ShipTo = '480';
data.PONumber = '37996C';
data.Address1 = '2204 DELIGHTFUL DR';
data.Address2 = '';
data.Address3 = 'RUSKIN';
data.Street = 'FL';
data.ZipCode = '33570';
data.ShipDate = '20180117';
data.UPSTracking = '1Z3545130317318146';
data.DefectCode = 'PF';
data.Defect = 'PEELING FINISH';
data.DefectLocationCode = 'BK';
data.DefectLocation = 'BACK';
data.Model = '9880035';
data1.ExtDetail = data;
ProductLineItem__c record = [SELECT Id,Part_Order_Number__c,Fulfiller_ID__c
FROM ProductLineItem__c Where ID =:PLI.Id];
mock.setStaticResource('soRoutingDatamockResponse');
mock.setStatusCode(200);
mock.setHeader('Content-Type', 'text/json');
Test.startTest();
// Set the mock callout mode
Test.setMock(HttpCalloutMock.class, mock);
//calls
try{
Boolean resq = ProductLineItemFromOrderHelper.getTrackingNumber(PLI.Id, record.Fulfiller_ID__c, record.Part_Order_Number__c);
}
catch(exception ex){}
try{
Boolean resq = ProductLineItemFromOrderHelper.getIsReload(PLI.Id);
}
catch(exception ex){}
try{
Boolean resq = ProductLineItemFromOrderHelper.getSerialNumber(PLI.Id,'');
Boolean resq1 = ProductLineItemFromOrderHelper.getSerialNumber(PLI.Id,'123456');
system.debug('getSerialNumber-----');
}
catch(exception ex){}
try{
Boolean resq = ProductLineItemFromOrderHelper.check();
}
catch(exception ex){}
Test.stopTest();
}
}