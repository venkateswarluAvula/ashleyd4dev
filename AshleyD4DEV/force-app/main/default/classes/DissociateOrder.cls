public class DissociateOrder {
    @AuraEnabled
    public static Boolean getCase(ID caseId){   
        string returnTxt;
        System.debug('My record CaseId'+CaseId);
        try{
            Case Caserecord = [SELECT AccountId,CaseNumber,Id,Sales_Order__c FROM Case WHERE Id =:CaseId];
            if(Caserecord.Sales_Order__c != null){
                Caserecord.Sales_Order__c = null;
                //Caserecord.Market__c = null;
                Update Caserecord;
                return true;
            }
            else
                return false;
        }
        catch(exception ex){
            return false; 
        }
    }
}