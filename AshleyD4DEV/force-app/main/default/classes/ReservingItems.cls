public class ReservingItems {
    
    public static final String AuthAPI = 'ReservingItemsauth';
    
    public static string returnResponse;
    public class responsewrapper {
        public Token token;
    }
    public class Token {
        public String accessToken;
        public String refreshToken;
    }
    
    public class ReservingRawPost{
        public String saleOrderId;
        public Decimal saleOrderLine;
        public String itemId;
        public String profitCenterId;
        public String buildingType;
        public Decimal quantity;
        public String customerId;
        public String shipToId;
        
    }
    public class Reservingresponse{
        public Decimal quantity;
        public String availabilityDate;
    }
    
    @AuraEnabled
    public static string getaccesstoken(string fulfillerId){ 
        Map<string, ReservingItemsAuthorization__c> reservingitems = ReservingItemsAuthorization__c.getAll();
        string uname     = reservingitems.get(AuthAPI).Username__c;
        string pword     = reservingitems.get(AuthAPI).Password__c;
        string epoint  = reservingitems.get(AuthAPI).Authorizationapi__c;
        string endpoint = String.format(epoint, new List<String>{fulfillerId});
        system.debug('endpoint----'+endpoint);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setTimeout(120000);
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        // Set the body as a JSON object
        request.setBody('{"UserName":"' + uname + '", "Password":"' + pword + '"}');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
            returnResponse = null;
        } else {
            
            responsewrapper responsewrap = (responsewrapper)JSON.deserialize(response.getBody(), responsewrapper.class);
            system.debug('responsewrap---'+responsewrap);
            system.debug('response--access-'+responsewrap.token.accessToken);
            returnResponse = responsewrap.token.accessToken;
        } 
        return returnResponse;   
    }
    
    @AuraEnabled
    public static List<String> reserveAllLineItems(string personAccId){ 
        List<Shopping_cart_line_item__c> shoppingDetail = new  List<Shopping_cart_line_item__c>();
        Opportunity opp = new Opportunity();
        boolean isupdate = false;
        list<string> Exceptionitems = new List<String>();
        //CartGuid from Opportunity
        string CartGuid;
        string oppid;
        for (Opportunity master : [Select Id,CartGuid__c,(Select Id, WarrantySku__c,  Product_Title__c, Product_SKU__c, Discount__c,Discount_Price__c,DiscountType__c,
                                                       Flat_Discounted_Amount__c,Flat_Discounted_Amount_On_Total__c,Discount_Approver__c,Discount_Approver__r.Name,Last_Price__c,List_Price__c, Average_Cost__c, Quantity__c,Extended_Price__c,ItemType__c,
                                                       Opportunity__c,Delivery_Mode__c,LineNumber__c,DeliveryType__c,Discount_Reason_Code__c,Discount_Status__c, Estimated_Tax__c,DeliveryDate__c,As_Is__c,isCallItem__c
                                                       from  Shopping_cart_line_items__r ) 
                                   From Opportunity where AccountId=:personAccId 
                                   and StageName != 'Closed Won' 
                                   and StageName != 'Closed Lost'
                                   and StageName != 'Sale Suspended' 
                                   and createdDate = LAST_N_DAYS:30]){
                                       CartGuid = master.CartGuid__c;
                                       oppid = master.Id;
                                       for (Shopping_cart_line_item__c detail : master.Shopping_cart_line_items__r) {
                                           if(detail.Delivery_Mode__c == 'TW'){
                                               shoppingDetail.add(detail);
                                           }
                                       }
                                   }
        if(shoppingDetail.size() > 0){
            //get API from custom label and make the api as per the Market fulfilelrid 
            StoreInfoWrapper si = StoreInfo.getStoreInfo();
            string accessToken = getaccesstoken(si.fulfillerId);
            system.debug('accessToken---'+accessToken);
            Map<string, ReserveandDeleteHmsApi__c> reservingAPI = ReserveandDeleteHmsApi__c.getAll();
            string api = reservingAPI.get('HmsApi').HmsReservingAPI__c;
            string endpoint = String.format(api, new List<String>{si.fulfillerId});
            system.debug('endpoint---'+endpoint);
            for(Shopping_cart_line_item__c lineitem:shoppingDetail){
                
                ReservingRawPost rawpost = new ReservingRawPost();
                rawpost.saleOrderId = CartGuid;
                rawpost.saleOrderLine = lineitem.LineNumber__c;
                rawpost.itemId = lineitem.Product_SKU__c;
                rawpost.profitCenterId = si.profitCtr;
                rawpost.buildingType = 'S';
                rawpost.quantity = lineitem.Quantity__c;
                rawpost.customerId = null;
                rawpost.shipToId = null;
                
                string rawpostbody = JSON.serialize(rawpost);
                system.debug('rawpostbody----'+rawpostbody);
                
                Http http = new Http();
                HttpRequest req = new Httprequest();
                req.setHeader('Authorization', 'Bearer '+accessToken);
                req.setHeader('Accept', 'application/json');
                req.setHeader('Content-Type','application/json');
                req.setEndpoint(endpoint);
                req.setTimeout(120000);
                req.setMethod('POST');
                req.setBody(rawpostbody);
                HttpResponse res = http.send(req);
                system.debug('res----'+res);
                if(res.getStatusCode() == 200){
                    system.debug('response----'+res.getBody());
                    Reservingresponse responsewrap = (Reservingresponse)JSON.deserialize(res.getBody(), Reservingresponse.class);
                    system.debug('quantity---'+responsewrap.quantity);
                    string datestring = responsewrap.availabilityDate;
                    string[] finaldate = datestring.split('T');
                    system.debug('finaldate----'+finaldate[0]);
                    system.debug('availabilityDate---'+responsewrap.availabilityDate);
                    system.debug('string Date------'+string.valueOf(lineitem.DeliveryDate__c));
                    if(finaldate[0] == string.valueOf(lineitem.DeliveryDate__c) && lineitem.Quantity__c == responsewrap.quantity){
                    isupdate = true;
                    opp.Id = oppid;
                    opp.IsReserved__c = true;
                    system.debug('reserved Successfully '+lineitem.Product_SKU__c); 
                    }else{
                    Exceptionitems.add(lineitem.Product_SKU__c);    
                    }
                }else{
                    Exceptionitems.add(lineitem.Product_SKU__c);   
                } 
            }
        }
        if(isupdate){
        update opp;
        }
        return Exceptionitems;
    }
    @AuraEnabled
    public static List<String> deleteReservedItems(string personAccId){
        List<Shopping_cart_line_item__c> shoppingDetail = new  List<Shopping_cart_line_item__c>();
        Opportunity opp = new Opportunity();
        
        list<string> Exceptionitems = new List<String>();
        //CartGuid from Opportunity
        string CartGuid;
        string oppid;
        
        for (Opportunity master : [Select Id,CartGuid__c,(Select Id, WarrantySku__c,  Product_Title__c, Product_SKU__c, Discount__c,Discount_Price__c,DiscountType__c,
                                                       Flat_Discounted_Amount__c,Flat_Discounted_Amount_On_Total__c,Discount_Approver__c,Discount_Approver__r.Name,Last_Price__c,List_Price__c, Average_Cost__c, Quantity__c,Extended_Price__c,ItemType__c,
                                                       Opportunity__c,Delivery_Mode__c,LineNumber__c,DeliveryType__c,Discount_Reason_Code__c,Discount_Status__c, Estimated_Tax__c,DeliveryDate__c,As_Is__c,isCallItem__c
                                                       from  Shopping_cart_line_items__r ) 
                                   From Opportunity where AccountId=:personAccId 
                                   and StageName != 'Closed Won' 
                                   and StageName != 'Closed Lost'
                                   and StageName != 'Sale Suspended' 
                                   and createdDate = LAST_N_DAYS:30]){
                                       CartGuid = master.CartGuid__c;
                                       oppid = master.Id;
                                       for (Shopping_cart_line_item__c detail : master.Shopping_cart_line_items__r) {
                                           if(detail.Delivery_Mode__c == 'TW'){
                                               shoppingDetail.add(detail);
                                           }
                                       }
                                   }
        if(shoppingDetail.size() > 0){
            //get API from custom label and make the api as per the Market fulfilelrid 
            StoreInfoWrapper si = StoreInfo.getStoreInfo();
            string accessToken = getaccesstoken(si.fulfillerId);
            system.debug('accessToken---'+accessToken);
            Map<string, ReserveandDeleteHmsApi__c> reservingAPI = ReserveandDeleteHmsApi__c.getAll();
            string api = reservingAPI.get('HmsApi').HmsReservingAPI__c;
            string endpoint = String.format(api, new List<String>{si.fulfillerId});
            for(Shopping_cart_line_item__c lineitem:shoppingDetail){
                string Epoint = endpoint +'/'+CartGuid+'/'+lineitem.LineNumber__c;
                
                Http http = new Http();
                HttpRequest req = new Httprequest();
                req.setHeader('Authorization', 'Bearer '+accessToken);
                req.setHeader('Accept', 'application/json');
                req.setEndpoint(Epoint);
                req.setTimeout(120000);
                req.setMethod('DELETE');
                HttpResponse res = http.send(req);
                system.debug('res----'+res);
                if(res.getStatusCode() == 200){
                    opp.Id = oppid;
                    opp.IsReserved__c = false;
                    system.debug('Deleted Successfully '+lineitem.Product_SKU__c); 
                }else{
                    Exceptionitems.add(lineitem.Product_SKU__c);   
                } 
            }
            update opp;
        }
        
        return Exceptionitems;
        
    }
}