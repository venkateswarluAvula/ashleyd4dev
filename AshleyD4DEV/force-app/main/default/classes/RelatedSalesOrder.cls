public class RelatedSalesOrder {
  @AuraEnabled
  public Static List<RSalesOrderWrapper> rsalesorder(String AId) {
        system.debug('AId----'+AId);
       List<RSalesOrderWrapper> rslist= new List<RSalesOrderWrapper>();
 
       //To Fetch D365_SalesOrder__c Fields
       DescribeSObjectResult describeResult = D365_SalesOrder__c.getSObjectType().getDescribe();    
       List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );   
       String query =     ' SELECT ' +  String.join( fieldNames, ',' ) +      ' FROM ' + describeResult.getName() +  ' WHERE ' +' id = :AId'  ;   //D365_AccountId__r.id
       List<D365_SalesOrder__c> RS = Database.query( query );
       System.debug('RS----'+ RS );     
        
      
      
       //To Fetch SalesOrder__x Fields
       /*
       DescribeSObjectResult describeResult1 = SalesOrder__x.getSObjectType().getDescribe();    
       List<String> fieldNames1 = new List<String>( describeResult1.fields.getMap().keySet() ); 
       String query1 =    ' SELECT ' +  String.join( fieldNames1, ',' ) +     ' FROM ' + describeResult1.getName() +     ' WHERE ' +' phhGuestID__r.id = :AId'  ;   
       List<SalesOrder__x> EDATA = Database.query( query1 );
       System.debug( EDATA ); 
         
          if(EDATA.size()>0){
             for(SalesOrder__x SO:EDATA){
                RSalesOrderWrapper RW=new RSalesOrderWrapper();  
                 RW.Ids=SO.Id;
                 RW.ExternalId=SO.ExternalId;
                 RW.CustomerID=SO.phhGuestID__c;
                 RW.CustomerName=SO.phhCustomerName__c;
                 RW.SalesOrderName=SO.phhSalesOrder__c; 
                 RW.DeliveryDate=SO.phhDatePromised__c;
                 RW.ASAP=SO.phhASAP__c;
                 RW.BackOrder=SO.phhBackOrder__c;
                 RW.BalanceDueUSD=SO.phhBalanceDue__c;
                 RW.ContactStatus=SO.phhContactStatus__c;
                 RW.CustomerAccount=SO.phhGuestID__c;
                 RW.CustomerID=SO.phhCustomerID__c;
                 RW.CustomerName=SO.phhCustomerName__c;
                 RW.CustomerType=SO.phhCustomerType__c;
                 RW.DeliveryComments=SO.phhDeliveryComments__c;
                 RW.DeliveryDate=SO.phhDatePromised__c;
                 RW.DeliveryType=SO.phhDeliveryType__c;
                 RW.DesiredDate=SO.phhDesiredDate__c;
                 RW.DoNotOpen=SO.LIBFlag__c;
                 RW.ERPNumber=SO.phhErpNumber__c;
                 RW.EstimatedArrival=SO.Estimated_Arrival__c;
                 RW.fulfillerID=SO.fulfillerID__c;
                 RW.HighDollarSale=SO.phhHighDollarSale__c;
                 RW.Hot=SO.phhHot__c;
                 RW.ItemCount=SO.phhItemCount__c;
                 RW.NPS=SO.phhNPS__c;
                 RW.NumberofDeliveryAttempts=SO.phhDeliveryAttempts__c;
                 RW.OrderNotes=SO.phhOrder_Notes__c;
                 RW.OrderStatus=SO.phhOrderStatus__c;
                 RW.OrderSubType=SO.phhOrderSubType__c;
                 RW.OrderType=SO.phhOrderType__c;
                 RW.PartialOrder=SO.phhPartialOrder__c;    
                 RW.PaymentType=SO.phhPaymentType__c;
                 RW.Profitcenter= SO.phhProfitcenter__c;  
                 RW.PurchaseDate=SO.phhPurchaseDate__c;
                 RW.PurchaseLocation=SO.phhStoreID__c;
                 RW.PurchaseTotalUSD=SO.phhPurchaseValue__c;   
                 RW.RescheduledReason=SO.phhRescheduledReason__c;    
                 RW.Residential=SO.phhResidential__c;   
                 RW.ReturnReasonCode=SO.phhReasonCode__c;
                 RW.SalesDate=SO.phhSalesOrderDate__c;
                 RW.SalesOrder=SO.phhSalesOrder__c;
                 RW.SalesStatus=SO.phhSaleType__c;
                 RW.SatDlvryFlg=SO.phhSatDlvryFlg__c;    
                 RW.ServiceLevel=SO.phhServiceLevel__c;
                 RW.StoreLocation=SO.phhStoreLocation__c;
                 RW.TransactionCode=SO.phhTransactionCode__c;
                 RW.usra=SO.usra__c;
                 RW.VIP=SO.VIPFlag__c;   
                 RW.WindowBegin=SO.phhWindowBegin__c;
                 RW.WindowEnd=SO.phhWindowEnd__c;
                     
                        
                  rslist.add(RW);
                                          } 
                       }
            */           
       
      if(RS.size() > 0){
        for(D365_SalesOrder__c DSR : RS){
         RSalesOrderWrapper RSW=new RSalesOrderWrapper();
            
            RSW.SalesOrderName=DSR.Name;
            RSW.Ids=DSR.Id;
            RSW.AccountId=DSR.D365_AccountId__c;  
            RsW.ActualDeliveryDate=DSR.D365_Actual_Delivery_Date__c;
            RSW.Address=DSR.D365_Address__c;
            RSW.Address1=DSR.D365_Address_1__c;
            RSW.Address2=DSR.D365_Address_2__c;    
            RSW.AddressName=DSR.D365_Address_Name__c;    
            RSW.AddressVerificationStatus=DSR.D365_Address_Verification_Status__c;   
            RSW.AdjustmentType=DSR.D365_Adjustment_Type__c;  
            RsW.Amount=DSR.D365_Amount__c;
            RSW.ASNNumber=DSR.D365_ASN_Number__c;
            RSW.ASNStatus=DSR.D365_ASN_Status__c;
            RSW.AuthenticationCode=DSR.D365_Authentication_Code__c;    
            RSW.BegindaterangebyMOD=DSR.D365_Begin_date_range_by_MOD__c;    
            RSW.BestdatebyMOD=DSR.D365_Best_date_by_MOD__c;    
            RSW.CanceledQuantity=DSR.D365_Canceled_Quantity__c;  
            RsW.Carrier=DSR.D365_Carrier__c;
            RSW.ChargesDescription=DSR.D365_Charges_Description__c;
            RSW.Checkedoutasguest=DSR.D365_Checked_out_as_guest__c;
            RSW.City=DSR.D365_City__c;    
            RSW.ConfirmedDeliveryDate=DSR.D365_Confirmed_Delivery_Date__c;   
            RSW.ContributionRatio =DSR.D365_Contribution_Ratio__c;   
            RSW.CostValue  =DSR.D365_Cost_Value__c;
            RsW.Country=DSR.D365_Country__c;
            RSW.CountryRegion=DSR.D365_Country_Region__c;
            RSW.CreditNoteReason=DSR.D365_Credit_Note_Reason__c;
            RSW.Currency1 =DSR.D365_Currency__c;   
            RSW.CurrentDateandTime=DSR.D365_Current_Date_and_Time__c;    
            RSW.CustomerID=DSR.D365_Customer_ID__c;   
            RSW.CustomerName =DSR.D365_Customer_Name__c; 
            RsW.Date1=DSR.D365_Date__c;
            RSW.DateTimeSend=DSR.D365_Date_Time_Send__c;
            RSW.Description=DSR.D365_Description__c;
            RSW.Discount=DSR.D365_Discount__c;
            RSW.DiscountDollor=DSR.D365_Discount_Currency__c;    
            RSW.DiscountPercent=DSR.D365_Discount_percent__c;    
            RSW.DiscountDescription=DSR.D365_Discount_Description__c;  
            RsW.DiscountOfferID=DSR.Discount_Offer_ID__c;
            RSW.DiscountQty=DSR.D365_Discount_Qty__c;
            RSW.EffectiveDate=DSR.D365_Effective_Date__c;
            RSW.EarliestdeliverydatebyMOD=DSR.D365_Earliest_delivery_date_by_MOD__c; 
            RSW.EmailData=DSR.D365_Email_Data__c;   
            RSW.EnddatebyMOD=DSR.D365_End_date_by_MOD__c;   
            RSW.ERPNumber1=DSR.D365_ERP_Number__c;  
            RsW.Expired=DSR.D365_Expired__c;
            RSW.Extension=DSR.D365_Extension__c;
            RSW.FirstName=DSR.D365_First_Name__c;
            RSW.Fulfilleremailaddress=DSR.D365_Fulfiller_email_address__c;   
            RSW.Fulfillerphonenumber=DSR.D365_Fulfiller_phone_number__c;   
            RSW.Fulfillershoursofoperation=DSR.D365_Fulfillers_hour_s_of_operation__c;    
            RSW.HoldFulfillment=DSR.D365_Hold_Fulfillment__c;  
            RsW.HOMESDeliveryDate=DSR.D365_HOMES_Delivery_Date__c;
            RSW.HOMESHot=DSR.D365_HOMES_Hot__c;
            RSW.HOMESOrderNotes=DSR.D365_HOMES_Order_Notes__c;
            RSW.HOMESRescheduleReason=DSR.D365_HOMES_Reschedule_Reason__c;    
            RSW.HOMESTimeframe=DSR.D365_HOMES_Timeframe__c;    
            RSW.Iamolderthan13=DSR.D365_I_am_older_than_13__c;    
            RSW.InternationalCallingCode=DSR.D365_International_Calling_Code__c;    
            RSW.InvoiceAmount=DSR.D365_Invoice_Amount__c;    
            RSW.InvoicedChargeAmount=DSR.D365_Invoiced_Charge_Amount__c;    
            RSW.Item=DSR.D365_Item__c;
            RSW.ItemNumber=DSR.D365_Item_Number__c;
            RSW.KitItem=DSR.D365_Kit_Item__c;
            RSW.LastName=DSR.D365_Last_Name__c;
            RSW.LatestdeliverydateMOD=DSR.D365_Latest_delivery_date_MOD__c;
            RSW.LineConfirmationID=DSR.D365_Line_Confirmation_ID__c;
            RSW.LineStage=DSR.D365_Line_Stage__c;
            RSW.Margin=DSR.D365_Margin__c;
            RSW.MarketingOptIn=DSR.D365_Marketing_Opt_In__c;
            RSW.MessagebyMOD=DSR.D365_Message_by_MOD__c;
            RSW.MiddleName=DSR.D365_Middle_Name__c;
            RSW.ModeofDelivery=DSR.D365_Mode_of_Delivery__c;
            RSW.NameorDescription=DSR.D365_Name_or_Description__c;
            RSW.NetAmount=DSR.D365_Net_Amount__c;
            RSW.NetAmountofSalesLine=DSR.D365_Net_Amount_of_Sales_Line__c;
            RSW.NetLineDiscount=DSR.D365_Net_Line_Discount__c;
            RSW.NewDeliveryDate=DSR.D365_New_Delivery_Date__c;
            RSW.NewQty=DSR.D365_New_Qty__c;
            RSW.NewStage=DSR.D365_New_Stage__c;
            RSW.NotificationStatus=DSR.D365_Notification_Status__c;
            RSW.NotificationType=DSR.D365_Notification_Type__c;
            RSW.OpenChargeAmount=DSR.D365_Open_Charge_Amount__c;
            RSW.OpenExtAmt=DSR.D365_Open_Ext_Amt__c;
            RSW.OrderLineStage=DSR.D365_Order_Line_Stage__c;
            RSW.OrderStatus=DSR.D365_Order_Status__c;
            RSW.OrderSubmitDateTime=DSR.D365_Order_Submit_Date_Time__c;
            RSW.OrderUpdateReason=DSR.D365_Order_Update_Reason__c;
            RSW.OriginalQty=DSR.D365_Original_Qty__c;
            RSW.OriginalSalesOrderNumber=DSR.D365_Original_Sales_Order_Number__c;
            RSW.PaymentMethod=DSR.D365_Payment_Method__c;
            RSW.PaymentService=DSR.D365_Payment_Service__c;
            RSW.PersonalSuffix=DSR.D365_Personal_Suffix__c;
            RSW.PersonalTitle=DSR.D365_Personal_Title__c;
            RSW.PhoneEmail=DSR.D365_Phone_Email__c;
            RSW.PONotes=DSR.D365_PO_Notes__c;
            RSW.POVendor=DSR.D365_PO_Vendor__c;
            RSW.Primary=DSR.D365_Primary__c;
            RSW.PriorDeliveryDate=DSR.D365_Prior_Delivery_Date__c;
            RSW.PriorQty=DSR.D365_Prior_Qty__c;
            RSW.PriorStage=DSR.D365_Prior_Stage__c;
            RSW.ProductName=DSR.D365_Product_Name__c;
            RSW.PromoCode=DSR.D365_Promo_Code__c;
            RSW.PuchaseUnitPrice=DSR.D365_Puchase_Unit_Price__c;
            RSW.Purpose=DSR.D365_Purpose__c;
            RSW.Quantity=DSR.D365_Quantity__c;
            RSW.RecordType=DSR.D365_Record_Type__c;
            RSW.ReferenceNumber=DSR.D365_Reference_Number__c;
            RSW.RequestedReceiptDate=DSR.D365_Requested_Receipt_Date__c;
            RSW.RequestedShipDate=DSR.D365_Requested_Ship_Date__c;
            RSW.ReturnSalesOrderNumber=DSR.D365_Return_Sales_Order_Number__c;
            RSW.RevSareVendor=DSR.D365_Rev_Sare_Vendor__c;
            RSW.RMANumber=DSR.D365_RMA_Number__c;
            RSW.RSAIndicator=DSR.D365_RSA_Indicator__c;
            RSW.RunningDiscount=DSR.D365_Running_Discount__c;
            RSW.SalesOrderNumber=DSR.D365_Sales_Order_Number__c;
            RSW.SalesOrderQty=DSR.D365_Sales_Order_Qty__c;
            RSW.Salestax=DSR.D365_Sales_tax__c;
            RSW.SalesOrderName=DSR.Name;
            RSW.ScheduleReason=DSR.D365_Schedule_Reason__c;
            RSW.Scheduledonline=DSR.D365_Scheduled_online__c;
            RSW.ScheduledReceiptDate=DSR.D365_Scheduled_Receipt_Date__c ;
            RSW.ScheduledShipDate=DSR.D365_Scheduled_Ship_Date__c;
            RSW.SerialNumber=DSR.D365_Serial_Number__c;
            RSW.SeriesCollection=DSR.D365_Series_Collection__c;
            RSW.SOCreateDate=Date.ValueOf(DSR.D365_SO_Create_Date__c);
            RSW.SSCCNumber=DSR.D365_SSCC_Number__c;
            RSW.State=DSR.D365_State__c;
            RSW.Status=DSR.D365_Status__c;
            RSW.SubtotalAmount=DSR.D365_Subtotal_Amount__c;
            RSW.SupplierDirectShipOnly=DSR.D365_Supplier_Direct_Ship_Only__c;
            RSW.TimeofActivity=DSR.D365_Time_of_Activity__c;
            RSW.TotalCharges=DSR.D365_Total_Charges__c;
            RSW.TotalDiscount=DSR.D365_Total_Discount__c;
            RSW.Totalsalesorderdiscount=DSR.D365_Total_sales_order_discount__c;
            RSW.TrackingNumber=DSR.D365_Tracking_Number__c;
            RSW.TripNumber=DSR.D365_Trip_Number__c;
            RSW.Type=DSR.D365_Type__c;
            RSW.Unit=DSR.D365_Unit__c;
            RSW.UnitCharge=DSR.D365_Unit_Charge__c;
            RSW.UnitDiscount=DSR.D365_Unit_Discount__c;
            RSW.UnitPrice=DSR.D365_Unit_Price__c;
            RSW.UnitPricePriortoDiscount=DSR.D365_Unit_Price_Prior_to_Discount__c;
            RSW.User=DSR.D365_User__c;
            RSW.VendorAccount=DSR.D365_Vendor_Account__c;
            RSW.VendorInvoiceAccountName=DSR.D365_Vendor_Invoice_Account_Name__c;
            RSW.Void1=DSR.D365_Void__c;
            RSW.ZipPostalCode=DSR.D365_Zip_Postal_Code__c;
            
        rslist.add(RSW);
         system.debug('RSW----'+RSW);   
            
                      }

                  }
       
        return rslist;
    }
              
    @AuraEnabled
    public static List<D365_SalesOrder__c> getOrderschild(String AId){
        system.debug('ParentId----'+AId);
        List < D365_SalesOrder__c> lstOfchild = [SELECT Id,D365_Address__c,D365_Address_1__c,D365_RSA_Indicator__c,D365_City__c,D365_State__c,
                                                     D365_Type__c,D365_Sales_Order_Qty__c,D365_Sales_Order_Number__c,D365_Customer_Name__c,
                                                     D365_Order_Submit_Date_Time__c,D365_Payment_Method__c,D365_Order_Status__c,D365_Customer_ID__c,
                                                     D365_Item_Number__c,D365_Product_Name__c,Parent_Sales_Order__c,D365_Adjustment_Type__c                                                     
                                                     From D365_SalesOrder__c where Parent_Sales_Order__c =: AId];
        
        system.debug('lstOfchild----'+lstOfchild);
        return lstOfchild;
    }
}