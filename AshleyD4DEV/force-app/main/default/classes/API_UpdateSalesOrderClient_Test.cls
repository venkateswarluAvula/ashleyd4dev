@isTest
private class API_UpdateSalesOrderClient_Test {
	@testSetup
	static void setupTestData(){
		// Create custom setting
        TestDataFactory.prepareAPICustomSetting();
    }

    @isTest 
	static void testUpdateContactStatusSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateContactStatus('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 44,'vamsi.pulikallu');
		Test.stopTest();

		System.assert(response.isSuccess);

	}
    
    @isTest 
	static void testUpdateContactStatusError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'error', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateContactStatus('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 44, 'vamsi.pulikallu');
		Test.stopTest();

		//System.assert(response.isSuccess);

	}
    
    @isTest 
	static void testUpdateASAPSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', '1990/"',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateShippingAddress('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 44, 'Test Customer',
		'123 test street', '', 'Atlanata', 'GA', '30067', system.today());
		Test.stopTest();

		System.assert(response.isSuccess);

	}

	@isTest 
	static void testUpdateASAPError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(400, 'error', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateShippingAddress('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 44, 'Test Customer',
		'123 test street', 'Apt 100', 'Atlanata', 'GA', '30067', system.today());
		Test.stopTest();

		System.assert(response.isSuccess == false);

	}
    
	@isTest 
	static void testUpdateShippingAddressSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'advise the customer',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateShippingAddress('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 44, 'Test Customer',
		'123 test street', 'Apt 100', 'Atlanata', 'GA', '30067', system.today());
		Test.stopTest();

		//System.assert(response.isSuccess);

	}

	@isTest 
	static void testUpdateShippingAddressError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'error', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateShippingAddress('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 44, 'Test Customer',
		'', 'Apt 100', 'Atlanata', 'GA', '30067', system.today());
		Test.stopTest();

		System.assert(response.isSuccess == false);

	}
	
	@isTest 
	static void testUpdateHotStatusSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderHotStatus('testfullfilledbyid', '00000001','vamsi.pulikallu',true,'9','10', Date.today(),44);
		Test.stopTest();

		System.assert(response.isSuccess);

	}
    
    @isTest 
	static void testUpdateHotStatusSuccess1() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderHotStatus('testfullfilledbyid', '00000001','vamsi.pulikallu',true,'9','10',null,44);
		Test.stopTest();

	}
    
    	//public static API_BaseResponse updateSalesOrderHotStatus(string fullfilledById, string orderNumber, boolean hotStatus, Date deliveryDate){
	@isTest 
	static void testUpdateHotStatusError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'error', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderHotStatus('testfullfilledbyid', '00000001','vamsi.pulikallu',true,'9','10', Date.today(),44);
		Test.stopTest();

		System.assert(response.isSuccess == false);

	}

	@isTest 
	static void testUpdateDeliveryDateSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderDeliveryDate('testfullfilledbyid', '00000001',true, 55,  Date.newInstance(2018, 1, 1), 'home x', 'reasoncode');
		Test.stopTest();

		System.assert(response.isSuccess);

	}

	@isTest 
	static void testUpdateDeliveryDateError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(400, 'error', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderDeliveryDate('testfullfilledbyid', '00000001',true, 55,  Date.newInstance(2018, 1, 1), 'home x', 'reasoncode');
		Test.stopTest();

		System.assert(response.isSuccess == false);

	}

	@isTest 
	static void testUpdateDeliveryDateWithNullDeliveryDate() {
        user u = new user(FederationIdentifier='1');
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_UpdateSalesOrderClient.buildASAPStatusURI('testfullfilledbyid', '00000001',true, Date.today());
        API_UpdateSalesOrderClient.buildDeliveryDateUpdateURI('testfullfilledbyid', '00000001',true,1, System.today(),'sdfg','xcvbn');
        API_UpdateSalesOrderClient.formatDateForOrderAPICall(Date.newInstance(2019, 06, 9));
        API_UpdateSalesOrderClient.getUserNameForAPiCall(u);
		Test.stopTest();

		//System.assert(response.isSuccess == false);

	}

	@isTest 
	static void testUpdateDeliveryCommentsSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderDeliveryComments('testfullfilledbyid', 'some comments', 23, 'ordernumber', 'Homex');
		Test.stopTest();

		System.assert(response.isSuccess);

	}
	@isTest 
	static void testUpdateDeliveryCommentError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'some error', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderDeliveryComments('testfullfilledbyid', 'some comments', 23, 'ordernumber', 'Homex');
		Test.stopTest();

		System.assert(response.isSuccess == false);

	}

	@isTest 
	static void testUpdateOrderCommentsSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderComments('testfullfilledbyid', 'some order notes', 'vamsi.pulikallu', 'ordernumber', 44);
		Test.stopTest();

		System.assert(response.isSuccess);

	}

	@isTest 
	static void testUpdateOrderCommentsError() {
		Test.startTest();
        API_BaseResponse response1 = API_UpdateSalesOrderClient.updateSalesOrderComments('', 'some order notes', 'vamsi.pulikallu', 'ordernumber', 44);
        API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderDeliveryDate('testfullfilledbyid', '00000001',true, 55,  Date.newInstance(2018, 1, 1), 'home x', 'reasoncode');
        API_BaseResponse response2 = API_UpdateSalesOrderClient.updateShippingAddress('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 44, 'Test Customer','123 test street', '', 'Atlanata', 'GA', '30067', system.today());
        API_BaseResponse response3 = API_UpdateSalesOrderClient.updateSalesOrderHotStatus('testfullfilledbyid', '00000001','vamsi.pulikallu',true,'9','10', Date.today(),44);
        API_BaseResponse response4 = API_UpdateSalesOrderClient.updateSalesOrderVIPLIBStatus('testfullfilledbyid', 'some order notes', 'vamsi.pulikallu', true,true,false,false);
        API_BaseResponse response5 = API_UpdateSalesOrderClient.updateSalesOrderservicelevel('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 'validation','THD');
        Test.stopTest();

		System.assert(response1.isSuccess == false);
        System.assert(response.isSuccess == false);
        System.assert(response2.isSuccess == false);
        System.assert(response3.isSuccess == false);
        System.assert(response4.isSuccess == false);
        System.assert(response5.isSuccess == false);
	}
    
    @isTest 
	static void testUpdateOrderCommentsCatch() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'some errors', 'sampleResponse',new Map<String, String>()));	
		API_UpdateSalesOrderClient.buildOrderCommentsUpdateURI(null, 'some order notes', 'vamsi.pulikallu', 'ordernumber', 44);
        API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderComments('testfullfilledbyid', 'some order notes', 'vamsi.pulikallu', 'ordernumber', 44);
		Test.stopTest();

		//System.assert(response.isSuccess == false);

	}
    
    @isTest 
	static void testUpdateVIPLIBStatusSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderVIPLIBStatus('testfullfilledbyid', 'some order notes', 'vamsi.pulikallu', true,true,false,false);
		Test.stopTest();

		System.assert(response.isSuccess);

	}
    
    @isTest 
	static void testUpdateVIPLIBStatusSuccess1() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderVIPLIBStatus('testfullfilledbyid', 'some order notes', 'vamsi.pulikallu', true,false,false,false);
		Test.stopTest();

		System.assert(response.isSuccess);

	}
    
    @isTest 
	static void testUpdateVIPLIBStatusSuccess2() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderVIPLIBStatus('testfullfilledbyid', 'some order notes', 'vamsi.pulikallu', true,true,true,false);
		Test.stopTest();

		System.assert(response.isSuccess);

	}
    
    @isTest 
	static void testUpdateVIPLIBStatusError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'some errors', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderVIPLIBStatus('testfullfilledbyid', 'some order notes', 'vamsi.pulikallu', true,true,false,false);
		Test.stopTest();

		System.assert(response.isSuccess == false);

	}
    
    @isTest 
	static void testUpdateServiceLevelSuccess() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderservicelevel('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 'validation','THD');
		Test.stopTest();

		System.assert(response.isSuccess);

	}
    @isTest 
	static void testUpdateServiceLevelError() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'some errors', 'sampleResponse',new Map<String, String>()));	
		API_BaseResponse response = API_UpdateSalesOrderClient.updateSalesOrderservicelevel('testfullfilledbyid', '00000001', 'vamsi.pulikallu', 'validation','THD');
		Test.stopTest();

		System.assert(response.isSuccess == false);

	}
}