public class D365_salesOrder{
        
    @AuraEnabled            
    public Static List<RSalesOrderWrapper> SalesOrderData(){
        
        List<RSalesOrderWrapper> SOwrapItems = new List<RSalesOrderWrapper>();
        List<D365_SalesOrder__c> Dsalesorderlst = new List<D365_SalesOrder__c>();
        List<D365_SalesOrder__c> SoDetaillst = new List<D365_SalesOrder__c>();
        List<D365_SalesOrder__c> Sochildlst = new List<D365_SalesOrder__c>();
        
        // Fetching Accesstoken
        D365_SalesOrderAuthorization sora = new D365_SalesOrderAuthorization(); 
        String accessTkn = sora.accessTokenData();   
        system.debug('accessTkn-----------'+ accessTkn);
        
       // system.debug('AId----'+ salesforceAccountId);
        //String conId = 0012F00000HB5j6QAD;
        
       String conId = '\'0012F00000HB5j6QAD\''; // need to load the component retrived id.
        system.debug('conId----'+ conId);
        String conId1 = EncodingUtil.urlEncode(conId, 'UTF-8');
        system.debug('conId1----'+ conId1);
        String test = 'ContactID eq'+ ' ' +conId;
        system.debug('test-----'+ test);
        String test2 = test.replaceAll(' ','%20');
        system.debug('test2-----'+ test2);
        String EPoint = 'https://cara-api-dev-slot1.azurewebsites.net/odata/CommonSalesOrderHeader?$filter=' + test2;
        //String EPoint = System.Label.CaraSORoutingAPIEndPoint+'CommonSalesOrderHeader?$filter=' + test2;
        EPoint=  Epoint.replace('+','%20');
        system.debug('EPoint-----'+ EPoint);
        
        Http http = new Http();
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpResp = new HttpResponse();
        httpReq.setHeader('Authorization', 'Bearer ' + accessTkn);
        httpReq.setEndpoint(EPoint);
        httpReq.setHeader('Content-Type', 'application/json');
        httpReq.setMethod('GET');
        httpResp = http.send(httpReq);
        system.debug('resp-------'+httpResp.getBody());
        system.debug('httpResp-------'+httpResp);
        
        String response;
        if (httpResp.getStatusCode() == 200) {
            response = httpResp.getBody(); 
            system.debug('response--RD---'+response);
            string rsp = response.replace('\\','');
            system.debug('rsp--RD---'+rsp); 
            string rsp1 = rsp.replace('"EDAJSON":"{','"EDAJSON":[{');
            string response2 = rsp1.replace('}","Fulfillerid":"','}],"Fulfillerid":"');
            system.debug('rsp1--RD---'+rsp1);
            system.debug('response2-----'+response2);
            
            D365_SalesOrderWrapper rso = new D365_SalesOrderWrapper();
            
            // Deserialize Main response
            rso = (D365_SalesOrderWrapper) JSON.deserialize(response2, D365_SalesOrderWrapper.class);    
            system.debug('rso----------'+rso );
            
            if(!rso.value.isEmpty()){
            
                List<D365_SalesOrderWrapper.value> SOValues = rso.value;
                system.debug('SOValues----------------'+SOValues);
                system.debug('SOValues size----------------'+SOValues.size());
                
               
               //Response For Loop
                for (D365_SalesOrderWrapper.value SOWrap: SOValues) {
                    system.debug('SOWrap----------------'+SOWrap);
                    system.debug('EDAJSON----------------'+SOWrap.EDAJSON);
                    
                    
                    // deserialize EDAJSON values
                    D365_SalesOrderWrapper EDAJSONval = new D365_SalesOrderWrapper();
                            
                    
                    // Assigning Response Values to Sobject
                    D365_SalesOrder__c Dsalesorder = new D365_SalesOrder__c();
                    
                    // Assigning Response Values to Wraperclass
                    RSalesOrderWrapper RSOwrap = new RSalesOrderWrapper();                    
                    
                    RSOwrap.Address1 = SOWrap.BillAddress1;
                    RSOwrap.Address2 = SOWrap.BillAddress2;
                    RSOwrap.ExternalId = String.Valueof(SOWrap.Id)+':'+SOWrap.ContactID; 
                    RSOwrap.RSAIndicator = SOWrap.RSA;
                    RSOwrap.PurchaseLocation  = SOWrap.BlobLocation;
                    RSOwrap.City = SOWrap.BillCity;
                    RSOwrap.State = SOWrap.BillState;
                    RSOwrap.CustomerType = SOWrap.CustomerType;
                    RSOwrap.Fulfillerid = SOWrap.Fulfillerid;
                    RSOwrap.OrderSubType = SOWrap.OrderSubType;
                    RSOwrap.OrderType = SOWrap.OrderType;
                    RSOwrap.Profitcenter = SOWrap.Profitcenter;
                    RSOwrap.SalesOrderNumber = SOWrap.SalesOrderNo;
                    RSOwrap.CustomerName =  SOWrap.CustomerName;
                    RSOwrap.StoreLocation = SOWrap.StoreLocation;
                    RSOwrap.SOCreateDate = Date.ValueOf(SOWrap.SalesOrderDate);
                    RSOwrap.PaymentMethod = SOWrap.PaymentType;
                    RSOwrap.CustomerType = SOWrap.CustomerType;
                    RSOwrap.OrderStatus = SOWrap.OrderStatus;
                    RSOwrap.CustomerID = SOWrap.ContactID;
                    
                    system.debug('RSOwrap-------'+RSOwrap);
                    SOwrapItems.add(RSOwrap);
                    system.debug('SOwrapItems size-------'+SOwrapItems.size());
                    
                    
                    // --------Loop for EDAJSON values--------
                    for(D365_SalesOrderWrapper.EDAJSON jasonval: SOWrap.EDAJSON){ 
                        system.debug('jasonval-------'+jasonval);
                        system.debug('jasonval sodetail-----'+jasonval.SODetails);
                        system.debug('sodetail size----------------'+jasonval.SODetails.size());
                        
                        //D365_SalesOrder__c DsoEDAJson = new D365_SalesOrder__c();
                        //Mapping values from inner Json (EDAJSON)
                        Dsalesorder.D365_Item_Number__c= String.Valueof(jasonval.ItemCount);
                        
                        //Dsalesorderlst.add(DsoEDAJson);
                        
                        RSOwrap.SalesOrderNumber = jasonval.OrderNumber;
                        
                        
                        
                        
                        // -------------SoDetails-------------------
                            //Integer sno = 0;
                            //sno = sno +1;
                            //SoDetailitem.son__c = sno;
                        if(jasonval.SODetails.size()>0){
                            for(D365_SalesOrderWrapper.cls_SODetails soDetail : jasonval.SODetails){
                                system.debug('soDetail-------'+soDetail);
                                system.debug('soDetail OrderNumber-------'+soDetail.OrderNumber);
                                D365_SalesOrder__c SoDetailitem = new D365_SalesOrder__c();
                                
                                SoDetailitem.D365_Sales_Order_Number__c = soDetail.OrderNumber;
                                SoDetailitem.D365_Payment_Method__c = soDetail.PaymentType;
                                //SoDetailitem.Parent_Sales_Order__c = 
                                SoDetailitem.D365_Sales_Order_Qty__c = soDetail.BOQty;
                                SoDetailitem.D365_Address__c = soDetail.ShiptTo.AddressLine1 +' '+soDetail.ShiptTo.AddressLine2;
                                SoDetailitem.D365_Product_Name__c = soDetail.ItemNumber;
                                SoDetailitem.D365_Adjustment_Type__c = soDetail.DeliveryType;
                                
                                SoDetaillst.add(SoDetailitem);
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    
                    // ------upserting the response into D365 sales order object.--------------
                    
                    Dsalesorder.D365_Address_1__c = SOWrap.BillAddress1;
                    Dsalesorder.D365_Address_2__c = SOWrap.BillAddress2;
                    Dsalesorder.D365_RSA_Indicator__c = SOWrap.RSA;
                    Dsalesorder.D365_City__c = SOWrap.BillCity;
                    Dsalesorder.D365_State__c = SOWrap.BillState;
                    Dsalesorder.D365_Type__c = SOWrap.CustomerType;
                    Dsalesorder.D365_Fulfiller_phone_number__c = SOWrap.Fulfillerid;
                    //Dsalesorder.OrderSubType = SOWrap.OrderSubType;
                    //Dsalesorder.OrderType = SOWrap.OrderType;
                    //Dsalesorder.Profitcenter = SOWrap.Profitcenter;
                    Dsalesorder.D365_Sales_Order_Number__c = SOWrap.SalesOrderNo;
                    Dsalesorder.D365_Customer_Name__c =  SOWrap.CustomerName;
                    Dsalesorder.D365_Order_Submit_Date_Time__c = Date.ValueOf(SOWrap.SalesOrderDate);
                    Dsalesorder.D365_Payment_Method__c = SOWrap.PaymentType;
                    Dsalesorder.D365_Order_Status__c = SOWrap.OrderStatus;
                    Dsalesorder.D365_Customer_ID__c = SOWrap.ContactID;
                    
                    Dsalesorderlst.add(Dsalesorder);
                    
                }
            }
        }   
        system.debug('SoDetaillst size-------'+SoDetaillst.size()+'---'+SoDetaillst);
        system.debug('Dsalesorderlst----'+Dsalesorderlst.size() +'--'+Dsalesorderlst);
        Schema.SObjectField extIdField = D365_SalesOrder__c.Fields.D365_Sales_Order_Number__c;
        list<Database.UpsertResult> result = database.upsert(Dsalesorderlst, extIdField, false);
        
        system.debug('Dsalesorderlst after----'+Dsalesorderlst.size() +'--'+Dsalesorderlst);
        system.debug('SoDetaillst after----'+SoDetaillst.size() +'--'+SoDetaillst);
        
        
        
        For(D365_SalesOrder__c parentrec: Dsalesorderlst ){
            system.debug('parentrec-------'+parentrec);
            for(D365_SalesOrder__c childrec: SoDetaillst){
                if(parentrec.D365_Sales_Order_Number__c == childrec.D365_Sales_Order_Number__c){
                    childrec.Parent_Sales_Order__c = parentrec.id;
                    system.debug('childrec-------'+childrec );
                    Sochildlst.add(childrec);
                }
            }
        }
        /* */
        
        
        
        
        
        Schema.SObjectField extIdField1 = D365_SalesOrder__c.Fields.D365_Sales_Order_Number__c;
        //list<Database.UpsertResult> childresult = database.upsert(SoDetaillst, extIdField1, false);
        //system.debug('childresult-------'+childresult );
        
        //Upserting Child records.
        //upsert Sochildlst;
        
        
         system.debug('SOwrapItems return-------'+SOwrapItems);    
         system.debug('SOwrapItems size-------'+SOwrapItems.size());  
         return SOwrapItems;
    }
    
    
    
    
     
}