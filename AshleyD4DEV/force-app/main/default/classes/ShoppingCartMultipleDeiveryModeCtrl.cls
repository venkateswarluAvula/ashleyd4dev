public class ShoppingCartMultipleDeiveryModeCtrl {
    
    @AuraEnabled  
    public static String saveDeliveryMode(String[] lineItemIdList, 
                                          String selectedShippingWay,String accId,string deliveryType){
                                              String error='';
                                              try{
                                                  
                                                  List<Shopping_cart_line_item__c> shoppingDetail = new  List<Shopping_cart_line_item__c>();
                                                
                                                  List<Shopping_cart_line_item__c> lineItem = [Select Id,Product_SKU__c,Delivery_Mode__c,Quantity__c,DeliveryType__c,WarrantySku__c from Shopping_cart_line_item__c where Id IN:lineItemIdList];
                                                  for(Shopping_cart_line_item__c itemId:lineItem){
                                                  itemId.Delivery_Mode__c = selectedShippingWay;
                                                   shoppingDetail.add(itemId);   
                                                  }
                                                  Shopping_cart_line_item__c opplineItems = [Select Id, Opportunity__c,Quantity__c from Shopping_cart_line_item__c where id=:lineItemIdList[0]];
                                    
                                                  if(selectedShippingWay == 'DS' || selectedShippingWay == 'TW'){                                           
                                                      if(shoppingDetail.size()>0){
                                                          
                                                          ShoppingCartItemDetailsAPIHelper taxAPIHelper = new ShoppingCartItemDetailsAPIHelper();
                                                          String sourceURL = taxAPIHelper.getAPIEndpoint();
                                                          System.debug('sourceURL:'+sourceURL);
                                                          String rawPost = taxAPIHelper.prepareCartItemRawPost(shoppingDetail,accId);
                                                          system.debug('rawPost---'+rawPost); 
                                                          String resJSON = taxAPIHelper.connectAPIPostJSON(sourceURL, rawPost);                                             
                                                          system.debug('resJSON--'+resJSON);
                                                          if(resJSON.contains('Direct Shipment is  not available') || resJSON.contains('TakeWith is not available'))
                                                          {                                                          
                                                              error=resJSON; 
                                                              error=error.remove('"');
                                                              system.debug('error--'+error);
                                                          }
                                                      }
                                                  }
                                                  if(String.isEmpty(error))
                                                  {
                                                       system.debug('Updated enter ');
                                                      update shoppingDetail;    
                                                  
                                                  List<Shopping_cart_line_item__c> lineitems=[Select Id, Product_SKU__c, Opportunity__c,Quantity__c,Delivery_Mode__c,DeliveryDate__c
                                                                                              From Shopping_cart_line_item__c 
                                                                                              Where Opportunity__c=: opplineItems.Opportunity__c and Delivery_Mode__c=: selectedShippingWay]; 
                                                  Opportunity opp = ShoppingCartDetailCmpCtrl.getShoppingCart(accId);
                                                  system.debug('opp---'+opp);
                                                  List<Shopping_cart_line_item__c> cartlineItems = ShoppingCartDetailCmpCtrl.getShoppingCartLineItems(accId);
                                                  Map<string,Shopping_cart_line_item__c> HDMap = New Map<string,Shopping_cart_line_item__c>();
                                                  for(Shopping_cart_line_item__c Hdtype:cartlineItems){
                                                      HDMap.put(Hdtype.Delivery_Mode__c,Hdtype);  
                                                  } 
                                                  system.debug('HDMap--'+HDMap);
                                                  if(!HDMap.containsKey('HD') && (opp.Shipping_Discount_Status__c == 'Discount Pending Manager Approval' || opp.Shipping_Discount_Status__c == 'Discount Approved' || opp.NDD_Discount_Status__c == 'NDD Discount Pending Manager Approval' || opp.NDD_Discount_Status__c == 'NDD Discount Approved')){
                                                      ProcessInstanceWorkitem[] workItems = [ SELECT Id
                                                                                             FROM ProcessInstanceWorkitem 
                                                                                             WHERE ProcessInstance.TargetObjectId =:opp.Id
                                                                                             AND ProcessInstance.Status = 'Pending' ];
                                                      system.debug('workItems---'+workItems);
                                                      //To avoid System.ListException: List index out of bounds
                                                      if(!workItems.isEmpty()){
                                                          Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();  
                                                          pwr.setAction('Removed');
                                                          pwr.setWorkItemId(workItems[0].id);
                                                          
                                                          Approval.ProcessResult result = Approval.process(pwr);
                                                          system.debug('result---'+result);
                                                      }
                                                      
                                                      opp.Shipping_Discount_Status__c = 'Not Applicable';
                                                      opp.Shipping_Discount__c = 0;
                                                      opp.DeliveryItemSKU__c = '';
                                                      opp.Shipping_Reason_Code__c = '';
                                                      opp.NDD_Discount_Status__c = 'Not Applicable';
                                                      opp.NDD_Reason_Code__c = '';
                                                      opp.NDD_Flat_Discount__c = 0;
  
                                                      update opp;    
                                                      
                                                  }
                                                  List<Shopping_cart_line_item__c> lstToBeUpdated= new List<Shopping_cart_line_item__c>();
                                                  if(!lineitems.isEmpty()){
                                                      for(Shopping_cart_line_item__c line:lineitems)
                                                      {                                                      
                                                          if(line.Delivery_Mode__c != 'TW')
                                                          {
                                                              line.DeliveryDate__c=null;
                                                              
                                                          }
                                                          else
                                                          {
                                                              //DEF-0416 default today as delivery date for TW
                                                              line.DeliveryDate__c=Date.today();
                                                          }
                                                          
                                                          
                                                          if(line.Delivery_Mode__c == 'HD')
                                                          {
                                                              line.DeliveryType__c = deliveryType;
                                                          }else{
                                                              
                                                              line.DeliveryType__c = deliveryType;  
                                                          }
                                                          
                                                          lstToBeUpdated.add(line);
                                                          
                                                      }
                                                      
                                                      update lstToBeUpdated;
                                                  }
                                              } 
                                              } catch (Exception ex){
                                                  System.debug(LoggingLevel.ERROR, 'Failed to Update delivery Mode: '+ ex.getMessage());
                                                  ErrorLogController.createLogFuture('ShoppingCartMultipleDeiveryModeCtrl', 'saveDeliveryMode', 'Failed to Update Delivery Mode: ' + ex.getMessage() +  ' Stack Trace: ' + ex.getStackTraceString() );  
                                                  system.debug('error message--'+ex.getMessage());
                                                  throw new AuraHandledException(ex.getMessage());
                                              }                                     
                                              if(String.isNotEmpty(error)){
                                                system.debug('enter error ');
                                                  return error;
                                              }
                                              else{
                                                  return 'Success';
                                              } 
                                          }
    
}