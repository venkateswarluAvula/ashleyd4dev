@isTest
public class ProductLineItemFromOrderValidationTest {
    @isTest static void testGetMethod(){
        //Create Account
        Account delacct = new Account();
        // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
        delacct.Primary_Language__pc = 'English';
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone2 = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone3 = '889' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        delacct.phone = phone;
        delacct.Phone_2__pc = phone2;
        delacct.Phone_3__pc = phone3;
        delacct.PersonEmail = 'pqr@mm.com';
        delacct.Email_2__pc ='pqr@mm.com';
        delacct.Strike_Counter__pc = 10;
        Insert delacct;
        System.debug('delacct-->'+delacct.Id);
        
               
        //Create Case
        Case c = new Case();
        c.Status = 'New';
        c.Priority = 'Medium';
        c.Origin = 'Web';
        c.Subject = 'Subject ' +  Math.random();
        c.Type = 'Delivery_Order_inquiry';
        c.Sub_Type__c = CaseTriggerHandler.CASE_SUB_TYPE_DAMAGE;			
        c.AccountId = delacct.Id;
       // c.Address__c = testDelAd.Id;
        c.Sales_Order__c = '21111310:001q000000raI3DAAU';
        Insert c;
        
        //Create PLI
        ProductLineItem__c PLI = new ProductLineItem__c();
        PLI.Case__c = c.Id;
        PLI.Part_Order_Tracking_Number__c = '999999';
        PLI.Defect_Location__c = 'BACK';
        // PLI.Part_Order_Shipping_Date__c = '2018/01/17';
        insert PLI;
        //Create PLI
        ProductLineItem__c PLI2 = new ProductLineItem__c();
        PLI2.Case__c = c.Id;
        PLI2.Part_Order_Tracking_Number__c = '999999';
        PLI2.Defect_Location__c = 'BACK';
        // PLI.Part_Order_Shipping_Date__c = '2018/01/17';
        insert PLI2;
        
        POJSON data1 = new POJSON();
        POJSON.ExtDetail data = new POJSON.ExtDetail();
        data.Customer = '8888300';
        data.ShipTo = '480';
        data.PONumber = '37996C';
        data.Address1 = '2204 DELIGHTFUL DR';
        data.Address2 = '';
        data.Address3 = 'RUSKIN';
        data.Street = 'FL';
        data.ZipCode = '33570';
        data.ShipDate = '20180117';
        data.UPSTracking = '1Z3545130317318146';
        data.DefectCode = 'PF';
        data.Defect = 'PEELING FINISH';
        data.DefectLocationCode = 'BK';
        data.DefectLocation = 'BACK';
        data.Model = '9880035';
        data1.ExtDetail = data;
        
        Util__c PLIStatusUpdate = new Util__c();
        PLIStatusUpdate.PLI_ID__c = String.valueOf(PLI.id);
        PLIStatusUpdate.Callout_Status__c = False;
        insert PLIStatusUpdate;
        try{
            Boolean resmsg = ProductLineItemFromOrderValidation.updateTrackingNumber(PLI.Id, '1Z3545130317318146');
        }
        Catch(exception ex){}
        try{
            Boolean res = ProductLineItemFromOrderValidation.UpdateValidation('PLIH', PLI.Id);
        }Catch(exception ex){}
        try{
            Boolean res2 = ProductLineItemFromOrderValidation.Validation('PLIH', PLI.Id);
        }Catch(exception ex){}
        
    }
    
    @isTest static void testGetMethod2(){
        //Create Account
        Account delacct = new Account();
        // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
        delacct.Primary_Language__pc = 'English';
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone2 = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone3 = '889' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        delacct.phone = phone;
        delacct.Phone_2__pc = phone2;
        delacct.Phone_3__pc = phone3;
        delacct.PersonEmail = 'pqr@mm.com';
        delacct.Email_2__pc ='pqr@mm.com';
        delacct.Strike_Counter__pc = 10;
        Insert delacct;
        System.debug('delacct-->'+delacct.Id);
        
               
        //Create Case
        Case c = new Case();
        c.Status = 'New';
        c.Priority = 'Medium';
        c.Origin = 'Web';
        c.Subject = 'Subject ' +  Math.random();
        c.Type = 'Delivery_Order_inquiry';
        c.Sub_Type__c = CaseTriggerHandler.CASE_SUB_TYPE_DAMAGE;			
        c.AccountId = delacct.Id;
     //   c.Address__c = testDelAd.Id;
        c.Sales_Order__c = '21111310:001q000000raI3DAAU';
        Insert c;
        
        //Create PLI
        ProductLineItem__c PLI = new ProductLineItem__c();
        PLI.Case__c = c.Id;
        PLI.Part_Order_Tracking_Number__c = '999999';
        PLI.Defect_Location__c = 'BACK';
        // PLI.Part_Order_Shipping_Date__c = '2018/01/17';
        insert PLI;
        //Create PLI
        ProductLineItem__c PLI2 = new ProductLineItem__c();
        PLI2.Case__c = c.Id;
        PLI2.Part_Order_Tracking_Number__c = '999999';
        PLI2.Defect_Location__c = 'BACK';
        // PLI.Part_Order_Shipping_Date__c = '2018/01/17';
        insert PLI2;
        
        POJSON data1 = new POJSON();
        POJSON.ExtDetail data = new POJSON.ExtDetail();
        data.Customer = '8888300';
        data.ShipTo = '480';
        data.PONumber = '37996C';
        data.Address1 = '2204 DELIGHTFUL DR';
        data.Address2 = '';
        data.Address3 = 'RUSKIN';
        data.Street = 'FL';
        data.ZipCode = '33570';
        data.ShipDate = '20180117';
        data.UPSTracking = '1Z3545130317318146';
        data.DefectCode = 'PF';
        data.Defect = 'PEELING FINISH';
        data.DefectLocationCode = 'BK';
        data.DefectLocation = 'BACK';
        data.Model = '9880035';
        data1.ExtDetail = data;
        try{
            Boolean res2 = ProductLineItemFromOrderValidation.UpdateValidation('PLIH', PLI2.Id);
        }Catch(exception ex){}
        try{
            Boolean res = ProductLineItemFromOrderValidation.Validation('PLIH', PLI.Id);
        }Catch(exception ex){}
    }
}