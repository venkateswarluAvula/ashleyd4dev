public with sharing class APIResponseTHD {
    
    
    @AuraEnabled
    public static string THdresponse(){
    List<TresholdAvailableDaysWrapper> tresholddayswrap = new List<TresholdAvailableDaysWrapper>();
     List<String> threshoresponseAvldays = new List<String>(); 
     List<Date> thresholdDays = new List<Date>();
   
     try{
     StoreInfoWrapper si;
     si = StoreInfo.getStoreInfo();
     string fulfiller = si.fulfillerId;
     string profitcenter = si.profitCtr;
     system.debug('fulfiller,profitcenter'+fulfiller+profitcenter);  
     String endpoint = system.label.AshleyApigeeEndpoint+fulfiller+'/salesorders/sales-orders/settings/THRES_HOLD_AVAILABLE_DAYS?profitCenter='+22;
      Http http = new http();
      Httprequest req = new HttpRequest();
      req.setHeader('apikey', system.label.AshleyApigeeApiKey);
      req.setEndpoint(endpoint);
      system.debug('tresholdenabledcall enpoint' + endpoint);
      req.setTimeOut(120000);
      req.setMethod('GET');
      HttpResponse res = http.send(req);

     
        if(res.getStatusCode()==200 && res.getBody() != null ){ 
		string responsedata = res.getBody();
         system.debug('responsedata'+responsedata);
          tresholddayswrap = (List<TresholdAvailableDaysWrapper>)System.JSON.deserialize(responsedata, List<TresholdAvailableDaysWrapper>.class); 
            if(tresholddayswrap != null){
            for(TresholdAvailableDaysWrapper tda: tresholddayswrap){
              string dayname=tda.value;
                threshoresponseAvldays.add(dayname);
            }
            }
            
            
        }else{
            
         tresholddayswrap=null;   
        }
            }catch (Exception e) {
            system.debug('Exception is ' + e);
              tresholddayswrap=null;
            }
  		
        
       return null; 
    }
    
}