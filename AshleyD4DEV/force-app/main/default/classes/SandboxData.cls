global class SandboxData {
    global void runDelete(SandboxContext context) {
        List<String> customSettings = new List<String>{'ConciergeProductAPISetting__c','Finance_Terms_Info__c',
            'Integration_Settings__c','MarketSettings__c','NewOpportunityDefaultAddress__c','OneSourceSettings__c',
            'Profile_Details__c','Trigger_Integration__c','TwilioConfig__c'};
                
                for(Integer i = 0; i<customSettings.size(); i++){
                    String str = 'SELECT Id FROM ' + customSettings.get(i);
                    List<SObject> currList = Database.query(str);
                    delete currList;
                }
    }
    global void runInsert(SandboxContext context) 
    {
        ConciergeProductAPISetting__c Cp = new ConciergeProductAPISetting__c (Name='ProductAPISetting', Id = 'a0Iq0000006PHFEEA4', 
                                                                              ATC_CALL_DAYS_API__c= '/homestores/{0}/salesorders/sales-orders/settings/ATC_CALL_DAYS?profitCenter={1}',
                                                                              ATPAPISetting__c = '/retail?as={0}&sqt={1}|{2}|{3}&pc={4}',
                                                                              CC_Print_Sales_API_Path__c = '/homestores/{0}/finance/payment-terminals/cc-sales/print?apikey={1}',
                                                                              CC_Sales_API_Path__c = '/homestores/{0}/finance/payment-terminals/cc-sales?apikey={1}',
                                                                              CreditAuthorizationAPI_Path__c = '/homestores/{0}/finance/credit-authorizations',
                                                                              Discount_Threshhold_API__c = '/homestores/{0}/salesorders/sales-orders/settings/DISCOUNT_MANAGER_OVERRIDE_THRESHOLD?profitCenter={1}',
                                                                              Payment_API_Terms_Path__c = '/homestores/{0}/finance/payment-terminals/payment-types/{1}?apikey={2}',
                                                                              Payment_API_Types_Path__c = '/homestores/{0}/finance/payment-terminals/payment-types?apikey={1}',
                                                                              Payment_Terminal__c = '/homestores/{0}/finance/payment-terminals/{1}?apikey={2}',
                                                                              Product_API_Category_Path__c = '/productinformation/retail/categories{0}?apikey={1}',
                                                                              Product_API_ItemDetail_Path__c = '/homestores/{0}/item-details?profitCenter={1}',
                                                                              Product_API_NavigatableCategory_Path__c = '/productinformation/retail/navigablecategories{0}?apikey={1}',
                                                                              Product_API_Price_Path__c = '/homestores/{0}/price-and-availability/products/{1}?profitCenter={2}',
                                                                              Product_API_ProdDetail_Path__c = '/productinformation/retail/products?sku={0}&apikey={1}',
                                                                              Product_API_ProdList_Path__c = '/productinformation/retail/categories/{0}/productabstracts?apikey={1}',
                                                                              Product_API_SalesOrders_Path__c = 'callout:Cara_Order_Odata_Sandbox/cara/sales-orders?sfGuestId={0}&storeID={1}',
                                                                              Product_API_SuspendedSales_Path__c = '/homestores/{0}/suspended-sales?profitCenter={1}',
                                                                              Product_Filter_API_Path__c = '/{0}/dev_ashleyfurniture_com-u1492545066797/search?{1}',
                                                                              Product_Reviews_API_Reviews_Path__c = '/data/reviews.json?apiversion={0}&passkey={1}&stats=reviews&filter=productid:{2}',
                                                                              Product_Reviews_API_Statistics_Path__c = '/data/statistics.json?apiversion={0}&passkey={1}&stats=reviews&filter=productid:{2}',
                                                                              Product_Reviews_API_Version__c = '5.4',
                                                                              Void_Payment__c = '?apikey={0}'
                                                                             );
        insert Cp;
    }
    {
        Finance_Terms_Info__c ft = new Finance_Terms_Info__c (Name='FinanceTermsInfo', Id = 'a0Sq0000004oFqcEAE', Key__c= '4r7MemB7fQEHPqve4kjKEUe2hFXfBUn9', Salt__c= 'S6xRCXyDw4e65XSN');
        insert ft;
    }
    {
        List<Integration_Settings__c> listIntegration_Settings = New List<Integration_Settings__c>();
        
        Integration_Settings__c Is1 = new Integration_Settings__c();
        Is1.Name = 'Order_Update_Sandbox';
        Is1.Id = 'a0Kq000000430BeEAI';
        Is1.API_Key__c = 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        Is1.End_Point_URL__c = '';
        listIntegration_Settings.add(Is1);
        
        Integration_Settings__c Is2 = new Integration_Settings__c();
        Is2.Name = 'ProductFilterAPI';
        Is2.Id = 'a0Kq000000430BfEAI';
        Is2.API_Key__c = '7e84d34e5d696f6eaac042a422b5e9f9';
        Is2.End_Point_URL__c = 'http://search.unbxdapi.com';
        listIntegration_Settings.add(Is2);
        
        Integration_Settings__c Is3 = new Integration_Settings__c();
        Is3.Name = 'ATC_Prod';
        Is3.Id = 'a0Kq000000430BgEAI';
        Is3.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is3.End_Point_URL__c = '';
        listIntegration_Settings.add(Is3);
        
        Integration_Settings__c Is4 = new Integration_Settings__c();
        Is4.Name = 'Cara_Order_Odata_Dev';
        Is4.Id = 'a0Kq000000430BhEAI';
        Is4.API_Key__c = '';
        Is4.End_Point_URL__c = 'https://test.cara.ashleyretail.com/odata/';
        listIntegration_Settings.add(Is4);
        
        Integration_Settings__c Is5 = new Integration_Settings__c();
        Is5.Name = 'SuspendedSalesAPI';
        Is5.Id = 'a0Kq000000430BiEAI';
        Is5.API_Key__c = 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        Is5.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com';
        listIntegration_Settings.add(Is5);
        
        Integration_Settings__c Is6 = new Integration_Settings__c();
        Is6.Name = 'ProductReviewsAPI';
        Is6.Id = 'a0Kq000000430BjEAI';
        Is6.API_Key__c = 'cahEQLoVNXaj7oT8J9Og6xTKBdO1kFPNZWNQtCvQ13a3A';
        Is6.End_Point_URL__c = 'http://api.bazaarvoice.com';
        listIntegration_Settings.add(Is6);
        
        Integration_Settings__c Is7 = new Integration_Settings__c();
        Is7.Name = 'RetailAPIs';
        Is7.Id = 'a0Kq000000430BkEAI';
        Is7.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is7.End_Point_URL__c = 'https://apigw3.ashleyfurniture.com';
        listIntegration_Settings.add(Is7);
        
        Integration_Settings__c Is8 = new Integration_Settings__c();
        Is8.Name = 'PaymentTypesAPI';
        Is8.Id = 'a0Kq000000430BlEAI';
        Is8.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is8.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com';
        listIntegration_Settings.add(Is8);
        
        Integration_Settings__c Is9 = new Integration_Settings__c();
        Is9.Name = 'ATC_Sandbox';
        Is9.Id = 'a0Kq000000430BmEAI';
        Is9.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is9.End_Point_URL__c = '';
        listIntegration_Settings.add(Is9);
        
        Integration_Settings__c Is10 = new Integration_Settings__c();
        Is10.Name = 'EcommPricingAPI';
        Is10.Id = 'a0Kq000000430BnEAI';
        Is10.API_Key__c = 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        Is10.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com';
        listIntegration_Settings.add(Is10);
        
        Integration_Settings__c Is11 = new Integration_Settings__c();
        Is11.Name = 'PaymentAPI';
        Is11.Id = 'a0Kq000000430BoEAI';
        Is11.API_Key__c = 'XXMGTyNiiGImIsvpInhs2H0TUydKcMJi';
        Is11.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com/authorizationgateway/authorizations';
        listIntegration_Settings.add(Is11);
        
        Integration_Settings__c Is12 = new Integration_Settings__c();
        Is12.Name = 'FinanceTerms';
        Is12.Id = 'a0Kq000000430BpEAI';
        Is12.API_Key__c = 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        Is12.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com/finance-terms/';
        listIntegration_Settings.add(Is12);
        
        Integration_Settings__c Is13 = new Integration_Settings__c();
        Is13.Name = 'Dispatch_Track';
        Is13.Id = 'a0Kq000000430BqEAI';
        Is13.API_Key__c = '';
        Is13.End_Point_URL__c = 'https://ashleyhomestore.dispatchtrack.com/service_orders';
        listIntegration_Settings.add(Is13);
        
        Integration_Settings__c Is14 = new Integration_Settings__c();
        Is14.Name = 'VoidPaymentsAPI';
        Is14.Id = 'a0Kq000000430BrEAI';
        Is14.API_Key__c = 'XXMGTyNiiGImIsvpInhs2H0TUydKcMJi';
        Is14.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com/authorizationgateway/voids';
        listIntegration_Settings.add(Is14);
        
        Integration_Settings__c Is15 = new Integration_Settings__c();
        Is15.Name = 'AtpAPI';
        Is15.Id = 'a0Kq0000004364EEAQ';
        Is15.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is15.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com/inventory';
        listIntegration_Settings.add(Is15);
        
        Integration_Settings__c Is16 = new Integration_Settings__c();
        Is16.Name = 'CCPaymentAPI';
        Is16.Id = 'a0Kq0000004364JEAQ';
        Is16.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is16.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com';
        listIntegration_Settings.add(Is16);
        
        Integration_Settings__c Is17 = new Integration_Settings__c();
        Is17.Name = 'PaymentTerminalAPI';
        Is17.Id = 'a0Kq0000004364OEAQ';
        Is17.API_Key__c = 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        Is17.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com';
        listIntegration_Settings.add(Is17);
        
        
        Integration_Settings__c Is18 = new Integration_Settings__c();
        Is18.Name = 'ATC_CALL_DAYS API';
        Is18.Id = 'a0Kq0000004364TEAQ';
        Is18.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is18.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com';
        listIntegration_Settings.add(Is18);
        
        Integration_Settings__c Is19 = new Integration_Settings__c();
        Is19.Name = 'CreditAuthorizationAPI';
        Is19.Id = 'a0Kq000000436bKEAQ';
        Is19.API_Key__c = 'NvcXrflst6O2sQurPOzOxytA1bUVvW8t';
        Is19.End_Point_URL__c = 'https://stageapigw.ashleyfurniture.com';
        listIntegration_Settings.add(Is19);
        
        Integration_Settings__c Is20 = new Integration_Settings__c();
        Is20.Name = 'ATC_Sandbox_UpdateDelDate';
        Is20.Id = 'a0K0n0000026pquEAA';
        Is20.API_Key__c = 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        Is20.End_Point_URL__c = '';
        listIntegration_Settings.add(Is20);
        
        Integration_Settings__c Is21 = new Integration_Settings__c();
        Is21.Name = 'ATC_Prod_UpdateDelDate';
        Is21.Id = 'a0K0n0000026ps7EAA';
        Is21.API_Key__c = 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        Is21.End_Point_URL__c = '';
        listIntegration_Settings.add(Is21);
    }
    {
        List<MarketSettings__c> listMarketSettings = New List<MarketSettings__c>();
        
        MarketSettings__c ms1 = new MarketSettings__c();
        ms1.Name = '8888000-130';
        ms1.Id = 'a0Uq00000030xwqEAA';
        ms1.Market_Code__c = 'SWF';
        listMarketSettings.add(ms1);
        
        MarketSettings__c ms2 = new MarketSettings__c();
        ms2.Name = '8888400-473';
        ms2.Id = 'a0Uq00000030xwrEAA';
        ms2.Market_Code__c = 'RLF';
        listMarketSettings.add(ms2);
        
        MarketSettings__c ms3 = new MarketSettings__c();
        ms3.Name = '8888300-480';
        ms3.Id = 'a0Uq00000030xwsEAA';
        ms3.Market_Code__c = 'KWF-FL';
        listMarketSettings.add(ms3);
        
        MarketSettings__c ms4 = new MarketSettings__c();
        ms4.Name = '8888300-164';
        ms4.Id = 'a0Uq00000030xwtEAA';
        ms4.Market_Code__c = 'KWF-GA';
        listMarketSettings.add(ms4);
        
        MarketSettings__c ms5 = new MarketSettings__c();
        ms5.Name = '8888600-656';
        ms5.Id = 'a0Uq00000030xwqEAA';
        ms5.Market_Code__c = 'SLF';
        listMarketSettings.add(ms5);
        
        insert listMarketSettings;
    }
    {
        NewOpportunityDefaultAddress__c No = new NewOpportunityDefaultAddress__c (Name='defaultOppAddress', Id = 'a0Lq0000007BK1yEAG', PostCode__c= '31004', StateOrProvince__c= 'GA');
        insert No;
    }
    {
        OneSourceSettings__c Os = new OneSourceSettings__c (Name='OneSourceConfig', Id = 'a0Mq0000008gKNtEAM', One_Source_API_Key__c= 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713', One_Source_API_Path__c= 'https://stageapigw.ashleyfurniture.com/onesource/store-information/', One_Source_API_Refresh_Minutes__c=1440);
        insert Os;
    }
    {
        List<Profile_Details__c> listProfileDetails = New List<Profile_Details__c>();
        
        Profile_Details__c pd1 = new Profile_Details__c();
        pd1.Name = 'ShowPaymentsTo';
        pd1.Id = 'a0Oq0000007rWguEAE';
        pd1.Profile_Names__c = 'System Administrator';
        
        Profile_Details__c pd2 = new Profile_Details__c();
        pd2.Name = 'DeliveryTechnicians';
        pd2.Id = 'a0Oq0000007rWgvEAE';
        pd2.Profile_Names__c = 'Delivery Driver - Restricted,3P Delivery - Restricted,Technician - Restricted,3P Technician - Restricted';
        
        Profile_Details__c pd3 = new Profile_Details__c();
        pd3.Name = 'Concierges';
        pd3.Id = 'a0Oq0000007rWgwEAE';
        pd3.Profile_Names__c = 'RSA,Store CSR,Store Management,System Administrator';
        
        Profile_Details__c pd4 = new Profile_Details__c();
        pd4.Name = 'ShowCheckoutTo';
        pd4.Id = 'a0Oq0000007rWgxEAE';
        pd4.Profile_Names__c = 'All';
    }
    {
        Trigger_Integration__c Ti = new Trigger_Integration__c (Name='a0O2F000000Kkyr', Id = 'a0Wq0000005xQD5EAM', Account_Updates_to_CARA__c= TRUE);
        insert Ti;
    }
    {
        List<TwilioConfig__c> listTwilioConfig = New List<TwilioConfig__c>();
        
        TwilioConfig__c tc1 = new TwilioConfig__c();
        tc1.Name = 'TWILIO_Sandbox_AshleyProd';
        tc1.Id = 'a0Hq0000004cdF1EAI';
        tc1.AccountSid__c = 'AC1c62d4e8a17a729b389329c31a679c27';
        tc1.ApplicationSid__c = '';
        tc1.AuthToken__c = 'ce443b78f07f79e6e53bbd05e92ad05c';
        tc1.Messaging_Service_Id__c = '';
        
        TwilioConfig__c tc2 = new TwilioConfig__c();
        tc2.Name = 'TWILIO_Sandbox_AshleyProd';
        tc2.Id = 'a0Hq0000004cdF1EAI';
        tc2.AccountSid__c = 'AC1c62d4e8a17a729b389329c31a679c27';
        tc2.ApplicationSid__c = '';
        tc2.AuthToken__c = 'ce443b78f07f79e6e53bbd05e92ad05c';
        tc2.Messaging_Service_Id__c = '';
        
        TwilioConfig__c tc3 = new TwilioConfig__c();
        tc3.Name = 'TwilioSandbox_Previous';
        tc3.Id = 'a0Hq0000004cdF3EAI';
        tc3.AccountSid__c = 'AC1c62d4e8a17a729b389329c31a679c27';
        tc3.ApplicationSid__c = '';
        tc3.AuthToken__c = 'ce443b78f07f79e6e53bbd05e92ad05c';
        tc3.Messaging_Service_Id__c = '';
    }
}