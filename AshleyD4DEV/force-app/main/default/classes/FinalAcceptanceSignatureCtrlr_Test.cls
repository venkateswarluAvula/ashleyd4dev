@isTest
public class FinalAcceptanceSignatureCtrlr_Test{
    
    /* test create sig obj */
    @isTest
    static void testCreateSignatureObj() {
        Account testAcc = setup();
        TestDataFactory.createOneSourceSetting();
        Test.setMock(HttpCalloutMock.class, new StoreInfoMock());
        // Producation Validation Fix
        System.runAs(TestDataFactory.createRSAUser()) {
            Test.startTest();        
            FinalAcceptanceSignatureCtrlr.createSignatureObj(testAcc.Id);
            Test.stopTest();
        }
        
    } 
    
    /* test create sig obj exception */
    @isTest
    static void testCreateSignatureObjException() {
        String custTypeId =[Select id From RecordType Where DeveloperName='Customer' AND SobjectType='Account'].id;

        // create test account
        Account testAcc = new Account(
            RecordTypeId = custTypeId, 
            FirstName    = 'Sig',
            LastName     = 'Test',
            Phone        = '6155551212'
        );
        
        insert testAcc;

        try {
            FinalAcceptanceSignatureCtrlr.createSignatureObj(testAcc.Id);
        } catch (Exception e) {
            
        }
    }   
    
    /* test check for sig */
    @isTest
    static void testCheckForSignature() {
        Account testAcc = setup();
        TestDataFactory.createOneSourceSetting();
        Test.setMock(HttpCalloutMock.class, new StoreInfoMock());
        // Producation Validation Fix
        System.runAs(TestDataFactory.createRSAUser()) {
            Test.startTest();   
            //Id sigId = FinalAcceptanceSignatureCtrlr.createSignatureObj(testAcc.Id);
            
            ContentVersion contVer = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content')
            );
            insert contVer;
            
            ContentDocumentLink docLink = new ContentDocumentLink();
            docLink.ContentDocumentId = [SELECT Id,ContentDocumentId FROM ContentVersion][0].ContentDocumentId;
            //docLink.LinkedEntityId = sigId;
            docLink.ShareType = 'V';
            insert docLink;
            //FinalAcceptanceSignatureCtrlr.checkForSignature(sigId);
            Test.stopTest();
        }
    } 
    
    /* test check for sig + generate exception */
    @isTest
    static void testCheckForSignatureException() {
        try {
            FinalAcceptanceSignatureCtrlr.checkForSignature('abc');
        } catch (Exception e) {
            system.debug('exception is ' + e.getMessage());
        }
    } 
    
    static Account setup () {
        TestDataFactory.prepareAPICustomSetting();
        String custTypeId =[Select id From RecordType Where DeveloperName='Customer' AND SobjectType='Account'].id;

        // create test account
        Account testAcc = new Account(
            RecordTypeId = custTypeId, 
            FirstName    = 'Sig',
            LastName     = 'Test',
            Phone        = '6155551212'
        );
        
        insert testAcc;
        
        // create test opportunity
        Opportunity testOpp = new Opportunity(
            Name      = 'Signature Test',
            StageName = 'Saved Shopping Cart', 
            AccountId = testAcc.Id, 
            CloseDate = system.today()
        );
        
        insert testOpp;  
        
        inserteSignTestData(testAcc.Id, testOpp.Id);
        return testAcc;
    }
    
    
    public static Opportunity prepareShoppingCart(Account acc){
        Opportunity opp =  new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name=acc.Name+'-'+system.today().format();
        opp.StageName='Saved Shopping Cart';
        opp.CloseDate=Date.Today().addDays(30);
        
        insert opp;
        return opp;
    }
    
    public static Electronic_Signature__c inserteSignTestData(String accId, String opptyId){
        Electronic_Signature__c signObj = new Electronic_Signature__c();
        signObj.AccountId__c = accId;
        signObj.Signature_Type__c = 'Opt into text messages';
        signObj.ObjectID__c = opptyId;
        
        insert signObj;
        return signObj;
    }
    
    @isTest
    public static void getOrCreateESignatureTest(){
        List<Account> lstAccountsCreated = TestDataFactory.initializePersonAccounts(1);
        insert lstAccountsCreated;
        Opportunity opptyCreated = prepareShoppingCart(lstAccountsCreated[0]);
        Electronic_Signature__c signObj = inserteSignTestData(lstAccountsCreated[0].Id, opptyCreated.Id);
        
        FinalAcceptanceSignatureCtrlr.getOrCreateESignature(lstAccountsCreated[0].Id, opptyCreated.Id, signObj.Id);
        
        FinalAcceptanceSignatureCtrlr.getOrCreateESignature(lstAccountsCreated[0].Id, opptyCreated.Id, '');
    }
    
    
    // Modified by Balakrishna on 13/08/2019
    @isTest
    public static void declineOptionsTest(){
        List<Account> lstAccountsCreated = TestDataFactory.initializePersonAccounts(1);
        insert lstAccountsCreated;
        Opportunity opptyCreated = prepareShoppingCart(lstAccountsCreated[0]);
        Electronic_Signature__c signObj = inserteSignTestData(lstAccountsCreated[0].Id, opptyCreated.Id);
        
        FinalAcceptanceSignatureCtrlr.declineOptions(lstAccountsCreated[0].Id, opptyCreated.Id);
    }   
    
    
    @isTest
    public static void acceptOptionsTest(){
        List<Account> lstAccountsCreated = TestDataFactory.initializePersonAccounts(1);
        insert lstAccountsCreated;
        Opportunity opptyCreated = prepareShoppingCart(lstAccountsCreated[0]);
        Electronic_Signature__c signObj = inserteSignTestData(lstAccountsCreated[0].Id, opptyCreated.Id);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData = Blob.valueOf('TestingSignature');          
        insert cv;         
                                                
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

        ContentDocumentLink newFileShare = new ContentDocumentLink();
        newFileShare.contentdocumentid = testcontent.contentdocumentid;
        newFileShare.LinkedEntityId = signObj.Id;
        newFileShare.ShareType= 'V';
        insert newFileShare;
        FinalAcceptanceSignatureCtrlr.acceptOptions(lstAccountsCreated[0].Id, opptyCreated.Id, signObj.Id, true, true, System.now()-3,' Legal Verbiage');
        FinalAcceptanceSignatureCtrlr.acceptOptions(lstAccountsCreated[0].Id, opptyCreated.Id, signObj.Id, true, false, System.now()-3,' ');
        
    }
    
    @isTest
    public static void coaSignatureObjTest(){
        List<Account> lstAccountsCreated = TestDataFactory.initializePersonAccounts(1);
        insert lstAccountsCreated;
        Opportunity opptyCreated = prepareShoppingCart(lstAccountsCreated[0]);
        Electronic_Signature__c signObj = inserteSignTestData(lstAccountsCreated[0].Id, opptyCreated.Id);
        
        FinalAcceptanceSignatureCtrlr.coaSignatureObj(lstAccountsCreated[0].Id);
        
    }
    
    @isTest
    public static void coaCheckForSignatureTest(){
         List<Account> lstAccountsCreated = TestDataFactory.initializePersonAccounts(1);
        insert lstAccountsCreated;
        Opportunity opptyCreated = prepareShoppingCart(lstAccountsCreated[0]);
         opptyCreated.Customer_Order_Acceptance__c = true;
                update opptyCreated;
        Electronic_Signature__c signObj = inserteSignTestData(lstAccountsCreated[0].Id, opptyCreated.Id);
        
        FinalAcceptanceSignatureCtrlr.coaCheckForSignature(signObj.Id);
        
    }
    
    
}