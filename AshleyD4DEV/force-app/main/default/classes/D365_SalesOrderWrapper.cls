public class D365_SalesOrderWrapper{
    
    public value[] value;
    
    public class value {
    
        //public String EDAJSON;  //{"MarketId":"8888300-164","BackOrder":"Y","BalanceDue":667.79,"BillTo":{"CustomerCode":"TEST        ","AddressLine1":"4269 ROSWELL ROAD                  ","AddressLine2":"MARIETTA, GA 30067                 ","Building":null,"City":"MARIETTA            ","CountryRegion":"USA","CountryRegionCode":"US","FloorLevel":null,"Name":"TESTER, TEST                       ","PostalCode":"30067     ","StateProvince":"NY"},"CommentText1":" Auto Invoice done on SO# 200448470 | PO# 2035629 | C# C654306                                                                                                                                                                                            ","CommentText2":"                                                                                                    ","CustomerAccountNumber":"TEST        ","CustomerType":"RET","DatePromised":"2017-12-01T00:00:00","DesiredDate":"2017-12-01T00:00:00","EarliestDeliveryDate":"2017-12-01T00:00:00","EntryDate":"2017-11-28T00:00:00","HighDollarSale":false,"Hot":false,"LatestDeliveryDate":"2017-12-01T00:00:00","LoadSeq":"0","NumberOfDeliveryAttempts":0,"NumberofItemsPurchased":1,"OrderNotes":" Auto Invoice done on SO# 200448470 | PO# 2035629 | C# C654306                                                                                                                                                                                                                                                                                                ","OrderNumber":"200441830","PartialOrder":"N","ProfitCenter":23,"PurchaseValue":667.79,"RescheduledReason":null,"Residential":"Y","SatDlvryFlg":"Y","ShiptTo":{"CustomerCode":"TEST        ","AddressLine1":"4269 ROSWELL ROAD                  ","AddressLine2":"                                   ","Building":null,"City":"MARIETTA            ","CountryRegion":"USA","CountryRegionCode":"US","FloorLevel":null,"Name":"TESTER, TEST                       ","PostalCode":"12345     ","StateProvince":"NY"},"ShipVia":"G09","ShipViaDescription":"GA TRUCK 09                        ","SODetails":[{"RSA":"HOU","Acknowledgement":"","BOQty":"1","DeliveryType":"G09","DetailCommentText1":null,"DetailCommentText2":null,"ExpectedQty":"1","FPP":{"Coverage":"False","EndDate":"0001-01-01T00:00:00"},"InStoreQuantity":0,"InWarehouseQuantity":0,"ItemDescription":"Athens Delivery Charge             ","ItemNumber":"*DELI-ATHENS","LineNumber":null,"LineType":null,"LOC_PO":"0","OrderNumber":"200441830","SalesOrderDate":"2017-11-28T00:00:00","ShiptTo":{"CustomerCode":"TEST        ","AddressLine1":"4269 ROSWELL ROAD                  ","AddressLine2":"                                   ","Building":null,"City":"MARIETTA            ","CountryRegion":"USA","CountryRegionCode":"US","FloorLevel":null,"Name":"TESTER, TEST                       ","PostalCode":"12345     ","StateProvince":"NY"},"ShipVia":"G09","ShipViaDescription":"GA TRUCK 09                        ","Status":"","WarrantyDaysLeft":0,"DiscountReasonCode":null,"OrderType":"ORDER","InvoiceNo":"0","PaymentType":"DEFAULT                            ","CustomerPO":"                                   ","LoadSeq":"20","EstimatedArrivalDateTime":null},{"RSA":"HOU","Acknowledgement":"","BOQty":"1","DeliveryType":"G09","DetailCommentText1":null,"DetailCommentText2":null,"ExpectedQty":"1","FPP":{"Coverage":"False","EndDate":"0001-01-01T00:00:00"},"InStoreQuantity":0,"InWarehouseQuantity":0,"ItemDescription":"Del date may chg due product delays","ItemNumber":"*ETA        ","LineNumber":null,"LineType":null,"LOC_PO":"0","OrderNumber":"200441830","SalesOrderDate":"2017-11-28T00:00:00","ShiptTo":{"CustomerCode":"TEST        ","AddressLine1":"4269 ROSWELL ROAD                  ","AddressLine2":"                                   ","Building":null,"City":"MARIETTA            ","CountryRegion":"USA","CountryRegionCode":"US","FloorLevel":null,"Name":"TESTER, TEST                       ","PostalCode":"12345     ","StateProvince":"NY"},"ShipVia":"G09","ShipViaDescription":"GA TRUCK 09                        ","Status":"","WarrantyDaysLeft":0,"DiscountReasonCode":null,"OrderType":"ORDER","InvoiceNo":"0","PaymentType":"DEFAULT                            ","CustomerPO":"                                   ","LoadSeq":"30","EstimatedArrivalDateTime":null},{"RSA":"HOU","Acknowledgement":"","BOQty":"1","DeliveryType":"G09","DetailCommentText1":null,"DetailCommentText2":null,"ExpectedQty":"1","FPP":{"Coverage":"False","EndDate":"0001-01-01T00:00:00"},"InStoreQuantity":0,"InWarehouseQuantity":0,"ItemDescription":"Sofa                               ","ItemNumber":"000003510038","LineNumber":null,"LineType":null,"LOC_PO":"0","OrderNumber":"200441830","SalesOrderDate":"2017-11-28T00:00:00","ShiptTo":{"CustomerCode":"TEST        ","AddressLine1":"4269 ROSWELL ROAD                  ","AddressLine2":"                                   ","Building":null,"City":"MARIETTA            ","CountryRegion":"USA","CountryRegionCode":"US","FloorLevel":null,"Name":"TESTER, TEST                       ","PostalCode":"12345     ","StateProvince":"NY"},"ShipVia":"G09","ShipViaDescription":"GA TRUCK 09                        ","Status":"","WarrantyDaysLeft":0,"DiscountReasonCode":null,"OrderType":"ORDER","InvoiceNo":"0","PaymentType":"DEFAULT                            ","CustomerPO":"                                   ","LoadSeq":"10","EstimatedArrivalDateTime":null}],"StoreLocation":"SOUTHLAKE                          ","StoreOrder":"0","TranDttm":null,"TransactionCode":"U","ItemCount":1,"WhseID":null,"DeliveryComments":"","VIP":false,"LeaveInBox":false,"ReturnedReason":"","Status":null,"OrderStatus":" ","EstimatedArrival":"01/01/1900","ServiceLevel":"PDI","ShippingName":"TESTER,  TEST                      ","phhWindowOpen":"00:00","phhWindowClose":"00:00","phhShiptoFirstName":"TEST","phhShiptoMiddleName":"","phhShiptoLastName":"TESTER","phhShiptoSalutation":"MR.","phhShiptoSuffix":"","phhShiptoPhoneNumber1":"8136035330               ","phhShiptoPhoneNumber2":"4076035330               ","phhShiptoPhoneNumber3":"                         ","phhShiptoCustEmail":"abcd@abcd.com                                     ","phhTextConsent":false}
        //public EDAJSON EDAJSON;
        public String Fulfillerid;  //8888300-164
        public String StoreLocation;    //SOUTHLAKE
        public Integer StoreID; //133
        public Integer Profitcenter;    //23
        public String CustomerID;   //HESSDAV
        public String CustomerName; //HESSE, DAVID
        public String CustomerType; //RET
        public String PaymentType;  //CASH BEFORE DELIVERY
        public String BillAddress1; //250 AMAL DR
        public String BillAddress2; //
        public String BillCity; //ATLANTA
        public String BillState;    //GA
        public String BillZip;  //30315
        public String SalesOrderNo; //200441830
        public String SalesOrderDate;   //2018-05-24T00:00:00Z
        public String OrderStatus;  // 
        public String OrderType;    //ORDER
        public String OrderSubType; //Enterprise
        public String SaleType; //U
        public String RSA;  //HOU
        public String BlobLocation; //eda/8888300-164/200441830
        public Integer Id;  //106367
        public String ContactID;    //0012F00000HB5j6QAD
        public List<EDAJSON> EDAJSON;
        //public EDAJSON[] EDAJSON;
    }
    
    //public cls_EDAJSON[] EDAJSON;
    public class EDAJSON {
        public String MarketId; //8888300-164
        public String BackOrder;    //Y
        public Double BalanceDue;   //667.79
        public cls_BillTo BillTo;
        public String CommentText1; // Auto Invoice done on SO# 200448470 | PO# 2035629 | C# C654306                                                                                                                                                                                            
        public String CommentText2; //                                                                                                    
        public String CustomerAccountNumber;    //TEST        
        public String CustomerType; //RET
        public String DatePromised; //2017-12-01T00:00:00
        public String DesiredDate;  //2017-12-01T00:00:00
        public String EarliestDeliveryDate; //2017-12-01T00:00:00
        public String EntryDate;    //2017-11-28T00:00:00
        public boolean HighDollarSale;
        public boolean Hot;
        public String LatestDeliveryDate;   //2017-12-01T00:00:00
        public String LoadSeq;  //0
        public Integer NumberOfDeliveryAttempts;    //0
        public Integer NumberofItemsPurchased;  //1
        public String OrderNotes;   // Auto Invoice done on SO# 200448470 | PO# 2035629 | C# C654306                                                                                                                                                                                                                                                                                                
        public String OrderNumber;  //200441830
        public String PartialOrder; //N
        public Integer ProfitCenter;    //23
        public Double PurchaseValue;    //667.79
        //public cls_RescheduledReason RescheduledReason;
        public String RescheduledReason;
        public String Residential;  //Y
        public String SatDlvryFlg;  //Y
        public cls_ShiptTo ShiptTo;
        public String ShipVia;  //G09
        public String ShipViaDescription;   //GA TRUCK 09                        
        public cls_SODetails[] SODetails;
        public String StoreLocation;    //SOUTHLAKE                          
        public String StoreOrder;   //0
        public cls_TranDttm TranDttm;
        public String TransactionCode;  //U
        public Integer ItemCount;   //1
        public cls_WhseID WhseID;
        public String DeliveryComments; //
        public boolean VIP;
        public boolean LeaveInBox;
        public String ReturnedReason;   //
        public cls_Status Status;
        public String OrderStatus;  // 
        public String EstimatedArrival; //01/01/1900
        public String ServiceLevel; //PDI
        public String ShippingName; //TESTER,  TEST                      
        public String phhWindowOpen;    //00:00
        public String phhWindowClose;   //00:00
        public String phhShiptoFirstName;   //TEST
        public String phhShiptoMiddleName;  //
        public String phhShiptoLastName;    //TESTER
        public String phhShiptoSalutation;  //MR.
        public String phhShiptoSuffix;  //
        public String phhShiptoPhoneNumber1;    //8136035330               
        public String phhShiptoPhoneNumber2;    //4076035330               
        public String phhShiptoPhoneNumber3;    //                         
        public String phhShiptoCustEmail;   //abcd@abcd.com                                     
        public boolean phhTextConsent;
    }
    public class cls_BillTo {
        public String CustomerCode; //TEST        
        public String AddressLine1; //4269 ROSWELL ROAD                  
        public String AddressLine2; //MARIETTA, GA 30067                 
        public cls_Building Building;
        public String City; //MARIETTA            
        public String CountryRegion;    //USA
        public String CountryRegionCode;    //US
        public cls_FloorLevel FloorLevel;
        public String Name; //TESTER, TEST                       
        public String PostalCode;   //30067     
        public String StateProvince;    //NY
    }
    public class cls_Building {
    }
    public class cls_FloorLevel {
    }
    public class cls_RescheduledReason {
    }
    public class cls_ShiptTo {
        public String CustomerCode; //TEST        
        public String AddressLine1; //4269 ROSWELL ROAD                  
        public String AddressLine2; //                                   
        public cls_Building Building;
        public String City; //MARIETTA            
        public String CountryRegion;    //USA
        public String CountryRegionCode;    //US
        public cls_FloorLevel FloorLevel;
        public String Name; //TESTER, TEST                       
        public String PostalCode;   //12345     
        public String StateProvince;    //NY
    }
    public class cls_SODetails {
        public String RSA;  //HOU
        public String Acknowledgement;  //
        public String BOQty;    //1
        public String DeliveryType; //G09
        public cls_DetailCommentText1 DetailCommentText1;
        public cls_DetailCommentText2 DetailCommentText2;
        public String ExpectedQty;  //1
        public cls_FPP FPP;
        public Integer InStoreQuantity; //0
        public Integer InWarehouseQuantity; //0
        public String ItemDescription;  //Athens Delivery Charge             
        public String ItemNumber;   //*DELI-ATHENS
        public cls_LineNumber LineNumber;
        public cls_LineType LineType;
        public String LOC_PO;   //0
        public String OrderNumber;  //200441830
        public String SalesOrderDate;   //2017-11-28T00:00:00
        public cls_ShiptTo ShiptTo;
        public String ShipVia;  //G09
        public String ShipViaDescription;   //GA TRUCK 09                        
        public String Status;   //
        public Integer WarrantyDaysLeft;    //0
        //public cls_DiscountReasonCode DiscountReasonCode;
        public String DiscountReasonCode;
        public String OrderType;    //ORDER
        public String InvoiceNo;    //0
        public String PaymentType;  //DEFAULT                            
        public String CustomerPO;   //                                   
        public String LoadSeq;  //20
        public String EstimatedArrivalDateTime;
    }
    class cls_DetailCommentText1 {
    }
    class cls_DetailCommentText2 {
    }
    class cls_FPP {
        public String Coverage; //False
        public String EndDate;  //0001-01-01T00:00:00
    }
    class cls_LineNumber {
    }
    class cls_LineType {
    }
    class cls_DiscountReasonCode {
    }
    class cls_EstimatedArrivalDateTime {
    }
    class cls_TranDttm {
    }
    class cls_WhseID {
    }
    class cls_Status {
    }
        
}