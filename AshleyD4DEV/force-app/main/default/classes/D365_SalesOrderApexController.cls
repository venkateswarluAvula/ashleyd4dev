public class D365_SalesOrderApexController {
    
    // Retrinving Values of parent record
    @AuraEnabled
    public static List<D365_SalesOrder__c> getOrders(){ 
    //Id salesforceOrderId
         List < D365_SalesOrder__c> lstOfOrders = [SELECT Id,D365_Address_1__c,D365_Address_2__c,D365_RSA_Indicator__c,D365_City__c,D365_State__c,
                                                     D365_Type__c,D365_Fulfiller_phone_number__c,D365_Sales_Order_Number__c,D365_Customer_Name__c,
                                                     D365_Order_Submit_Date_Time__c,D365_Payment_Method__c,D365_Order_Status__c,D365_Customer_ID__c,
                                                     D365_Item_Number__c,D365_Original_Sales_Order_Number__c 
                                                     From D365_SalesOrder__c where D365_Original_Sales_Order_Number__c != Null];
            system.debug('lstOfOrders value is'+lstOfOrders);
          return lstOfOrders;
        
    }
    
    // Retrinving Values of Child record
    @AuraEnabled
    public static List<D365_SalesOrder__c> getOrderschild(String AId){
        system.debug('ParentId----'+AId);
        List < D365_SalesOrder__c> lstOfchild = [SELECT Id,D365_Address__c,D365_Address_1__c,D365_RSA_Indicator__c,D365_City__c,D365_State__c,
                                                     D365_Type__c,D365_Sales_Order_Qty__c,D365_Sales_Order_Number__c,D365_Customer_Name__c,
                                                     D365_Order_Submit_Date_Time__c,D365_Payment_Method__c,D365_Order_Status__c,D365_Customer_ID__c,
                                                     D365_Item_Number__c,D365_Product_Name__c,Parent_Sales_Order__c,D365_Adjustment_Type__c                                                     
                                                     From D365_SalesOrder__c where Parent_Sales_Order__c =: AId];
        
        system.debug('lstOfchild----'+lstOfchild);
        return lstOfchild;
    }
}