@isTest
public class EDAServiceRequestCaseCommentsTest {        
    @isTest static void testPostMethod(){ 
        Account acc=new Account();
        acc.name='Test';
        acc.AccountNumber='1232332';
        acc.Site='site';
        insert acc;
        System.debug('my accnt id-->'+acc.id);
        List<Contact> conlist = new List<Contact>();
        Contact con = new Contact();
        con.AccountId = acc.id;
        con.LastName = 'Test';
        con.Email = 'abc@abc.com';
        insert con;
        conlist.add(con);
        System.debug('my con.AccountId-->'+con.AccountId);
        List<Case> caselist = new List<Case>();
        Case Casee = new Case();
        Casee.Subject='Test record';
        Casee.Type = 'General Inquiry';
        Casee.AccountId = acc.id;
        Casee.Description = 'test record';
        Casee.Origin = 'Phone';
        Casee.Status = 'New';
        Casee.Priority='Medium';
        Casee.Legacy_Service_Request_ID__c = '88731';
        insert Casee; 
        caselist.add(Casee);
        
        List<Legacy_Comment__c> comlist = new List<Legacy_Comment__c>();
        Legacy_Comment__c newComment = new Legacy_Comment__c();
        newComment.Case__c = Casee.id;
        newComment.Legacy_Service_Request_ID__c = '88731';
        newComment.Comment__c = 'CaseComment';
        newComment.Legacy_CSMComment_ID__c = '735928';
        System.debug('New Comment inserting :'+newComment.Comment__c);        
        insert  newComment;
        comlist.add(newComment);
        EDAServiceRequestCaseComments.CommentWrap someval = new EDAServiceRequestCaseComments.CommentWrap();
        someval.SFDCAccountId = acc.id;
        someval.CommentID = '735928';
        someval.EntityType = '1';
        someval.EntityID = '88731';
        someval.CommentType = '2';
        someval.CommentText = 'test';
        someval.OrderNumber = null;
        someval.AccountShipto = '8888300-164';
        someval.RequestID = '88731';
        someval.CustomerType = 'RET';
        someval.CustomerID = 'JOSHDEE';
        someval.ShipToAddress1 = '1670 E 8TH AVE';
        someval.ShipToAddress2 = 'test';
        someval.ShipToCityName = 'ATLANTA';
        someval.ShipToStateCode = 'GA';
        someval.ShipToZipCode = '30309';
        someval.LastModifiedUserName = 'abc';
        
        Set<String> AccId = new Set<String>();
        Set<String> reqId = new set<String>();
        Set<String> commentId = new Set<String>();
        Set<string> AccShipto = new Set<string>();
        
        commentId.add(someval.CommentID);
        reqId.add(someval.RequestID);
        AccId.add(someval.SFDCAccountId);
        AccShipto.add(Someval.AccountShipto);
        Boolean flag = true;
        
        EDAServiceRequestCaseComments reqst=new EDAServiceRequestCaseComments();
        String JsonMsg=JSON.serialize(someval);
        Map<String,List<EDAServiceRequestCaseComments.CommentWrap>> JSONreqBody = new Map<String,List<EDAServiceRequestCaseComments.CommentWrap>>();
        List<EDAServiceRequestCaseComments.CommentWrap> JSONm = new List<EDAServiceRequestCaseComments.CommentWrap>();
        JSONm.add(someval);
        JSONreqBody.put('CsrComments', JSONm);
        JsonMsg=JSON.serialize(JSONreqBody);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/Services/apexrest/ServiceRequests-CaseComments/';  
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response = res;
        EDAServiceRequestCaseComments.getComment(Casee.id, Someval);
        EDAServiceRequestCaseComments.createNewCase(acc.id, Someval, conlist);
        String result = EDAServiceRequestCaseComments.doPost();
           
    }
    
    @isTest static void testPostMethod2(){ 
        
        Account acc=new Account();
        acc.name='Test';
        acc.AccountNumber='1232332';
        acc.Site='site';
        insert acc;
        System.debug('my accnt id-->'+acc.id);
        List<Contact> conlist = new List<Contact>();
        Contact con = new Contact();
        con.AccountId = acc.id;
        con.LastName = 'Test';
        con.Email = 'abc@abc.com';
        insert con;
        conlist.add(con);
        System.debug('my con.AccountId-->'+con.AccountId);
        List<Case> caselist = new List<Case>();
        Case Casee = new Case();
        Casee.Subject='Test record';
        Casee.Type = 'General Inquiry';
        Casee.AccountId = acc.id;
        Casee.Description = 'test record';
        Casee.Origin = 'Phone';
        Casee.Status = 'New';
        Casee.Priority='Medium';
        Casee.Legacy_Service_Request_ID__c = '88731';
        insert Casee; 
        caselist.add(Casee);
        
        List<Legacy_Comment__c> comlist = new List<Legacy_Comment__c>();
        Legacy_Comment__c newComment = new Legacy_Comment__c();
        newComment.Case__c = Casee.id;
        newComment.Legacy_Service_Request_ID__c = '88731';
        newComment.Comment__c = 'CaseComment';
        newComment.Legacy_CSMComment_ID__c = '735928';
        System.debug('New Comment inserting :'+newComment.Comment__c);        
        insert  newComment;
        comlist.add(newComment);
        EDAServiceRequestCaseComments.CommentWrap someval = new EDAServiceRequestCaseComments.CommentWrap();
        someval.SFDCAccountId = acc.id;
        someval.CommentID = '735928';
        someval.EntityType = '1';
        someval.EntityID = '88731';
        someval.CommentType = '2';
        someval.CommentText = 'test';
        someval.OrderNumber = null;
        someval.AccountShipto = '8888300-164';
        someval.RequestID = '88731';
        someval.CustomerType = 'RET';
        someval.CustomerID = 'JOSHDEE';
        someval.ShipToAddress1 = '1670 E 8TH AVE';
        someval.ShipToAddress2 = 'test';
        someval.ShipToCityName = 'ATLANTA';
        someval.ShipToStateCode = 'GA';
        someval.ShipToZipCode = '30309';
        someval.LastModifiedUserName = 'abc';
        
        Set<String> AccId = new Set<String>();
        Set<String> reqId = new set<String>();
        Set<String> commentId = new Set<String>();
        Set<string> AccShipto = new Set<string>();
        commentId.add(someval.CommentID);
        reqId.add(someval.RequestID);
        AccId.add(someval.SFDCAccountId);
        AccShipto.add(Someval.AccountShipto);
        
        Boolean flag = false;
        EDAServiceRequestCaseComments reqst=new EDAServiceRequestCaseComments();
        String JsonMsg=JSON.serialize(someval);
        Map<String,List<EDAServiceRequestCaseComments.CommentWrap>> JSONreqBody = new Map<String,List<EDAServiceRequestCaseComments.CommentWrap>>();
        List<EDAServiceRequestCaseComments.CommentWrap> JSONm = new List<EDAServiceRequestCaseComments.CommentWrap>();
        JSONm.add(someval);
        JSONreqBody.put('CsrComments', JSONm);
        JsonMsg=JSON.serialize(JSONreqBody);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/Services/apexrest/ServiceRequests-CaseComments/';  
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response = res;
        EDAServiceRequestCaseComments.getComment(Casee.id, Someval);
        EDAServiceRequestCaseComments.createNewCase(acc.id, Someval, conlist);
        String result = EDAServiceRequestCaseComments.doPost();
        
    }   

    @isTest static void testCreateCaseAndComment(){ 
        Account acc=new Account();
        acc.name='Test';
        acc.AccountNumber='1232332';
        acc.Site='site';
        insert acc;
        System.debug('my accnt id-->'+acc.id);

        List<Contact> conlist = new List<Contact>();
        Contact con = new Contact();
        con.AccountId = acc.id;
        con.LastName = 'Test';
        con.Email = 'abc@abc.com';
        insert con;
        conlist.add(con);
        System.debug('my con.AccountId-->'+con.AccountId);

        EDAServiceRequestCaseComments.CommentWrap someval = new EDAServiceRequestCaseComments.CommentWrap();
        someval.SFDCAccountId = acc.id;
        someval.CommentID = '67890';
        someval.EntityType = '1';
        someval.EntityID = '88731';
        someval.CommentType = '2';
        someval.CommentText = 'test';
        someval.OrderNumber = null;
        someval.AccountShipto = '8888300-164';
        someval.RequestID = '12345';
        someval.CustomerType = 'RET';
        someval.CustomerID = 'JOSHDEE';
        someval.ShipToAddress1 = '1670 E 8TH AVE';
        someval.ShipToAddress2 = 'test';
        someval.ShipToCityName = 'ATLANTA';
        someval.ShipToStateCode = 'GA';
        someval.ShipToZipCode = '30309';
        someval.LastModifiedUserName = 'abc';
        
        Map<String,List<EDAServiceRequestCaseComments.CommentWrap>> JSONreqBody = new Map<String,List<EDAServiceRequestCaseComments.CommentWrap>>();
        List<EDAServiceRequestCaseComments.CommentWrap> JSONm = new List<EDAServiceRequestCaseComments.CommentWrap>();
        JSONm.add(someval);
        JSONreqBody.put('CsrComments', JSONm);
        string JsonMsg=JSON.serialize(JSONreqBody);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/Services/apexrest/ServiceRequests-CaseComments/';  
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response = res;
        String result = EDAServiceRequestCaseComments.doPost();
        
        //query and get the created case
        list<case> caseList = [SELECT AccountId, Id, Legacy_Service_Request_ID__c ,Legacy_Account_Ship_To__c
						FROM Case WHERE Legacy_Account_Ship_To__c = '8888300-164' AND Legacy_Service_Request_ID__c = '12345'];
        if (caseList.size() > 0) {
            EDAServiceRequestCaseComments.CommentWrap someval1 = new EDAServiceRequestCaseComments.CommentWrap();
            someval1.SFDCAccountId = acc.id;
            someval1.CommentID = '67890';
            someval1.EntityType = '1';
            someval1.EntityID = '88731';
            someval1.CommentType = '2';
            someval1.CommentText = 'test';
            someval1.OrderNumber = null;
            someval1.AccountShipto = '8888300-164';
            someval1.RequestID = '12345';
            someval1.CustomerType = 'RET';
            someval1.CustomerID = 'JOSHDEE';
            someval1.ShipToAddress1 = '1670 E 8TH AVE';
            someval1.ShipToAddress2 = 'test';
            someval1.ShipToCityName = 'ATLANTA';
            someval1.ShipToStateCode = 'GA';
            someval1.ShipToZipCode = '30309';
            someval1.LastModifiedUserName = 'abc';

            Map<String,List<EDAServiceRequestCaseComments.CommentWrap>> JSONreqBody1 = new Map<String,List<EDAServiceRequestCaseComments.CommentWrap>>();
            List<EDAServiceRequestCaseComments.CommentWrap> JSONm1 = new List<EDAServiceRequestCaseComments.CommentWrap>();
            JSONm1.add(someval1);
            JSONreqBody1.put('CsrComments', JSONm1);
            string JsonMsg1 = JSON.serialize(JSONreqBody1);

            RestRequest req1 = new RestRequest(); 
            RestResponse res1 = new RestResponse();
            req1.requestURI = '/Services/apexrest/ServiceRequests-CaseComments/';  
            req1.httpMethod = 'POST';
            req1.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req1;
            RestContext.response = res1;
            String result1 = EDAServiceRequestCaseComments.doPost();
        }
    }
}