global class NpsOptController {
    /* method used by customr update rest endpoint to update all listed customer records */
    public static void updateAllNpsRecords(List<NpsWrapper> customerList) 
    {
        Map<Id, NpsWrapper> mappedNps = new Map<Id, NpsWrapper>();
        
        for (NpsWrapper customer:customerList) {
            mappedNps.put(customer.SFPersonAccountID, customer);
        }
        
        Map<ID,Account> accounts = new Map<ID, Account>([SELECT Id, 
                                                         Survey_Opt_In__pc,
                                                         Store_Id__c FROM Account WHERE Id IN :mappedNps.keyset()]);

        for (Id accId : accounts.keySet()) {
            NpsWrapper customer = mappedNps.get(accId);
            Account acct = accounts.get(accId);
            acct.Survey_Opt_In__pc = (customer.OptIn == 'START' ? true : false);
        }
        
        try{
            update(accounts.values());
        }
        catch(exception e) {
            system.debug('e: ' + e);
        }
    }
    
    
}