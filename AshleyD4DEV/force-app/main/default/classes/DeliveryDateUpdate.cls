global class DeliveryDateUpdate implements Database.Batchable<Sobject>{
    
    // Start Method
    global Database.Querylocator start (Database.BatchableContext BC) {
        return Database.getQueryLocator('Select Id, DeliveryDate__c From Shopping_cart_line_item__c limit 10');
    }
    // Execute method
    global void execute (Database.BatchableContext BC, List<Shopping_cart_line_item__c> scope) {
        List<Shopping_cart_line_item__c> updteShcrtlnitms = new List<Shopping_cart_line_item__c>();
        
        
        DateTime dT = System.now();
        date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        string sdate = String.valueOf(myDate);
        system.debug('sdate==>'+sdate);
        
        for (Shopping_cart_line_item__c  lineItems: scope)
        {
        DateTime dT1 = lineItems.DeliveryDate__c;
        date myDate1 = date.newinstance(dT1.year(), dT1.month(), dT1.day());
        string sdate1 = String.valueOf(myDate1);
             system.debug('sdate1==>'+sdate1);
            if( sdate1 == sdate)
            {
                system.debug('entered if');
                lineItems.DeliveryDate__c = null;
                updteShcrtlnitms.add(lineItems);
            }
            // else{
            // lineItems.DeliveryDate__c = null;
            //   updteShcrtlnitms.add(lineItems);
            // }
        }
        
        
        update updteShcrtlnitms;   
    }
    
    
    // Finish Method
    global void finish(Database.BatchableContext BC) {
        
    }
    
}