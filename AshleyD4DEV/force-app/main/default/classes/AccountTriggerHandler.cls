/**************************************************************************************************
* Name       : AccountTriggerHandler
* Purpose    : TriggerHandler for Account
***************************************************************************************************
* Author            | REQ    | Created Date    | Description
***************************************************************************************************
*Perficient         |        | 03/05/2018      | Initial Draft
**************************************************************************************************/

public class AccountTriggerHandler 
{
    
    //Added for NPS API by Vamsi
    @future(callout=true)
    public static void NPSupdate (string SFPersonAccID, boolean OptIn){
        
        AccDelAuthorization accda = new AccDelAuthorization();
        String accessTkn = accda.accessTokenData();
        
        //Added for username
        User objUsr = [Select Id, Name, Alias,CommunityNickname from User where Id=:UserInfo.getUserId()];
        String sfUserName = objUsr.CommunityNickname;
        //End
        
        system.debug('SFPersonAccID--'+SFPersonAccID+'OptIn--'+OptIn+'sfUserName--'+sfUserName);  
        String endpoint = system.label.AshleyNPSEndPoint+SFPersonAccID+'&OptIn='+OptIn+'&sfUserName='+sfUserName;
        //https://test.cara.ashleyretail.com/NPSTextConsent?SFPersonAccountID=0010n000011abpUAAQ&OptIn=false&sfUserName=UserName
        Http http = new http();
        Httprequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        HttpResponse res = CaseHelper.callApi(req, 'Update NPS checkbox', 'AccountTriggerHandler', 'NPSUpdate', endpoint);
        
        System.debug('NPS apiresponse...' + res.getBody());
        System.debug('NPS status--' + res.getStatusCode());
    }
    
    private static void updateNPSAccount(List<Account> Accounts, Map<Id,Account> oldAccountMap){
        for (Account acc: Accounts){
            if (oldAccountMap.get(acc.Id).Survey_Opt_In__pc != acc.Survey_Opt_In__pc){
                AccountTriggerHandler.NPSupdate(acc.Id,acc.Survey_Opt_In__pc);
            }
        }
    }
    
    //Ended for NPS API by Vamsi
   
    
    public static void afterUpdate(Map<Id, Account> oldMap, 
                                   Map<Id, Account> newMap)
    {        
        list<id> accid = new list<Id>();
        for(Id accountId : newMap.keySet())
        {
            Account newAccount =  newMap.get(accountId);
            Account oldAccount =  oldMap.get(accountId);
            
            if(newAccount.Survey_Opt_In__pc != oldAccount.Survey_Opt_In__pc)
            {
                accid.add(accountId);    
            }
        } 
		
       
            if(accid.size() > 0)
            {
                List<Case> casetoUpdateList = new List<Case>();
                List<case> caselist = [select id,Survey_Opt_In__c,AccountId,status,Type_of_Resolution__c,Resolution_Notes__c from case 
                                       where AccountId IN :accid AND Status NOT IN ('Closed', 'Closed in Salesforce', 'Paid and Closed', 'Stale')];
                List<case> caseClosedList = [select id,Survey_Opt_In__c,AccountId,status,Type_of_Resolution__c,Resolution_Notes__c from case 
                                       		 where AccountId IN :accid AND Status IN ('Closed', 'Closed in Salesforce', 'Paid and Closed', 'Stale') 
                                             AND Type_of_Resolution__c != NULL];
                
                for(case cc:caselist){   
                    cc.Survey_Opt_In__c = newMap.get(cc.AccountId).Survey_Opt_In__pc;
                    casetoUpdateList.add(cc);
                }
                for(case cc1:caseClosedList){   
                    cc1.Survey_Opt_In__c = newMap.get(cc1.AccountId).Survey_Opt_In__pc;
                    casetoUpdateList.add(cc1);
                }
                if (!casetoUpdateList.isEmpty()){
                    update casetoUpdateList;
                }
            }
       
        
        //Ended for NPS checkbox
        
        //Added for NPS web callout
        updateNPSAccount(newMap.values(), oldMap);
        //Ended for NPS web callout
        
        Set<Id> accountIdsToSendCARA = new Set<Id>();
        for(Id accountId : newMap.keySet())
        {
            // if any integration fields are changed, send the account to CARA
            if(newMap.get(accountId).IsPersonAccount 
               && hasIntegrationFieldsChanged(oldMap.get(accountId), 
                                              newMap.get(accountId)))
            {
                accountIdsToSendCARA.add(accountId);
            }
        }
        // custom setting flag to switch on/off integration
        // probably for a bulk load of more than 10K records
        if(Trigger_Integration__c.getInstance().Account_Updates_to_CARA__c
           && API_CaraAccountUpdates.accountsToCARACodeLevelFlag && !accountIdsToSendCARA.isEmpty() && !system.isBatch() && !system.isFuture())
        {
            API_CaraAccountUpdates.sendAccountsToCara(accountIdsToSendCARA);
        }
        
        
    }
    
    // method to check if any integration fields are changed; list of integration fields are in API_CaraAccountUpdates.cls
    private static boolean hasIntegrationFieldsChanged(Account oldAccount, 
                                                       Account newAccount)
    {
        for(SObjectField sf : API_CaraAccountUpdates.accountFields)
        {
            if(oldAccount.get(sf) != newAccount.get(sf))
            {
                return true;
            }
        }
        return false;
    }
}