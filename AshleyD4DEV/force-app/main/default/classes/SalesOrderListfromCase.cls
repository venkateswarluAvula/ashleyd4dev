public class SalesOrderListfromCase {
    
    @AuraEnabled
    public static Case getCaseinfo(Id caseId){
        Case caseData = [Select Id, Legacy_Account_Ship_To__c from Case where id=:caseId];
        system.debug('*** caseData: ' + caseData);
        return caseData;
    }
    
    @AuraEnabled
    public static List<soWrapper> getSalesOrders(Id caseId) {
        List<soWrapper> sowList = new List<soWrapper>();
        Case objCas = [Select Id, AccountId, Account.Name,Survey_opt_In__c from Case where id=:caseId];
        system.debug('*** case: ' + objCas);

        if (objCas.AccountId != null) {
            List<SalesOrder__x> soList = new List<SalesOrder__x>();
            if (Test.isRunningTest()) {
                SalesOrder__x salesOrderObj = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                                ExternalId = '17331400:001q000000raDkvAAE',
                                                                phhCustomerID__c= '009048870576',
                                                                phhSalesOrder__c='300493250',
                                                                phhSaleType__c='D',
                                                                phhMarketAccount__c = 'Kingswere Georgia - #8888300',
                                                                phhStoreNameStoreNumberPC__c = 'Southlake - 166 - 23',
                                                                phhDatePromised__c = system.today(),
                                                                phhERPAccounShipTo__c = '8888300-164'
                                                               );
                soList.add(salesOrderObj );
            } else {
                soList = [Select Id, ExternalId, phhGuestID__c, phhSalesOrder__c, phhMarketAccount__c, phhStoreNameStoreNumberPC__c, phhSaleType__c, phhCustomerID__c, phhDatePromised__c, phhERPAccounShipTo__c, fulfillerID__c,Text_consent__c from SalesOrder__x where phhGuestID__c=: objCas.AccountId];
            }
            system.debug('*** soList: ' + soList);
        
            for (SalesOrder__x so:soList) {
                soWrapper sow = new soWrapper();
                sow.externId = so.ExternalId;
                sow.MarketAccount = so.phhMarketAccount__c;
                sow.StoreNameStoreNumberPC = so.phhStoreNameStoreNumberPC__c;
                sow.Salesorder = so.phhSalesOrder__c;
                sow.salesType = so.phhSaleType__c;
                sow.NPSTextConsent = so.Text_consent__c;
                if (so.fulfillerID__c == null){
                    sow.AccountShipto = so.phhERPAccounShipTo__c;                
                }
                else{
                    sow.AccountShipto = so.fulfillerID__c;
                }
                
                system.debug('*** sowList: ' + sowList);
                sowList.add(sow);
            }
        }
        return sowList;
    }
    
        
    @AuraEnabled
    public static string updateSalesOrderRecord(Id caseId, string soId, boolean NPS, string storeName, string accShipto) {
        string resultStr;
        resultStr = 'error';
        system.debug('*** soId: ' + soId + ' :caseId: ' + caseId + ' :accShipto: ' + accShipto + ' :storeName: ' + storeName);
        
        Case caseObj = [Select Id, Sales_Order__c, Profit_Center__c, Market__c, Legacy_Account_Ship_To__c,Survey_Opt_In__c from Case where Id=:caseId];
        
        //Added to map market account for Ashcomm orders
        list<Market_Configuration__mdt> marketConfigList = [SELECT MasterLabel FROM Market_Configuration__mdt where Fulfiller_Id__c =:accShipto];
        string market = marketConfigList[0].MasterLabel;
        
        caseObj.Sales_Order__c = soId;
        caseObj.Market__c = market;
        caseObj.Profit_Center__c = storeName;
        caseObj.Survey_Opt_In__c = NPS;
        
        if (caseObj.Legacy_Account_Ship_To__c == null){
            caseObj.Legacy_Account_Ship_To__c = accShipto;
        }
        try {
            Database.SaveResult sr = Database.update(caseObj);
            if (sr.isSuccess()) {
                resultStr = 'success';
            }
            CaseHelper.databaseErrorLog(sr, 'Case update try Error', 'SalesOrderListfromCase', 'updateSalesOrderRecord', JSON.serialize(caseObj));
        } catch (DmlException de) {
            CaseHelper.dmlExceptionError(de, 'Case update catch Error', 'SalesOrderListfromCase', 'updateSalesOrderRecord', JSON.serialize(caseObj));
        }
        system.debug('*** caseObj: ' + caseObj);
        return resultStr;
    }
    
    public class soWrapper {
        @AuraEnabled
        public string externId {get;set;}
        @AuraEnabled
        public string MarketAccount {get;set;}
        @AuraEnabled
        public string StoreNameStoreNumberPC {get;set;}
        @AuraEnabled
        public string Salesorder {get;set;}
        @AuraEnabled
        public string salesType {get;set;}
        @AuraEnabled
        public string AccountShipto {get;set;}
        @AuraEnabled
        public boolean NPSTextConsent {get;set;}
    }
}