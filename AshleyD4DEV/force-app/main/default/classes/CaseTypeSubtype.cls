public class CaseTypeSubtype {
    @InvocableMethod
    public static List<string> getOrderByExternalId(List<Case> cases) {
        List<String> salesOrders = new List<String>();
        list<Case> updatecaselist = new List<Case>();
        List<SalesOrder__x> salesOrderObj = new List<SalesOrder__x>();
        map<string,SalesOrder__x> externalmap = new Map<string,SalesOrder__x>();
        
        for(Case c: cases){
            if(c.Sales_Order__c != null && !salesOrders.contains(c.Sales_Order__c)){
                salesOrders.add(c.Sales_Order__c);
            }
        }
        if(salesOrders.size() > 0 ){
            salesOrderObj = [Select ExternalId,phhProfitCenter__c, phhStoreID__c from SalesOrder__x where ExternalId in:salesOrders];  
        }
        
        if(salesOrderObj.size() > 0){
            for(SalesOrder__x ex:salesOrderObj){
                externalmap.put(ex.ExternalId,ex);
            }
            for(Case c: cases){
                if(externalmap.get(c.Sales_Order__c).phhProfitCenter__c == 444 && externalmap.get(c.Sales_Order__c).phhStoreID__c == '636'){
                    if(c.Type_of_Resolution__c == 'No'){
                        c.Status = 'Ready for Review';
                        c.OwnerId = '00G6A000000TVC0UAO';
                    }
                    else if((c.Type_of_Resolution__c == 'Yes') && (c.Resolution_Notes__c != null)){
                        c.Type_of_Resolution__c = 'No';
                        c.Status = 'Ready for Review';
                        c.OwnerId = '00G6A000000TVC0UAO';	//Online Sales
                    } 
                } else{
                    if(c.Type_of_Resolution__c == 'Yes'){
                        c.Status = 'Closed in Salesforce';
                    }
                    else if((c.Type_of_Resolution__c == 'No')&& (c.Resolution_Notes__c == null)){
                        c.Type_of_Resolution__c = 'Yes';
                        c.Status = 'Closed in Salesforce';
                    }
                }
                updatecaselist.add(c);
            }
            //Database.UpsertResult[] results = Database.upsert(updatecaselist);
            update updatecaselist;
            return salesOrders;
        }else{
            return null;
        }
        
    }
}