public class SOStatusDetailWrapper {
 public value[] value;
    public class value {
        public String SaleOrderNo; //133
        public String TimeStamp; //200442870
        public String LoginUser;    //01330000861945
        public String ManagerApproved;
        public String ChangeType;    
        public String OriginalValue;    
        public String UpdatedValue; //SBalasubraman
        public String ReasonCode; //1
        public String StoreID; //0 SOLineItems SOLItems
    }
}