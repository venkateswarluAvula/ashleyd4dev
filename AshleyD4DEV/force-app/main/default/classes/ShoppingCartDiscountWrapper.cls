public class ShoppingCartDiscountWrapper {
  @AuraEnabled
    public List<SalesOrderLineWrapper> SalesOrderLineList{get;set;}
    
    @AuraEnabled
    public ShoppingCartDeliveryWrapper Delivery{get;set;}
    
    @AuraEnabled
    public String IsDeliveryFeeDiscountApprovedByManager{get;set;}
    
    @AuraEnabled
    public Decimal NextDayChargeOverride{get;set;}
    
    @AuraEnabled
    public String NextDayChargeOverrideReason{get;set;}
   
    @AuraEnabled
    public String CartGuid{get;set;}  
       
}