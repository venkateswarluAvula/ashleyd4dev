/***********
Class Name: MergeDuplicateAddressController
Description:This controller merge the Address Duplictae Records
**********/
public class MergeDuplicateAddressController {
    
    
    public static void mergeDuplicateAddresses(List<Case> newCaseLst) {
        
        List<Account> acctList = new List<Account>();
        List<Address__c> addressAccntList = new List<Address__c>();
        List<Address__c> addressCaseList = new List<Address__c>(); 
        List<Address__c> addressMerged = new List<Address__c>();
        List<Address__c> lstAccAdrss = new List<Address__c>();
        List<Address__c> lstAdrss = new List<Address__c>(); 
        List<Case> caseObj = new List<Case>();
        Set<String> setCaseAddress = new Set<String>();
        Set<String> setAcct = new Set<String>();
        Map<String, Address__c> addressMap = new Map<String, Address__c>();
        Map<String, Address__c> addressListMap = new Map<String, Address__c>();
        String dupAddrValues;
        
        for(Case objCse :newCaseLst){
            
            setAcct.add(objCse.AccountId); 
            setCaseAddress.add(objCse.Address__c);
            
        }
        System.debug('setAcct-----'+setAcct);
        System.debug('setCaseAddress-----'+setCaseAddress);
        
        
        addressAccntList = [Select Id,Name,AccountId__c,AccountId__r.Name,Address_Line_1__c,Address_Line_2__c,Address_Type__c,
                            Address_Validation_Status__c,Address_Validation_Timestamp__c,City__c,Country__c,Geocode__c,
                            Preferred__c,StateList__c,Zip_Code__c 
                            From Address__c Where AccountId__c IN :setAcct AND Preferred__c=true];
        
        System.debug('addressAccntList-------'+addressAccntList );
        
        //addressAccntList = = [SELECT Id,Name,AccountId__c From Address__c];
        addressCaseList = [SELECT Id,Name,AccountId__c,AccountId__r.Name,Address_Line_1__c,Address_Line_2__c,Address_Type__c,
                           Address_Validation_Status__c,Address_Validation_Timestamp__c,City__c,Country__c,Geocode__c,Preferred__c,StateList__c,
                           Zip_Code__c From Address__c Where Id IN :setCaseAddress AND AccountId__c IN :acctList ];
        
        System.debug('addressCaseList-------'+addressCaseList);
        
        acctList = [Select Id, Name, (SELECT Id,Name,AccountId__c,Address_Line_1__c,Address_Line_2__c,Address_Type__c,
                                      Address_Validation_Status__c,Address_Validation_Timestamp__c,City__c,Country__c,
                                      Geocode__c,Preferred__c,StateList__c,Zip_Code__c From Addresses__r)
                    From Account WHERE Id IN :setAcct];
         System.debug('acctList-------'+acctList );
        
        for(Account objAcc :acctList){
            
            lstAccAdrss = objAcc.Addresses__r;
            System.debug('lstAccAdrss-------'+lstAccAdrss);
        }
        
        
        for(Address__c objAddress : addressCaseList){
            
                addressMap.put(objAddress.Id, objAddress); 
                dupAddrValues = objAddress.AccountId__c+','+objAddress.Address_Line_1__c+','+objAddress.Address_Line_2__c+','+objAddress.City__c+','+objAddress.StateList__c+','+objAddress.Zip_Code__c;
                
            
            
            if(addressMap.containsKey(dupAddrValues )){
                
                
            }
            
        }
        
        
    }
    
}