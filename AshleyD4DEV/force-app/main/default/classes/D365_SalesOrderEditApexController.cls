public class D365_SalesOrderEditApexController {
       
 @AuraEnabled
    public static D365_SalesOrder__c getSalesInfo(string SoId) {
        system.debug('SoId------'+SoId);
        D365_SalesOrder__c SalesInfo=[SELECT D365_Address__c,D365_Customer_ID__c,D365_AccountId__c,Name, D365_Sales_Order_Number__c,D365_Customer_Name__c,
                                     D365_RSA_Indicator__c, D365_Address_1__c,D365_Fulfiller_phone_number__c,D365_Order_Submit_Date_Time__c,
                                 D365_Payment_Method__c,D365_Item_Number__c,D365_SO_Create_Date__c,D365_Type__c
                                 FROM D365_SalesOrder__c where Id =: SoId];
        system.debug('SalesInfo------'+SalesInfo);           
        return SalesInfo;
    }
        
       
    @AuraEnabled
    public Static D365_SalesOrder__c saveAccount(D365_SalesOrder__c SalesData) {
        SalesData.D365_AccountId__r = null;  
        upsert SalesData;
        return SalesData;
        
        
    }

}