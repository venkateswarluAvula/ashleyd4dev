@isTest
public class EDAServiceRequestCaseTest {

    @isTest static void testPostMethodUpdateCase(){
        Account acc = new Account();
        acc.Name = 'ARON';
        Insert acc;
        System.debug('Account'+acc);

        List<Contact> conlist = new List<Contact>();
        Contact con = new Contact();
        con.AccountId = acc.Id;
        con.LastName = 'ARON';
        con.Email = 'abc@abc.com';
        Insert con;
        conlist.add(con);

        List<Case> caselist = new List<Case>();
        Case Casee = new Case();
        Casee.Subject='Test record';
        Casee.Type = 'General Inquiry';
        Casee.AccountId = acc.Id;
        Casee.Description = 'test record';
        Casee.Origin = 'Phone';
        Casee.Status = 'New';
        Casee.Priority='Medium';
        Casee.Legacy_Service_Request_ID__c = '88804';
        Casee.Estimated_time_for_stop__c = '2';
        Casee.Request_Status__c = 'New';
        Casee.Legacy_Assignee__c = '1';
        Casee.Legacy_Account_Ship_To__c = '8888300-164';
        Casee.Reason = 'Delivery Issue';
        Casee.Category_Reason_Codes__c = 'Delivery Issue';
        Casee.Tech_Scheduled_Date__c = Date.parse('12/27/2018');
        //Casee.TechnicianNameScheduled__c = 'qq';
        Casee.Technician_Schedule_Date__c = Date.parse('12/27/2018');
        Casee.TechnicianNameScheduled__c = null;
        Casee.Technician_Schedule_Date__c = null;
        Casee.Follow_up_Date__c = Date.parse('12/25/2018');
        Casee.CreatedDate = Date.parse('5/31/2018');
        Insert Casee;
        caselist.add(Casee);
        System.debug('Insertion case'+Casee);

        List<Address__c> addlist = new List<Address__c>();
        /*
        Address__c Add = new Address__c();
        Add.AccountId__c = acc.Id;
        Add.Address_Line_1__c = 'qq';
        Add.Address_Line_2__c = 'QA';
        Add.Preferred__c = true;
        Add.City__c = 'QA';
        Add.StateList__c = 'CA';
        Add.Zip_Code__c = '522407';
        Add.Address_Type__c = 'Ship To';
        Insert Add;
        addlist.add(Add);
        System.debug('Address'+addlist);
		*/

        EDAServiceRequestCase.CaseWrap val = new EDAServiceRequestCase.CaseWrap();
        val.SFDCAccountId = acc.Id;
        val.RequestType = 'Part(s) Broken';
        val.RequestSubType = 'Parts Issue';
        val.RequestOrigin = 'Migration';
        val.AssigneeName = 'ASHLEY';
        val.SalesOrderNumber = '';
        val.ServiceTechVendorId = '0';
        val.OpenDate = '06/20/2018';
        val.ReasonCodeText = 'Part(s) Broken';
        val.OpenDateAsChar ='20180620';
        val.RequestPriority ='High';
        val.AssigneeCode = '378';
        val.IsTechResource = 'N';
        val.ReasonCode = '29';
        val.FollowUpDate = '06/22/2018';
        val.FollowUpDateAsChar = '20180622';
        val.ReopenDate = '';
        val.ReopenDateAsChar = '';
        val.ProfitCenterCode = '23';
        val.ServiceTechDesc = 'ASHLEY B M W F    (Skill Level )';
        val.ServiceTechID = '12345';
        val.ScheduleDate = '06/27/2018';
        val.ScheduleDateAsChar = '20180627';
        val.RequestSaleOrderNumber = 'NULL';
        val.RequestStatus = 'Open';
        val.RequestActiveFlag = 'Y';
        val.CreatedTime = '06/20/2018';
        val.CreatedUserID = '378';
        val.LastTime = '06/20/2018';
        val.LastUserID = '378';
        val.CustomerPhone1 = '8056482946';
        val.CustomerPhone2 = '';
        val.CustomerPhone3 = '';
        val.CustomerEmail = '';
        val.EstimatedTimeForStop = '1.08';
        val.ShipToAddress1 = 'Add1';
        val.ShipToAddress2 = 'Add2';
        val.ShipToCityName = 'City';
        val.ShipToStateCode = 'State';
        val.ShipToZipCode = '12345';
        val.ProfitCenterDescription = null;
        val.Subject = 'Part(s) Broken';
        val.Description = 'Parts Issue';
        val.MarketAccount = 'Kingswere Georgia - #8888300';
        val.AccountShipto = '8888300-164';
        val.RequestID = '88804';
        val.CustomerID = 'HESSDAV';
        val.CustomerType = 'RET';
        val.StoreNameStoreNumberPC = 'SOUTHLAKE-133-23';
        val.UniqueID = '16665277';

        Set<String> AccId = new Set<String>();
        Set<String> reqId = new set<String>();
        Set<string> AccShipto = new Set<string>();
        reqId.add(val.RequestID);
        AccId.add(val.SFDCAccountId);
        AccShipto.add(val.AccountShipto);

        Map<String,List<EDAServiceRequestCase.CaseWrap>> JSONreqBody = new Map<String,List<EDAServiceRequestCase.CaseWrap>>();
        List<EDAServiceRequestCase.CaseWrap> JSONm = new List<EDAServiceRequestCase.CaseWrap>();
        JSONm.add(val);
        JSONreqBody.put('CsrMaster', JSONm);
        string JsonMsg = JSON.serialize(JSONreqBody);
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/Services/apexrest/ServiceRequests-Case/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        try{
        	RestContext.response = res;
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.doPost();
        }
        catch(Exception ex){}

       /* try{
            EDAServiceRequestCase.getAddresses(AccShipto, reqId);
        }
        catch(Exception ex){}*/

        try{
        	EDAServiceRequestCase.myCasee(val, false, val.RequestID, val.AccountShipto, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.myCasee(val, true, val.RequestID, val.AccountShipto, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}
    }

    @isTest static void testPostMethodScheduledCase(){
        Account acc = new Account();
        acc.Name = 'ARON';
        Insert acc;
        System.debug('Account'+acc);

        List<Contact> conlist = new List<Contact>();
        Contact con = new Contact();
        con.AccountId = acc.Id;
        con.LastName = 'ARON';
        con.Email = 'abc@abc.com';
        Insert con;
        conlist.add(con);

        List<Case> caselist = new List<Case>();
        Case Casee = new Case();
        Casee.Subject='Test record';
        Casee.Type = 'General Inquiry';
        Casee.AccountId = acc.Id;
        Casee.Description = 'test record';
        Casee.Origin = 'Phone';
        Casee.Status = 'New';
        Casee.Priority='Medium';
        Casee.Legacy_Service_Request_ID__c = '88804';
        Casee.Estimated_time_for_stop__c = '2';
        Casee.Request_Status__c = 'New';
        Casee.Legacy_Assignee__c = '1';
        Casee.Legacy_Account_Ship_To__c = '8888300-164';
        Casee.Reason = 'Delivery Issue';
        Casee.Category_Reason_Codes__c = 'Delivery Issue';
        Casee.Tech_Scheduled_Date__c = Date.parse('12/27/2018');
        Casee.Technician_ServiceReqId__c = '88804';
        Casee.TechnicianNameScheduled__c = 'qq';
        Casee.Technician_Schedule_Date__c = Date.parse('12/27/2018');
        Casee.Technician_Address__c = 'Address1';
        Casee.Technician_Company__c = 'Company';
        Casee.followup_Priority_EstimatedTime__c = '12/25/2018';
        Casee.Follow_up_Date__c = Date.parse('12/25/2018');
        Casee.CreatedDate = Date.parse('5/31/2018');
        Insert Casee;
        caselist.add(Casee);
        System.debug('Insertion case'+Casee);

        List<Address__c> addlist = new List<Address__c>();

        EDAServiceRequestCase.CaseWrap val = new EDAServiceRequestCase.CaseWrap();
        val.SFDCAccountId = acc.Id;
        val.RequestType = 'Part(s) Broken';
        val.RequestSubType = 'Parts Issue';
        val.RequestOrigin = 'Migration';
        val.AssigneeName = 'ASHLEY';
        val.SalesOrderNumber = '';
        val.ServiceTechVendorId = '0';
        val.OpenDate = '06/20/2018';
        val.ReasonCodeText = 'Part(s) Broken';
        val.OpenDateAsChar ='20180620';
        val.RequestPriority ='High';
        val.AssigneeCode = '378';
        val.IsTechResource = 'N';
        val.ReasonCode = '29';
        val.FollowUpDate = '06/22/2018';
        val.FollowUpDateAsChar = '20180622';
        val.ReopenDate = '';
        val.ReopenDateAsChar = '';
        val.ProfitCenterCode = '23';
        val.ServiceTechDesc = 'ASHLEY B M W F    (Skill Level )';
        val.ServiceTechID = '12345';
        val.ScheduleDate = '06/27/2018';
        val.ScheduleDateAsChar = '20180627';
        val.RequestSaleOrderNumber = 'NULL';
        val.RequestStatus = 'Open';
        val.RequestActiveFlag = 'Y';
        val.CreatedTime = '06/20/2018';
        val.CreatedUserID = '378';
        val.LastTime = '06/20/2018';
        val.LastUserID = '378';
        val.CustomerPhone1 = '8056482946';
        val.CustomerPhone2 = '';
        val.CustomerPhone3 = '';
        val.CustomerEmail = '';
        val.EstimatedTimeForStop = '1.25';
        val.ShipToAddress1 = '';
        val.ShipToAddress2 = '';
        val.ShipToCityName = '';
        val.ShipToStateCode = '';
        val.ShipToZipCode = '';
        val.ProfitCenterDescription = null;
        val.Subject = 'Part(s) Broken';
        val.Description = 'Parts Issue';
        val.MarketAccount = 'Kingswere Georgia - #8888300';
        val.AccountShipto = '8888300-164';
        val.RequestID = '88804';
        val.CustomerID = 'HESSDAV';
        val.CustomerType = 'RET';
        val.StoreNameStoreNumberPC = 'SOUTHLAKE-133-23';
        val.UniqueID = '16665277';

        Set<String> AccId = new Set<String>();
        Set<String> reqId = new set<String>();
        Set<string> AccShipto = new Set<string>();
        reqId.add(val.RequestID);
        AccId.add(val.SFDCAccountId);
        AccShipto.add(val.AccountShipto);
        Map<String,List<EDAServiceRequestCase.CaseWrap>> JSONreqBody = new Map<String,List<EDAServiceRequestCase.CaseWrap>>();
        List<EDAServiceRequestCase.CaseWrap> JSONm = new List<EDAServiceRequestCase.CaseWrap>();
        JSONm.add(val);
        JSONreqBody.put('CsrMaster', JSONm);
        string JsonMsg = JSON.serialize(JSONreqBody);
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/Services/apexrest/ServiceRequests-Case/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        try{
        	RestContext.response = res;
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.doPost();
        }
        catch(Exception ex){}

       /* try{
            EDAServiceRequestCase.getAddresses(AccShipto, reqId);
        }
        catch(Exception ex){}*/

        try{
        	EDAServiceRequestCase.myCasee(val, false, val.RequestID, val.AccountShipto, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.myCasee(val, true, val.RequestID, val.AccountShipto, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}
    }

    @isTest static void testPostMethodScheduledCase2(){
        Account acc = new Account();
        acc.Name = 'ARON';
        Insert acc;
        System.debug('Account'+acc);

        List<Contact> conlist = new List<Contact>();
        Contact con = new Contact();
        con.AccountId = acc.Id;
        con.LastName = 'ARON';
        con.Email = 'abc@abc.com';
        Insert con;
        conlist.add(con);

        List<Case> caselist = new List<Case>();
        Case Casee = new Case();
        Casee.Subject='Test record';
        Casee.Type = 'General Inquiry';
        Casee.AccountId = acc.Id;
        Casee.Description = 'test record';
        Casee.Origin = 'Phone';
        Casee.Status = 'New';
        Casee.Priority='Medium';
        Casee.Legacy_Service_Request_ID__c = '88804';
        Casee.Estimated_time_for_stop__c = '2';
        Casee.Request_Status__c = 'New';
        Casee.Legacy_Assignee__c = '1';
        Casee.Legacy_Account_Ship_To__c = '8888300-164';
        Casee.Reason = 'Delivery Issue';
        Casee.Category_Reason_Codes__c = 'Delivery Issue';
        Casee.Tech_Scheduled_Date__c = Date.parse('12/27/2018');
        Casee.Technician_ServiceReqId__c = '88804';
        Casee.TechnicianNameScheduled__c = 'qq';
        Casee.Technician_Schedule_Date__c = Date.parse('12/27/2018');
        Casee.Technician_Address__c = 'Address1';
        Casee.Technician_Company__c = '';
        Casee.followup_Priority_EstimatedTime__c = '12/25/2018';
        Casee.Follow_up_Date__c = Date.parse('12/25/2018');
        Casee.CreatedDate = Date.parse('5/31/2018');
        Insert Casee;
        caselist.add(Casee);
        System.debug('Insertion case'+Casee);

        List<Address__c> addlist = new List<Address__c>();

        EDAServiceRequestCase.CaseWrap val = new EDAServiceRequestCase.CaseWrap();
        val.SFDCAccountId = acc.Id;
        val.RequestType = 'Part(s) Broken';
        val.RequestSubType = 'Parts Issue';
        val.RequestOrigin = 'Migration';
        val.AssigneeName = 'ASHLEY';
        val.SalesOrderNumber = '200465789';
        val.ServiceTechVendorId = '';
        val.OpenDate = '06/20/2018';
        val.ReasonCodeText = 'Part(s) Broken';
        val.OpenDateAsChar ='20180620';
        val.RequestPriority ='High';
        val.AssigneeCode = '378';
        val.IsTechResource = 'N';
        val.ReasonCode = '29';
        val.FollowUpDate = '06/22/2018';
        val.FollowUpDateAsChar = '20180622';
        val.ReopenDate = '';
        val.ReopenDateAsChar = '';
        val.ProfitCenterCode = '23';
        val.ServiceTechDesc = 'ASHLEY B M W F    (Skill Level )';
        val.ServiceTechID = 'ASHLEY B M W F    (Skill Level )';
        val.ScheduleDate = '06/27/2018';
        val.ScheduleDateAsChar = '20180627';
        val.RequestSaleOrderNumber = 'NULL';
        val.RequestStatus = 'Parts Order Not Tech Required';
        val.RequestActiveFlag = 'Y';
        val.CreatedTime = '06/20/2018';
        val.CreatedUserID = '378';
        val.LastTime = '06/20/2018';
        val.LastUserID = '378';
        val.CustomerPhone1 = '8056482946';
        val.CustomerPhone2 = '';
        val.CustomerPhone3 = '';
        val.CustomerEmail = '';
        val.EstimatedTimeForStop = '1.25';
        val.ShipToAddress1 = '';
        val.ShipToAddress2 = '';
        val.ShipToCityName = '';
        val.ShipToStateCode = '';
        val.ShipToZipCode = '';
        val.ProfitCenterDescription = null;
        val.Subject = 'Part(s) Broken';
        val.Description = 'Parts Issue';
        val.MarketAccount = 'Kingswere Georgia - #8888300';
        val.AccountShipto = '8888300-164';
        val.RequestID = '88804';
        val.CustomerID = 'HESSDAV';
        val.CustomerType = 'RET';
        val.StoreNameStoreNumberPC = 'SOUTHLAKE-133-23';
        val.UniqueID = '16665277';

        EDAServiceRequestCase.CaseWrap val2 = new EDAServiceRequestCase.CaseWrap();
        val2.SFDCAccountId = acc.Id;
        val2.RequestType = 'Part(s) Broken';
        val2.RequestSubType = 'Parts Issue';
        val2.RequestOrigin = 'Migration';
        val2.AssigneeName = 'ASHLEY';
        val2.SalesOrderNumber = '200465789';
        val2.ServiceTechVendorId = '';
        val2.OpenDate = '06/20/2018';
        val2.ReasonCodeText = 'Part(s) Broken';
        val2.OpenDateAsChar ='20180620';
        val2.RequestPriority ='High';
        val2.AssigneeCode = '378';
        val2.IsTechResource = 'N';
        val2.ReasonCode = '29';
        val2.FollowUpDate = '06/22/2018';
        val2.FollowUpDateAsChar = '20180622';
        val2.ReopenDate = '';
        val2.ReopenDateAsChar = '';
        val2.ProfitCenterCode = '23';
        val2.ServiceTechDesc = 'ASHLEY B M W F    (Skill Level )';
        val2.ServiceTechID = '12345';
        val2.ScheduleDate = '06/27/2018';
        val2.ScheduleDateAsChar = '20180627';
        val2.RequestSaleOrderNumber = 'NULL';
        val2.RequestStatus = 'Open';
        val2.RequestActiveFlag = 'Y';
        val2.CreatedTime = '06/20/2018';
        val2.CreatedUserID = '378';
        val2.LastTime = '06/20/2018';
        val2.LastUserID = '378';
        val2.CustomerPhone1 = '8056482946';
        val2.CustomerPhone2 = '';
        val2.CustomerPhone3 = '';
        val2.CustomerEmail = '';
        val2.EstimatedTimeForStop = '1.25';
        val2.ShipToAddress1 = '';
        val2.ShipToAddress2 = '';
        val2.ShipToCityName = '';
        val2.ShipToStateCode = '';
        val2.ShipToZipCode = '';
        val2.ProfitCenterDescription = null;
        val2.Subject = 'Part(s) Broken';
        val2.Description = 'Parts Issue';
        val2.MarketAccount = 'Kingswere Georgia - #8888300';
        val2.AccountShipto = '8888300-164';
        val2.RequestID = '88814';
        val2.CustomerID = 'HESSDAV';
        val2.CustomerType = 'RET';
        val2.StoreNameStoreNumberPC = 'SOUTHLAKE-133-23';
        val2.UniqueID = '16665277';

        EDAServiceRequestCase.CaseWrap val3 = new EDAServiceRequestCase.CaseWrap();
        val3.SFDCAccountId = acc.Id;
        val3.RequestType = 'Part(s) Broken';
        val3.RequestSubType = 'Parts Issue';
        val3.RequestOrigin = 'Migration';
        val3.AssigneeName = 'ASHLEY';
        val3.SalesOrderNumber = '200465789';
        val3.ServiceTechVendorId = '';
        val3.OpenDate = '06/20/2018';
        val3.ReasonCodeText = 'Part(s) Broken';
        val3.OpenDateAsChar ='20180620';
        val3.RequestPriority ='High';
        val3.AssigneeCode = '378';
        val3.IsTechResource = 'N';
        val3.ReasonCode = '29';
        val3.FollowUpDate = '06/22/2018';
        val3.FollowUpDateAsChar = '20180622';
        val3.ReopenDate = '';
        val3.ReopenDateAsChar = '';
        val3.ProfitCenterCode = '23';
        val3.ServiceTechDesc = 'ASHLEY B M W F    (Skill Level )';
        val3.ServiceTechID = '12345';
        val3.ScheduleDate = '06/27/2018';
        val3.ScheduleDateAsChar = '20180627';
        val3.RequestSaleOrderNumber = 'NULL';
        val3.RequestStatus = 'Open';
        val3.RequestActiveFlag = 'I';
        val3.CreatedTime = '06/20/2018';
        val3.CreatedUserID = '378';
        val3.LastTime = '06/20/2018';
        val3.LastUserID = '378';
        val3.CustomerPhone1 = '8056482946';
        val3.CustomerPhone2 = '';
        val3.CustomerPhone3 = '';
        val3.CustomerEmail = '';
        val3.EstimatedTimeForStop = '1.25';
        val3.ShipToAddress1 = '';
        val3.ShipToAddress2 = '';
        val3.ShipToCityName = '';
        val3.ShipToStateCode = '';
        val3.ShipToZipCode = '';
        val3.ProfitCenterDescription = null;
        val3.Subject = 'Part(s) Broken';
        val3.Description = 'Parts Issue';
        val3.MarketAccount = 'Kingswere Georgia - #8888300';
        val3.AccountShipto = '8888300-164';
        val3.RequestID = '91506';
        val3.CustomerID = 'HESSDAV';
        val3.CustomerType = 'RET';
        val3.StoreNameStoreNumberPC = 'SOUTHLAKE-133-23';
        val3.UniqueID = '16665277';

        List<EDAServiceRequestCase.CaseWrap> JSONm = new List<EDAServiceRequestCase.CaseWrap>();
        JSONm.add(val);
        JSONm.add(val2);
        JSONm.add(val3);

        Map<String,List<EDAServiceRequestCase.CaseWrap>> JSONreqBody = new Map<String,List<EDAServiceRequestCase.CaseWrap>>();
        JSONreqBody.put('CsrMaster', JSONm);
        string JsonMsg = JSON.serialize(JSONreqBody);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/Services/apexrest/ServiceRequests-Case/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        try{
        	RestContext.response = res;
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.doPost();
        }
        catch(Exception ex){}

        try{
        	EDAServiceRequestCase.myCasee(val, false, val.RequestID, val.AccountShipto, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.myCasee(val, true, val.RequestID, val.AccountShipto, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}
    }

    @isTest static void testPostMethodInsertCase(){
        Account acc = new Account();
        acc.Name = 'ARON';
        Insert acc;
        System.debug('Account'+acc);

        List<Contact> conlist = new List<Contact>();
        Contact con = new Contact();
        con.AccountId = acc.Id;
        con.LastName = 'ARON';
        con.Email = 'abc@abc.com';
        Insert con;
        conlist.add(con);

        List<Address__c> addlist = new List<Address__c>();
        List<Case> caselist = new List<Case>();

        EDAServiceRequestCase.CaseWrap val = new EDAServiceRequestCase.CaseWrap();
        val.SFDCAccountId = acc.Id;
        val.RequestType = 'Part(s) Broken';
        val.RequestSubType = 'Parts Issue';
        val.RequestOrigin = 'Migration';
        val.AssigneeName = 'ASHLEY';
        val.SalesOrderNumber = '';
        val.ServiceTechVendorId = '0';
        val.OpenDate = '06/20/2018';
        val.ReasonCodeText = 'Part(s) Broken';
        val.OpenDateAsChar ='20180620';
        val.RequestPriority ='High';
        val.AssigneeCode = '378';
        val.IsTechResource = 'N';
        val.ReasonCode = '29';
        val.FollowUpDate = '06/22/2018';
        val.FollowUpDateAsChar = '20180622';
        val.ReopenDate = '';
        val.ReopenDateAsChar = '';
        val.ProfitCenterCode = '23';
        val.ServiceTechDesc = 'ASHLEY B M W F    (Skill Level )';
        val.ServiceTechID = '12345';
        val.ScheduleDate = string.valueOf(date.today().month()+'/'+date.today().day()+'/'+date.today().year());
        val.ScheduleDateAsChar = '20180627';
        val.RequestSaleOrderNumber = 'NULL';
        val.RequestStatus = 'Open';
        val.RequestActiveFlag = 'Y';
        val.CreatedTime = '06/20/2018';
        val.CreatedUserID = '378';
        val.LastTime = '06/20/2018';
        val.LastUserID = '378';
        val.CustomerPhone1 = '8056482946';
        val.CustomerPhone2 = '';
        val.CustomerPhone3 = '';
        val.CustomerEmail = '';
        val.EstimatedTimeForStop = '1.17';
        val.ShipToAddress1 = '';
        val.ShipToAddress2 = '';
        val.ShipToCityName = '';
        val.ShipToStateCode = '';
        val.ShipToZipCode = '';
        val.ProfitCenterDescription = null;
        val.Subject = 'Part(s) Broken';
        val.Description = 'Parts Issue';
        val.MarketAccount = 'Kingswere Georgia - #8888300';
        val.AccountShipto = '8888300-164';
        val.RequestID = '88804';
        val.CustomerID = 'HESSDAV';
        val.CustomerType = 'RET';
        val.StoreNameStoreNumberPC = 'SOUTHLAKE-133-23';
        val.UniqueID = '16665277';

        Set<String> AccId = new Set<String>();
        Set<String> reqId = new set<String>();
        reqId.add(val.RequestID);
        AccId.add(val.SFDCAccountId);
        Map<String,List<EDAServiceRequestCase.CaseWrap>> JSONreqBody = new Map<String,List<EDAServiceRequestCase.CaseWrap>>();
        List<EDAServiceRequestCase.CaseWrap> JSONm = new List<EDAServiceRequestCase.CaseWrap>();
        JSONm.add(val);
        JSONreqBody.put('CsrMaster', JSONm);
        string JsonMsg = JSON.serialize(JSONreqBody);
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/Services/apexrest/ServiceRequests-Case/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        try{
        	RestContext.response = res;
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.doPost();
        }
        catch(Exception ex){}

       /* try{
            EDAServiceRequestCase.getAddresses(AccId, reqId);
            EDAServiceRequestCase.myAdd(val);
        }
        catch(Exception ex){}*/

        try{
        	EDAServiceRequestCase.myCasee(val, false, val.RequestID, val.SFDCAccountId, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}

        try{
	        EDAServiceRequestCase.myCasee(val, true, val.RequestID, val.SFDCAccountId, addlist, conlist, caselist, null, null);
        }
        catch(Exception ex){}
    }

    @isTest static void testFormatEstTimeforStop(){
    	test.startTest();
    	EDAServiceRequestCase.formatEstTimeforStop('1');
    	EDAServiceRequestCase.formatEstTimeforStop('1.8');
    	EDAServiceRequestCase.formatEstTimeforStop('1.17');
    	EDAServiceRequestCase.formatEstTimeforStop('1.25');
    	EDAServiceRequestCase.formatEstTimeforStop('1.33');
    	EDAServiceRequestCase.formatEstTimeforStop('1.42');
    	EDAServiceRequestCase.formatEstTimeforStop('1.5');
    	EDAServiceRequestCase.formatEstTimeforStop('1.58');
    	EDAServiceRequestCase.formatEstTimeforStop('1.67');
    	EDAServiceRequestCase.formatEstTimeforStop('1.75');
    	EDAServiceRequestCase.formatEstTimeforStop('1.83');
    	EDAServiceRequestCase.formatEstTimeforStop('1.92');
    	test.stopTest();
    }

    @isTest static void testFormatTechScheduleAddress(){
    	test.startTest();
        EDAServiceRequestCase.CaseWrap val = new EDAServiceRequestCase.CaseWrap();
        val.ShipToAddress1 = 'Address 1';
        val.ShipToAddress2 = 'Address 2';
        val.ShipToCityName = 'City Name';
        val.ShipToStateCode = 'State Code';
        val.ShipToZipCode = '12345';
    	EDAServiceRequestCase.formatTechScheduleAddress(val);
       // EDAServiceRequestCase.getAdrStr('Address 1', 'Address 2', 'City', 'State', '12354');
    	test.stopTest();
    }

}