/****** v1 | Description: Test methods for SignatureController class | 8/16/2018 | JoJo Zhao */
@isTest
public class SignatureControllerTest {

    @isTest
    public static void testSaveCapturedSignature(){
        TestDataFactory.prepareAPICustomSetting();
        User rsaUser = TestDataFactory.createRSAUser();
        insert rsaUser;
        
        System.runAs(rsaUser) {     
            Account acc= TestDataFactory.prepareCustomerInfo();
            ContentVersion cv = new ContentVersion();
            cv.title = 'Signature.png';      
            cv.PathOnClient ='test';           
            cv.VersionData = Blob.valueOf('TestingSignature');          
            insert cv;         
            
            ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
            
            Test.startTest();
            Boolean savedFlag = SignatureController.checkSignatureSaved();
            System.assertEquals(true, savedFlag);
            Id escObjId = SignatureController.saveCapturedSignature();
            Boolean saveFlagCheck = SignatureController.checkSignatureSaved();
            System.assertEquals(false, saveFlagCheck);
            List<Id> attId = SignatureController.getCaliforniaSignatureList();
            System.assertNotEquals(null, attId);
            System.assertEquals(1, attId.size());
            
            SignatureController.saveSignatureToCustomer(attId[0], acc.id);
       
        }

    }
    
    
    @isTest
    public static void testSaveCapturedSignature1(){
        TestDataFactory.prepareAPICustomSetting();
        User rsaUser = TestDataFactory.createRSAUser();
        insert rsaUser;
        
        System.runAs(rsaUser) {     
            Account acc= TestDataFactory.prepareCustomerInfo();
            ContentVersion cv = new ContentVersion();
            cv.title = 'Signature.png';      
            cv.PathOnClient ='test';           
            cv.VersionData = Blob.valueOf('TestingSignature');          
            insert cv;         
            
            ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
            
            Test.startTest();
            Boolean savedFlag = SignatureController.checkSignatureSaved();
            System.assertEquals(true, savedFlag);
            Id escObjId = SignatureController.saveCapturedSignature();
            Boolean saveFlagCheck = SignatureController.checkSignatureSaved();
            System.assertEquals(false, saveFlagCheck);
            
            List<Id> attId = SignatureController.getCaliforniaSignatureList();
            System.assertNotEquals(null, attId);
            System.assertEquals(1, attId.size());
            
            try
            {
                SignatureController.saveSignatureToCustomer('signatre', '12345'); //attId[0]
            }
            catch(Exception e)
            {
                e.getMessage();
            }
        }

    }
    
    @isTest
    public static void testIsCaliforniaStore(){
        TestDataFactory.prepareAPICustomSetting();
        User rsaUser = TestDataFactory.createRSAUser();
        insert rsaUser;
        
        System.runAs(rsaUser) {     
            Test.setMock(HttpCalloutMock.class, new StoreInfoMock());
            Test.startTest();
            Boolean isCaliforniaStore = SignatureController.isCaliforniaStore();
            System.assertEquals(false, isCaliforniaStore);
  
            Test.stopTest();
        } 
        
    } 
    
    public static void prepareAPICustomSetting(){
        TestDataFactory.prepareAPICustomSetting();
        
        NewOpportunityDefaultAddress__c defaultOppAddress = new NewOpportunityDefaultAddress__c();
        defaultOppAddress.Name = 'defaultOppAddress';
        defaultOppAddress.PostCode__c = '31009';
        defaultOppAddress.StateOrProvince__c = 'GA';
        insert defaultOppAddress;
    }
    
    public static Account createAccounts() 
    {
        Account acc =new Account();
        String rtypeId=[Select id From RecordType Where DeveloperName='Customer' AND SobjectType='Account'].id;
        acc.RecordTypeId=rtypeId;
        acc.FirstName='Test';
        acc.LastName='Test';
        acc.Phone='9090909090';
        acc.PersonEmail = 'MyCustomerControllerTest@test.com';
        insert acc;
        
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = acc.id;
        Test.setFixedSearchResults(fixedSearchResults);
        
        return acc;        
    }
    
     @isTest
    public static void updateLineItemWithSKUtest(){
        prepareAPICustomSetting();
        
        Account con=SignatureControllerTest.createAccounts();
        Opportunity opp = new Opportunity();
        opp.Name='test';
        opp.AccountId=con.Id;
        opp.CloseDate=system.today();
        opp.StageName='Saved Shopping Cart';
        insert opp;
       
        Test.startTest();
        
        String fetchType = 'other';
        Test.setMock(HttpCalloutMock.class, new ShoppingCartItemDetailsCallOutMock(fetchType));
        
        //Test.setMock(HttpCalloutMock.class, new ProductDetailCallOutMock(fetchType));
        Shopping_cart_line_item__c lineItem =new Shopping_cart_line_item__c();
        lineItem.Opportunity__c = opp.Id;
        lineItem.Product_SKU__c = 'D596-00';
        lineItem.quantity__c = 1;
        insert lineItem;                                               
        
        SignatureController.updateLineItemWithSKU(con.Id,lineItem,'1');
        SignatureController.getProductDetailWithSKU(lineItem,'1');
        Test.stopTest();
        
    } 
    
}