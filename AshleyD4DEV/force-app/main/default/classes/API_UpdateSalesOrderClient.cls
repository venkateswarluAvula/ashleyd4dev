/**
* This is an API client class that will facilitates calls to Ashley systems to make updates on Orders associated to Customers
* in salesforce. 
 */
public with sharing class API_UpdateSalesOrderClient {
    public static final string ORDER_UPDATE_NAMED_CREDENTIAL_PRODUCTION = 'Order_Update_Prod';
    public static final string ORDER_UPDATE_NAMED_CREDENTIAL_SANDBOX = 'Order_Update_Sandbox';
    public static final string INTEGRATION_SETTING_KEY_PRODUCTION = 'Order_Update_Prod';
    public static final string INTEGRATION_SETTING_KEY_SANDBOX = 'Order_Update_Sandbox'; 
        
    public static final Integer CALLOUT_TIMEOUT_IN_MILLI_SECONDS = 60000;

    public static API_BaseResponse updateShippingAddress(string fullfilledById, string orderNumber, string UserName, Integer profitCenter, string shipToName,
	string shipToAddress1, string shipToAddress2, string shipToCity, string shipToState, string shipToZip, date dateOfDelivery){
        API_BaseResponse response = new API_BaseResponse();

        //Added to get delivery date
        //SalesOrder__x order = SalesOrderDAO.getOrderById(salesforceOrderId);
        //List<SalesOrderItem__x> lineItems = SalesOrderDAO.getOrderLineItemsByOrderExternalId(order.ExternalId);
		//End

        if(String.isBlank(shipToAddress1) || String.isBlank(shipToCity) || String.isBlank(shipToState) || 
            String.isBlank(shipToZip)){
            response.isSuccess = false;
            response.message = Label.Missing_Required_Field_For_Address_Update;
            return response;
        }
        if(String.isBlank(shipToAddress2)){
            shipToAddress2=null;
        }      

        try{
            HttpRequest req = new HttpRequest();
            if(Utilities_SalesAndService.isSandbox){
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_SANDBOX + buildShipToAddressURI(fullfilledById, orderNumber, UserName, profitCenter, shipToName,
                shipToAddress1, shipToAddress2, shipToCity, shipToState, shipToZip));
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_SANDBOX);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }
            //use production named creds
            else{
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_PRODUCTION + buildShipToAddressURI(fullfilledById, orderNumber, UserName, profitCenter, shipToName,
                shipToAddress1, shipToAddress2, shipToCity, shipToState, shipToZip));
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_PRODUCTION);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }           
            req.setMethod('PUT');
            req.setHeader('Content-Type' ,'application/json');
            req.setHeader('Accept' ,'application/json');
            req.setBody('{}');
            req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);

            HttpResponse res = new Http().send(req);
            string responseBody;
            responseBody = res.getBody();
            system.debug('responseBody 57'+responseBody);
            System.debug('***** ship address status update url: '+ req.getEndpoint());
            System.debug('***** ship address status response: '+ res.getBody() + '-' + res.getStatus());

            if(res.getStatusCode() == 200){
                if (responseBody.contains('\"')){
                   responseBody = responseBody.replace('\"',' ');
                }
                
                if (responseBody.contains('1990')){
                    response.isSuccess = true;
                	response.message = Label.Order_Ship_To_Updated_SUccess_Message;
                } else{
                    if (responseBody.contains('advise the customer'))
                    {
                        string updDate = formatDateForOrderAPICall(dateOfDelivery);
                        system.debug('updDate---'+updDate);
            			string newmsg = Label.ShipTo_New_Success;
                        string newmsg1 = newmsg.replace('{DELIVERY_DATE}', updDate);
            			system.debug('newmsg1---'+newmsg1);

                        response.isSuccess = false;
                        response.message = newmsg1;
                        system.debug('Message for shipto address------>' + response.message);
                        system.debug('responseBody----->'+responseBody);
                    } else {
                        response.isSuccess = true;
                        response.message = Label.Order_Ship_To_Updated_SUccess_Message;
                    }
				}
            }
            else{
                response.isSuccess = false;
                response.message = Label.Order_Ship_To_Updated_Error_Message + res.getStatusCode() + ' - ' + res.getBody();
            }
        }
        catch(Exception ex){
            response.isSuccess = false;
            response.message = Label.Order_Ship_To_Updated_Error_Message + ex.getMessage();
        }
        return response;
        
    }
    public static API_BaseResponse updateSalesOrderHotStatus(string fullfilledById, string orderNumber,  string UserName, boolean hotStatus, String WindowBegin, String WindowEnd, Date deliveryDate,Decimal profitCenter){

        API_BaseResponse response = new API_BaseResponse();
        
        if(deliveryDate == null){
            response.isSuccess = false;
            response.message = Label.Delivery_Date_Required_For_Hot_Status_Update;
            return response;
        }
        
        try{
            HttpRequest req = new HttpRequest();
            if(Utilities_SalesAndService.isSandbox){
                system.debug('ORDER_UPDATE-------'+ORDER_UPDATE_NAMED_CREDENTIAL_SANDBOX);
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_SANDBOX + buildHotStatusURI(fullfilledById, orderNumber, UserName,hotStatus, WindowBegin, WindowEnd, deliveryDate, profitCenter));
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_SANDBOX);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }
            //use production named creds
            else{
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_PRODUCTION + buildHotStatusURI(fullfilledById, orderNumber,  UserName, hotStatus, WindowBegin, WindowEnd, deliveryDate,profitCenter));
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_PRODUCTION);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }           
            req.setMethod('PUT');
            req.setHeader('Content-Type' ,'application/json');
            req.setHeader('Accept' ,'application/json');
            req.setBody('{}');
            req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);
            HttpResponse res = new Http().send(req);
            System.debug('***** hot status update url: '+ req.getEndpoint());
            System.debug('***** hot status response: '+ res.getBody() + '-' + res.getStatus());
            if(res.getStatusCode() == 200){
                response.isSuccess = true;
                response.message = Label.Order_Hot_Status_Updated_Success_Message;
            }
            else{
                response.isSuccess = false;
                //response.message = Label.Order_Hot_Status_Updated_Error_Message + res.getStatusCode() + ' - ' + res.getBody();
                response.message = Label.Order_Hot_Status_Updated_Error_Message + ' - ' + res.getBody();
            }
        }
        catch(Exception ex){
            response.isSuccess = false;
            response.message = Label.Order_Hot_Status_Updated_Error_Message  + ex.getMessage();
        }
        return response;
    }
    
     
    //update VIP and LIB status
     //Added by Anil and Sudeshna-Ashley for VIP functionality – Code starts.
     public static API_BaseResponse updateSalesOrderVIPLIBStatus(String fullfilledById, String orderNumber, string UserName,Boolean vipStatus,Boolean libStatus, Boolean vipCurStatus, Boolean libCurStatus){

        API_BaseResponse response = new API_BaseResponse();
          try{
            HttpRequest req = new HttpRequest();
              //'https://ashley-preprod-dev.apigee.net/homestores/' add to custom label and specify a name and populate
              //bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713  - add a value in label and Ashley-VIP apikey
            String Endpoint = System.label.AshleyApigeeEndpoint+ fullfilledById+'/salesorders/'+orderNumber+'/VIPLIBStatus?VIPFlag='+vipStatus+'&LIBFlag='+libStatus+'&userName='+UserName;
            req.setEndpoint(Endpoint);
            req.setHeader('apikey',System.label.AshleyApigeeApiKey);
            req.setMethod('PUT');
            req.setHeader('Content-Type' ,'application/json');
            req.setHeader('Accept' ,'application/json');
            req.setBody('{}');
            req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);

            HttpResponse res = new Http().send(req);
            System.debug('***** viplib status update url: '+ req.getEndpoint());
            System.debug('***** viplib status response: '+ res.getBody() + '-' + res.getStatus());
            if(res.getStatusCode() == 200){
                System.debug('My Vip'+vipStatus);
                System.debug('My lib'+libStatus);
                System.debug('My currentvip'+vipCurStatus);
                System.debug('My Currentlib'+libCurStatus);
                response.isSuccess = true;
                if((vipStatus!=vipCurStatus)&&(libStatus==libCurStatus))
                    response.message = Label.Order_vip_Status_Updated_Success_Message;
                else if((libStatus!=libCurStatus)&&(vipStatus==vipCurStatus))
                    response.message = Label.Order_lib_Status_Updated_Success_Message;
                else if((libStatus!=libCurStatus)&&(vipStatus!=vipCurStatus))
                    response.message = Label.Order_VIP_and_LIB_Status_Updated_Success_Message;
            }
            else{
                response.isSuccess = false;
                response.message = Label.Order_vip_Status_Updated_Error_Message + res.getStatusCode() + ' - ' + res.getBody();
            }
        }
        catch(Exception ex){
            response.isSuccess = false;
            response.message = Label.Order_vip_Status_Updated_Error_Message  + ex.getMessage();
        }
        return response;
    }       
 
     //Added by Anil and Sudeshna-Ashley for LIB functionality – Code End.
     
     
     //Added by Sudeshna-Ashley for ServiceLevel functionality – Code Start.
   public static API_BaseResponse updateSalesOrderservicelevel(String fullfilledById, String orderNumber, string UserName, String Val,String servicelevel){

        API_BaseResponse response = new API_BaseResponse();
          try{
            HttpRequest req = new HttpRequest();
            String Endpoint = System.label.AshleyApigeeEndpoint+ fullfilledById+'/salesorders/'+orderNumber+'/ServiceLevel?serviceType='+servicelevel +'&userName='+UserName;
            req.setEndpoint(Endpoint);
            req.setHeader('apikey',System.label.AshleyApigeeApiKey);
            req.setMethod('PUT');
            req.setHeader('Content-Type' ,'application/json');
            req.setHeader('Accept' ,'application/json');
            req.setBody('{}');
            req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);

            HttpResponse res = new Http().send(req);
            System.debug('***** Threshold status update url: '+ req.getEndpoint());
            System.debug('***** Threshold status response: '+ res.getBody() + '-' + res.getStatus());
            if(res.getStatusCode() == 200){
                System.debug('servicelevel'+servicelevel);

                response.isSuccess = true;
            }
            else{
                response.isSuccess = false;
                response.message = Label.Order_vip_Status_Updated_Error_Message + res.getStatusCode() + ' - ' + res.getBody();
            }
        }
        catch(Exception ex){
            response.isSuccess = false;
            response.message = Label.Order_vip_Status_Updated_Error_Message  + ex.getMessage();
        }
        return response;
    }       
    //Added by Sudeshna-Ashley for ServiceLevel functionality – Code End.
    
     public static API_BaseResponse updateSalesOrderDeliveryDate(string fullfilledById, string orderNumber,Boolean asap,Integer profitCenter, 
                                                                Date deliveryDate, string deliveryType, string reasonDescription){
        API_BaseResponse response = new API_BaseResponse();
        //ASAP Functionality By venkat - Start                                                        
        System.debug('ASAP-----'+asap);
        if(asap)
        {
           deliveryDate = null;
        }
        //ASAP Functionality By venkat - End                                                          
        try{
            HttpRequest req = new HttpRequest();
            if(Utilities_SalesAndService.isSandbox){
                system.debug('deliveryDate------'+deliveryDate);
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_SANDBOX + buildDeliveryDateUpdateURI(fullfilledById, orderNumber,asap, profitCenter, deliveryDate, deliveryType, reasonDescription));
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_SANDBOX);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }
            //use production named creds
            else{
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_PRODUCTION + buildDeliveryDateUpdateURI(fullfilledById, orderNumber,asap, profitCenter, deliveryDate, deliveryType, reasonDescription));
                system.debug('setEndpoint------'+req);
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_PRODUCTION);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }           
            req.setMethod('PUT');
            req.setHeader('Content-Type' ,'application/json');
            req.setHeader('Accept' ,'application/json');
            req.setBody('{}');
            req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);

            HttpResponse res = new Http().send(req);            

            System.debug('***** Delivery date update url: '+ req.getEndpoint());
            System.debug('***** Delivery date response: '+ res.getBody() + '-' + res.getStatus());
            
            if(res.getStatusCode() == 200){
                response.isSuccess = true;
                response.message = Label.Order_Delivery_Date_Updated_Success_Message;
            }
            else{
                response.isSuccess = false;
                response.message = Label.Order_Delivery_Date_Updated_Error_Message + res.getStatusCode() + ' - ' + res.getBody();
            }
        }
        catch(Exception ex){
            response.isSuccess = false;
            response.message = Label.Order_Delivery_Date_Updated_Error_Message  + ex.getMessage();
        }
        return response;
    }   

    //Edited for Delivery Comments
    public static API_BaseResponse updateSalesOrderDeliveryComments(string fullfilledById, string salesOrderDeliveryComments, Integer profitCenter, string orderNumber, string UserName){
        
        API_BaseResponse response = new API_BaseResponse();
        string orderDeliverycomment;
        
        system.debug('fulfiller,profitcenter,orderNumber,conStatus'+fullfilledById+profitCenter+orderNumber);  
        String endpoint = system.label.AshleyApigeeEndpoint+fullfilledById+ '/salesorders/' + orderNumber + '/SalesOrderDeliveryComments?profitCenter=' +profitCenter + '&username='+UserName + '&apikey=' +system.label.AshleyApigeeApiKey ;
        string endpoint1 = endpoint.replace(' ','%20'); 
        Http http = new http();
        Httprequest req = new HttpRequest();
        req.setHeader('apikey', system.label.AshleyApigeeApiKey);
        req.setEndpoint(endpoint1);
        system.debug('Order Delivery Comments put enpoint' + endpoint1);
        
        orderDeliverycomment = '{"DeliveryComments":"'+salesOrderDeliveryComments+'"}';
        
        req.setMethod('PUT');
        req.setHeader('Content-Type' ,'application/json');
        req.setHeader('Accept' ,'application/json');
        req.setBody(orderDeliverycomment);
        req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);
        
        HttpResponse res = http.send(req);
        
        System.debug('***** Order Delivery Comment update url: '+ req.getEndpoint());
        System.debug('***** Order Delivery Comment response: '+ res.getBody() + '-' + res.getStatus());
        if(res.getStatusCode() == 200){
            response.isSuccess = true;
            response.message = Label.Order_Delivery_Comment_Updated_Success_Message;
        }
        else{
            response.isSuccess = false;
            response.message = Label.Order_Delivery_Comment_Updated_Error_Message + res.getStatusCode() + ' - ' + res.getBody();
        }
        
        return response;
    }

    //Ended
    
    public static API_BaseResponse updateSalesOrderComments(string fullfilledById, string salesOrderComments, string UserName, string orderNumber, Integer profitCenter){
        API_BaseResponse response = new API_BaseResponse();
        string ordercomment;
        system.debug('--response-----'+response);
        try{
            HttpRequest req = new HttpRequest();
            if(Utilities_SalesAndService.isSandbox){
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_SANDBOX + buildOrderCommentsUpdateURI(fullfilledById, salesOrderComments, UserName, orderNumber, profitCenter));
                ordercomment = '{"SaleComments":"'+salesOrderComments+'"}';
                system.debug('ordercomment-----'+ordercomment);
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_SANDBOX);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }
            //use production named creds
            else{
                req.setEndpoint('callout:'+ ORDER_UPDATE_NAMED_CREDENTIAL_PRODUCTION + buildOrderCommentsUpdateURI(fullfilledById, salesOrderComments, UserName,orderNumber, profitCenter));
                ordercomment = '{"SaleComments":"'+salesOrderComments+'"}';
                //get key from custom setting
                Integration_Settings__c setting = Utilities_SalesAndService.getIntegrationSetting(INTEGRATION_SETTING_KEY_PRODUCTION);
                if(setting != null){
                    req.setHeader('apikey', setting.API_Key__c);
                }
            }           
            req.setMethod('PUT');
            req.setHeader('Content-Type' ,'application/json');
            req.setHeader('Accept' ,'application/json');
            req.setBody(ordercomment);
            req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);
            
            HttpResponse res = new Http().send(req);
            System.debug('***** Order Comments  update url: '+ req.getEndpoint());
            System.debug('***** Order Comments update response: '+ res.getBody() + '-' + res.getStatus());
            
            if(res.getStatusCode() == 200){
                response.isSuccess = true;
                response.message = Label.Order_Comment_Updated_Success_Message;
            }
            else{
                response.isSuccess = false;
                response.message = Label.Order_Comment_Updated_Error_Message + res.getStatusCode() + ' - ' + res.getBody();
            }
        }
        catch(Exception ex){
            response.isSuccess = false;
            response.message = Label.Order_Comment_Updated_Error_Message + ex.getMessage() + ex.getStackTraceString();
        }
        return response;
    }
    
    //Added for Contact status by Vamsi
    public static API_BaseResponse updateContactStatus(string fullfilledById, string orderNumber, string conStatus, decimal profitCenter, string UserName ){
        API_BaseResponse response = new API_BaseResponse();
        system.debug('fulfiller,profitcenter,orderNumber,conStatus'+fullfilledById+profitCenter+orderNumber+conStatus);  
        String endpoint = system.label.AshleyApigeeEndpoint+fullfilledById+ '/salesorders/' + orderNumber + '/ContactStatus?contactStatus=' + conStatus +'&profitCenter='+profitCenter+'&apikey='+system.label.AshleyApigeeApiKey + '&userName='+UserName;
        string endpoint1 = endpoint.replace(' ','%20'); 
        Http http = new http();
        Httprequest req = new HttpRequest();
        req.setHeader('apikey', system.label.AshleyApigeeApiKey);
        req.setEndpoint(endpoint1);
        system.debug('statuscall put enpoint' + endpoint1);
        
        req.setMethod('PUT');
        req.setHeader('Content-Type' ,'application/json');
        req.setHeader('Accept' ,'application/json');
        req.setBody('{}');
        req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);
        
        HttpResponse res = http.send(req);
        
        System.debug('contactstatus response' + res.getBody());
        system.debug('contactstatus statusCode' + res.getStatusCode());
        if(res.getStatusCode()==200) {
            response.isSuccess = true;
            response.message = 'Contact status is updated successfully';
            
        } else {
            response.isSuccess = false;
            response.message='Failed to update Contact status, please contact your System Administrator'+res.getStatusCode() + ' - ' + res.getBody();  
            
        }
        return response;
    }
    //Ended
    
    //Added for ship Via by Vamsi
    public static API_BaseResponse updateShipVia(string fullfilledById, string orderNumber, string shipVia, decimal profitCenter, string UserName ){
        API_BaseResponse response = new API_BaseResponse();
        system.debug('fulfiller,profitcenter,orderNumber,string'+fullfilledById+profitCenter+orderNumber+shipVia);  
        String endpoint = system.label.AshleyApigeeEndpoint+fullfilledById+ '/salesorders/' + orderNumber + '/SalesOrderShipVia?profitCenter=' + profitCenter + '&ShipViaCode=' + shipVia + + '&userName='+UserName + '&apikey='+system.label.AshleyApigeeApiKey;
        // https://stageapigw.ashleyfurniture.com/homestores/8888300-164/salesorders/200475920/SalesOrderShipVia?profitCenter=23&ShipViaCode=G1&userName=sfusername&apikey=bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713    
        string endpoint1 = endpoint.replace(' ','%20'); 
        Http http = new http();
        Httprequest req = new HttpRequest();
        req.setHeader('apikey', system.label.AshleyApigeeApiKey);
        req.setEndpoint(endpoint1);
        system.debug('shipVia put endpoint---' + endpoint1);
        
        req.setMethod('PUT');
        req.setHeader('Content-Type' ,'application/json');
        req.setHeader('Accept' ,'application/json');
        req.setBody('{}');
        req.setTimeout(CALLOUT_TIMEOUT_IN_MILLI_SECONDS);
        
        HttpResponse res = http.send(req);
        
        System.debug('shipVia response' + res.getBody());
        system.debug('shipVia statusCode' + res.getStatusCode());
        if(res.getStatusCode()==200) {
            response.isSuccess = true;
            response.message = 'Ship Via is updated successfully';
            
        } else {
            response.isSuccess = false;
            response.message='Failed to update Ship Via, please contact your System Administrator'+res.getStatusCode() + ' - ' + res.getBody();  
            
        }
        return response;
    }
    //Ended
    
    //Added by Venkat D4
    @TestVisible
    private static string buildASAPStatusURI(string fullfilledById, string orderNumber, Boolean asapStatus, Date deliveryDate){
        string uri = '/' + fullfilledById +'/salesorders/' + orderNumber +'/ASAPStatus?asapflag=' + asapStatus + '&deliveryDate=' + formatDateForOrderAPICall(deliveryDate);
        system.debug('asapuri-----'+uri);
        return  uri;
    }

    private static string buildHotStatusURI(string fullfilledById, string orderNumber, string UserName, Boolean hotStatus,String WindowBegin, String WindowEnd,  Date deliveryDate, Decimal profitCenter){
        string uri = '/' + fullfilledById +'/salesorders/' + orderNumber +'/HOTStatus?hotflag=' + hotStatus + '&windowOpen=' + WindowBegin + '&windowClose=' + WindowEnd + '&deliveryDate=' + formatDateForOrderAPICall(deliveryDate)+ '&profitCenter=' +profitCenter + '&userName='+UserName;
        //string uri = '/8888300-164/salesorders/200564820/HotStatus?hotFlag=true&deliveryDate=2018-07-12&windowOpen=09:00&windowClose=11:00&profitCenter=23';
        system.debug('hoturi-----'+uri);
        return  uri;
    }
    
     private static string buildShipToAddressURI(string fullfilledById, string orderNumber, string UserName, Integer profitCenter, string shipToName,
        string shipToAddress1, string shipToAddress2, string shipToCity, string shipToState, string shipToZip){
        string uri = '/' + fullfilledById +'/salesorders/' + orderNumber +'/SalesOrderShipTo?profitCenter=' + String.valueOf(profitCenter) +'&userName='+UserName;
        uri += '&shipToName=' + EncodingUtil.URLENCODE(shipToName,'UTF-8');
        uri += '&shipToAddress1=' + EncodingUtil.URLENCODE(shipToAddress1,'UTF-8');
        if(shipToAddress2 != null){
            uri += '&shipToAddress2=' + EncodingUtil.URLENCODE(shipToAddress2,'UTF-8');
        }
        uri += '&shipToCity=' + EncodingUtil.URLENCODE(shipToCity,'UTF-8');
        uri += '&shipToState=' + EncodingUtil.URLENCODE(shipToState,'UTF-8'); 
        uri += '&shipToZip=' + EncodingUtil.URLENCODE(shipToZip,'UTF-8');
            string uri1 = uri.replace('+','%20'); 
            system.debug('uri---'+uri);
            system.debug('uri1---'+uri1);
        return  uri1;
    }
    
	@TestVisible
    private static string buildDeliveryDateUpdateURI(string fullfilledById, string orderNumber,Boolean asap, Integer profitCenter, 
                                                    Date deliveryDate, string deliveryType, string reasonCode){
        //use false for confirmFlag, use user federation id or username for userName
        string uri;
        
        if(deliveryDate != null)
        {
            uri = '/' + fullfilledById +'/salesorders/' + orderNumber +'/SalesOrderDeliveryDate?profitCenter=' + profitCenter + '&newShipDate=' + formatDateForOrderAPICall(deliveryDate);
        }
        else if(deliveryDate == null)
        {
            uri = '/' + fullfilledById +'/salesorders/' + orderNumber +'/SalesOrderDeliveryDate?profitCenter=' + profitCenter + '&newShipDate=' + '';
        }
            uri += '&shipViaCode=' + EncodingUtil.URLENCODE(deliveryType, 'UTF-8');     
            uri += '&reasonCode=' + reasonCode.replace(' ','%20'); 
            uri += '&confirmFlag=false';
            uri += '&userName=' + EncodingUtil.URLENCODE(getUserNameForAPiCall(Utilities_SalesAndService.currentUser), 'UTF-8');
        system.debug('deliverydateurl-->' + uri);
        return uri;
    }

 /*   private static string buildDeliveryCommentsUpdateURI(string fullfilledById, string salesOrderDeliveryComments, string orderNumber, 
                                                    Date deliveryDate, string deliveryType){
        //use false for confirmFlag, use user federation id or email for userName
        //https://stageapigw.ashleyfurniture.com/homestores/8888300-164/salesorders/200463110/SalesOrderDeliveryComments?profitCenter=23&username=SFUser&apikey=bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713
        string uri = '/' + fullfilledById +'/salesorders/'+ orderNumber +'/SalesOrderDeliveryComments?deliveryComments=' + EncodingUtil.URLENCODE(salesOrderDeliveryComments, 'UTF-8') + '&deliverDate=' + formatDateForOrderAPICall(deliveryDate);
        uri += '&shipVia=' + EncodingUtil.URLENCODE(deliveryType, 'UTF-8');
        uri += '&ashleyOrderNumber=' + EncodingUtil.URLENCODE(orderNumber, 'UTF-8');
        uri += '&confirmFlag=false';
        uri += '&userName=' + EncodingUtil.URLENCODE(getUserNameForAPiCall(Utilities_SalesAndService.currentUser), 'UTF-8');
		system.debug('delivery comments----'+uri);
        return uri;
    }*/
@TestVisible
    private static string buildOrderCommentsUpdateURI(string fullfilledById, string salesOrderComments, string UserName, string orderNumber, Integer profitCenter){
        //string uri = '/' + fullfilledById +'/salesorders/'+ orderNumber +'/SalesOrderComments?salesOrderComments=' + EncodingUtil.URLENCODE(salesOrderComments, 'UTF-8') ;
        //uri += '&profitCenter=' + profitCenter;
        string uri = '/' + fullfilledById +'/salesorders/'+ orderNumber +'/SalesOrderComments?';
        uri += 'profitCenter=' + profitCenter;
        uri += '&userName='+UserName;
        system.debug('Order_Comments_uri----'+uri);
        return uri;
    }
    
    
    public static string formatDateForOrderAPICall(Date d){
        string formattedString = '';
        if(d.month() <= 9){
            formattedString += '0';
        }
        formattedString += String.valueOf(d.month()) + '-';
        if(d.day() <= 9){
            formattedString += '0';
        }
        formattedString += String.valueOf(d.day()) + '-' + String.valueOf(d.year());
        return formattedString;
    }

    public static string getUserNameForAPiCall(User U){
        if(u.FederationIdentifier != null){
            return u.FederationIdentifier;
        }
        else{
            return u.Email;
        }
    }
}