/* This is a controller class for NewCaseLineFromCase lightning component which has a botton to launch a new Case Line Item 
screen populated with default values from the case and associated order. The Component is added to the Case Lightning Page.*/

public with sharing class NewProductLineFromCaseController {
	@AuraEnabled
	public static InitializationDataResponse getInitializationData(Id caseId) {
		InitializationDataResponse response = new InitializationDataResponse();
		try{
			Case caseInContext = [Select Id, Sales_order__c from Case where Id =: caseId];
			response.caseId = caseInContext.Id;
			if(caseInContext.Sales_order__c != null){
				SalesOrder__x order = SalesOrderDAO.getOrderByExternalId(caseInContext.Sales_order__c);
				if(order != null){
					response.salesOrderNumber = order.phhSalesOrder__c;
                    if(order.fulfillerID__c != null){
                        response.fulfillerID = order.fulfillerID__c;
                    }
                    else{
                        response.fulfillerID = order.phhERPAccounShipTo__c;
                    }
				} 
			}
			response.isSuccess = true;
		}
		catch(Exception ex){
			response.isSuccess = false;
			response.errorMessage = ex.getMessage();
		}
		return response;
		
	}

	public class InitializationDataResponse implements Attributable{
		@AuraEnabled
		public Id caseId {get;set;}
		@AuraEnabled
		public String salesOrderNumber {get;set;}
		@AuraEnabled
		public boolean isSuccess {get;set;}
		@AuraEnabled
		public String errorMessage {get;set;}
		@AuraEnabled
		public String fulfillerID {get;set;}

	}
    
    public static Id caseId;
    public NewProductLineFromCaseController(case caseis , String salesorder , Id contactname , String casetype, String casesubtype){
        
        caseId = create_case(caseis,salesorder,contactname,casetype,casesubtype); 
        
        System.debug('my new Case Id2--->'+caseId);
    }
    
    @TestVisible private static list<SalesOrder__x> mockedcustlist3 = new List<SalesOrder__x>();
    @AuraEnabled
    //Getting external ID for the respective Sales Order
    public static string getOrderLineItems(Id recordId) {
        SalesOrder__x salesOrderObj = new SalesOrder__x();
        //List<SalesOrder__x> salesOrderObjList = new List<SalesOrder__x>(); // Added by Venkat
        if(Test.isRunningTest()){
            salesOrderObj = new SalesOrder__x(id ='x010n000000CuZuAAK',
                                   ExternalId = '17331400:001q000000raDkvAAE',
                                   phhHot__c = true,
                                   phhOrder_Notes__c = 'Test Type',
                                   phhDesiredDate__c = system.today(),
                                   fulfillerID__c = '1-',
                                   phhStoreID__c = '23-12345',
                                   phhDeliveryType__c = 'TW',
                                   phhProfitcenter__c  = 23,
                                   phhSaleType__c = 'POS',
                                   phhSalesOrder__c = '200460320',
                                   phhSOSource__c = 'POS9',
                                   phhCustomerName__c = 'DAVID HESSE',
                                   phhStoreLocation__c = 'TAMPA',
                                   phhReasonCode__c = 'Invalid delivery date',
                                   phhGuestID__c = '001e000001FinfrAAB',
                                   phhDatePromised__c = system.today(),
                                   phhWindowBegin__c = '07:00',
                                   phhWindowEnd__c = '19:00',
                                   phhASAP__c = false,
                                   phhServiceLevel__c = 'THD',
                                   VIPFlag__c = true,
                                   phhBackOrder__c = 'test',
                                   phhBalanceDue__c = 100,
                                   phhContactStatus__c = 'QA',
                                   phhErpNumber__c = '8886000',
                                   Estimated_Arrival__c = '09-09-2019',
                                   phhHighDollarSale__c = false,
                                   IsMarketActive__c = true,
                                   phhDeliveryAttempts__c = 1, 
                                   phhOrderSubType__c = 'Home',
                                   LIBFlag__c = true);
            
        }else{
            salesOrderObj = [SELECT ExternalId, fulfillerID__c, Id, phhProfitcenter__c, phhCustomerID__c FROM SalesOrder__x WHERE ExternalId=:recordId];
        }
        
        
        return salesOrderObj.ExternalId;
    }
    
    @AuraEnabled
    public static String getCaseObj() {
        case caseObj = new case();
        caseObj = [select Type,sub_Type__c from Case];
        
        return caseObj.Type;
    }
    
    @AuraEnabled
    public static String getAddr (Id recordId) {
        SalesOrder__x salesOrderObj = new SalesOrder__x();
        if(Test.isRunningTest()){
            salesOrderObj = new SalesOrder__x(id ='x010n000000CuZuAAK',
                                   ExternalId = '17331400:001q000000raDkvAAE',
                                   phhHot__c = true,
                                   phhOrder_Notes__c = 'Test Type',
                                   phhDesiredDate__c = system.today(),
                                   fulfillerID__c = '1-',
                                   phhStoreID__c = '23-12345',
                                   phhDeliveryType__c = 'TW',
                                   phhProfitcenter__c  = 23,
                                   phhSaleType__c = 'POS',
                                   phhSalesOrder__c = '200460320',
                                   phhSOSource__c = 'POS9',
                                   phhCustomerName__c = 'DAVID HESSE',
                                   phhStoreLocation__c = 'TAMPA',
                                   phhReasonCode__c = 'Invalid delivery date',
                                   phhGuestID__c = '001e000001FinfrAAB',
                                   phhDatePromised__c = system.today(),
                                   phhWindowBegin__c = '07:00',
                                   phhWindowEnd__c = '19:00',
                                   phhASAP__c = false,
                                   phhServiceLevel__c = 'THD',
                                   VIPFlag__c = true,
                                   phhBackOrder__c = 'test',
                                   phhBalanceDue__c = 100,
                                   phhContactStatus__c = 'QA',
                                   phhErpNumber__c = '8886000',
                                   Estimated_Arrival__c = '09-09-2019',
                                   phhHighDollarSale__c = false,
                                   IsMarketActive__c = true,
                                   phhDeliveryAttempts__c = 1, 
                                   phhOrderSubType__c = 'Home',
                                   LIBFlag__c = true);
            
        }else{
            salesOrderObj =   [SELECT ExternalId, fulfillerID__c, Id, phhProfitcenter__c, phhCustomerID__c ,phhGuestID__c FROM SalesOrder__x   WHERE Id=:recordId];
        }
        
        Address__c addr = new Address__c();
        addr = [Select Id,AccountId__c from Address__c Where AccountId__c= :salesOrderObj.phhGuestID__c];
        return  addr.Id;
    }
    
   // @TestVisible private static list<SalesOrder__x> mockedcustlist4 = new List<SalesOrderItem__x>(); 
    @AuraEnabled
    public static SalesOrder__x getSalesOrderInfo(Id recordId){
        Case casee = [SELECT Id,Sales_Order__c FROM Case WHERE Id =:recordId];
        String SOrecordId = casee.Sales_Order__c;
        SalesOrder__x salesOrderObj = new SalesOrder__x();
        
        if(Test.isRunningTest()){
            salesOrderObj = new SalesOrder__x(id ='x010n000000CuZuAAK',
                                   ExternalId = '17331400:001q000000raDkvAAE',
                                   phhHot__c = true,
                                   phhOrder_Notes__c = 'Test Type',
                                   phhDesiredDate__c = system.today(),
                                   fulfillerID__c = '1-',
                                   phhStoreID__c = '23-12345',
                                   phhDeliveryType__c = 'TW',
                                   phhProfitcenter__c  = 23,
                                   phhSaleType__c = 'POS',
                                   phhSalesOrder__c = '200460320',
                                   phhSOSource__c = 'POS9',
                                   phhCustomerName__c = 'DAVID HESSE',
                                   phhStoreLocation__c = 'TAMPA',
                                   phhReasonCode__c = 'Invalid delivery date',
                                   phhGuestID__c = '001e000001FinfrAAB',
                                   phhDatePromised__c = system.today(),
                                   phhWindowBegin__c = '07:00',
                                   phhWindowEnd__c = '19:00',
                                   phhASAP__c = false,
                                   phhServiceLevel__c = 'THD',
                                   VIPFlag__c = true,
                                   phhBackOrder__c = 'test',
                                   phhBalanceDue__c = 100,
                                   phhContactStatus__c = 'QA',
                                   phhErpNumber__c = '8886000',
                                   Estimated_Arrival__c = '09-09-2019',
                                   phhHighDollarSale__c = false,
                                   IsMarketActive__c = true,
                                   phhDeliveryAttempts__c = 1, 
                                   phhOrderSubType__c = 'Home',
                                   LIBFlag__c = true);
            
        }else{
            salesOrderObj = [SELECT ExternalId, fulfillerID__c, Id, phhProfitcenter__c, phhCustomerID__c ,phhSalesOrder__c,phhGuestID__c,phhSaleType__c FROM SalesOrder__x WHERE ExternalId =:SOrecordId];
        }
        
        return salesOrderObj;
    }
    
    @TestVisible private static list<SalesOrderItem__x> mockedcustlist1 = new List<SalesOrderItem__x>();
    @AuraEnabled
    //Get Product line item by sales order external ID
    public static List<SalesOrderItem__x> getOrderLineItemsByOrderExternalId(Id recordId) {
        Case casee = [SELECT Id,Sales_Order__c FROM Case WHERE Id =:recordId];
        SalesOrderItem__x lineItems = new SalesOrderItem__x();
        if(Test.isRunningTest()){
            List<SalesOrderItem__x> lineItem2 = new List<SalesOrderItem__x>(); 
            lineItem2.add(new SalesOrderItem__x(Id = 'x00q00000000DMoAAM',
                                       ExternalId = '21111310:001q000000raI3DAAU',
                                       phdItemSKU__c = 'B502-87',
                                       phdItemDesc__c = 'Full Panel Headboard / Kaslyn / White',
                                       phdItemDesc2__c = 'Full Panel Headboard / Kaslyn / White',
                                       phdQuantity__c = 1,
                                       phdSalesOrder__c = '21111310',
                                       phdIsFPP__c = false,
                                       phdShipAddress1__c = '250 AMAL DR',
                                       phdShipAddress2__c = 'apt 102',
                                       phdShipCity__c = 'ATLANTA',
                                       phdShipState__c = 'GA',
                                       phdShipZip__c = '30315',
                                       phdInStoreQty__c = 0,
                                       phdInWarehouseQty__c = 0,
                                       phdWarrantyDaysLeft__c = 365,
                                       phdDeliveryType__c = 'G02',
                                       phdDeliveryTypeDesc__c = 'GA TRUCK 02',
                                       phdSaleType__c = 'POS',
                                       phdItemSeq__c = 10,
                                       phdOrderType__c = 'ORD(Exch)',
                                       phdPaymentType__c = 'Payment Test',
                                       phdRSA__c = 'HOU',
                                       phdAmount__c = 199.99,
                                       Ship_Customer_Name__c = 'HESSE',
                                       phdWholeSalePrice__c = 99.01,
                                       phdVendorName__c='vendor'));
            return lineItem2;
        }
        else{
            return [SELECT Id, ExternalId, phdSalesOrder__c, phdItemSKU__c, phdItemSeq__c, phdItemDesc__c, phdItemDesc2__c, phdQuantity__c, phdReturnedReason__c, 
                    phdDeliveryDueDate__c, phdIsFPP__c, phdItemStatus__c, phdWarrantyExpiredOn__c, 
                    phdShipAddress1__c, phdShipAddress2__c, phdShipCity__c, phdShipState__c, phdShipZip__c,
                    phdLOC_PO__c, phdInStoreQty__c, phdInWarehouseQty__c, phdPurchaseDate__c, phdSaleType__c, phdWarrantyDaysLeft__c, phdDeliveryType__c
                    FROM SalesOrderItem__x 
                    WHERE phdSalesOrder__r.ExternalId =:casee.Sales_Order__c];
        }
    }

    @AuraEnabled
    public static List<SalesOrderItem__x> getSoLineItemsBySoExternalId(Id recordId) {
        system.debug('******* Case Id ' + recordId);
        //return list
        List<SalesOrderItem__x> returnlineItemList = new List<SalesOrderItem__x>();

    	//map to hold product or case line item assocaiated to case with count
    	Map<string, integer> liMap = new Map<string, integer>();
    	//pli or cli key string
    	string tmpKey;
    	//case pli or cli count
    	integer liCnt;

		//query to get the list of product line item associated to case
    	List<ProductLineItem__c> pliList = [Select Id, Sales_Order_Number__c, Item_SKU__c, Item_Seq_Number__c, Item_Serial_Number__c FROM ProductLineItem__c WHERE Case__c = :recordId];
    	for(ProductLineItem__c pli:pliList){
    		tmpKey = pli.Sales_Order_Number__c + '|' + pli.Item_SKU__c + '|' + pli.Item_Seq_Number__c;
    		liCnt = 0;
    		if(liMap.get(tmpKey) != null){
    			//pli available for that key
    			liCnt = liMap.get(tmpKey);
    			liMap.put(tmpKey, ++liCnt);
    		}
    		else{
    			//no pli exist for that key
    			liMap.put(tmpKey, 1);
    		}
    	}

		//query to get the list of case line item associated to case
		//comment out by sekar to add line item that are added in CLI on 10-01-2019
		/*
    	List<Case_Line_Item__c> cliList = [Select Id, Sales_Order_Number__c, Item_SKU__c, Item_Seq_Number__c, Item_Serial_Number__c FROM Case_Line_Item__c WHERE Case__c = :recordId];
    	for(Case_Line_Item__c cli:cliList){
    		tmpKey = cli.Sales_Order_Number__c + '|' + cli.Item_SKU__c + '|' + cli.Item_Seq_Number__c;
    		liCnt = 0;
    		if(liMap.get(tmpKey) != null){
    			//cli available for that key
    			liCnt = liMap.get(tmpKey);
    			liMap.put(tmpKey, ++liCnt);
    		}
    		else{
    			//no cli exist for that key
    			liMap.put(tmpKey, 1);
    		}
    	}
    	*/

		//get the sales order line items associated to this case
        List<SalesOrderItem__x> lineItemList = getOrderLineItemsByOrderExternalId(recordId);
        if(lineItemList.size() > 0){
        	for(SalesOrderItem__x lineItem : lineItemList){
        		liCnt = 0;
        		tmpKey = lineItem.phdSalesOrder__c + '|' + lineItem.phdItemSKU__c + '|' + lineItem.phdItemSeq__c;
        		//if the line item is already associated to that case then reduce the quantity
        		if((liMap.size() > 0) && (liMap.get(tmpKey) != null)){
        			//get the list of line items associated
        			liCnt = liMap.get(tmpKey);
        		}

        		if(lineItem.phdQuantity__c >= 0){
        			//if the quantity is greater than or equal to 0 then return to display in product line items list
        			if ((((lineItem.phdQuantity__c == 0) && (liCnt == 0)) || (lineItem.phdQuantity__c > liCnt)) && (!(lineItem.phdSaleType__c).equalsIgnoreCase('Canceled'))) {
        				//lineItem.phdQuantity__c = lineItem.phdQuantity__c - liCnt;
        				returnlineItemList.add(lineItem);
        			}
        		}
        	}
        }

        return returnlineItemList;
    }

    @AuraEnabled
    public static string create_case(case caseis,String salesorder,Id contactname,String casetype, String casesubtype){
        system.debug('cae-->'+ caseis); 
        system.debug('add-->'+ caseis.Address__c);
        system.debug('salesorder-->'+ salesorder);
        Boolean flag=false;
        Boolean Addressflag=false;
        Contact contactid = new Contact();
        Address__c myAddress = new Address__c();
        try{
            contactid = [SELECT AccountId,Id FROM Contact WHERE AccountId =:contactname];
            flag = true;
        }
        catch(Exception e){
            
        }
        system.debug('flag-->'+ flag);
        try{
            myAddress = [SELECT id, AccountId__r.Id FROM Address__c WHERE id =:caseis.Address__c];
            Addressflag = true;
            
        }
        catch(Exception e){
            
        } 
        
        system.debug('Addressflag-->'+ Addressflag);
        if(flag==true){
            caseis.ContactId = contactid.Id;
        }
        else{
            caseis.ContactId = '';
        }
        if(Addressflag==true){
            caseis.Address__r.Id = caseis.Address__C;
        }
        else{
            myAddress = [SELECT id, AccountId__r.Id FROM Address__c WHERE AccountId__r.Id =:contactname Limit 1	];
            caseis.Address__r.Id = myAddress.Id;
        }
        
        
        System.debug('contactname-->'+contactname);
        String ownerId = caseis.OwnerId;
        caseis.Sales_Order__c = salesorder;
        caseis.Type = casetype;
        caseis.Sub_Type__c = casesubtype;
        system.debug('caseis insert values-->'+ caseis);
        insert caseis;
        return caseis.id;
    }
    @AuraEnabled
    public static string getRecordId(Id recordId) {
        SalesOrder__x salesOrderObj = new SalesOrder__x();
        if(Test.isRunningTest()){
            salesOrderObj = new SalesOrder__x(id ='x010n000000CuZuAAK',
                                   ExternalId = '17331400:001q000000raDkvAAE',
                                   phhHot__c = true,
                                   phhOrder_Notes__c = 'Test Type',
                                   phhDesiredDate__c = system.today(),
                                   fulfillerID__c = '1-',
                                   phhStoreID__c = '23-12345',
                                   phhDeliveryType__c = 'TW',
                                   phhProfitcenter__c  = 23,
                                   phhSaleType__c = 'POS',
                                   phhSalesOrder__c = '200460320',
                                   phhSOSource__c = 'POS9',
                                   phhCustomerName__c = 'DAVID HESSE',
                                   phhStoreLocation__c = 'TAMPA',
                                   phhReasonCode__c = 'Invalid delivery date',
                                   phhGuestID__c = '001e000001FinfrAAB',
                                   phhDatePromised__c = system.today(),
                                   phhWindowBegin__c = '07:00',
                                   phhWindowEnd__c = '19:00',
                                   phhASAP__c = false,
                                   phhServiceLevel__c = 'THD',
                                   VIPFlag__c = true,
                                   phhBackOrder__c = 'test',
                                   phhBalanceDue__c = 100,
                                   phhContactStatus__c = 'QA',
                                   phhErpNumber__c = '8886000',
                                   Estimated_Arrival__c = '09-09-2019',
                                   phhHighDollarSale__c = false,
                                   IsMarketActive__c = true,
                                   phhDeliveryAttempts__c = 1, 
                                   phhOrderSubType__c = 'Home',
                                   LIBFlag__c = true);
            
        }else{
            salesOrderObj = [SELECT ExternalId, fulfillerID__c, Id, phhProfitcenter__c, phhCustomerID__c, phhERPAccounShipTo__c FROM SalesOrder__x WHERE Id=:recordId];
        }
        
        return salesOrderObj.ExternalId;
    }  
    //Creating new line item and a respective case
    @AuraEnabled
    public static void newProductLineItemRecord(List<String> ProductId, Id caseId){
        //Id caseId = create_case(caseis);
        Case casee = [SELECT Id,Sales_Order__c,Last_Action_By__c FROM Case WHERE Id =:caseId];
        String salesorder = casee.Sales_Order__c;
        System.debug('my new Case Id'+caseId);
        createLineItem CLI = new createLineItem(caseId);
        createLineItem.newProductLineItemRecordwithCaseId2(ProductId,caseId,salesorder);
        
        casee.Last_Action_By__c = 'Product Line item record Created';
        update casee;
    }
}