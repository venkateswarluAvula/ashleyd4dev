global class ProductLineItemFromOrderHelper {
    public boolean isvalidNum;
    public ProductLineItemFromOrderHelper(ApexPages.StandardController controller){
        
    }
    @AuraEnabled
    public static ProductLineItem__c  getRecord(ID recId){
        ProductLineItem__c PLI = [SELECT AckNo__c,Address_Line1_Case__c,Address_Line1__c,Address_Line2_Case__c,Address_Line2__c,Ashley_Direct_Link_ID__c,Case__c,City_Case__c,City__c,Country_Case__c,Country__c,CreatedById,CreatedDate,Customer_Number__c,Defect_Details__c,Defect_Location__c,Delivery_Date__c,fieldcompare__c,Fulfiller_ID__c,Id,Invoice_Date__c,Invoice_Number__c,IsDeleted,isReload__c,Item_Defect__c,Item_Description__c,Item_Number__c,Item_Seq_Number__c,Item_Serial_Number__c,Item_SKU__c,LastActivityDate,LastModifiedById,LastModifiedDate,Legacy_Service_Request_ID__c,Legacy_Service_Request_Item_ID__c,Manufacturing_Defect__c,Name,Part_Order_Delivery_Date__c,Part_Order_Number__c,Part_Order_Num__c,Part_Order_Shipping_Date__c,Part_Order_Ship_To_Info__c,Part_Order_Status__c,Part_Order_Tracking_Number__c,Quantity__c,Record_Source__c,Replacement_Part_Item_Number__c,Sales_Order_Item_PO__c,Sales_Order_Number__c,Sales_Status__c,Serial_Number__c,Ship_To_Number__c,SKU_Description__c,State_Case__c,State__c,SystemModstamp,warranty_date__c,Zip_Case__c,Zip__c FROM ProductLineItem__c WHERE Id=:recId];
       System.debug('PLI--->'+PLI);
        return PLI;
    }
    @AuraEnabled
    public static Case getCaseDetail(ID ProductLineItemId){
        string caseNumber;
        Case PLIcase = new Case();
        ProductLineItem__c PLI = [SELECT Case__c,Id,Ashley_Direct_Link_ID__c FROM ProductLineItem__c WHERE Id =:ProductLineItemId];
        if(PLI.Case__c != null) {
            PLIcase = [SELECT CaseNumber,Id FROM Case WHERE Id =:PLI.Case__c ];
            caseNumber = PLI.Case__c;
        }
        return PLIcase;
    }
    @AuraEnabled
    public static Boolean getSerialNumber(ID ProductLineItemId, String plsrNum){   
        string returnTxt;
        System.debug('My record ProductLineItemId'+ProductLineItemId);
        ProductLineItem__c record = [SELECT Id,Item_SKU__c,Item_Serial_Number__c,case__c 
                                     FROM ProductLineItem__c WHERE Id =:ProductLineItemId LIMIT 1];
        ID mynum = record.Id;
        // String PLSerialNum = record.Item_Serial_Number__c;
        String PLSerialNum;
        if(plsrNum != ''){
            PLSerialNum = plsrNum;
        }
            
        else{
            PLSerialNum = '';
        }
        String PLSKU = record.Item_SKU__c;
        
        //trim 0s
        String ItemNum = PLSKU;
        System.debug(ItemNum);
        String[] ItemNum1 = ItemNum.split('[^0]*'); 
        integer i=0;
        for(i=0; i< ItemNum1.size(); i++){
            if(ItemNum1[i]=='') break;
            System.debug('Num1-->'+ItemNum1[i]);
            System.debug('\n');
        }
        System.debug('i-->'+i);
        String s1 = ItemNum.substring(i);
        s1.trim();
        System.debug('s1-->'+s1);
        PLSKU = s1;
		//stale case 
        case mycase = new case();
        mycase.id = record.Case__c;
        mycase.Last_Action_By__c = 'Product line Item Updated';
        
        System.debug('PLSerialNumfromedit' +PLSerialNum);
        System.debug('PLSKU' +PLSKU);
        Boolean isValid = true;
        // get Access token call
        ServiceReplacementPartsAuthorization replPart= new ServiceReplacementPartsAuthorization();
        String response;
        String accessTkn = replPart.accessTokenData();
        system.debug('accesstokenresponse' + accessTkn);
        //Send the request
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System.label.ReplacementPartSerialNumber+'/serialnumbers/validation?serialNumber='+PLSerialNum+'&itemNumber='+PLSKU);
        req.setMethod('GET');
        req.setHeader('Content-Type' ,'application/json;charset=utf-8');
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('apikey', System.label.TechSchedulingApiKey);
        string errMsg;
        Http http = new Http();
        HttpResponse res = http.send(req);
        // Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        System.debug('***Serial num Response Status: '+ res.getStatus());
        System.debug('***Serial num Response Body: '+ res.getBody());
        System.debug('***Serial num Response Status Code: '+ res.getStatusCode());
        Integer statusCode = res.getStatusCode();
        //Check the response
        System.debug('My Serial num entered--' +record.Item_Serial_Number__c);
        system.debug('body ' + res.getBody()); 
        update mycase;
        if(statusCode == 200){
            if(res.getBody() =='false' && PLSerialNum!=null){
                System.debug('serial number not valid'+record);
                System.debug('mynum--> '+mynum);
                isValid = ProductLineItemFromOrderValidation.Validation('PLIH',mynum);
                System.debug('isValid--> '+isValid);
                return false;
            }
            else{
                System.debug('No Serial Number'+record);
                System.debug('mynum--> '+mynum);
                isValid = ProductLineItemFromOrderValidation.UpdateValidation('PLIH',mynum);
                System.debug('isValid--> '+isValid);
                System.debug('true'+record);
                return true;
            }
        }
        return true;
    }
    
    @AuraEnabled
    public static boolean getIsReload(id recId){
        System.debug('recId-->'+recId);
        ProductLineItem__c   po = [Select id, isReload__c from ProductLineItem__c  where id = :recId];
        Boolean isRld = po.isReload__c;
        if(isRld){
            po.isReload__c = false;
            update po;
        }
        System.debug('po.isReload__c-->'+po.isReload__c);
        return isRld;
    }
    
    @AuraEnabled
    public static boolean check(){ 
        return true;
    }
    
    @AuraEnabled
    public static boolean getTrackingNumber(ID mynum, String FulfillerId, String PoNum){
        System.debug('My record ID' +mynum);
        ProductLineItem__c record = [SELECT Id,Part_Order_Number__c,case__c,Fulfiller_ID__c,Part_Order_Num__c,Ashley_Direct_Link_ID__c
                                     FROM ProductLineItem__c Where ID =:mynum];
        String strTest = FulfillerId;
        System.debug(strTest);
        List<String> arrTest = strTest.split('\\-');
        String customerNumber = arrTest[0];
        String shipTo = arrTest[1];
        String PONumber = PoNum;
        System.debug(customerNumber);
        System.debug(shipTo);
        case Updatetocase = new Case();
        updatetocase.Id = record.Case__c;
        updatetocase.Last_Action_By__c = 'Part Order Status Updated';
        if((strTest != null || strTest != '') && (PONumber != null || PONumber != '' )){
            // get Access token call
            ServiceReplacementPartsAuthorization replParts= new ServiceReplacementPartsAuthorization();
            
            //  String response;
            String accessTkn = replParts.accessTokenData();
            system.debug('accesstokenresponse' + accessTkn);
            //Send API the request
            HttpRequest req = new HttpRequest();
            req.setEndpoint(System.label.ReplacementPartSerialNumber+'/orders/status?ponumber='+PONumber+'&customerNumber='+customerNumber+'&shipTo='+shipTo+'&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t');
            //https://stageapigw.ashleyfurniture.com/replacement-parts/v1/orders/status?ponumber=2622632&customerNumber=700&shipTo=01&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t
            req.setMethod('GET');
            req.setHeader('Content-Type' ,'application/json;charset=utf-8');
            req.setHeader('Authorization', 'Bearer '+accessTkn);
            //req.setHeader('apikey', System.label.TechSchedulingApiKey);
            system.debug('req--->' + req);
            string errMsg;
            Http http = new Http();
            HttpResponse res = http.send(req);
            // Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            System.debug('***Tracking Number Response Status: '+ res.getStatus());
            System.debug('***Tracking Number Response Body: '+ res.getBody());
            System.debug('***Tracking Number Response Status Code: '+ res.getStatusCode());
            Integer statusCode = res.getStatusCode();
            //Check the response
            System.debug('My PO NUM entered--' +PoNum);
            System.debug('My FulfillerId entered--' +FulfillerId);
            system.debug('body ' + res.getBody()); 
            //POJSON.ExtDetail data = (POJSON.ExtDetail)JSON.deserialize(res.getBody(), POJSON.ExtDetail.class); 
            Type wrapperType = Type.forName('POJSON'); 
            POJSON data = (POJSON)JSON.deserialize(res.getBody(),wrapperType);
            //System.debug('SO--->'+data.Detail.Street);
            if(statusCode == 200){
                System.debug('Success');
                boolean isRes = ProductLineItemFromOrderValidation.updateTrackingNumber(mynum,data.ExtDetail.UPSTracking);
                if(isRes == true){
                    update updatetocase;
                    return true;
                }
                else{
                	return false;
                }
            }
            else{
                System.debug('FAILED');
                return false;
            }
        }
        return false;
    }
    
}