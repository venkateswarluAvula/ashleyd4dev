global class DeleteOldErrorLogBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //get all the records that are created 30 days back from error log object
        return Database.getQueryLocator([SELECT Id FROM ErrorLog__c WHERE CreatedDate != LAST_N_DAYS:30]); 
    }
    global void execute(Database.BatchableContext bc, List<ErrorLog__c> elList) {
        delete elList;
    }
    global void finish(Database.BatchableContext bc) {}
}