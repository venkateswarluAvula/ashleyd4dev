public class ADIPutWrapper {
public value[] value;
    public class value {
        public string SFContactID;
        public String SalesOrderNumber; //200442870
        public Integer StoreID; //133
        public String TransportationOrderID;    //01330000861945
        public boolean IsConfirmed;
        public DateTime BegunTime;    
        public DateTime CompletedTime;    
        public String UserName; //SBalasubraman
        public Integer RoutingPass; //1
        public Integer TimeChanged; //0 SOLineItems SOLItems
        public Integer ProfitCenter;
        public Date DeliverDate;
        public String CustomerWindowOpen; 
        public String CustomerWindowClose; 
        public String TruckID;  
        public String ConfirmationDateTime;
        public String AccountShipTo;
}
}