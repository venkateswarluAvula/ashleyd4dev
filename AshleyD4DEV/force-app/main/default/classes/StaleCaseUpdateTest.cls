@isTest
public class StaleCaseUpdateTest {
    @isTest
    static void testMethod1(){
        Account acc = new Account(Name='test');
        insert acc;
        List<Case> Caselist = new List<Case>();
        Case mycase = new Case();
        mycase.AccountId = acc.id;
        mycase.Status = 'Open';
        mycase.Type= 'Delivery order Inquery';
        mycase.Sub_Type__c = 'test ';
        mycase.CreatedDate = date.today()-65;
        mycase.LastModifiedDate = Date.today()-64;
        mycase.Tech_Scheduled_Date__c = date.today()-70;
        mycase.Technician_Schedule_Date__c = date.today()-80;
        
        insert mycase;
        
        
        Test.startTest();
        Database.executeBatch(new StaleCaseUpdate());
        Test.stopTest();
    } 
    
}