global class DeleteOldErrorLogScheduler implements Schedulable {
   global void execute(SchedulableContext sc) {
      database.executebatch(new DeleteOldErrorLogBatch());
   }
}