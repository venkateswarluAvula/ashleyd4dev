public class D365_SalesOrderXML{
    
    @AuraEnabled
    public static string SOrecordXML(string recId){
               
        Dom.Document doc = new Dom.Document();
        
        Dom.Xmlnode soNode = doc.createRootElement('D365_SalesOrder__c', null, null);
        
        list<D365_SalesOrder__c> accountList = [ 
            select  id, D365_Notification_Status__c, D365_Notification_Type__c, D365_Date_Time_Send__c from  D365_SalesOrder__c where id =:recId
        ];
        
        for (D365_SalesOrder__c eachso : accountList) {
                //Dom.Xmlnode accountNode = rootNode.addChildElement('Account', null, null);
                //accountNode.setAttribute('id', eachAccount.Id);
                //accountNode.setAttribute('name', eachAccount.Name);
                soNode.addChildElement('Id', null, null).addTextNode(eachso.Id);
            soNode.addChildElement('Notification Status', null, null).addTextNode(eachso.D365_Notification_Status__c);
            soNode.addChildElement('Notification Type', null, null).addTextNode(eachso.D365_Notification_Type__c);
            soNode.addChildElement('Date Time Send', null, null).addTextNode(String.ValueOf(eachso.D365_Date_Time_Send__c));
        }
        
        system.debug(doc.toXmlString()); 
        //system.debug(JSON.deserialize(doc));
        system.debug('======='+doc.toXmlString());
        return doc.toXmlString();
    }
}