global class StaleCaseUpdate implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([Select Id,status,type,Resolution_Notes__c,LastModifiedDate,Type_of_Resolution__c,Technician_Schedule_Date__c,Tech_Scheduled_Date__c From Case where LastModifiedDate < LAST_N_DAYS:30 AND (Sub_Type__c != 'Home Damage' AND Sub_Type__c != 'FPP') AND ownerId != '00G6A000000U4OdUAK' AND (Status = 'Open' OR Status = 'New' OR Status = 'Working' OR Status = 'Awaiting Customer Response' OR Status = 'New Customer Reply' OR Status = 'Sent to Store' OR Status = 'New Store Reply' OR Status = 'Ready for Review') AND AccountId != null]);  
    }
    global void execute(Database.BatchableContext bc, List<Case> Caselist){
        try{
            
            List<Case> listtoupdate = new List<Case>();
            for(Case mycase:Caselist){
                boolean techdate;
                system.debug('Technician_Schedule_Date__c---'+mycase.Technician_Schedule_Date__c);
                if(mycase.Technician_Schedule_Date__c > Date.today() && mycase.Technician_Schedule_Date__c != null ){
                    techdate = false;
                    
                }else if(mycase.Tech_Scheduled_Date__c > Date.today() && mycase.Tech_Scheduled_Date__c != null){
                    techdate = false;
                } else {
                    techdate = true;
                }
                if(techdate){
                    case updateCase = new case();
                    updateCase.id = mycase.id;
                    updateCase.Type_of_Resolution__c = 'Stale - 30 Day Limit Reached';
                    updateCase.Resolution_Notes__c = 'This case is Stale due to inactivity for 30 days';
                    updateCase.status = 'Closed in Salesforce';
                    listtoupdate.add(updateCase);
                }
            }
            if(listtoupdate.size() > 0){
                Database.update(listtoupdate, false);
            }
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR,'Error :'+e.getMessage() + 'at line Number' + e.getLineNumber());   
        }
    }
    global void finish(Database.BatchableContext bc)
    {
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id =
                          :BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Sharing Recalculation ' + a.Status);
        mail.setPlainTextBody
            ('The batch Apex job processed ' + a.TotalJobItems +
             ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}