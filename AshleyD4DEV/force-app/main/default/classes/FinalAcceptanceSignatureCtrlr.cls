public class FinalAcceptanceSignatureCtrlr {
    
    @auraEnabled public Account guestRecord;
    @auraEnabled public Opportunity optyRecord;
    @auraEnabled public String signId;
    @auraEnabled public DateTime checkedTime;
    //REQ-455 - Text Opt In
    @auraEnabled public String entityFullName;
    
    public static final String SIGNATURE_TYPE = 'Terms and Conditions';
    public static final String TEXT_OPT_IN_TYPE = 'Opt into text messages';
    public static final String COA_SIGNATURE_TYPE = 'Customer Order Acceptance';
    
    @AuraEnabled 
    public static Electronic_Signature__c createSignatureObj(String personAccId) {
        try {
            String Tsandcs;
            String finalTsandCs;
            TermsAndCondition__mdt [] tsAndCsMdt = [SELECT content__c FROM TermsAndCondition__mdt];
            for(TermsAndCondition__mdt ts:tsAndCsMdt){
                Tsandcs=ts.content__c;
            }
            
            StoreInfoWrapper storeWrap = Storeinfo.getStoreInfo();
            string legalentity = storeWrap.legalEntityName;
            List<Object> parameters = new List<Object>{legalentity};
            
            finalTsandCs = String.format(Tsandcs,parameters);
            Opportunity[] oppt = [SELECT Id FROM Opportunity WHERE AccountId=:personAccId And StageName != 'Closed Won' And StageName != 'Closed Lost' and StageName != 'Sale Suspended' AND createdDate=LAST_N_DAYS:30];
            Electronic_Signature__c  newSig = new Electronic_Signature__c(
                AccountId__c = personAccId, 
                Logged_In_User__c  = UserInfo.getUserId(), 
                ObjectID__c = oppt[0].id, 
                Signature_Date_Time__c = Datetime.now(),
                Signature_Type__c = SIGNATURE_TYPE,
                LegalVerbiage__c= finalTsandCs
            );
            insert newSig;            
            return newSig;
        } catch (Exception e) {
            throw new AuraHandledException('Error creating signature. ' + 
                                           'Please contact administrator.'+ e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Boolean checkForSignature(String tcSigId) {
        try {
            system.debug('tcSigId----'+tcSigId);
            // if sig object was created, this component load completed
            Electronic_Signature__c sig = [SELECT Id, ObjectID__c FROM Electronic_Signature__c WHERE Id=:tcSigId];
            if (sig == null) {
                return false;
            }
            
            // if signature has been provided, attachment will be found
            ContentDocumentLink[] attachments = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId=:tcSigId];
            if (attachments.size() == 0) {
                return false;
              }
           
        //if (!attachments.isEmpty()) {
                //return false;
                          // Set ToC Accpetance on the Opportunity to true
            Opportunity oppt = [SELECT Id, Sales_Order_ToC_Acceptance__c FROM Opportunity WHERE Id=:sig.ObjectID__c];
            oppt.Sales_Order_ToC_Acceptance__c = true;
            update oppt;
            
            return true;
          
       // } 
        //return false;    
        } catch (Exception e) {
            throw new AuraHandledException('Error finding signature. ' + 
                                           'Please contact administrator.'+ e.getMessage());
        }
    }
    
    @auraEnabled
    public static FinalAcceptanceSignatureCtrlr getOpty(String personAccountId, String opportunityId) {
        try {
            FinalAcceptanceSignatureCtrlr ctrl=new FinalAcceptanceSignatureCtrlr();
            Account personAcc = [SELECT Id, Name, Phone,PersonEmail,createdDate,Email_Opt_In__pc FROM Account 
                                                                                where Id = :personAccountId];
            ctrl.guestRecord = personAcc;
            Opportunity oppty = [Select Id, Survey_Opt_In__c, Text_Message_Opt_In__c, Declined_Survey_and_Text_Opt_In__c,
                                                                    Shipping_Phone__c 
                                                                    from Opportunity where Id = :opportunityId];
            
            if(personAcc.Email_Opt_In__pc)
            {
                oppty.Survey_Opt_In__c = true;
            }
            
            ctrl.optyRecord = oppty;
            List<Electronic_Signature__c> eSign =[SELECT Id,Signature_Type__c FROM Electronic_Signature__c WHERE AccountId__c = :personAccountId 
                                                                                and Signature_Type__c = :TEXT_OPT_IN_TYPE
                                                                                and ObjectID__c = :opportunityId
                                                                                order by CreatedDate desc Limit 1];
            if(eSign != null && !eSign.isEmpty()) {
                ctrl.signId = eSign[0].Id;
            }
            //REQ-455 - Text Opt In
            ctrl.entityFullName = TextOptInConsent.textOptIn();
            return ctrl;
        }
        catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @auraEnabled
    public static FinalAcceptanceSignatureCtrlr getOrCreateESignature(String personAccountId, String opportunityId, String eSignId) {
        Savepoint sp = Database.setSavepoint();
        try {
            FinalAcceptanceSignatureCtrlr ctrl=new FinalAcceptanceSignatureCtrlr();
            
            if(eSignId == null || eSignId == '') {
                Electronic_Signature__c eSignObj = new Electronic_Signature__c(AccountId__c = personAccountId, Logged_In_User__c = userInfo.getUserId(),
                                                                                Signature_Date_Time__c = System.Now(), Signature_Type__c = TEXT_OPT_IN_TYPE,
                                                                                ObjectID__c = opportunityId);
                insert eSignObj;
                ctrl.signId=eSignObj.Id;
            }
            else {
                ctrl.signId = eSignId;
            }
            ctrl.checkedTime = system.now();
            return ctrl;
        }
        catch(Exception e) {
            Database.rollback(sp);
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @auraEnabled
    public static String declineOptions(String personAccountId, String opportunityId) {
        Savepoint sp = Database.setSavepoint();
        try {
            update new Account(Id = personAccountId, Survey_Opt_In__pc = false, Text_Message_Opt_In__pc = false, Declined_Survey_and_Text_Opt_In__pc = true);
            update new Opportunity(Id = opportunityId, Survey_Opt_In__c = false, Text_Message_Opt_In__c = false, Declined_Survey_and_Text_Opt_In__c = true);
            List<Electronic_Signature__c> eSign = [Select Id from Electronic_Signature__c where AccountId__c = :personAccountId
                                                                                            and Signature_Type__c = :TEXT_OPT_IN_TYPE
                                                                                            and ObjectID__c = :opportunityId];
            if(eSign != null && !eSign.isEmpty()) {
                delete eSign;
            }
            return 'Decline Success';
        }
        catch(Exception e) {
            Database.rollback(sp);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @auraEnabled
    public static String acceptOptions(String personAccountId, String opportunityId, String eSignId, Boolean promotionalEmails, Boolean textOptIn,
                                        DateTime timeWhenCheckedTextOptIn,String legalContent) {
        Savepoint sp = Database.setSavepoint();
        Boolean customerSigned = false;
        if(!promotionalEmails && !textOptIn) {
            throw new AuraHandledException('Please check at least one of the options');
        }
        if(textOptIn && timeWhenCheckedTextOptIn != null) {
            List<ContentDocumentLink> attachments = [SELECT Id, ContentDocument.ContentModifiedDate FROM ContentDocumentLink WHERE LinkedEntityId = :eSignId order by ContentDocument.ContentModifiedDate Limit 1];
            if(attachments != null && !attachments.isEmpty()) {
                if(attachments[0].ContentDocument.ContentModifiedDate > timeWhenCheckedTextOptIn) {
                    customerSigned = true;
                }
                else {
                    throw new AuraHandledException('Please sign the signature panel before clicking Accept');
                }
            }
            else {
                throw new AuraHandledException('Please sign the signature panel before clicking Accept');
            }
        }
        else if(!textOptIn && eSignId != null) {
            delete new Electronic_Signature__c(Id = eSignId);
        }
        //DEF-0863
        Electronic_Signature__c eSign;
        if(String.isNotBlank(legalContent)){
            eSign = [Select Id,LegalVerbiage__c from Electronic_Signature__c 
                                               where Id = :eSignId
                                               and Signature_Type__c = :TEXT_OPT_IN_TYPE];
        }
        try {
            if(eSign != null) 
            {
                    eSign.LegalVerbiage__c= legalContent;
                    update eSign;
             }
      //end of DEF-0863
            upsert new Account(Id = personAccountId, Survey_Opt_In__pc = promotionalEmails, Text_Message_Opt_In__pc = textOptIn, Declined_Survey_and_Text_Opt_In__pc = (!promotionalEmails && !textOptIn));
            upsert new Opportunity(Id = opportunityId, Survey_Opt_In__c = promotionalEmails, Text_Message_Opt_In__c = textOptIn, Declined_Survey_and_Text_Opt_In__c = (!promotionalEmails && !textOptIn));
            return 'Success';
        }
        catch(Exception e) {
            Database.rollback(sp);
            throw new AuraHandledException(e.getMessage());
        }
      
    }
    
    // Method to create a signature object instance of type 'Customer Order Acceptance'
    @AuraEnabled
    public static Id coaSignatureObj(String personAccId) {
        try {
            Opportunity[] oppt = [SELECT Id FROM Opportunity WHERE AccountId=:personAccId And StageName != 'Closed Won' And StageName != 'Closed Lost' And StageName != 'Sale Suspended' AND createdDate=LAST_N_DAYS:30];
            StoreInfoWrapper storeWrap = Storeinfo.getStoreInfo();
            String customerOrderAccptTerms = System.Label.Customer_Order_Acceptance_Terms;
            customerOrderAccptTerms = String.format(customerOrderAccptTerms, new String[]{storeWrap.legalEntityName});
            if(!oppt.isEmpty()) {
                Electronic_Signature__c  newSig = new Electronic_Signature__c(
                    AccountId__c = personAccId, 
                    Logged_In_User__c  = UserInfo.getUserId(), 
                    ObjectID__c = oppt[0].id, 
                    Signature_Date_Time__c = Datetime.now(),
                    Signature_Type__c = COA_SIGNATURE_TYPE,
                    LegalVerbiage__c= customerOrderAccptTerms
                );
                insert newSig;            
                return newSig.Id;
            }
            return null;
        } catch (Exception e) {
            throw new AuraHandledException('Error creating signature. ' + 
                                           'Please contact administrator.'+ e.getMessage());
        }
    }
    
    // Method to check if customer has signed and to store customer order acceptance in opportunity object
    @AuraEnabled
    public static Boolean coaCheckForSignature(String coaSigId) {
        try {
            // if sig object was created, this component load completed
            Electronic_Signature__c sig = [SELECT Id, ObjectID__c FROM Electronic_Signature__c WHERE Id=:coaSigId];
            if (sig == null) {
                return false;
            }
            // if signature has been provided, attachment will be found
            ContentDocumentLink[] attachments = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId=:coaSigId];
            if (attachments.size() == 0) {
                return false;
            }
            // Set Customer Order Accpetance on the Opportunity to true
            List<Opportunity> oppt = [SELECT Id, Customer_Order_Acceptance__c FROM Opportunity WHERE Id = :sig.ObjectID__c];
            if(!oppt.isEmpty()) {
                oppt[0].Customer_Order_Acceptance__c = true;
                update oppt[0];
            }
            return true;
        } catch (Exception e) {
            throw new AuraHandledException('Error finding signature. ' + 
                                           'Please contact administrator.'+ e.getMessage());
        }
    }
}