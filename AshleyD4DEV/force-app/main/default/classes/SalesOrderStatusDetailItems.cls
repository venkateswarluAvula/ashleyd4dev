public class SalesOrderStatusDetailItems {

   @AuraEnabled
    public String SaleOrderNo {get;set;}
    @AuraEnabled
    public String TimeStamp {get;set;}
    @AuraEnabled
    public String LoginUser {get;set;}
    @AuraEnabled
    public String ManagerApproved {get;set;}
    @AuraEnabled
    public String ChangeType{get;set;}
    @AuraEnabled
    public String OriginalValue {get;set;}
    @AuraEnabled
    public String UpdatedValue {get;set;}
    @AuraEnabled
    public String ReasonCode {get;set;}
    @AuraEnabled
    public String StoreID {get;set;}
    
}