@isTest
private class DeliveryWindowLookupController_Test {
    
    @testSetup
    public static void prepareAPICustomSetting(){
        TestDataFactory.prepareAPICustomSetting();
                
    }
	
	@isTest 
	static void testWithOutAPIMock() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];
        

        SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

		DeliveryWindowLookupController.DeliveryCalendarResponse response = DeliveryWindowLookupController.getDeliveryCalendarDays('2017-01-01', 'ord-001', 'ordline-001', '00154', '118');
		System.assert(response.isSuccess == false);
	}

	@isTest 
	static void testWithAPIMockAndAvailableDates() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

		SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

		String sampleResponse = '{"deliveryWindows": [';
		for(Integer i=0; i < 5; i++){
			sampleResponse += '{';
			sampleResponse += '"start": "' + API_ATCClient.formatDateToISO8601(Date.today().addDays(i)) + '",';
			sampleResponse += '"end": "' + API_ATCClient.formatDateToISO8601(Date.today().addDays(i)) + '",';
			sampleResponse += '"sortIndex": ' + String.valueOf(i);
			sampleResponse += '}';
			if(i != 4){
				sampleResponse += ',';
			}
		}        
		sampleResponse += ']}';
        
		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', sampleResponse,new Map<String, String>()));
		DeliveryWindowLookupController.DeliveryCalendarResponse response = DeliveryWindowLookupController.getDeliveryCalendarDays('2017-01-01', 'ord-001', 'ordline-001', '00154', '118');
		Test.stopTest();
		
		System.assert(response.isSuccess == true);
		System.assert(response.calendar != null, 'A calendar should have been returned');
	}	
    
    @isTest
    static void shoppingcart(){
        
        Shopping_cart_line_item__c scli= new Shopping_cart_line_item__c(Id='',Product_SKU__c='vfsvfvf',Quantity__c=1,DeliveryDate__c=date.today(),Delivery_Mode__c='HD',DeliveryType__c='THD',Opportunity__c='');
        insert scli;
    }
/*
	@isTest 
	static void testWithAPIMockAndNoAvailableDates() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

		SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

		String sampleResponse = '{"deliveryWindows": [';  
		sampleResponse += ']}';

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', sampleResponse,new Map<String, String>()));
		DeliveryWindowLookupController.DeliveryCalendarResponse response = DeliveryWindowLookupController.getDeliveryCalendarDays('2017-01-01', 'ord-001', 'ordline-001', '00154', '118');
		Test.stopTest();
		
		System.assert(response.isSuccess == false);
		System.assert(response.calendar == null);
		System.assert(response.message == 'No Delivery Dates available');
	}
	
    @isTest 
	static void testWithAPIMockAndAvailableDeliveryDates() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

		SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

		String sampleResponse = '{"deliveryWindows": [';
		for(Integer i=0; i < 5; i++){
			sampleResponse += '{';
			sampleResponse += '"start": "' + API_ATCClient.formatDateToISO8601(Date.today().addDays(i)) + '",';
			sampleResponse += '"end": "' + API_ATCClient.formatDateToISO8601(Date.today().addDays(i)) + '",';
			sampleResponse += '"sortIndex": ' + String.valueOf(i);
			sampleResponse += '}';
			if(i != 4){
				sampleResponse += ',';
			}
		}        
		sampleResponse += ']}';
        
        string thrResponse = 'DateWrapper:[availableForDelivery=false, d=2018-12-29 00:00:00, dateIsInCurrentMonth=true, dateStringForDisplay=12/29, dayofweek=Saturday]';

		Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', sampleResponse,new Map<String, String>()));
        Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', thrResponse,new Map<String, String>()));
        Account acc =  prepareCustomerInfo();
        Opportunity opp =  TestDataFactory.prepareShoppingCart(acc);
        List<Shopping_cart_line_item__c> lineItems = TestDataFactory.prepareShoppingCartLineItems(opp, true);
        Address__c addr = new Address__c();
        addr.AccountId__c = acc.Id;
        addr.Address_Line_1__c = 'Sample address 1';
        addr.Address_Line_2__c = 'Sample address 2';
        addr.City__c = 'Sample city';
        addr.StateList__c = 'Sample state';
        addr.Country__c = 'Sample country';
        addr.Zip_Code__c = '123456';
        insert addr;
		DeliveryWindowLookupController.DeliveryCalendarResponse response = DeliveryWindowLookupController.getDeliveryCalendarDays('2017-01-01', null, lineItems[0].Id, '00154', '118',addr.Id);
		Test.stopTest();
		System.assert(response != null);
	}	

*/
    
    @IsTest
     public static Account prepareCustomerInfo(){
        
        RecordType customerAccRT = [Select Id from recordType where SObjectType='Account' and Name='Customer'];
        Account acc =  new Account();
        acc.FirstName = 'testAccF';
        acc.LastName = 'testAccL';
        acc.RecordTypeId = customerAccRT.Id;
        acc.Phone ='(0)888-998-766';
        insert acc;
        
        return acc;
    }
   
    @IsTest
    static void monWrap(){
        DeliveryWindowLookupController.MonthWrapper dmw = new DeliveryWindowLookupController.MonthWrapper();
        dmw.name = 'November';
        dmw.index = 1;
        dmw.isFirst = true;
        dmw.isLast = false;
        dmw.isCurrentSeletedDateMonth = true;
    }
    
    @IsTest
    static void dateWrap(){
        DeliveryWindowLookupController.DateWrapper ddw = new DeliveryWindowLookupController.DateWrapper(system.today());
        ddw.d = system.today();
        ddw.availableForDelivery = true;
        ddw.dateIsInCurrentMonth = true;
        ddw.dateStringForDisplay = '';
        ddw.dayofweek = 'Mon';
    }
   
    @IsTest
    static void TmonWrap(){
        DeliveryWindowLookupController.TMonthWrapper tmw = new DeliveryWindowLookupController.TMonthWrapper();
        tmw.Tname = 'November';
        tmw.Tindex = 1;
        tmw.TisFirst = true;
        tmw.TisCurrentSeletedDateMonth = true;
        tmw.TisLast = true;
    }
    
    @IsTest
    static void DThrWrap(){
        DeliveryWindowLookupController.DateWrappethresholdr tdw = new DeliveryWindowLookupController.DateWrappethresholdr(system.today());
        tdw.da = system.today();
        tdw.TavailableForDelivery = true;
        tdw.TNopastdays = true;
        tdw.TdateIsInCurrentMonth = true;
        tdw.TdateStringForDisplay = '26/11';
        tdw.Tdayofweek = 'Monday';
    }
     @isTest 
	static void testTresholdAvlbDays() {
        String sampleResponse = '[{';
        sampleResponse += '"DefaultValue": "Monday",';
        sampleResponse += '"Name": "THRES_HOLD_AVAILABLE_DAYS",';
        sampleResponse += '"ProfitCenter": 25,';
        sampleResponse += '"SubProfitCenter": "",';
        sampleResponse += '"PropertyGroup": "1",';
        sampleResponse += '"Value": "Monday"';
        sampleResponse += '}]';
     
		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', sampleResponse,new Map<String, String>()));	 
        List<TresholdAvailableDaysWrapper> tresholddayswrap = DeliveryWindowLookupController.TresholdAvlbDays();
        System.assertNotEquals(null,tresholddayswrap,
            'The callout returned a null response.');
        Test.stopTest();
}
}