/****** v1 | Description: Methods for viewing customer associated records | 12/5/2017 | L OMeara */
/****** v2 | Description: Updated for person accounts | 2/1/2018 | L OMeara */
/****** v3 | Description: Updated for Preferred filed,Duplicate Address and checking address related to any case| 01/04/2019 | Ajay Sagar */
public with sharing class ViewCustomerController {
    @AuraEnabled
    public static List<Address__c> getCustomerAddresses(Id customerId){      
        return [SELECT Id,Address_Line_1__c,Address_Line_2__c,City__c,StateList__c,Zip_Code__c, Preferred__c,Address_Type__c,Address_Validation_Status__c, Address_Validation_Timestamp__c
                         FROM Address__c WHERE AccountId__c=:customerId ORDER BY Preferred__c DESC, LastModifiedDate DESC LIMIT 5];
    }
    
    @AuraEnabled
    public static String saveAddress(Address__c toSave) {
        string validMessage;
    	try {
            Map<string, id> accAdrMap = AddressHelper.getCustomerAddressWithType(toSave.AccountId__c, toSave.Id);
            string dupStr = AddressHelper.getAddressStr(toSave.Address_Line_1__c, toSave.Address_Line_2__c, toSave.City__c, toSave.StateList__c, toSave.Zip_Code__c, toSave.Address_Type__c);
            if ((dupStr != null) && (accAdrMap.get(dupStr) != null)) {
                //duplicate address
                validMessage = 'duplicate address';
            }else{
                List<Address__c> addrs = [Select Id,Address_Type__c from Address__c Where AccountId__c =:toSave.AccountId__c and Address_Type__c = 'Ship To' ];
                if(!toSave.Preferred__c){
                if(addrs.isEmpty() && toSave.Address_Type__c == 'Ship To' ){
                 toSave.Preferred__c = true;   
                }
                }
                List<Case> Caselist = new List<Case>();
                if(toSave.Id != null) {
                     Caselist = [Select Id from Case where Address__c =:toSave.Id];  
                }
                if(Caselist.size() > 0 ){
                 toSave.id = null;
                 insert toSave;
                }else{
                 upsert toSave;   
                }
                if(toSave.Preferred__c){
                    AddressHelper.setCustomerPreferredAddress(toSave.AccountId__c, toSave.Id);    
                }  
                validMessage = 'Success'; 
            }
        }catch(Exception e){
            system.debug(e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
        return validMessage;
    }

    @AuraEnabled
    public static String SearchAddress(string searchTerm, string country, Integer take) {
        return EDQService.SearchAddress(searchTerm, country, take);
    }

    @AuraEnabled
    public static String FormatAddress(string formatUrl) {
        return EDQService.FormatAddress(formatUrl);
    }

    @AuraEnabled 
    public static void updateOwner(String Id) {
        List<Account> acc=[Select Id,OwnerId from Account Where id=:Id];
        if(!acc.isEmpty()){
            acc[0].OwnerId=userInfo.getUserId();
            update acc[0];
        }
        List<Opportunity> cartLst= new List<Opportunity>();
        String oppExpireLimit=System.Label.Cart_limit;
        String query='Select Id,Name, AccountId, Account.Name,CreatedDate,StageName,Cart_Grand_Total__c '+
            'From Opportunity '+
            'Where  Account.RecordType.DeveloperName=\'Customer\' and  AccountId =:Id and StageName != \'Closed Won\' and StageName != \'Sale Suspended\' and StageName != \'Closed Lost\' and CreatedDate = LAST_N_DAYS:'+oppExpireLimit;


        cartLst=Database.query(query);
         if(!cartLst.isEmpty()){
            cartLst[0].OwnerId=userInfo.getUserId(); 
            update cartLst[0];
        }
    }
   
  
    @AuraEnabled
    public static Map<String, Object> updateLineItemWithSKU(Id recordId, Shopping_cart_line_item__c lineItem,string MultiLineType){
        List<Shopping_cart_line_item__c> lstLineItems = new  List<Shopping_cart_line_item__c>();
        //system.debug('MultiLineType'+MultiLineType);
        system.debug('recordId------------'+recordId);
        //REQ-438,  add Product_Title__c for Discount Modal product name column show for items list
        for (Opportunity master : [Select (Select Id, WarrantySku__c,  Product_Title__c, Product_SKU__c, Discount__c,Discount_Price__c,DiscountType__c,
                                           Flat_Discounted_Amount__c,Flat_Discounted_Amount_On_Total__c,Last_Price__c,List_Price__c, Average_Cost__c, Quantity__c,Extended_Price__c,ItemType__c,
                                           Opportunity__c,Delivery_Mode__c,Discount_Reason_Code__c,Discount_Status__c, Estimated_Tax__c
                                           from  Shopping_cart_line_items__r ) 
                                   From Opportunity where 
                                   AccountId=:recordId 
                                   and StageName != 'Closed Won' 
                                   and StageName != 'Closed Lost'
                                   and StageName != 'Sale Suspended'
                                   and createdDate = LAST_N_DAYS:30]) {
                                       
                                       // for (Shopping_cart_line_item__c detail : master.Shopping_cart_line_items__r) {
                                       //     system.debug('detailProduct value is'+detail.Product_SKU__c);
                                       //    system.debug('lineItemProduct value is'+lineItem.Product_SKU__c);
                                       //   if(detail.Product_SKU__c.equals(lineItem.Product_SKU__c)){
                                       //       lstLineItems.add(detail);
                                       //   }
                                       //}
                                   }
        if(lstLineItems !=null && lstLineItems.size() >0){
        
            Shopping_cart_line_item__c sLineItem = lstLineItems.get(0);
            sLineItem.Quantity__c = sLineItem.Quantity__c + lineItem.Quantity__c;
            system.debug('slineitemquantity'+sLineItem.Quantity__c);
            update lstLineItems;
            
            return populateResult('Success', 'Updated', lstLineItems);
        }else{
            
            //Use central addToCart function in MyCustomerController -JoJoToTheyagu
            Map<String, String> objMap = new Map<String, String> ();
            objMap.put('sku', lineItem.Product_SKU__c.toUpperCase());
            objMap.put('productTitle', lineItem.Product_Title__c);
            
            //DEF-0648 getting Price from Api  
            try{
                ProductPriceWrapper prodPrice= new ProductPriceWrapper();
                //exclude *SKU when call Price API
                if(!lineItem.Product_SKU__c.startsWith('*')){                    
                    prodPrice=ProductPriceCmpCtrl.getProductPrice(lineItem.Product_SKU__c.toUpperCase());   
                    system.debug('prodPrice is -->'+prodPrice);
                }
                //Use central addToCart function in MyCustomerController
                lstLineItems = MyCustomerController.addToCart(recordId,JSON.serialize(objMap),JSON.serialize(prodPrice),Integer.valueOf(lineItem.Quantity__c),MultiLineType);
                system.debug('lstLineItems is -->'+lstLineItems);
            }catch(AuraHandledException auex){
                //ProductPriceCmpCtrl.getProductPrice throw exception, product is not found in Price API.
                //Because product list will exclude this kind of prod, so exclude this kind of prod from mannual add product process also.
                return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
            }catch(MyCustomerController.ItemDetailsAPIException ex){
                //MyCustomerController.addToCart throw exception, product is not found in item-details API
                return populateResult('Error', 'ItemDetailsAPIError', lstLineItems);
            }
            return populateResult('Success', 'Inserted', lstLineItems);
        }
        return null;
    }
    
   private static Map<String, Object> populateResult(String status, String action, List<Shopping_cart_line_item__c> lineItems) {
        Set<Id> lineItemIds = new Set<Id>();
        for(Shopping_cart_line_item__c lineItem : lineItems) {
            lineItemIds.add(lineItem.Id);
        }
       return new Map<String, Object> {'status' => status, 'msg' => action, 'lineItemIds' => lineItemIds};
  }

    
}